import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService, Comando } from 'src/app/services/api.service';
import { ViewController } from '@ionic/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-comandos',
  templateUrl: './comandos.page.html',
  styleUrls: ['./comandos.page.scss'],
})
export class ComandosPage implements OnInit, AfterViewInit {


  private comando: any;
  public loaded = false;

  constructor(private api: ApiService, private menu: MenuController) {

  }

  ngOnInit() {
    console.log('ngOnInit');
  }

  async ngAfterViewInit() {
    console.log('AfterViewInit Comandos');
    this.comando = await this.api.GetComando();
    this.loaded = true;
    this.menu.close();
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter COMANDOS PAGE'); // NOT CALLED
  }

  async Update() {
    console.log(await this.api.PutComando(this.comando));
  }

  /* printTypeNames<T>(obj: T) {
    const objectKeys = Object.keys(obj) as Array<keyof T>;
    for (const key of objectKeys) {
      console.log('key:' + key);
    }
  } */

  extractParameters(objeto: any) {
    return Object.keys(objeto);
  }

  /* describeClass(typeOfClass: any) {
    const a = new typeOfClass();
    const array = Object.getOwnPropertyNames(a);
    return array;
  } */

  /* getElement(el: string) {
    return this.comando[el];
  } */

  Toggle(el, i) {
    this.comando[i] = el;
    console.log(this.comando[i]);

  }



}
