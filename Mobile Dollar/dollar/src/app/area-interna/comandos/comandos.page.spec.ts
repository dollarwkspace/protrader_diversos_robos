import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComandosPage } from './comandos.page';

describe('ComandosPage', () => {
  let component: ComandosPage;
  let fixture: ComponentFixture<ComandosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComandosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComandosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
