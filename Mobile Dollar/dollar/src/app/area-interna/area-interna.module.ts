import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AreaInternaPage } from './area-interna.page';

const routes: Routes = [
  {
    path: '', component: AreaInternaPage, children: [
      { path: 'comandos', loadChildren: './comandos/comandos.module#ComandosPageModule' },
      { path: 'parametros-ativos', loadChildren: './parametros-ativos/parametros-ativos.module#ParametrosAtivosPageModule' },
      { path: 'parametros', loadChildren: './parametros/parametros.module#ParametrosPageModule' },
      { path: '', redirectTo: 'comandos', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AreaInternaPage]
})
export class AreaInternaPageModule { }
