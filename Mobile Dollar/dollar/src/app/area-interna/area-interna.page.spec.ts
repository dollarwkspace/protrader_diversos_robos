import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaInternaPage } from './area-interna.page';

describe('AreaInternaPage', () => {
  let component: AreaInternaPage;
  let fixture: ComponentFixture<AreaInternaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaInternaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaInternaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
