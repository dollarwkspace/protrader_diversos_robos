import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ParametrosAtivosPage } from './parametros-ativos.page';

const routes: Routes = [
  {
    path: '',
    component: ParametrosAtivosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ParametrosAtivosPage]
})
export class ParametrosAtivosPageModule {}
