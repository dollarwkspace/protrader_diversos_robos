import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrosAtivosPage } from './parametros-ativos.page';

describe('ParametrosAtivosPage', () => {
  let component: ParametrosAtivosPage;
  let fixture: ComponentFixture<ParametrosAtivosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametrosAtivosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrosAtivosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
