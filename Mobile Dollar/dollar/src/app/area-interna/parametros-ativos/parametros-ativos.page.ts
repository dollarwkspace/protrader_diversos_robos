import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MenuController, ToastController, IonSlides } from '@ionic/angular';
import { ParametroAtivo, ApiService } from 'src/app/services/api.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-parametros-ativos',
  templateUrl: './parametros-ativos.page.html',
  styleUrls: ['./parametros-ativos.page.scss'],
})
export class ParametrosAtivosPage implements OnInit, OnDestroy {

  @ViewChild(IonSlides) slides: IonSlides;

  public loaded = false;
  public parametrosAtivos: ParametroAtivo[] = [];
  public page = 1;
  public total = 0;
  public subsGetAllParams: Subscription = this.api.GetAllParametrosAtivos().subscribe(
    objs => {
      if (objs === undefined || objs === null) { return; }
      if (!Array.isArray(objs)) {
        objs = [objs];
      }
      this.parametrosAtivos = objs;
      this.total = this.parametrosAtivos.length;
      this.loaded = true;
      console.log('parâmetros:', this.parametrosAtivos);
    }
    , err => this.callToast(err.error));

  constructor(private menu: MenuController,
    public api: ApiService,
    public toastController: ToastController) {

  }

  async ngOnInit() {
    this.menu.close();
  }

  ngOnDestroy() {
    this.subsGetAllParams.unsubscribe();
  }

  extractParameters(objeto: any) {
    return Object.keys(objeto);
  }

  async Update(parametro) {
    try {
      console.log('objeto enviado:', parametro);
      console.log(await this.api.PutParametroAtivo(parametro));
      this.callToast('Parâmetros Atualizados');
    } catch (ex) {
      this.callToast(ex.error);
    }
  }

  async SlideChanged(el) {
    this.page = await this.slides.getActiveIndex() + 1;
  }

  async callToast(message: any) {
    const toast = await this.toastController.create({
      message: message,
      position: 'middle',
      duration: 2000
    });
    toast.present();
  }

}
