import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuController, IonSlide, ToastController } from '@ionic/angular';
import { ApiService, Parametro } from 'src/app/services/api.service';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-parametros',
  templateUrl: './parametros.page.html',
  styleUrls: ['./parametros.page.scss'],
})
export class ParametrosPage implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;
  public loaded = false;
  public parametros: Parametro[] = [];
  public page = 1;
  public total = 0;
  public clicouAdd = false;

  constructor(private menu: MenuController,
    public api: ApiService,
    public toastController: ToastController) { }

  async ngOnInit() {
    this.menu.close();
    this.parametros = await this.api.GetAllParameters();
    this.loaded = true;
    this.total = this.parametros.length;
  }

  extractParameters(objeto: any) {
    return Object.keys(objeto);
  }

  async Update(parametro) {
    try {
      console.log('objeto enviado:', parametro);
      if (this.page === this.total && this.clicouAdd) {
        console.log(await this.api.PostParametro(parametro));
        this.callToast('Parâmetros Inseridos');
        return;
      }
      console.log(await this.api.PutParametro(parametro));
      this.callToast('Parâmetros Atualizados');
    } catch (ex) {
      this.callToast(ex.error);
    }
  }

  async Delete(parametro) {
    try {
      console.log('Deletando', parametro);
      this.callToast(await this.api.DeleteParametro(parametro));
      this.parametros = await this.api.GetAllParameters();
      this.slides.update();
      this.total = this.parametros.length;
      this.page = await this.slides.getActiveIndex() + 1;
    } catch (ex) {
      this.callToast(ex.error);
    }
  }

  async Create(parametro) {
    const novoParam: any = new Parametro;
    Object.keys(parametro).forEach(element => {
      novoParam[element] = parametro[element];
    });
    novoParam.nome = 'NOVO';
    novoParam.papel = 'NOVO';
    this.parametros[this.parametros.length] = novoParam;
    this.total = this.parametros.length;
    this.slides.slideTo(this.total);
    this.clicouAdd = true;
  }

  async SlideChanged(el) {
    this.page = await this.slides.getActiveIndex() + 1;
  }

  async callToast(message: any) {
    const toast = await this.toastController.create({
      message: message,
      position: 'middle',
      duration: 2000
    });
    toast.present();
  }

}
