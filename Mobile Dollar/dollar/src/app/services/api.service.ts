import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataService } from './data.service';
import { debounceTime, distinctUntilChanged, timeout, retry, single, map, filter, tap } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private dataProvider: DataService) { }


  public GetToken(login: string, senha: string): Promise<string> {

    const server = this.dataProvider.url + '/auth';
    const headers = new HttpHeaders(
      { 'Content-Type': 'application/json; charset=utf-8' }
    );
    const body = { Login: login, Senha: senha };

    return this.http.post(server, body, { headers: headers, responseType: 'text' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      timeout(2000),
      tap(token => console.log('token:', token))
    ).toPromise()
      .then((token: string) => token);
  }

  public GetComando(): Promise<Comando> {
    const server = this.dataProvider.url + '/comandos';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );

    return this.http.get(server, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: Comando) => retorno);

  }

  public GetAllParameters(): Promise<Parametro[]> {
    const server = this.dataProvider.url + '/parametros/all';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );

    return this.http.get(server, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: Parametro[]) => retorno);
  }

  public GetAllParametrosAtivos(): Observable<any> {
    const server = this.dataProvider.url + '/parametrosAtivos/all';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );

    return this.http.get(server, { headers: headers, responseType: 'json' }).pipe(
      debounceTime(1000),
      retry(5),
      timeout(5000),
      distinctUntilChanged(),
      tap(params => console.log('tap:', params))
    );
  }

  public PutComando(comando: Comando): Promise<Comando> {
    const server = this.dataProvider.url + '/comandos';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );
    const body = comando;

    return this.http.put(server, body, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: Comando) => retorno);
  }

  public PutParametroAtivo(parametro: ParametroAtivo): Promise<ParametroAtivo> {
    const server = this.dataProvider.url + '/parametrosAtivos';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );
    const body = parametro;

    return this.http.put(server, body, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: ParametroAtivo) => retorno);
  }


  public PutParametro(parametro: Parametro): Promise<Parametro> {
    const server = this.dataProvider.url + '/parametros';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );
    const body = parametro;

    return this.http.put(server, body, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: Parametro) => retorno);
  }

  public PostParametro(parametro: Parametro): Promise<Parametro> {
    const server = this.dataProvider.url + '/parametros';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token
      }
    );
    const body = parametro;

    return this.http.post(server, body, { headers: headers, responseType: 'json' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: Parametro) => retorno);
  }



  public DeleteParametro(parametro: any): Promise<string> {
    const server = this.dataProvider.url + '/parametros';
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'x-auth-token': this.dataProvider.token,
        'nome': parametro.nome,
        'papel': parametro.papel
      }
    );

    return this.http.delete(server, { headers: headers, responseType: 'text' }).pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      retry(5),
      timeout(5000)
    ).toPromise()
      .then((retorno: string) => retorno);
  }



}


export class Comando { }
export class Parametro { }
export class ParametroAtivo { }

