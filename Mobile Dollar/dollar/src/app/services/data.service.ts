import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  public url = 'http://159.65.238.114:3000/api';
  public token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzRjYjNhOWRlYWEwMzcxMTg1MzRhZTYiLCJpYXQiOjE1NDkwNDIxMTl9.SI4Z81TBa6QEAM1VxqMW56bsBh5bWeU8utyHTnZfWdE';

  constructor() { }
}
