import { Component } from '@angular/core';
import { ApiService } from '../services/api.service';
import { DataService } from '../services/data.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  constructor(
    private api: ApiService,
    private dataProvider: DataService,
    private navCtrl: NavController
  ) { }

  Login(login, senha) {
    this.api.GetToken(login, senha).then(token => {
      this.dataProvider.token = token;
      this.navCtrl.navigateForward('/area-interna');
    })
      .catch(err => console.log(err.error));
  }
}
