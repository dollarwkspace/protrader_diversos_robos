﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{

    public enum State { limbo, inicio, reentrada, saidaTendencia, avalanche, termino, bolsaFechada, lockdown };
    public enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
    public enum TipoDeSaida { DifMédias, LossMaximo };
    public enum TipoDeEntrada { MA3, DifMédias };
    public enum TipoDeRobo { tendencia, contraTendencia, tendenciaEcontra }
    public enum TipoDeOrdem { limite, mercado };
    public enum Acao
    {
        eventoOrderAdded, aguardandoOrderAdded,
        eventoOrderRemoved,
        eventoOrderExecuted, aguardandoOrderExecuted,
        eventoPositionAdded, aguardandoPositionAdded,
        eventoPositionRemoved,
        eventoPositionUpdate, aguardandoPositionUpdate,
        eventoFillAdded,
        passeLivre,
        solicitouReEntrada, reEntradaConcluida,
        solicitouEntrada, entradaConcluida,
        solicitouEntradaAvalanche, entradaAvalancheConcluida,
        solicitouSaidaLimite, saidaLimiteConcluida,

    };


    public class DetectaVazamento
    {
        public int contador = 0;

        public void incrementa()
        {
            contador++;
        }
    }

    public class PosicaoAtiva
    {
        internal double _openPrice;
        internal double _amount;
        internal Operation _side;
        internal DateTime _openTime;
        internal double _lucro;
        internal double _pontos;
        internal string _comments;

        internal PosicaoAtiva(double openPrice, double amount, Operation side, DateTime openTime, string comments = "", double lucro = 0, double pontos = 0)
        {
            _openPrice = openPrice; _amount = amount; _side = side; _openTime = openTime; _lucro = lucro; _pontos = pontos; _comments = comments;
        }

    }

    public class LogWriter
    {
        private string m_exePath = string.Empty;

        public void LogWrite(string logMessage, string arq = "log.txt")
        {
            //m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (!File.Exists(arq))
            {
                string s = ""; s = DateTime.Now.ToString("h:mm:ss tt");
                FileStream fs = File.Create(arq);
                Byte[] info = new UTF8Encoding(true).GetBytes("File Created:" + s + "\r\n");
                fs.Write(info, 0, info.Length);
                fs.Close();
            }

            try
            {
                using (StreamWriter w = File.AppendText(arq))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("{0};", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }

    public class Funcoes
    {
        private ProjetoEverestUltimate robo;

        public Funcoes(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public void InfoConexao(Connection myConnection)
        {
            //outputting of all the information about the current connection.
            robo.Print(
            "\nConnection name : \t" + myConnection.Name +
            "\nConnection address : \t" + myConnection.Address +
            "\nConnection login : \t" + myConnection.Login +
            "\nSLL on?  : \t" + myConnection.IsUseSSL +
            "\nProtocol version : \t" + myConnection.ProtocolVersion +
            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
            "\nConnection status : \t" + myConnection.Status
            );
        }

        public void AccountInformation(Account acc)
        {
            //outputting of all the 'Account' properties
            robo.Print(
                "Id : \t" + acc.Id + "\n" +
                "Name : \t" + acc.Name + "\n" +
                "User Login: \t" + acc.User.Login + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "Leverage : \t" + acc.Leverage + "\n" +
                "Currency : \t" + acc.Currency + "\n" +
                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
                "IsDemo : \t" + acc.IsDemo + "\n" +
                "IsReal : \t" + acc.IsReal + "\n" +
                "IsLocked : \t" + acc.IsLocked + "\n" +
                "IsInvestor : \t" + acc.IsInvestor + "\n" +
                "Status : \t" + acc.Status + "\n" +
                "StopReason : \t" + acc.StopReason + "\n" +
                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "BeginBalance : \t" + acc.BeginBalance + "\n" +
                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
                "Credit : \t" + acc.Credit + "\n" +
                "CashBalance : \t" + acc.CashBalance + "\n" +
                "TodayVolume : \t" + acc.TodayVolume + "\n" +
                "TodayNet : \t" + acc.TodayNet + "\n" +
                "TodayTrades : \t" + acc.TodayTrades + "\n" +
                "TodayFees : \t" + acc.TodayFees + "\n" +
                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
                "MarginTotal : \t" + acc.MarginTotal + "\n" +
                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
                "Equity : \t" + acc.Equity + "\n" +
                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
            );
        }

        public void PosInfo(Position pos)
        {
            string log =
                "PosInfo()-> " +
                "  Papel: " + pos.Instrument.BaseName + "\t" +
                "  Id: " + pos.Id + "\t" +
                "  Qtde: " + pos.Amount + "\t" +
                "  Tipo: " + pos.Side.ToString() + "\t" +
                "  Preço Abertura: R$ " + pos.OpenPrice + "\t" +
                "  Preço Atual: R$ " + pos.CurrentPrice + "\t" +
                "  Número Mágico: " + pos.MagicNumber + "\t" +
                "  Lucro ou Prejuízo: R$" + pos.GetProfitNet();
            robo.Print(log);
        }

        public void OrderInfo(Order ord)
        {
            string log = "OrderInfo()-> " +
                "  Papel: " + ord.Instrument.BaseName + "\t" +
                "  Id: " + ord.Id + "\t" +
                "  Qtde: " + ord.Amount + "\t" +
                "  Lado: " + ord.Side + "\t" +
                "  Tipo: " + ord.Type + "\t" +
                "  Preço: R$ " + ord.Price + "\t" +
                "  Status: " + ord.Status + "\t" +
                "  Número Mágico: " + ord.MagicNumber;
            robo.Print(log);
        }

        public bool EqualizaOrdemPosicaoAvalanche(PosicaoAtiva pos, Order ord)
        {
            try
            {
                double price = 0;
                price = (pos._side == Operation.Buy) ? robo.instrument.RoundPrice(pos._openPrice + robo.TPavalanche * robo.tickSize) : robo.instrument.RoundPrice(pos._openPrice - robo.TPavalanche * robo.tickSize);
                string log = "EqualizaOrdemPosiçãoAvalanche: Qtde Pos: " + pos._amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + robo.TPavalanche * robo.tickSize;
                robo.Print(log);
                if (pos._amount != ord.Amount)
                {
                    log = robo.horario + "EqualizaOrdemPosiçãoAvalanche: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + robo.TPavalanche * robo.tickSize;
                    robo.Print(log);
                    return ord.Modify(price, pos._amount);
                }

                if (Math.Abs(ord.Price - price) > robo.TPavalanche * robo.tickSize)
                {
                    log = robo.horario + "EqualizaOrdemPosiçãoAvalanche: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + robo.TPavalanche * robo.tickSize;
                    robo.Print(log);
                    return ord.Modify(price);
                }
            }
            catch (Exception exc)
            {
                robo.Alert("Erro equalizaOrdemAvalanche");
            }
            return false;
        }

        public bool EqualizaOrdemPosicao(Position pos, Order ord)
        {
            double price = 0;
            price = (pos.Side == Operation.Buy) ? robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize) : robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize);
            string log = "EqualizaOrdemPosição: Qtde Pos: " + pos.Amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.tickSize;
            robo.Print(log);

            if (pos.Amount != ord.Amount)
            {
                log = robo.horario + "EqualizaOrdemPosição: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.tickSize;
                robo.Print(log);
                return ord.Modify(price, pos.Amount);
            }

            if (Math.Abs(ord.Price - price) > robo.TP * robo.tickSize)
            {
                log = robo.horario + "EqualizaOrdemPosição: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.tickSize;
                robo.Print(log);
                return ord.Modify(price);
            }
            return false;
        }


        public bool CalculaCalorMaximo()
        {
            try
            {
                if (robo.positions.Length > 0)
                {
                    Position pos = robo.positions[0];
                    if (pos != null)
                    {
                        if (pos.GetProfitNet() < robo.calor) { robo.calor = pos.GetProfitNet(); robo.horaDoCalorMaximo = robo.hora.ToString(); }
                        if (Math.Abs(pos.Amount) > robo.maximoDeContratosAbertos) { robo.maximoDeContratosAbertos = pos.Amount; }
                    }
                }
                robo.Print("CalculaCalorMaximo: Calor Máximo até o momento: " + robo.calor);
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("CalculaCalorMaximo: Erro setor Calor Máximo . " + exc.Message);
                robo.Print("CalculaCalorMaximo: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool CheckMarketStatus(Trade trade)
        {
            try
            {
                if (robo.myConnection.Status == ConnectionStatus.Connected)
                {
                    robo.horario = DateTime.Now.TimeOfDay; robo.hora = robo.horario.Hours;
                }
                else
                {
                    robo.horario = robo.instrument.LastQuote.Time.ToLocalTime().TimeOfDay;
                    robo.hora = robo.horario.Hours;
                }

                robo.Print("CheckMarketStatus: Hora Atual = " + robo.horario.ToString());
                if (robo.hora == 0) { return false; }
                if (robo.hora >= 9) { robo.horarioTerminou = false; }
                if (robo.horario.CompareTo(robo.HoraDeInicio) < 0) { robo.Print("Aguardando hora de Início..."); return false; }
                if (robo.horario.CompareTo(robo.HoraDeTermino) >= 0) { robo.Print("CheckMarketStatus: Hora de término atingida!"); robo.horarioTerminou = true; }
                if (robo.hora >= 18)
                {
                    robo.horarioTerminou = true;
                    if (robo.Positions.Count > 0) robo.Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); FechamentoTotal();
                    robo.Print("CheckMarketStatus: Bolsa fechada !");
                    return false;
                }
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("CheckMarketStatus: Erro na coleta da Hora . Erro: " + exc.Message);
                robo.Print("CheckMarketStatus: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = robo.Positions.GetPositions();
            Order[] ordensGlobais = robo.Orders.GetOrders(true);
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => PosInfo(pos)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => OrderInfo(ord)); }
        }

        public void FechamentoParcial()
        {
            robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
        }

        public void FechamentoTotal()
        {
            string log = robo.horario + " --> Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas caso existam";
            robo.Print(log);

        LoopFechamento:
            robo.Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == robo.papel).ForEach(ord =>
            {
                log = "Fechamento Total: Ordem ID " + ord.Id + "  Status:" + ord.Status + " Active:" + ord.IsActive + " Qtde:" + ord.Amount + " " + ord.Side + " Price:" + ord.CurrentPrice;
                robo.Print(log);
                ord.Cancel();
            });
            robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ForEach(p =>
            {
                log = "Fechamento Total: Posição ID " + p.Id + " Qtde:" + p.Amount + " " + p.Side + " Price:" + p.OpenPrice;
                robo.Print(log);
                p.Close();
            });

            RefreshPositionsOrders();
            if (robo.positions.Length > 0 || robo.orders.Length > 0)
            {
                log = "ERRO FechamentoTotal: Aguardando fechamento de Ordens e Posições";
                robo.Print(log);
                goto LoopFechamento;
            }
        }

        public void SaidaPorLossMaximo()
        {
            try
            {
                //SAÍDA POR LOSS MÁXIMO
                if (robo.positions.Length > 0 && robo.tipoDeSaida == TipoDeSaida.LossMaximo)
                {
                    Position pos = robo.positions[0];
                    if (pos != null)
                    {
                        robo.Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize) + " Limite: " + robo.lossMaximo + " ticks");

                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize >= robo.lossMaximo)
                        {
                            string log = "*****************    LOSS MÁXIMO ATINGIDO    ********************";
                            robo.Print(log);
                            robo.contadorDeReEntradas = 0;
                            FechamentoTotal();
                            log = "Fechando todas as Posições por LOSS MÁXIMO !!!"; robo.Print(log);
                            robo.estado = State.limbo; robo.acao.Clear(); robo.acao.Add(Acao.passeLivre); return;
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                robo.Print("SaídaPorLossMáximo: Erro setor Loss Máximo . " + exc.Message);
                robo.Print("SaídaPorLossMáximo: InnerException " + exc.InnerException.Message);
            }
        }

        public void DetectaAvalanche()
        {
            try
            {
                RefreshPositionsOrders();
                if (robo.positions.Length > 0)
                {
                    if (!robo.modoAvalanche)
                    {
                        Position pos = robo.positions[0];
                        if (pos != null)
                        {
                            if (pos.Amount >= robo.avalanche)
                            {
                                if (Math.Abs(pos.OpenPrice - pos.CurrentPrice) / robo.tickSize > robo.ticksAvalanche)
                                {
                                    robo.Print("*****************    MODO AVALANCHE ACIONADO   ********************");

                                    robo.estado = State.avalanche;
                                    robo.modoAvalanche = true;
                                    robo.ultimaPosicaoAtivaAvalanche = robo.posicaoAtivaAvalanche;
                                    robo.posicaoAtivaAvalanche = null;
                                    LimparTodasAsOrdens();

                                }
                                else
                                {
                                    robo.Print("Modo Avalanche após " + robo.ticksAvalanche + " pontos. Posição atual: " + Math.Abs(pos.OpenPrice - pos.CurrentPrice) / robo.tickSize);
                                }
                            }
                            else
                            {
                                robo.Print("Detecta Avalanche: Avalanche será acionada após " + robo.avalanche + " contratos");
                            }
                        }
                    }
                }
                else
                {
                    robo.modoAvalanche = false;
                }

            }
            catch (Exception exc)
            {
                robo.Print("Detecta Avalanche: Erro setor Detector de Avalanche . " + exc.Message);
                robo.Print("Detecta Avalanche: InnerException " + exc.InnerException.Message);
            }
        }

        public void LimparTodasAsOrdens()
        {
            string log = "";

        LoopLimpar:

            robo.Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == robo.papel
            && ord.Status != OrderStatus.PendingCancel
            && ord.Status != OrderStatus.PendingExecution
            && ord.Status != OrderStatus.PendingNew
            && ord.Status != OrderStatus.PendingReplace
            ).ForEach(ord =>
            {
                var result = ord.Cancel();
                if (!result)
                {
                    log = "LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA! Status: " + ord.Status;
                    robo.Print(log);
                }
                else
                {
                    log = "LimparTodasAsOrdens: Ordem " + ord.Id + " " + ord.Side + " FOI CANCELADA! Status: " + ord.Status;
                    robo.Print(log);
                }
            });

            RefreshPositionsOrders();
            if (robo.orders.Length > 0)
            {
                log = "LimparTodasAsOrdens: Existem ordens que não foram limpadas ainda";
                robo.Print(log);
                goto LoopLimpar;
            }
        }

        public bool RefreshIndicators()
        {
            try
            {
                robo.Print("RefreshIndicators: Lendo indicadores...");
                robo.cotacaoAtual = robo.CurrentData.GetPrice(PriceType.Close);
                robo.curta = robo.MM.GetValue(0, 0);
                robo.curtaAnterior = robo.MM.GetValue(0, 1);
                robo.suporte = robo.MM.GetValue(1, 0);
                robo.suporteAnterior = robo.MM.GetValue(1, 1);
                robo.histoResult = robo.sniper.GetValue(0, 0);
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                robo.Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool RefreshPositionsOrders()
        {
            try
            {
                robo.positions = robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel)?.ToArray();
                robo.orders = robo.Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == robo.papel)?.ToArray();

                if (robo.tapeReadingPosicoes.Count > 0)
                {
                    double lucro = robo.tapeReadingPosicoes.Sum(x => x._lucro);

                    Operation tipoBase = robo.tapeReadingPosicoes.First()._side;

                    robo.tapeReadingPosicoes.ForEach(posicao =>
                    {
                        posicao._pontos = (posicao._side == Operation.Buy) ? Math.Round(((robo.cotacaoAtual - posicao._openPrice) / robo.tamanhoPonto), 2) : -Math.Round(((robo.cotacaoAtual - posicao._openPrice) / robo.tamanhoPonto), 2);
                        posicao._lucro = posicao._pontos * robo.custoContratoPorPonto * Math.Abs(posicao._amount);
                    });


                    double _precoMedio = Math.Round(robo.tapeReadingPosicoes.Sum(x => x._openPrice * x._amount) / robo.tapeReadingPosicoes.Sum(x => x._amount), 2);
                    double _lucroMedio = robo.tapeReadingPosicoes.Sum(x => x._lucro);
                    double _amountMedio = robo.tapeReadingPosicoes.Sum(x => x._amount);

                    robo.posicaoAtivaMedia._amount = _amountMedio;
                    robo.posicaoAtivaMedia._lucro = _lucroMedio;
                    robo.posicaoAtivaMedia._openPrice = _precoMedio;
                    robo.posicaoAtivaMedia._side = tipoBase;

                }
                else
                {
                    robo.posicaoAtivaMedia._amount = 0;
                    robo.posicaoAtivaMedia._lucro = 0;
                    robo.posicaoAtivaMedia._openPrice = 0;
                    robo.posicaoAtivaMedia._comments = "";

                }

                if (robo.positions.Length > 0 && robo.tapeReadingPosicoes.Count == 0 && robo.posicaoAtivaAvalanche == null && robo.ultimaPosicaoAtivaAvalanche == null)
                {
                    robo.Print("Existe posição pré-existente." + "\t Total Posição: " + robo.positions[0]?.Amount + "\t Total Ordens: " + robo.orders.Length);
                    robo.tapeReadingPosicoes.Add(new PosicaoAtiva(robo.positions[0].OpenPrice, robo.positions[0].Amount, robo.positions[0].Side, DateTime.Now, "Inicio Timeline"));
                    robo.ultimaPosicaoAtivaAvalanche = new PosicaoAtiva(robo.positions[0].OpenPrice, robo.positions[0].Amount, robo.positions[0].Side, DateTime.Now, "Inicio Timeline");
                    robo.ultimaReentrada = robo.positions[0].OpenPrice;
                    robo.Print("Posição ativa inicial setada", robo._log);
                }

                return true;
            }
            catch (Exception exc)
            {
                robo.Print("RefreshPositionsOrders: Erro no RefreshPositionsOrders. Erro:" + exc.Message);
                return false;
            }
        }



    }
}
