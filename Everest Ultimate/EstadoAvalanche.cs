﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{
    public class EstadoAvalanche
    {
        private ProjetoEverestUltimate robo;

        public EstadoAvalanche(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public void Avalanche()
        {
            try
            {

                string log = "********************   Estado AVALANCHE ***********************";
                robo.Print(log);
                robo.liberaReEntradaAvalanche = false;

                if (robo.ultimaPosicaoAtivaAvalanche == null)
                {
                    robo.tapeReadingPosicoes.Clear();
                }

                robo.funcoes.RefreshPositionsOrders();

                if (robo.positions.Length == 0)
                {
                    log = "Estado AVALANCHE: Não existe mais posição. Migrando para inicio";
                    robo.Print(log);
                    robo.estado = State.inicio;
                    robo.funcoes.LimparTodasAsOrdens();
                    robo.modoAvalanche = false;
                    return;
                }

                if (robo.positions.Length > 0)
                {
                    robo.Print("Estado AVALANCHE: Lucro Atual: " + robo.posicaoAtivaMedia._lucro);
                    if (robo.posicaoAtivaMedia._lucro >= robo.lucroDeSaidaAvalanche)
                    {
                        log = "Estado AVALANCHE: SAÍDA POR LUCRO ATINGIDO. Lucro: " + robo.posicaoAtivaMedia._lucro;
                        robo.Print(log);

                        robo.funcoes.FechamentoTotal();
                        robo.bufferDeOrdens.Clear();
                        robo.modoAvalanche = false;
                        robo.estado = State.inicio;
                        robo.acao.Clear();
                        return;
                    }

                    double entradaAvalanche = Math.Floor(robo.contratosAvalanche * robo.positions[0].Amount / 100);
                    if (entradaAvalanche < robo.minimumLot) entradaAvalanche = robo.minimumLot;

                    Operation tipoReferencia = robo.positions[0].Side;
                    Operation estadoDoMercado = ((robo.curta - robo.curtaAnterior) * robo.multiplicador > 0) ? Operation.Buy : Operation.Sell;

                    robo.Print("Estado Avalanche: |media-mediaAnterior|*multiplicador  = " + Math.Abs(robo.curta - robo.curtaAnterior) * robo.multiplicador + " Resiliência de Entrada:" + robo.resilienciaDeSaida + " Preço Última Posição:" + robo.ultimaPosicaoAtivaAvalanche._openPrice + " Tipo:" + robo.ultimaPosicaoAtivaAvalanche._side);

                    if (robo.orders.Length != 0 && robo.posicaoAtivaAvalanche != null && robo.positions.Length > 0)
                    {
                        log = "Estado Avalanche: Ordem != 0 , Posição Ativa Avalanche != null, Posições > 0";
                        robo.Print(log);
                        robo.funcoes.EqualizaOrdemPosicaoAvalanche(robo.posicaoAtivaAvalanche, robo.orders[0]);
                        robo.acao.Clear();
                    }

                    if (robo.posicaoAtivaAvalanche == null && robo.ultimaPosicaoAtivaAvalanche != null)
                    {
                        log = "Estado Avalanche: Ordem == 0 , Posição Ativa Avalanche == null";
                        robo.Print(log);
                        double pontoCritico = Math.Abs(Math.Round(((robo.cotacaoAtual - robo.ultimaPosicaoAtivaAvalanche._openPrice) / robo.tamanhoPonto), 2));
                        log = "Estado Avalanche: Posição Ativa é NULA. Aguardando Ponto Crítico. PontoCrítico: " + pontoCritico + " Limite:" + robo.reEntradaAvalanche;
                        robo.Print(log);

                        if (pontoCritico >= Math.Abs(robo.reEntradaAvalanche))
                        {
                            robo.liberaReEntradaAvalanche = true;
                            goto LoopReEntradaAvalanche;
                        }
                        else
                        {
                            return;
                        }
                    }

                    if (robo.orders.Length == 0 && robo.posicaoAtivaAvalanche != null)
                    {
                        log = "Estado Avalanche: Ordem == 0 , Posição Ativa Avalanche != null";
                        robo.Print(log);
                        robo.comando.SaidaAvalanche(robo.posicaoAtivaAvalanche);
                        robo.acao.Clear();
                        robo.estado = State.limbo;
                        robo.acao.Add(Acao.aguardandoOrderAdded);
                        return;
                    }

                    if (robo.posicaoAtivaAvalanche != null)
                    {
                        log = "Estado Avalanche: Posição Ativa Avalanche != null";
                        robo.Print(log);

                        if (robo.orders.Length > 0)
                            if (robo.funcoes.EqualizaOrdemPosicaoAvalanche(robo.posicaoAtivaAvalanche, robo.orders[0]))
                                robo.Print("Estado Avalanche: Equalização BEM SUCEDIDA!");

                        double pontoCritico = Math.Abs(Math.Round(((robo.cotacaoAtual - robo.posicaoAtivaAvalanche._openPrice) / robo.tamanhoPonto), 2));

                        log = "Estado Avalanche: Analisando ponto crítico... Ponto crítico atual: " + pontoCritico + "   Limite: " + robo.reEntradaAvalanche + " pontos";
                        robo.Print(log);

                        if (pontoCritico >= Math.Abs(robo.reEntradaAvalanche))
                        {
                            robo.liberaReEntradaAvalanche = true;
                            robo.Print("Entrada Avalanche: Ponto Crítico Atingido! Verificando tendência do mercado ..");
                        }
                    }

                    if (robo.orders.Length > 1 && robo.posicaoAtivaAvalanche != null)
                    {
                        robo.Print("Estado Avalanche: Existe mais do que 1 ordem. Limpando uma delas");
                        robo.funcoes.LimparTodasAsOrdens();
                        robo.acao.Clear();
                        robo.acao.Add(Acao.passeLivre);
                        robo.estado = State.limbo;
                        return;
                    }

                LoopReEntradaAvalanche:

                    if (tipoReferencia == estadoDoMercado && robo.liberaReEntradaAvalanche)
                    {
                        if (robo.bloqueieNovaEntradaAvalanche)
                        {
                            robo.Print("Estado Avalanche: Aguardando desbloqueio avalanche..."); return;
                        }
                        if ((Math.Abs(robo.curta - robo.curtaAnterior) * robo.multiplicador > robo.resilienciaDeSaida))
                        {
                            robo.acao.Clear();
                            if (robo.comando.Send_order(OrdersType.Market, tipoReferencia, robo.cotacaoAtual, entradaAvalanche, 0, "entrada avalanche") != "-1")
                            {
                                log = "Estado AVALANCHE: Entrada Avalanche enviada!";
                                robo.Print(log);
                                robo.bloqueieNovaEntradaAvalanche = true;
                                robo.estado = State.limbo;
                                robo.acao.Add(Acao.aguardandoPositionUpdate);
                                robo.acao.Add(Acao.solicitouEntradaAvalanche);
                                return;
                            }
                            else
                            {
                                log = "Estado AVALANCHE: Ordem de ReEntrada Avalanche negada!";
                                robo.Print(log);
                                robo.estado = State.avalanche;
                                robo.acao.Clear();
                                return;
                            }
                        }
                        else
                        {
                            robo.Print("Estado AVALANCHE: Aguardando Sensibilidade da Média ficar maior que " + robo.resilienciaDeSaida);
                        }
                    }
                    else
                    {
                        robo.Print("Estado AVALANCHE: Tendência desfavorável ! bloqueioAvalanche: " + robo.bloqueieNovaEntradaAvalanche + "   Tendência:" + estadoDoMercado);
                    }
                }

                robo.funcoes.PrintaPosicoesOrdensAtivas();
            }
            catch (Exception exc)
            {
                string log = "Erro no estado avalanche! " + exc.Message;
                robo.Alert(log);
                robo.acao.Clear();
                robo.estado = State.limbo;
                robo.funcoes.LimparTodasAsOrdens();
                robo.acao.Add(Acao.passeLivre);
                robo.modoAvalanche = false;
            }
        }


    }


}
