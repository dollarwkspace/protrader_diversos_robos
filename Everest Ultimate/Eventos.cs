﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{
    public class Eventos
    {
        private ProjetoEverestUltimate robo = null;

        public Eventos(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }


        public void Orders_OrderAdded(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.bloqueioGeral = false;
                    robo.funcoes.RefreshPositionsOrders();
                    robo.acao.Add(Acao.eventoOrderAdded);
                    string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderAdded: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + robo.estado;
                    robo.Print(log); 
                }
            }
            catch (Exception exc)
            {
                robo.Alert("Erro OrderAdded : " + exc.Message);
                robo.bloqueioGeral = false;
            }
        }

        public void Orders_OrderRemoved(Order obj)
        {
            if (obj.Instrument.BaseName == robo.papel)
            {
                robo.bloqueioGeral = true;
                robo.bloqueioGeralPorOrdemRemovida = true;
                robo.funcoes.RefreshPositionsOrders();
                robo.acao.Add(Acao.eventoOrderRemoved);
                string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderRemoved: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + robo.estado;
                robo.Print(log);
                robo.posicaoAtivaAvalanche = null;
                robo.ultimaPosicaoAtivaAvalanche = null;
                robo.estado = State.limbo;
            }
        }


        public void Orders_OrderExecuted(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.bloqueioGeral = true;
                    robo.funcoes.RefreshPositionsOrders();
                    robo.acao.Add(Acao.eventoOrderExecuted);
                    string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderExecuted: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + robo.estado;
                    robo.Print(log);

                    //NOVA TIMELINE
                    if (obj.Status == OrderStatus.Filled || obj.Status == OrderStatus.PartiallyFilled)
                    {
                        double amount = obj.Amount;
                        double preco = obj.Price;

                        bool inversaoDetectada = false;
                        if (robo.positions.Length > 0)
                        {
                            Operation tipoBase = robo.positions[0].Side;
                            if (obj.Side != tipoBase) { amount = -amount; inversaoDetectada = true; }
                        }                        

                        if (inversaoDetectada)
                        {
                            if (robo.positions.Length > 0)
                            {
                                if (robo.modoAvalanche) { robo.ultimoComentario = "saida avalanche"; } else { robo.ultimoComentario = "saida limite"; }
                                robo.bufferDeOrdens.Add(new PosicaoAtiva(preco, amount, obj.Side, obj.Time, robo.ultimoComentario));
                                robo.ultimaPosicaoAtivaAvalanche = new PosicaoAtiva(preco, amount, obj.Side, obj.Time, robo.ultimoComentario);
                                robo.posicaoAtivaAvalanche = null;
                                robo.bloqueieNovaEntradaAvalanche = false;
                            }
                        }
                        else
                        {
                            robo.ultimoComentario = obj.Comment;
                            if (robo.modoAvalanche) robo.ultimoComentario = "entrada avalanche";
                            robo.posicaoAtivaAvalanche = new PosicaoAtiva(preco, amount, obj.Side, DateTime.Now, robo.ultimoComentario);
                            robo.bufferDeOrdens.Add(new PosicaoAtiva(preco, amount, obj.Side, obj.Time, robo.ultimoComentario, 0, 0));
                            PosicaoAtiva pos = robo.bufferDeOrdens.Last();
                            robo.bloqueieNovaEntradaAvalanche = false;
                            robo.ultimaPosicaoAtivaAvalanche = robo.posicaoAtivaAvalanche;
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                robo.Alert("Erro OrderExecuted : " + exc.Message);
                robo.bloqueioGeral = false;
            }
        }

        public void Positions_PositionRemoved(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.bloqueioGeral = false;
                    robo.funcoes.RefreshPositionsOrders();
                    robo.acao.Add(Acao.eventoPositionRemoved);
                    string log = obj.CloseTime.ToLocalTime().TimeOfDay + " EVENTO PositionRemoved: ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + robo.estado;
                    robo.Print(log);
                    robo.ultimaPosicaoAdd = null;                    
                    robo.modoAvalanche = false;
                    robo.posicaoAtivaAvalanche = null;
                    robo.bufferDeOrdens.Clear();
                    robo.acao.Clear();
                    robo.estado = State.limbo;
                    robo.acao.Add(Acao.eventoPositionRemoved);
                    robo.bloqueieNovaEntradaAvalanche = false;
                    return;
                }
            }
            catch (Exception exc)
            {
                robo.Alert("Erro PositionRemoved : " + exc.Message);
                robo.bloqueioGeral = false;
            }
        }

        public void Positions_PositionAdded(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.bloqueioGeral = false;
                    string log = "";
                    robo.funcoes.RefreshPositionsOrders();
                    if (robo.ultimaPosicaoAdd == null)
                    {
                        robo.acao.Add(Acao.eventoPositionAdded);
                        log = obj.OpenTime.ToLocalTime().TimeOfDay + " EVENTO PositionAdded: ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + robo.estado;
                        robo.tapeReadingPosicoes.Clear();
                        Position pos = robo.positions[0];
                        robo.tapeReadingPosicoes.Add(new PosicaoAtiva(obj.OpenPrice, obj.Amount, obj.Side, obj.OpenTime, "entrada mercado"));
                        if (!robo.modoAvalanche) { robo.comando.CriaParDeSaida(obj); robo.acao.Add(Acao.aguardandoOrderAdded); }
                    }
                    else
                    {
                        robo.acao.Add(Acao.eventoPositionUpdate);
                        log = obj.OpenTime.ToLocalTime().TimeOfDay + " EVENTO PositionAdded: (POSITION UPDATE) ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + robo.estado;
                        robo.Print(log);
                        robo.bufferDeOrdens.ForEach(buffer => robo.tapeReadingPosicoes.Add(buffer));

                        if (robo.modoAvalanche) { robo.acao.Add(Acao.entradaAvalancheConcluida); }

                        if (robo.orders.Length == 0 && robo.positions.Length > 0 && !robo.modoAvalanche) { robo.comando.CriaParDeSaida(robo.positions[0]); robo.acao.Add(Acao.aguardandoOrderAdded); robo.estado = State.limbo; }

                    }


                    robo.ultimaPosicaoAdd = obj;
                    robo.ultimaReentrada = obj.OpenPrice;
                    robo.bufferDeOrdens.Clear();
                    robo.bloqueieNovaEntradaAvalanche = false;
                    return;
                }
            }
            catch (Exception exc)
            {
                robo.Alert("Erro PositionAdded : " + exc.Message);
                robo.bloqueioGeral = false;
            }
        }

        public void Instruments_NewLevel2(Instrument inst, Level2 quote)
        {
            if (inst.BaseName == robo.papel)
            {
                if (robo.level2.Count < robo.qtdeLevel2)
                {
                    robo.level2.Enqueue(quote);
                }
                else
                {
                    robo.level2.Dequeue();
                    robo.level2.Enqueue(quote);
                }
            }
        }

        public void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                Graphics gr = args.Graphics;
                Rectangle rect = args.Rectangle;
                robo.funcoes.RefreshPositionsOrders();

                if (args.Rectangle.Height > 350)
                {
                    if (!robo.plotarLevel2 && robo.acao.Count > 0 && robo.Instruments.Current.BaseName == robo.papel)
                    {
                        int i = 0;
                        robo.acao.ForEach(x =>
                        {
                            gr.DrawString(x.ToString(), robo.font, robo.brush, rect.Width - 200, rect.Height - 5 - i);
                            i += 15;
                        });
                    }


                    gr.DrawString(("Calor máximo:" + robo.calor.ToString() + " Máximo de Contratos abertos: " + robo.maximoDeContratosAbertos.ToString() + " Estado atual:" + robo.estado.ToString()) + " contadorDeReEntradas:" + robo.contadorDeReEntradas, robo.font, robo.brush, rect.X + 110, rect.Height - 5);

                    if (robo.level2.Count > 0 && robo.plotarLevel2)
                    {
                        gr.DrawString("Origem", robo.font, robo.brush, rect.X + 720, rect.Y + 20);
                        gr.DrawString("Preço", robo.font, robo.brush, rect.X + 820, rect.Y + 20);
                        gr.DrawString("Tamanho", robo.font, robo.brush, rect.X + 920, rect.Y + 20);
                        gr.DrawString("Lado", robo.font, robo.brush, rect.X + 1000, rect.Y + 20);
                        gr.DrawLine(new Pen(Color.White, 1), new PointF(rect.X + 710, rect.Y), new PointF(rect.X + 710, rect.Y + rect.Y + rect.Height));
                        DrawLevel2(gr, rect);

                    }
                }

                if (robo.positions.Length > 0 && args.Rectangle.Height > 350)
                {
                    gr.DrawString("Posição", robo.font, robo.brush, rect.X + 110, rect.Y + 20);
                    gr.DrawString("Qtde", robo.font, robo.brush, rect.X + 180, rect.Y + 20);
                    gr.DrawString("Preço Médio", robo.font, robo.brush, rect.X + 220, rect.Y + 20);
                    gr.DrawString("Preço Atual", robo.font, robo.brush, rect.X + 320, rect.Y + 20);
                    gr.DrawString("Tipo", robo.font, robo.brush, rect.X + 420, rect.Y + 20);
                    gr.DrawString("Lucro/Prejuízo", robo.font, robo.brush, rect.X + 480, rect.Y + 20);
                    gr.DrawString("Comentário", robo.font, robo.brush, rect.X + 590, rect.Y + 20);

                    gr.DrawString("Protrader ->", robo.font, robo.brush, rect.X + 10, rect.Y + 40);
                    gr.DrawString("Robô ->", robo.font, robo.brush, rect.X + 10, rect.Y + 60);
                    gr.DrawLine(new Pen(Color.White), new PointF(rect.X + 110, rect.Y + 80), new PointF(rect.X + 610, rect.Y + 80));

                    gr.DrawString(robo.positions[0].Id, robo.font, robo.brush, rect.X + 110, rect.Y + 40);
                    gr.DrawString(robo.positions[0].Amount.ToString(), robo.font, robo.brush, rect.X + 180, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", robo.positions[0].OpenPrice), robo.font, robo.brush, rect.X + 220, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", robo.positions[0].CurrentPrice), robo.font, robo.brush, rect.X + 320, rect.Y + 40);
                    gr.DrawString(robo.positions[0].Side.ToString(), robo.font, robo.brush, rect.X + 420, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", robo.positions[0].GetProfitGross().ToString()), robo.font, robo.brush, rect.X + 480, rect.Y + 40);
                    gr.DrawString(robo.positions[0].Comment.ToString(), robo.font, robo.brush, rect.X + 590, rect.Y + 40);


                    if (robo.modoAvalanche) gr.DrawString("Modo Avalanche LIGADO", robo.font, robo.brush, rect.X + 110, rect.Height - 40);

                    gr.DrawString("Posicao Ref:", robo.font, robo.brush, rect.X + 110, rect.Height - 20);
                    if (robo.posicaoAtivaAvalanche != null)
                    {
                        gr.DrawString(("Qtde:" + robo.posicaoAtivaAvalanche._amount.ToString()), robo.font, robo.brush, rect.X + 190, rect.Height - 20);
                        gr.DrawString(("OpenPrice:" + String.Format("{0:C}", robo.posicaoAtivaAvalanche._openPrice)), robo.font, robo.brush, rect.X + 260, rect.Height - 20);
                        gr.DrawString(("Tipo:" + robo.posicaoAtivaAvalanche._side.ToString()), robo.font, robo.brush, rect.X + 420, rect.Height - 20);
                    }
                    else
                    {
                        gr.DrawString("NULA", robo.font, robo.brush, rect.X + 250, rect.Height - 20);
                    }



                    if (robo.tapeReadingPosicoes.Count > 0 && robo.posicaoAtivaMedia != null)
                    {
                        string precoMedioString = String.Format("{0:C}", robo.posicaoAtivaMedia._openPrice);
                        double pontos = (robo.posicaoAtivaMedia._side == Operation.Buy) ? Math.Round(((robo.cotacaoAtual - robo.posicaoAtivaMedia._openPrice) / robo.tamanhoPonto), 2) : -Math.Round(((robo.cotacaoAtual - robo.posicaoAtivaMedia._openPrice) / robo.tamanhoPonto), 2);

                        gr.DrawString(robo.tapeReadingPosicoes.Count.ToString(), robo.font, robo.brush, rect.X + 110, rect.Y + 60);
                        gr.DrawString(robo.posicaoAtivaMedia._amount.ToString(), robo.font, robo.brush, rect.X + 180, rect.Y + 60);
                        gr.DrawString(precoMedioString, robo.font, robo.brush, rect.X + 220, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", pontos.ToString()), robo.font, robo.brush, rect.X + 320, rect.Y + 60);
                        gr.DrawString(robo.posicaoAtivaMedia._side.ToString(), robo.font, robo.brush, rect.X + 420, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", robo.posicaoAtivaMedia._lucro.ToString()), robo.font, robo.brush, rect.X + 480, rect.Y + 60);
                        DrawPositions(args.Graphics, args.Rectangle);
                    }
                }

            }
            catch (Exception exc)
            {
                robo.Print("OnPaintChart Erro " + exc.Message);
            }
        }


        private void DrawPositions(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));           

            int i = 100;
            try
            {
                Operation tipoBase = robo.tapeReadingPosicoes.First()._side;

                robo.tapeReadingPosicoes.ForEach(posicao =>
                {
                    gr.DrawString(posicao._openTime.ToLocalTime().ToShortTimeString(), robo.font, robo.brush, rect.X + 110, rect.Y + i);
                    gr.DrawString(posicao._amount.ToString(), robo.font, robo.brush, rect.X + 180, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._openPrice), robo.font, robo.brush, rect.X + 220, rect.Y + i);
                    gr.DrawString(posicao._pontos.ToString(), robo.font, robo.brush, rect.X + 320, rect.Y + i);
                    gr.DrawString(posicao._side.ToString(), robo.font, robo.brush, rect.X + 420, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._lucro.ToString()), robo.font, robo.brush, rect.X + 480, rect.Y + i);
                    gr.DrawString(posicao._comments, robo.font, robo.brush, rect.X + 590, rect.Y + i);

                    i += 15;
                });

            }
            catch (Exception exc)
            {
                robo.Print("DrawPositions: Erro " + exc.Message);
                robo.Print("DrawPositions: Erro Intero " + exc.InnerException.Message);
            }
        }

        private void DrawLevel2(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));

            int i = 40;
            try
            {

                robo.level2.ToList().ForEach(_level2 =>
                {
                    gr.DrawString(_level2.Source.ToString(), robo.font, robo.brush, rect.X + 720, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", _level2.Price), robo.font, robo.brush, rect.X + 820, rect.Y + i);
                    gr.DrawString(_level2.Size.ToString(), robo.font, robo.brush, rect.X + 920, rect.Y + i);
                    gr.DrawString(_level2.Side.ToString(), robo.font, robo.brush, rect.X + 1000, rect.Y + i);

                    i += 15;
                });

            }
            catch (Exception exc)
            {
                robo.Print("DrawLevel2: Erro " + exc.Message);
                robo.Print("DrawLevel2: Erro Interno " + exc.InnerException.Message);
            }
        }





    }
}
