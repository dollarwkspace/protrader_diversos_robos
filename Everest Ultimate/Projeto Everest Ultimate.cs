﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{

    public class ProjetoEverestUltimate : NETStrategy
    {
        public Instrument instrument;
        public Account conta;
        public Indicator MM;
        public Indicator sniper;
        public Connection myConnection = Connection.CurrentConnection; //create connection object.    
        public Position[] positions;
        public Position ultimaPosicaoAdd = null;
        public PosicaoAtiva posicaoAtivaAvalanche;
        public PosicaoAtiva ultimaPosicaoAtivaAvalanche;
        public PosicaoAtiva posicaoAtivaMedia = new PosicaoAtiva(0, 0, Operation.Buy, DateTime.Now);
        public Order[] orders;
        public Font font;
        public Brush brush;
        public List<PosicaoAtiva> tapeReadingPosicoes = new List<PosicaoAtiva>();
        public List<PosicaoAtiva> bufferDeOrdens = new List<PosicaoAtiva>();
        public List<Acao> acao = new List<Acao>();
        public Queue<Level2> level2 = new Queue<Level2>();
        public LogWriter logWriter = new LogWriter();
        public DetectaVazamento detectaVazamento = new DetectaVazamento();
        public Funcoes funcoes = null;
        public Comando comando = null;
        public Eventos eventos = null;
        public Analise analise = null;
        private EstadoLimbo estadoLimbo = null;
        private EstadoInicio estadoInicio = null;
        private EstadoReEntrada estadoReEntrada = null;
        private EstadoAvalanche estadoAvalanche = null;

        public TimeSpan horario;

        public double calor = 0;
        public double curta = 0;
        public double curtaAnterior = 0;
        public double histoResult = 0;
        public double cotacaoAtual = 0;
        public double suporte = 0;
        public double suporteAnterior = 0;
        public int hora = 0;
        public int contadorDeReEntradas = 0;
        public ushort[] ReEntradas;
        public ushort[] ValoresDeReEntradas;
        public double tickSize = 0;
        public double ultimaReentrada = 0;
        public string horaDoCalorMaximo = "";
        public State estado = State.limbo;
        public bool horarioTerminou = false;
        public bool modoAvalanche = false;
        public string papel;
        public int numeroMagico = 0;
        public double maximoDeContratosAbertos = 0;
        public string ultimoComentario = "";
        public bool liberaReEntradaAvalanche = false;
        public bool bloqueieNovaEntradaAvalanche = false;
        public bool bloqueioGeral = false;
        public bool bloqueioGeralPorOrdemRemovida = false;
        public int contadorOrdemTravada = 0;
        public bool ordemDeSaida = false;

        public ProjetoEverestUltimate()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto Everest One";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "16.01.2019";
            base.ExpirationDate = 0;
            base.Version = "1.0.5";
            base.Password = "";
            base.ProjectName = "Everest One";
            #endregion

            eventos = new Eventos(this);
            comando = new Comando(this);
            analise = new Analise(this);
            funcoes = new Funcoes(this);
            estadoLimbo = new EstadoLimbo(this);
            estadoInicio = new EstadoInicio(this);
            estadoReEntrada = new EstadoReEntrada(this);
            estadoAvalanche = new EstadoAvalanche(this);
        }

        [InputParameter("Média: Sinal", 0)]
        public int periodoMediaMovelSinal = 4;
        [InputParameter("Média: Suporte", 1)]
        public int periodoMediaMovelSuporte = 15;
        [InputParameter("Média contínua ou por fechamento de candle?", 2)]
        public bool mediaContinua = true;

        [InputParameter("Permitir até quantas posições abertas? ", 3)]
        public double maxPosAbertas = 40;

        [InputParameter("Contratos", 4)]
        public int Contratos = 5;

        [InputParameter("Início(hora)", 5)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

        [InputParameter("Término(hora)", 6)]
        public TimeSpan HoraDeTermino = new TimeSpan(17, 50, 00);

        [InputParameter("Array Ticks de Reentradas", 7)]
        public string ticksDeReentrada = "10,10";

        [InputParameter("Array Valores de Reentradas", 8)]
        public string valoresDeReentrada = "5,5";

        [InputParameter("Distância da Média (tend)", 9)]
        public double distanciaDaMedia = 4;

        [InputParameter("Distância da Média (contra)", 10)]
        public double distanciaDaMediaContra = 7;

        [InputParameter("Take Profit (Pontos)", 11)]
        public double TP = 1;

        [InputParameter("Multiplicador da Dif.Das Médias", 12, 0, 100, 3, 0.001)]
        public double multiplicador = 10;

        [InputParameter("Resiliência de Saída", 13, 0, 100, 3, 0.001)]
        public double resilienciaDeSaida = 0;

        [InputParameter("Loss máximo em Ticks", 14)]
        public double lossMaximo = 50;

        [InputParameter("Tipo de Saída", 15, new Object[]
     {"Dif Médias", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;

        [InputParameter("Tipo de Robô", 16, new Object[]
     {"Tendência", TipoDeRobo.tendencia, "Contra Tendência",TipoDeRobo.contraTendencia, "Tendência e Contra", TipoDeRobo.tendenciaEcontra })]
        public TipoDeRobo tipoDeRobo = TipoDeRobo.tendenciaEcontra;

        [InputParameter("Market Range", 17)]
        public int marketRange = 1;

        [InputParameter("Custo de 1 contrato por ponto", 18)]
        public double custoContratoPorPonto = 50;

        [InputParameter("Tamanho do Ponto", 19)]
        public double tamanhoPonto = 1;

        [InputParameter("Lateralidade abaixo de: (suporte-sinal) ", 20)]
        public double lateralidade = 0.2;

        [InputParameter("Plotar Level2? ", 21)]
        public bool plotarLevel2 = true;

        [InputParameter("Qtde Level2 na tela  ", 22)]
        public int qtdeLevel2 = 30;

        [InputParameter("log:", 23)]
        public string _log = "log.txt";

        [InputParameter("log tela:", 24)]
        public string _logPosicoes = "logPosicoes.txt";

        [InputParameter("TP Avalanche", 25)]
        public double TPavalanche = 2;

        [InputParameter("Avalanche após N contratos", 26)]
        public double avalanche = 5;

        [InputParameter("ReEntrada % Avalanche", 27)]
        public double contratosAvalanche = 10;

        [InputParameter("Lote Mínimo Avalanche", 28)]
        public double minimumLot = 5;

        [InputParameter("ReEntrada Avalanche em Ticks", 29)]
        public double reEntradaAvalanche = 4;

        [InputParameter("Lucro de saída avalanche R$", 30)]
        public double lucroDeSaidaAvalanche = 250;

        [InputParameter("Pontos para Inicio Avalanche", 31)]
        public double ticksAvalanche = 5;


        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            try
            {
                font = new Font("Tahoma", 10);
                brush = new SolidBrush(Color.White);
                instrument = Instruments.Current;
                Print("Nome do Instrumento ativo: " + instrument.Name);
                papel = instrument.BaseName;
                conta = Accounts.Current;
                tickSize = instrument.TickSize;
                Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote | QuoteTypes.Level2);
                funcoes.InfoConexao(myConnection);
                funcoes.AccountInformation(conta);

                ReEntradas = ticksDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                //ReEntradas[0] = reEntrada0; ReEntradas[1] = reEntrada1; ReEntradas[2] = reEntrada2; ReEntradas[3] = reEntrada3; ReEntradas[4] = reEntrada4; ReEntradas[5] = reEntrada5; ReEntradas[6] = reEntrada6;
                ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();

                if (ReEntradas.Length != ValoresDeReEntradas.Length)
                {
                    throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
                }

                Print("Papel: " + instrument.Name);
                Print("Tamanho do Point: " + Point);
                Print("Tamanho do tick: " + instrument.TickSize);


                //INDICADORES
                Print("Inicializando indicador Camilo 2MM");
                MM = Indicators.iCustom("camilo_2mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelSuporte);
                Print("Inicializando indicador Sniper Elite");
                sniper = Indicators.iCustom("Camilo_SniperElite", CurrentData, periodoMediaMovelSinal, resilienciaDeSaida, multiplicador);


                //EVENTOS            
                Positions.PositionAdded += Positions_PositionAdded;
                Positions.PositionRemoved += Positions_PositionRemoved;
                Instruments.NewTrade += Instruments_NewTrade;
                Instruments.NewLevel2 += Instruments_NewLevel2;
                Orders.OrderAdded += Orders_OrderAdded;
                Orders.OrderExecuted += Orders_OrderExecuted;
                Orders.OrderRemoved += Orders_OrderRemoved;


                Print("Fim da Inicialização do Robô");

                if (myConnection.Status == ConnectionStatus.Disconnected)
                {
                    Print("RODANDO ROBÔ NO SIMULADOR. NÚMERO MÁGICO É 0");
                    numeroMagico = 0; // SE ESTIVER NO SIMULADOR O NUMERO MÁGICO É ZERO
                }
                else
                {
                    Print("RODANDO ROBÔ NA CONTA DEMO OU REAL. Número mágico deste robô é " + numeroMagico);
                }


                //VERIFICANDO ESTADO ANTERIOR                  
                Print("Inicio ");
                tapeReadingPosicoes.Clear();

                funcoes.RefreshIndicators();

                if (estado == State.limbo) acao.Add(Acao.passeLivre);

            }
            catch (Exception exc)
            {
                Print("Erro na inicialização do Robô... " + exc.Message);
                Print("Finalizando Robô");
                TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
                foreach (var robo in robos)
                {
                    //robo.Stop();
                    Print("Nome do robô:" + robo.Name);
                }
            }
        }

        public override void OnQuote()
        {
            funcoes.RefreshPositionsOrders();

            if (myConnection.Status == ConnectionStatus.Disconnected)
            {
                //ou seja, está no simulador onde não tem OnTrades...                
                Instruments_NewTrade(instrument, instrument.GetLastTrade());
            }

        }

        public override void NextBar()
        {
            if (estado == State.limbo) acao.Add(Acao.passeLivre);
            if (bloqueioGeralPorOrdemRemovida) { bloqueioGeral = false; bloqueioGeralPorOrdemRemovida = false; }
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade | QuoteTypes.Level2);
            Positions.PositionAdded -= Positions_PositionAdded;
            Positions.PositionRemoved -= Positions_PositionRemoved;
            Instruments.NewTrade -= Instruments_NewTrade;
            Instruments.NewLevel2 -= Instruments_NewLevel2;
            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;
        }

        private void Instruments_NewLevel2(Instrument inst, Level2 quote)
        {
            eventos.Instruments_NewLevel2(inst, quote);
        }

        private void Orders_OrderAdded(Order obj)
        {
            eventos.Orders_OrderAdded(obj);
        }

        private void Orders_OrderRemoved(Order obj)
        {
            eventos.Orders_OrderRemoved(obj);
        }

        private void Orders_OrderExecuted(Order obj)
        {
            eventos.Orders_OrderExecuted(obj);
        }

        private void Positions_PositionRemoved(Position obj)
        {
            eventos.Positions_PositionRemoved(obj);
        }

        private void Positions_PositionAdded(Position obj)
        {
            eventos.Positions_PositionAdded(obj);
        }


        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            eventos.OnPaintChart(sender, args);
        }

        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************         
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************  


        private void Instruments_NewTrade(Instrument inst, Trade trade)
        {
            if (inst.BaseName == papel)
            {
                string log = "********************   NewTrade   ********************";
                Print(log);
                if (!funcoes.RefreshPositionsOrders()) return; //atualiza as ordens e posições ativas
                if (!funcoes.RefreshIndicators()) return; //lê os indicadores
                if (!funcoes.CheckMarketStatus(trade)) return; //checa se bolsa está aberta e se está fechada fecha as posições
                if (!funcoes.CalculaCalorMaximo()) return;
                if (curta == 0 || curtaAnterior == 0) { Print("Aguardando indicadores!"); return; }

                funcoes.SaidaPorLossMaximo();

                if (bloqueioGeralPorOrdemRemovida) { Print("ROBÔ EM LOCKDOWN TEMPORÁRIO POR ORDEM REMOVIDA - Saída no próximo candle"); return; }

                if (bloqueioGeral)
                {
                    Print("Robô está em LOCKDOWN!");
                    return;
                }

                funcoes.DetectaAvalanche();

                try
                {
                    switch (estado)
                    {
                        case State.limbo: { estadoLimbo.Limbo(); break; }
                        case State.inicio: { estadoInicio.Inicio(); break; }
                        case State.reentrada: { estadoReEntrada.ReEntrada(); break; }
                        case State.avalanche: { estadoAvalanche.Avalanche(); break; }
                        case State.termino: { Print("Horário de Término atingido... "); break; }
                        case State.bolsaFechada:
                            {
                                if (Positions.Count > 0) Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); funcoes.FechamentoTotal();
                                Print(horario + " BOLSA FECHADA ");
                                break;
                            }
                        case State.lockdown: { Print("Estado Lockdown: Robô Bloqueado aguardando Eventos da corretora"); break; }
                    }

                }
                catch (Exception exc)
                {
                    Print("NewTrade:  Erro setor switch . Mensagem -> " + exc.Message);
                    Print("NewTrade: Erro:  InnerMessage -> " + exc.InnerException.Message);
                    return;
                }
            }
        }




    }

}
