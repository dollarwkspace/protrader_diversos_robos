﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;
namespace EverestUltimate
{
    public class EstadoInicio
    {
        private ProjetoEverestUltimate robo;

        public EstadoInicio(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public void Inicio()
        {
            string log = "********************   Estado Início  ********************";
            robo.Print(log);

            if (robo.horarioTerminou) { robo.Print("Horário de Término atingido, não entra mais..."); return; }

            robo.ultimoComentario = "";
            robo.posicaoAtivaAvalanche = null;
            robo.modoAvalanche = false;
            robo.tapeReadingPosicoes.Clear();
            robo.contadorDeReEntradas = 0;

        LoopInicio:
            robo.funcoes.RefreshPositionsOrders();
            if (robo.orders.Length > 0)
            {
                log = "Estado Início: Tem ordem ativa ainda ... verificando o status dela .. " + robo.orders[0].Status;
                robo.Print(log);

                if (robo.orders[0].Status == OrderStatus.PendingCancel || robo.orders[0].Status == OrderStatus.PendingExecution)
                {
                    robo.Print("Devemos aguardar a corretora definir o que fazer com a ordem.");
                    goto LoopInicio;
                }
            }

            if (robo.positions.Length > 0)
            {
                log = "Estado início: Tem posição ativa ainda. Fecha tudo e continua...";
                robo.Print(log);
                robo.funcoes.FechamentoTotal();
            }

            robo.acao.Clear();

            if (Math.Abs(robo.suporte - robo.curta) >= robo.lateralidade)
            {
                robo.Print("Hora: " + robo.horario + "Estado Inicio: Preço atual:" + robo.cotacaoAtual + " Média curta:" + robo.curta + " Ticks da média:" + (Math.Abs(robo.cotacaoAtual - robo.curta) / robo.tickSize));
                robo.Print("Estado Inicio: Modo de Entrada: Sinal Atual - Anterior = " + (robo.curta - robo.curtaAnterior));
                robo.Print("Estado Inicio: Indicador de Lateralidade |sinal-suporte| = " + Math.Abs(robo.suporte - robo.curta) + " Lateralidade:" + robo.lateralidade);
                if ((Math.Abs(robo.cotacaoAtual - robo.curta) / robo.tickSize) <= robo.distanciaDaMedia && (robo.tipoDeRobo == TipoDeRobo.tendencia || robo.tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                {
                    double diff = 0;
                    if (robo.mediaContinua) diff = robo.curta - robo.curtaAnterior;
                    if (!robo.mediaContinua) diff = robo.histoResult;

                    if (robo.comando.EntradaDiffMedias(robo.cotacaoAtual, diff) != "-1")
                    {
                        log = robo.horario + " Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo";
                        robo.Print(log);
                        robo.acao.Add(Acao.aguardandoPositionAdded);
                        robo.acao.Add(Acao.solicitouEntrada);
                        robo.estado = State.limbo;
                        return;
                    }
                    else
                    {
                        log = robo.horario + " Estado Inicio: Ordem TENDÊNCIA recusada"; robo.Print(log);
                        robo.estado = State.inicio;
                        return;
                    }

                }
                else if ((Math.Abs(robo.cotacaoAtual - robo.curta) / robo.tickSize) >= robo.distanciaDaMediaContra && (robo.tipoDeRobo == TipoDeRobo.contraTendencia || robo.tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                {
                    if (robo.comando.EntradaContra(robo.cotacaoAtual) != "-1")
                    {
                        log = robo.horario + " Estado Inicio: Ordem CONTRA-TENDÊNCIA foi enviada ! Aguardando sua resolução no limbo";
                        robo.Print(log);
                        robo.acao.Add(Acao.aguardandoPositionAdded);
                        robo.acao.Add(Acao.solicitouEntrada);
                        robo.estado = State.limbo;
                        return;
                    }
                    else
                    {
                        log = robo.horario + "Estado Inicio: Ordem CONTRA-TENDÊNCIA recusada"; robo.Print(log);
                        robo.estado = State.inicio;
                        return;
                    }
                }
                else robo.Print("Estado Inicio: Aguardando condições de entrada");
            }
            else robo.Print("Estado Inicio: Mercado está lateral! |sinal-suporte| = " + Math.Abs(robo.suporte - robo.curta) + " Lateralidade:" + robo.lateralidade);
        }



    }
}
