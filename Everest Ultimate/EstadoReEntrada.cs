﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{
    public class EstadoReEntrada
    {
        private ProjetoEverestUltimate robo;

        public EstadoReEntrada(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public void ReEntrada()
        {
            robo.Print("********************   Estado ReEntrada   ***********************");
            robo.funcoes.RefreshPositionsOrders();

            if (robo.positions.Length == 0)
            {
                string log = "Estado ReEntrada: Não existe mais posição. Migrando para inicio";
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                robo.estado = State.inicio;
                robo.acao.Clear();
                robo.modoAvalanche = false;
                robo.posicaoAtivaAvalanche = null;
                return;
            }

            if (robo.orders.Length == 0)
            {
                if (robo.positions.Length > 0)
                {
                    string log = "Estado ReEntrada: Ordem de saída zero, e tem posição! Criando Par de Saída";
                    robo.Print(log);
                    robo.comando.CriaParDeSaida();
                    robo.acao.Clear();
                    robo.estado = State.limbo;
                    robo.acao.Add(Acao.aguardandoOrderAdded);
                    return;
                }
                else
                {
                    string log = "Estado ReEntrada: Sem ordem e sem posição? Migra pro inicio...";
                    robo.Print(log);
                    robo.estado = State.inicio;
                    robo.acao.Clear();
                    return;
                }
            }

            if (robo.positions.Length > 0) robo.funcoes.SaidaPorLossMaximo();

            if (robo.contadorDeReEntradas > robo.ValoresDeReEntradas.Length - 1)
            {
                robo.Print("Estado ReEntrada: Máximo número de ReEntradas atingido!");
                return;
            }

            robo.funcoes.RefreshPositionsOrders();
            if (robo.positions.Length != 0)
            {
                Position pos = robo.positions[0];

                Order ord = null;

                if (robo.orders.Length > 0) ord = robo.orders[0];

                if (ord != null) robo.funcoes.EqualizaOrdemPosicao(pos, ord);
                robo.acao.Clear();

                var deltaMoeda = robo.tickSize * robo.ReEntradas[robo.contadorDeReEntradas];
                var liberaReEntrada = false;
                robo.Print("Hora:" + robo.horario + "Estado ReEntrada: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize));

                if (robo.tipoDeSaida == TipoDeSaida.DifMédias)
                {
                    if (robo.mediaContinua) robo.Print("Estado ReEntrada: (média - média Anterior) x multiplicador = " + (robo.curta - robo.curtaAnterior) * robo.multiplicador);
                    if (!robo.mediaContinua) robo.Print("Estado ReEntrada: histograma = " + robo.histoResult);
                }
                robo.Print("Estado ReEntrada: Entrada atual = " + robo.contadorDeReEntradas);

                if (pos.Side == Operation.Buy && pos.CurrentPrice <= robo.ultimaReentrada - deltaMoeda)
                {
                    robo.Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                    liberaReEntrada = true;
                }

                if (pos.Side == Operation.Sell && pos.CurrentPrice >= robo.ultimaReentrada + deltaMoeda)
                {
                    robo.Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                    liberaReEntrada = true;
                }

                if (liberaReEntrada)
                {
                    robo.Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                    if (robo.positions[0].Amount <= robo.maxPosAbertas / 2)
                    {
                        var result = robo.comando.ReEntrada(robo.cotacaoAtual, robo.positions[0]);
                        if (result != "-1")
                        {
                            string log = robo.horario + " Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo.";
                            robo.Print(log);
                            robo.acao.Add(Acao.aguardandoPositionAdded);
                            robo.acao.Add(Acao.solicitouReEntrada);
                            robo.contadorDeReEntradas++;
                            robo.estado = State.limbo;
                            return;
                        }
                    }
                    else
                    {
                        robo.Print("Estado ReEntrada: Máximo de Contratos atingidos!");
                    }
                }

                double diff = 0;
                if (robo.mediaContinua) diff = (robo.curta - robo.curtaAnterior) * robo.multiplicador;
                if (!robo.mediaContinua) diff = robo.histoResult;

                if (robo.tipoDeSaida == TipoDeSaida.DifMédias && diff >= robo.resilienciaDeSaida && pos.Side == Operation.Sell)
                {
                    string log = robo.horario + "Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                    robo.Print(log);
                    robo.funcoes.FechamentoTotal();
                    robo.acao.Clear();
                    robo.estado = State.limbo;
                    robo.acao.Add(Acao.passeLivre);
                    return;
                }

                if (robo.tipoDeSaida == TipoDeSaida.DifMédias && diff <= -robo.resilienciaDeSaida && pos.Side == Operation.Buy)
                {
                    string log = robo.horario + " Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                    robo.Print(log);
                    robo.funcoes.FechamentoTotal();
                    robo.acao.Clear();
                    robo.estado = State.limbo;
                    robo.acao.Add(Acao.passeLivre);
                    return;
                }

                robo.funcoes.PrintaPosicoesOrdensAtivas();
            }
        }

    }
}
