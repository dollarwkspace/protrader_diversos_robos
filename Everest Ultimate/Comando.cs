﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;

namespace EverestUltimate
{
    public class Comando
    {
        private ProjetoEverestUltimate robo = null;

        public Comando (ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public string EntradaDiffMedias(double preco, double diff)
        {
            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada");
            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, robo.instrument.RoundPrice(preco), robo.Contratos, 0);
            return "-1";
        }

        public string EntradaContra(double preco)
        {
            if (preco > robo.curta + robo.distanciaDaMediaContra * robo.tickSize) return Send_order(OrdersType.Market, Operation.Sell, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada contra");
            if (preco < robo.curta - robo.distanciaDaMediaContra * robo.tickSize) return Send_order(OrdersType.Market, Operation.Buy, robo.instrument.RoundPrice(preco), robo.Contratos, 0);
            return "-1";
        }

        public string ReEntrada(double preco, Position pos)
        {
            var precoMedio = pos.OpenPrice;
            var deltaMoeda = robo.instrument.TickSize * robo.ReEntradas[robo.contadorDeReEntradas];

            if (pos.Side == Operation.Buy)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0, "reEntrada mercado " + robo.contadorDeReEntradas.ToString());
            }

            if (pos.Side == Operation.Sell)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0, "reEntrada mercado " + robo.contadorDeReEntradas.ToString());
            }

            return "-1";
        }


        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit, string comment = "entrada")
        {
            robo.acao.Clear();
            robo.ultimoComentario = comment;
            price = robo.instrument.RoundPrice(price);

            string log = robo.horario + " Send_Order(): Type:" + type + " Side:" + side + " Price:" + price + " Contratos:" + contratos + " Comment:" + comment;
            robo.Print(log);

            NewOrderRequest request = new NewOrderRequest
            {
                Instrument = robo.instrument,
                Account = robo.conta,
                Type = type,
                Side = side,
                Amount = contratos,
                MarketRange = robo.marketRange,
                MagicNumber = robo.numeroMagico,
                Comment = comment
            };

            Quote quote = robo.instrument.LastQuote;
            if (quote != null)
            {
                price = (side == Operation.Buy) ? quote.Ask : quote.Bid;

                if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? robo.instrument.RoundPrice(price + takeProfit * robo.instrument.TickSize) : robo.instrument.RoundPrice(price - takeProfit * robo.instrument.TickSize);
                if (type != OrdersType.StopLimit) { request.Price = robo.instrument.RoundPrice(price); } else { request.StopPrice = robo.instrument.RoundPrice(price); }

                robo.Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + robo.instrument.RoundPrice(price) + " Qtde:" + request.Amount + " Ponto Atual da Média:" + robo.curta);
                var result = robo.Orders.Send(request);
                if (result == "-1") { log = robo.horario + " SendOrder: Ordem não aceita pela corretora"; robo.Print(log); }
                if (result != "-1")
                {
                    if (robo.acao.Count == 0) robo.bloqueioGeral = true; log = robo.horario + " SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!"; robo.Print(log);
                    return result;
                }

                return "-1";

            }

            return "-1";
        }

        public bool CriaParDeSaida(Position posicao = null, string comment = "saida limite")
        {
            //TAKE PROFIT LIMIT 
            robo.acao.Clear();
            Position[] positions;
            if (posicao == null)
            {
                positions = robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ToArray();
                if (positions.Length > 0) posicao = positions[0];
            }

            if (posicao != null)
            {
                string log = robo.horario + " CriaParDeSaida: Ok! Tentando setar TAKE PROFIT LIMIT";
                robo.Print(log);
                try
                {
                    Position pos = posicao;
                    if (pos.Side == Operation.Buy)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateSellLimit(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize), robo.conta);
                        request.MagicNumber = robo.numeroMagico;
                        request.MarketRange = robo.marketRange;
                        request.Comment = comment;
                        var id = robo.Orders.Send(request);
                        if (id == "-1")
                        {
                            log = robo.horario + "CriaParDeSaida: Ordem Limit de saída SELL não colocada... tentando ordem STOP";
                            robo.Print(log);
                            NewOrderRequest req = NewOrderRequest.CreateSellStop(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize), robo.conta);
                            req.MagicNumber = robo.numeroMagico;
                            req.MarketRange = robo.marketRange;
                            robo.ultimoComentario = comment;
                            req.Comment = comment;
                            var id2 = robo.Orders.Send(req);
                            if (id2 == "-1") { log = robo.horario + "CriaParDeSaida: Ordem SELL STOP não aceita!"; robo.Print(log); return false; } else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                        }
                        else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                    }
                    if (pos.Side == Operation.Sell)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateBuyLimit(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize), robo.conta);
                        request.MagicNumber = robo.numeroMagico;
                        request.MarketRange = robo.marketRange;
                        robo.ultimoComentario = comment;
                        request.Comment = comment;
                        var id = robo.Orders.Send(request);
                        if (id == "-1")
                        {
                            log = robo.horario + "CriaParDeSaida: Ordem Limit de saída BUY não colocada... tentando ordem STOP";
                            robo.Print(log);
                            NewOrderRequest req = NewOrderRequest.CreateBuyStop(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize), robo.conta);
                            req.MagicNumber = robo.numeroMagico;
                            req.MarketRange = robo.marketRange;
                            req.Comment = comment;
                            var id2 = robo.Orders.Send(req);
                            if (id2 == "-1") { log = robo.horario + "CriaParDeSaida: Ordem BUY STOP não aceita!"; robo.Print(log); return false; } else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                        }
                        else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                    }
                }
                catch (Exception ext)
                {
                    robo.Print("CriaParDeSaida: Erro ao Setar Take Profit! " + ext.Message);
                    return false;
                }
            }
            else
            {
                robo.Print("CriaParDeSaída: Não existe posição");
                robo.funcoes.LimparTodasAsOrdens();
            }

            return false;
        }

        public bool SaidaAvalanche(PosicaoAtiva posicao, string comment = "saida avalanche")
        {

            robo.acao.Clear();
            string log = "SaidaAvalanche: Ok! Tentando setar TAKE PROFIT LIMIT";
            robo.Print(log);
            try
            {
                PosicaoAtiva pos = posicao;
                robo.ultimoComentario = comment;
                if (pos._side == Operation.Buy)
                {
                    NewOrderRequest request = NewOrderRequest.CreateSellLimit(robo.instrument, pos._amount, robo.instrument.RoundPrice(pos._openPrice + robo.TPavalanche * robo.tickSize), robo.conta);
                    request.MagicNumber = robo.numeroMagico;
                    request.MarketRange = robo.marketRange;
                    request.Comment = comment;
                    var id = robo.Orders.Send(request);
                    if (id == "-1")
                    {
                        log = "SaidaAvalanche: Ordem Limit de saída SELL não colocada... tentando ordem STOP";
                        robo.Print(log);
                        NewOrderRequest req = NewOrderRequest.CreateSellStop(robo.instrument, pos._amount, robo.instrument.RoundPrice(pos._openPrice + robo.TPavalanche * robo.tickSize), robo.conta);
                        req.MagicNumber = robo.numeroMagico;
                        req.MarketRange = robo.marketRange;
                        req.Comment = comment;
                        var id2 = robo.Orders.Send(req);
                        if (id2 == "-1")
                        {
                            log = "SaidaAvalanche: Ordem SELL STOP não aceita!";
                            robo.Print(log);
                            return false;
                        }
                        else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                    }
                    else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                }
                if (pos._side == Operation.Sell)
                {
                    NewOrderRequest request = NewOrderRequest.CreateBuyLimit(robo.instrument, pos._amount, robo.instrument.RoundPrice(pos._openPrice - robo.TPavalanche * robo.instrument.TickSize), robo.conta);
                    request.MagicNumber = robo.numeroMagico;
                    request.MarketRange = robo.marketRange;
                    request.Comment = comment;
                    var id = robo.Orders.Send(request);
                    if (id == "-1")
                    {
                        log = "SaidaAvalanche: Ordem Limit de saída BUY não colocada... tentando ordem STOP";
                        robo.Print(log);
                        NewOrderRequest req = NewOrderRequest.CreateBuyStop(robo.instrument, pos._amount, robo.instrument.RoundPrice(pos._openPrice - robo.TPavalanche * robo.instrument.TickSize), robo.conta);
                        req.MagicNumber = robo.numeroMagico;
                        req.MarketRange = robo.marketRange;
                        req.Comment = comment;
                        var id2 = robo.Orders.Send(req);
                        if (id2 == "-1") { log = "SaidaAvalanche: Ordem BUY STOP não aceita!"; robo.Print(log); return false; } else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                    }
                    else { robo.ordemDeSaida = true; if (robo.acao.Count == 0) robo.bloqueioGeral = true; return true; }
                }
            }
            catch (Exception ext)
            {
                robo.Print("SaidaAvalanche: Erro ao Setar Take Profit! " + ext.Message);
                return false;
            }

            return false;
        }



    }
}
