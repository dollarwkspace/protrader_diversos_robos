﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestUltimate
{
    public class EstadoLimbo
    {
        private ProjetoEverestUltimate robo;

        public EstadoLimbo(ProjetoEverestUltimate _robo)
        {
            robo = _robo;
        }

        public void Limbo()
        {
            string log = robo.horario + "  ********************   Estado Limbo *****************";
            robo.funcoes.RefreshPositionsOrders();
            robo.Print(log);
            robo.Print("Hora: " + robo.horario + "  Limbo: Papel:" + robo.instrument.BaseName + "\t Numero mágico " + robo.numeroMagico + "\t Lucro de hj: " + robo.conta.TodayNet + "\t Qtde de Trades: " + robo.conta.TodayTrades + "\t hora último trade:" + robo.horario);
            robo.funcoes.PrintaPosicoesOrdensAtivas();
            bool passeLivreNoLimbo = false;

            robo.acao.ForEach(x => { robo.Print(x); });

            if (robo.acao.Count > 0)
            {
                if (robo.acao.Exists(x => x == Acao.passeLivre) ||
                    robo.acao.Exists(x => x == Acao.eventoOrderRemoved) ||
                    robo.acao.Exists(x => x == Acao.eventoPositionRemoved))
                {
                    passeLivreNoLimbo = true;
                    log = "Estado Limbo: Saída por PasseLivre, Ordem Removida ou Posição Removida";
                    robo.Print(log);
                    goto PasseLivre;
                }

                if ((robo.acao.Exists(x => x == Acao.aguardandoPositionAdded) && robo.acao.Exists(x => x == Acao.eventoPositionAdded)) ||
                    (robo.acao.Exists(x => x == Acao.aguardandoOrderAdded) && robo.acao.Exists(x => x == Acao.eventoOrderAdded))
                    )
                {
                    passeLivreNoLimbo = true;
                    log = "Estado Limbo: Saída por Posição Adicionada ou Ordem Adicionada";
                    robo.Print(log);
                    goto PasseLivre;
                }

                if ((robo.acao.Exists(x => x == Acao.aguardandoPositionUpdate) && robo.acao.Exists(x => x == Acao.eventoPositionUpdate)) ||
                  (robo.acao.Exists(x => x == Acao.solicitouEntradaAvalanche) && robo.acao.Exists(x => x == Acao.entradaAvalancheConcluida))
                  )
                {
                    passeLivreNoLimbo = true;
                    log = "Estado Limbo: Saída por Posição Atualizada ou entrada Avalanche concluída ";
                    robo.bloqueieNovaEntradaAvalanche = false;
                    robo.Print(log);
                    goto PasseLivre;
                }


            }

        PasseLivre:
            if (passeLivreNoLimbo)
            {
                robo.acao.Clear();
                robo.analise.Juiz();
            }
        }
    }
}
