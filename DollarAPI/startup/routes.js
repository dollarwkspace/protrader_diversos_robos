const express = require('express');
const error = require('../middlewares/error');
const users = require('../routes/users');
const auth = require('../routes/auth');
const comandos = require('../routes/comandos');
const parametros = require('../routes/parametros');
const parametrosAtivos = require('../routes/parametrosAtivos');

module.exports = function (app) {
    //Middlewares
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, x-auth-token, nome, papel");
        res.header('Access-Control-Allow-Methods', '*'); 
        next();
    });
    app.use(express.json());    
    app.use(express.urlencoded({extended: true}));
    app.use(express.static('public'));
    
    //Rotas
    app.use('/api/users', users);
    app.use('/api/auth', auth);    
    app.use('/api/comandos', comandos);    
    app.use('/api/parametros', parametros);   
    app.use('/api/parametrosAtivos', parametrosAtivos);   

    //error middleware after all middlewares and routes
    app.use(error);
}