const mongoose = require('mongoose');
const winston = require('winston');
/* Note to users :
Change the mongodb addr to the location of your server*/

module.exports = function () {
    var uri=`mongodb+srv://${process.env.usuario}:${process.env.senha}@cluster0-1l9gv.mongodb.net/dollar?retryWrites=true`;
    mongoose.connect(uri,{ useNewUrlParser: true })
        .then(() => winston.info('Connected to MongoDB...'));

}