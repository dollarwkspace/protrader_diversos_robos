const Joi = require('joi');
const bcrypt = require('bcrypt');
const {User} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const asyncMiddleware = require('../middlewares/async');


router.post('/', asyncMiddleware(async (req, res) => { 
       
    const {error} = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({Login: req.body.Login});
    if(!user) return res.status(400).send('Login inválido');

    const validPassword = await bcrypt.compare(req.body.Senha, user.Senha);
    if(!validPassword) return res.status(400).send('Senha inválida');

    const token = user.generateAuthToken();
    res.status(200).send(token);
}));

function validate(req) {
    const schema = {      
        _id: Joi.any(), 
        Nome: Joi.any(),
        Sobrenome: Joi.any(),
        Login: Joi.string().required(),
        Senha: Joi.string().min(5).max(1024).required()
    };
    return Joi.validate(req, schema);
}

module.exports = router;

