const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const {Comandos, validate} = require('../models/comandos.js');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const asyncMiddleware = require('../middlewares/async');

router.get('/', auth, asyncMiddleware(async (req, res) => {
    let comandos = await Comandos.findOne({IdUser: req.user._id}).select("-_id -IdUser");
    if (!comandos) { 
        comandos = new Comandos(); 
        comandos.IdUser=req.user._id; 
        await comandos.save(); 
        comandos = await Comandos.findOne({IdUser: req.user._id}).select("-_id -IdUser");
        return res.status(200).send(comandos);
    }    
    res.status(200).send(comandos);
}));

router.post('/', auth, asyncMiddleware(async (req, res) => {
    let comandos = await Comandos.findOne({IdUser: req.user._id});
    if (comandos) { return res.status(200).send("Já existe registro de comandos para esse usuário");}
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);        
    comandos = new Comandos(req.body);
    comandos.IdUser = req.user._id;
    await comandos.save();
    comandos = await Comandos.findOne({IdUser: req.user._id}).select("-_id -IdUser");
    res.status(200).send(comandos);
}));

router.put('/', auth, asyncMiddleware(async (req, res) => {
    let comandos = await Comandos.findOne({IdUser: req.user._id});
    if (!comandos) { return res.status(200).send("Não existe Comandos para esse usuário");}
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);        
    var query = {IdUser: req.user._id};
    var updatedObject = req.body;
    comandos = await Comandos.findOneAndUpdate(query, updatedObject, {new: true}).select("-_id -IdUser");
    res.status(200).send(comandos);
}));

router.delete('/', auth, asyncMiddleware(async (req, res) => {
    const comandos = await Comandos.findOne({IdUser: req.user._id});
    if (!comandos) { return res.status(200).send("Não existe base de comandos para esse usuário");}
    await Comandos.findOneAndRemove({IdUser: req.user._id});
    res.status(200).send(`Comandos APAGADOS`);
})); 

module.exports = router;