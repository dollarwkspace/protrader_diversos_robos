const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const {User, validate} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const asyncMiddleware = require('../middlewares/async');

router.get('/me', auth, asyncMiddleware(async (req, res) => {
    const user = await User.findOne({_id: req.user._id}).select('-Senha');
    res.send(user);
}));

router.post('/', asyncMiddleware(async (req, res) => {

    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({Login: req.body.Login});
    if (user) return res.status(400).send('Usuário já registrado');

    user = new User(_.pick(req.body, ['Nome', 'Sobrenome', 'Login', 'Senha']));

    const salt = await bcrypt.genSalt(10);

    user.Senha = await bcrypt.hash(user.Senha, salt);
    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(_.pick(user, ['_id', 'Login']));
}));

router.delete('/', auth, asyncMiddleware(async (req, res) => {
    const user = await User.findOne({_id: req.user._id});
    await User.findOneAndRemove({_id: req.user._id});
    res.status(200).send(`Usuário ${user.Login} APAGADO`);
}));


 /*router.post('/login', auth, asyncMiddleware(async (req, res) => {    
     let user = await User.findById(req.user._id).select('-Senha');
     if (!user) return res.status(400).send('LOGIN não encontrado');
     res.status(200).send(user);
 }));

 router.post('/logout', auth, asyncMiddleware(async (req, res) => {
     let user = await User.findById(req.user._id).select('-Senha');
     if (!user) return res.status(400).send('ID não encontrado');
     res.status(200).send(user);
 }));*/


module.exports = router;