const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const {ParametrosAtivos,validate} = require('../models/parametrosAtivos.js');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const asyncMiddleware = require('../middlewares/async');

router.get('/', auth, asyncMiddleware(async (req, res) => {
    var query = {IdUser: req.user._id, nome:req.header('nome'), papel:req.header('papel')};
    let parametros = await ParametrosAtivos.findOne(query).select("-_id -IdUser");
    if (!parametros) {return res.status(400).send("Não existe parâmetros para nome:"+req.header('nome')+" papel:"+req.header('papel'));}
    res.status(200).send(parametros);
}));

router.get('/all', auth, asyncMiddleware(async (req, res) => {
    var query = {IdUser: req.user._id};
    let parametros = await ParametrosAtivos.find(query).select("-_id -IdUser");
    if (!parametros) {return res.status(400).send("Não existe parâmetros ativos para este usuário");}
    res.status(200).send(parametros);
}));

router.post('/', auth, asyncMiddleware(async (req, res) => {
    var query = {IdUser: req.user._id, nome:req.body.nome, papel:req.body.papel};
    let parametros = await ParametrosAtivos.findOne(query).select("-_id -IdUser");
    if (parametros) return res.status(400).send("registro nome: " + req.body.nome + " papel:" + req.body.papel + "  já existe. Tente PUT ou DELETE.");;
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    parametros = new ParametrosAtivos(req.body);    
    parametros.IdUser = req.user._id;      
    await parametros.save();
    parametros = await ParametrosAtivos.findOne(query).select("-_id -IdUser");
    res.status(200).send(parametros);
}));

router.put('/', auth, asyncMiddleware(async (req, res) => {
    var query = {IdUser: req.user._id, nome:req.body.nome, papel:req.body.papel};
    let parametros = await ParametrosAtivos.findOne(query).select("-_id -IdUser");
    if (!parametros) return res.status(400).send("registro nome: " + req.body.nome + " papel:" + req.body.papel + "  NÃO ENCONTRADO.");;
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);    
    var updatedObject = req.body;
    parametros = await ParametrosAtivos.findOneAndUpdate(query, updatedObject, {new: true});
    res.status(200).send(parametros);
}));

router.delete('/', auth, asyncMiddleware(async (req, res) => {
    var query = {IdUser: req.user._id, nome:req.header('nome'), papel:req.header('papel')};
    const parametros = await ParametrosAtivos.findOne(query).select("-_id -IdUser");
    if (!parametros) {return res.status(400).send("Não existem esses Parâmetros"); }
    await ParametrosAtivos.findOneAndDelete(query).select("-_id -IdUser");
    res.status(200).send(`Parâmetros APAGADOS`);
}));

module.exports = router;