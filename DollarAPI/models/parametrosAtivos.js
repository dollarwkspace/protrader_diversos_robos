const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const parametrosAtivosSchema = new mongoose.Schema({
    IdUser: {type: String, required: false },
    nome: {type: String, required: true, default: "PADRÃO", uppercase:true },
    papel: {type: String, required: true, default:"WDO", uppercase:true },
    periodoMediaMovelSinal: {type: Number, required:false, default:4},
    periodoMediaMovelSuporte: {type: Number, required:false, default:20},
    periodoMediaMovelBloqueio: {type: Number, required:false, default:25},
    mediaContinua: {type: Boolean, required:false, default:false},
    deltaSuporte: {type: Number, required:false, default:2},
    multiplicadorATR: {type: Number, required:false, default:1.5},
    maxPosAbertas: {type: Number, required:false, default:50},
    Contratos: {type: Number, required:false, default:1},
    ticksDeReentrada: {type: String, required:false, default:"10,10"},
    valoresDeReentrada: {type: String, required:false, default:"1,2"},
    distanciaDaMedia: {type: Number, required:false, default: 10},
    TP: {type: Number, required:false, default:2},
    multiplicador: {type: Number, required:false, default:10},
    resilienciaDeSaida: {type: Number, required:false, default:2},
    lossMaximo: {type: Number, required:false, default:100},
    marketRange: {type: Number, required:false, default:10},
    custoContratoPorPonto: {type: Number, required:false, default:50},
    tamanhoPonto: {type: Number, required:false, default:1},
    lateralidade: {type: Number, required:false, default:0},
    TPavalanche: {type: Number, required:false, default:2},
    avalanche : {type: Number, required:false, default:100},
    contratosAvalanche : {type: Number, required:false, default:10},
    minimumLot : {type: Number, required:false, default:1},
    reEntradaAvalanche : {type: Number, required:false, default:5},
    lucroDeSaidaAvalanche : {type: Number, required:false, default:250},
    ticksAvalanche : {type: Number, required:false, default:0},
    qtdeLevel2: {type: Number, required:false, default:30},
    mulTPdin : {type: Number, required:false, default:1},
    TPdinMin: {type: Number, required:false, default:2},
    contratosSaidaSeguranca: {type: Number, required:false, default:25},
    ticksSaidaSeguranca : {type: Number, required:false, default:5}    
}, {
    versionKey: false
});

const ParametrosAtivos = mongoose.model('ParametrosAtivos', parametrosAtivosSchema);

function validateParametros(parametros) {
    const schema = {               
        nome: Joi.string().required(),
        papel: Joi.string().required(),         
        periodoMediaMovelSinal: Joi.number(),
        periodoMediaMovelSuporte: Joi.number(),
        periodoMediaMovelBloqueio: Joi.number(),
        mediaContinua: Joi.boolean(),
        deltaSuporte: Joi.number(),
        multiplicadorATR: Joi.number(),
        maxPosAbertas: Joi.number(),
        Contratos: Joi.number(),
        ticksDeReentrada: Joi.string(),
        valoresDeReentrada: Joi.string(),
        distanciaDaMedia: Joi.number(),
        TP: Joi.number(),
        multiplicador: Joi.number(),
        resilienciaDeSaida: Joi.number(),
        lossMaximo: Joi.number(),
        marketRange: Joi.number(),
        custoContratoPorPonto: Joi.number(),
        tamanhoPonto: Joi.number(),
        lateralidade: Joi.number(),
        TPavalanche: Joi.number(),
        avalanche : Joi.number(),
        contratosAvalanche : Joi.number(),
        minimumLot : Joi.number(),
        reEntradaAvalanche : Joi.number(),
        lucroDeSaidaAvalanche : Joi.number(),
        ticksAvalanche : Joi.number(),
        qtdeLevel2: Joi.number(),
        mulTPdin : Joi.number(),
        TPdinMin: Joi.number(),
        contratosSaidaSeguranca: Joi.number(),
        ticksSaidaSeguranca : Joi.number()        
    };
    return Joi.validate(parametros, schema);
}

exports.ParametrosAtivos = ParametrosAtivos;
exports.validate = validateParametros;