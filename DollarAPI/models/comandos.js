const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const comandosSchema = new mongoose.Schema({
    IdUser: {type: String,required: true},
    FecharPosicoes: {type: Boolean,default: false},
    LimparOrdens: {type: Boolean,default: false},
    BloquearVendas: {type: Boolean,default: false},
    BloquearCompras: {type: Boolean, default: false },
    BloquearNovasOrdens: {type: Boolean, default: false },
    PararRobo: {type: Boolean,default: false},
    TimelineCompleta: {type: Boolean, default: false },
    EsconderParametros: {type: Boolean, default: false },
    EsconderLog: {type: Boolean,default: false},
    EsconderTimeline: {type: Boolean, default: false},
    EsconderComandos: {type: Boolean, default: false}
}, {versionKey: false});

const Comandos = mongoose.model('Comandos', comandosSchema);

function validateComandos(comandos) {
    const schema = {        
        FecharPosicoes: Joi.bool().required(),
        LimparOrdens: Joi.bool().required(),
        BloquearVendas: Joi.bool().required(),
        BloquearCompras: Joi.bool().required(),
        BloquearNovasOrdens: Joi.bool().required(),
        PararRobo: Joi.bool().required(),
        TimelineCompleta: Joi.bool().required(),
        EsconderParametros: Joi.bool().required(),
        EsconderLog: Joi.bool().required(),
        EsconderTimeline: Joi.bool().required(),
        EsconderComandos: Joi.bool().required()
    };
    return Joi.validate(comandos, schema);
}

exports.Comandos = Comandos;
exports.validate = validateComandos;