const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({        
    Nome: {type: String, required: false, lowercase:true },
    Sobrenome: {type: String, required: false, lowercase:true },
    Login: {type: String, required: true },
    Senha: {type: String, required: true, minlength: 5, maxlength: 1024 }    
},  { versionKey: false });

userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({
        _id: this._id
    }, config.get('jwtPrivateKey'));
    return token;
};

const User = mongoose.model('User', userSchema);

function validateUser(user) {
    const schema = {         
        Nome: Joi.any(),
        Sobrenome: Joi.any(),    
        Login: Joi.string().min(3).required(),
        Senha: Joi.string().min(5).max(1024).required()
    };
    return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;