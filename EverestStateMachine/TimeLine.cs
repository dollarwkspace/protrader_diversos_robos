﻿using EverestSM;
using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using EverestSM.Web;
using System.IO;

namespace EverestSM
{
    [DataContract]
    public class TimeLine
    {
        public ProjetoEverestSMOnQuote robo;

        public TimeLine(ProjetoEverestSMOnQuote robo) => this.robo = robo;


        [DataMember]
        public PosicaoAtiva posicaoAtivaMedia = new PosicaoAtiva(0, 0, Operation.Buy, DateTime.Now);
        [DataMember]
        private List<PosicaoAtiva> bufferDeOrdens = new List<PosicaoAtiva>();
        [DataMember]
        private List<TimeLinePair> timeline = new List<TimeLinePair>();

        public TimeLinePair GetLastTimeLinePair() => timeline?.Last<TimeLinePair>();
        public List<TimeLinePair> GetAllItems() => (timeline.Count > 0) ? timeline : null;
        public List<TimeLinePair> GetActiveItems() => GetAllItems()?.FindAll(x => x.Entrada != null && x.Saida == null);
        public TimeLinePair GetLastActiveTimeLinePair() => GetActiveItems()?.Last<TimeLinePair>();
        public PosicaoAtiva GetLastActiveItem() => GetLastActiveTimeLinePair()?.Entrada;
        public void ClearTimeLine() { timeline.Clear(); }
        public void ClearBuffer() { bufferDeOrdens.Clear(); }
        public void ClearAll() { ClearBuffer(); ClearTimeLine(); DeleteTimeLineFile(); }
        public void FlushBuffer()
        {
            bufferDeOrdens?.ForEach(x => AddToTimeline(x));
            ClearBuffer();
            //string json = JsonHelper.Serializar<TimeLine>(this);
            //LogWriter.LogWrite(json, $"c:\\robo\\{robo.papel}_timeline.json");
        }


        private void DeleteTimeLineFile()
        {
            if (File.Exists("c:\\robo\\timeline.json"))
            {
                File.Delete("c:\\robo\\timeline.json");
            }
        }

        public void AddToTimeline(PosicaoAtiva pos)
        {
            if (GetLastActiveItem() == null || GetLastActiveItem()?._side == pos._side)
            {
                timeline.Add(new TimeLinePair(pos));
                if (timeline.Count == 1)
                {
                    if (timeline[0].Entrada._openPrice != robo.positions[0].OpenPrice) CorrecaoPorErroCorretora();
                }
                return;
            }
            GetLastActiveTimeLinePair().Saida = pos;
        }

        public void AddToBuffer(PosicaoAtiva pos)
        {
            if (GetLastActiveItem() == null || GetLastActiveItem()?._side == pos._side) { bufferDeOrdens.Add(pos); return; }
            bufferDeOrdens.Add(new PosicaoAtiva(pos._openPrice, -pos._amount, pos._side, pos._openTime, pos._comments));
        }

        public void UpdatePosicaoMedia(ProjetoEverestSMOnQuote robo)
        {
            try
            {
                if (timeline.Count > 0)
                {
                    Operation tipoBase = timeline.First().Entrada._side;
                    timeline.ForEach(posicao =>
                    {
                        posicao.Entrada._pontos = CalculaPontos(posicao.Entrada, robo);
                        posicao.Entrada._lucro = posicao.Entrada?._pontos * robo.custoContratoPorPonto * Math.Abs(posicao.Entrada?._amount ?? 0) ?? 0;
                        if (posicao.Saida == null) return;
                        posicao.Saida._pontos = CalculaPontos(posicao.Saida, robo);
                        posicao.Saida._lucro = posicao.Saida?._pontos * robo.custoContratoPorPonto * Math.Abs(posicao.Entrada?._amount ?? 0) ?? 0;
                    });

                    double _amountTotal = timeline.Sum(x => (x.Entrada._amount) + (x?.Saida?._amount ?? 0));
                    double _precoMedio = Math.Round(timeline.Sum(x => (x.Entrada._openPrice) * (x.Entrada._amount) + (x.Saida?._openPrice ?? 0) * (x?.Saida?._amount ?? 0)) / _amountTotal, 2);
                    double _lucroTotal = timeline.Sum(x => (x?.Entrada?._lucro ?? 0) + (x?.Saida?._lucro ?? 0));

                    posicaoAtivaMedia._amount = _amountTotal;
                    posicaoAtivaMedia._lucro = _lucroTotal;
                    posicaoAtivaMedia._openPrice = _precoMedio;
                    posicaoAtivaMedia._side = tipoBase;

                }
                else
                {
                    posicaoAtivaMedia._amount = 0;
                    posicaoAtivaMedia._lucro = 0;
                    posicaoAtivaMedia._openPrice = 0;
                    posicaoAtivaMedia._comments = "";
                }

            }
            catch (Exception ex)
            {
                robo.Alert("Erro UpdatePosiçãoMédia: " + ex.Message);
            }
        }

        private double CalculaPontos(PosicaoAtiva pos, ProjetoEverestSMOnQuote robo)
        {
            if (pos == null) return 0;
            return (pos?._side == Operation.Buy) ? Math.Round(((robo.cotacaoAtual - pos._openPrice) / robo.tamanhoPonto), 2) : -Math.Round(((robo.cotacaoAtual - pos?._openPrice ?? 0) / robo.tamanhoPonto), 2);
        }

        public void CorrigeTimeline()
        {
            robo.Print("Existe posição pré-existente." + "\t Total Posição: " + robo.positions[0]?.Amount + "\t Total Ordens: " + robo.orders.Length);
            AddToTimeline(new PosicaoAtiva(robo.positions[0].OpenPrice, robo.positions[0].Amount, robo.positions[0].Side, DateTime.Now, "Inicio Timeline"));
            UpdatePosicaoMedia(robo);
            robo.Print("Posição ativa inicial setada");
        }

        public void CorrecaoPorErroCorretora()
        {
            robo.Print("Correcao Preco Medio por Erro Corretora");
            ClearTimeLine();
            ClearBuffer();
            AddToTimeline(new PosicaoAtiva(robo.positions[0].OpenPrice, robo.positions[0].Amount, robo.positions[0].Side, DateTime.Now, "Inicio Timeline"));
            UpdatePosicaoMedia(robo);
            robo.Print("Posição ativa corrigida");
        }


    }

    [DataContract]
    public class TimeLinePair
    {
        [DataMember]
        public PosicaoAtiva Entrada;
        [DataMember]
        public PosicaoAtiva Saida;

        public TimeLinePair(PosicaoAtiva Entrada = null, PosicaoAtiva Saida = null)
        {
            this.Entrada = Entrada; this.Saida = Saida;
        }
    }

}

