﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Diagnostics;
using EverestSM.Web;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

namespace EverestSM
{

    public class ProjetoEverestSMOnQuote : NETStrategy
    {
        public Instrument instrument;
        public Account conta;
        public Indicator MM;
        public Indicator sniper;
        public Indicator ATR;
        public Connection myConnection = Connection.CurrentConnection; //create connection object.    
        public Position[] positions;
        public Position ultimaPosicaoAdd = null;
        public Order[] orders;
        public Font font = new Font("Tahoma", 10);
        public Font fontAlarm = new Font("Tahoma", 10, FontStyle.Bold);
        public Brush brush = new SolidBrush(Color.White);
        public Brush brushAlarm = new SolidBrush(Color.Red);
        public Brush brushAlert = new SolidBrush(Color.Yellow);
        public Queue<Level2> level2 = new Queue<Level2>();
        public Funcoes funcoes = null;
        public Comando comando = null;
        public Eventos eventos = null;
        public Analise analise = null;
        private EstadoInicio estadoInicio = null;
        private EstadoReEntrada estadoReEntrada = null;
        public EstadoAvalanche estadoAvalanche = null;
        public TimeLine timeline = null;
        public Quote lastQuote;
        public TimeSpan horario;
        public Stopwatch watch = new Stopwatch();
        public List<string> logDin = new List<string>();
        public bool onQuoteWatcher = false;
        public State estado = State.analise;
        public State nextState = State.lockdown;
        public APIfunc api = new APIfunc();
        public APIRobo apiRobo;
        public List<string> parametros = new List<string>();
        public List<string> comandosTela = new List<string>();
        public Colecao _colecao = new Colecao();
        public Parametro parametrosAtivos = new Parametro();
        public ComandoNET comandoNet = null;
        public Timer syncTimer;
        //public Excellib excel = null;

        public string log;
        public double calor = 0;
        public double curta = 0;
        public double curtaAnterior = 0;
        public double histoResult = 0;
        public double cotacaoAtual = 0;
        public double suporte = 0;
        public double suporteAnterior = 0;
        public double mBloqueio = 0;
        public double mBloqueioAnterior = 0;
        public double ATRsuperior = 0;
        public double ATRinferior = 0;
        public int hora = 0;
        public int contadorDeReEntradas = 0;
        public double[] ReEntradas;
        public double[] ValoresDeReEntradas;
        public double tickSize = 0;
        public string horaDoCalorMaximo = "";
        public string papel;
        public int numeroMagico = 0;
        public double maximoDeContratosAbertos = 0;
        public int contadorOrdemTravada = 0;
        public long ms = 0;
        public long maxMs = 0;
        public long minMs = 10000;
        public double TPatr = 0;
        public string saidaDeSeguranca = "";
        public string token = "";
        public string erroLogin = "";
        public bool parametrosSync = false;
        public bool SyncJsonOnStart = false;
        public bool autorizaLogPrint = false;
        public bool saidaDeSegurancaEmAnalise = false;
        public bool horarioTerminou = false;
        public bool modoAvalanche = false;
        public bool roboTerminouInicializacao = false;
        public double periodoSinalAntigo = 0;
        public double periodoSuporteAntigo = 0;
        public double periodoBloqueioAntigo = 0;
        public double proximaReEntrada = 0;        

        public ProjetoEverestSMOnQuote()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto Everest SM";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "29.01.2019";
            base.ExpirationDate = 0;
            base.Version = "1.1.0";
            base.Password = "";
            base.ProjectName = "Everest SM";
            #endregion

            timeline = new TimeLine(this);
            eventos = new Eventos(this);
            comando = new Comando(this);
            analise = new Analise(this);
            funcoes = new Funcoes(this);
            estadoInicio = new EstadoInicio(this);
            estadoReEntrada = new EstadoReEntrada(this);
            estadoAvalanche = new EstadoAvalanche(this);
            comandoNet = new ComandoNET();
            apiRobo = new APIRobo(this);
            //excel = new Excellib(this);
        }

        [InputParameter("Nome Conj. Parâmetros", 0)]
        public string nomeParam = "PLUS";
        [InputParameter("Início(hora)", 1)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);
        [InputParameter("Término(hora)", 2)]
        public TimeSpan HoraDeTermino = new TimeSpan(17, 50, 00);
        [InputParameter("Média: Sinal", 3)]
        public int periodoMediaMovelSinal = 2;
        [InputParameter("Média: Suporte", 4)]
        public int periodoMediaMovelSuporte = 20;
        [InputParameter("Média: Bloqueio", 5)]
        public int periodoMediaMovelBloqueio = 75;
        [InputParameter("Média contínua ou por fechamento de candle?", 6)]
        public bool mediaContinua = true;
        [InputParameter("Delta Média Suporte", 7, 0, 100, 3, 0.001)]
        public double deltaSuporte = 20;
        [InputParameter("Multiplicador ATR", 8, 0, 100, 3, 0.001)]
        public double multiplicadorATR = 1.5;
        [InputParameter("Permitir até quantas posições abertas? ", 9)]
        public double maxPosAbertas = 32;
        [InputParameter("Contratos", 10)]
        public int Contratos = 1;
        [InputParameter("Array Ticks de Reentradas", 11)]
        public string ticksDeReentrada = "10,25,40,60,80";
        [InputParameter("Array Valores de Reentradas", 12)]
        public string valoresDeReentrada = "1,2,4,8,16";
        [InputParameter("Distância da Média (tend)", 13, 0, 10, 2, 0.01)]
        public double distanciaDaMedia = 20;
        //[InputParameter("Distância da Média (contra)", 14)]
        //public double distanciaDaMediaContra = 7;
        [InputParameter("Take Profit (Pontos)", 14, 0, 20, 2, 0.01)]
        public double TP = 2;
        [InputParameter("Multiplicador da Dif.Das Médias", 15, 0, 1000, 3, 0.001)]
        public double multiplicador = 1;
        [InputParameter("Resiliência de Saída", 16, 0, 1000, 3, 0.001)]
        public double resilienciaDeSaida = 10;
        [InputParameter("Loss máximo em Ticks", 17)]
        public double lossMaximo = 100;
        [InputParameter("Tipo de Saída", 18, new Object[]
     {"Dif Médias", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;
        [InputParameter("Tipo de Robô", 19, new Object[]
     {"Tendência", TipoDeRobo.tendencia, "Contra Tendência",TipoDeRobo.contraTendencia, "Tendência e Contra", TipoDeRobo.tendenciaEcontra })]
        public TipoDeRobo tipoDeRobo = TipoDeRobo.tendenciaEcontra;
        [InputParameter("Market Range", 20)]
        public int marketRange = 10;
        [InputParameter("Custo de 1 contrato por ponto", 21)]
        public double custoContratoPorPonto = 1;
        [InputParameter("Tamanho do Ponto", 22, 0, 100, 3, 0.001)]
        public double tamanhoPonto = 5;
        [InputParameter("Lateralidade abaixo de: (suporte-sinal) ", 23, 0, 100, 3, 0.001)]
        public double lateralidade = 0;

        [InputParameter("TP Avalanche", 24, 0, 100, 3, 0.001)]
        public double TPavalanche = 2;
        [InputParameter("Avalanche após N contratos", 25)]
        public double avalanche = 100;
        [InputParameter("ReEntrada % Avalanche", 26)]
        public double contratosAvalanche = 10;
        [InputParameter("Lote Mínimo Avalanche", 27)]
        public double minimumLot = 5;
        [InputParameter("ReEntrada Avalanche em Ticks", 28, 0, 100, 3, 0.001)]
        public double reEntradaAvalanche = 7;
        [InputParameter("Lucro de saída avalanche R$", 29)]
        public double lucroDeSaidaAvalanche = 100;
        [InputParameter("Pontos para Inicio Avalanche", 30, 0, 100, 3, 0.001)]
        public double ticksAvalanche = 0;

        [InputParameter("Plotar Level2? ", 31)]
        public bool plotarLevel2 = false;
        [InputParameter("Qtde Level2 na tela  ", 32)]
        public int qtdeLevel2 = 30;

        [InputParameter("Tipo de BLOQUEIO", 33, new Object[]
{"BUY", TipoDeBloqueio.BUY, "SELL",TipoDeBloqueio.SELL, "Nenhum", TipoDeBloqueio.NULL })]
        public TipoDeBloqueio tipoDeBloqueio = TipoDeBloqueio.NULL;

        [InputParameter("Mostrar toda Timeline? ", 34)]
        public bool showAllTimeline = false;

        [InputParameter("ReEntradas DINÂMICAS ? ", 35)]
        public bool reEntradasDinamicas = true;

        [InputParameter("Tipo de TP", 36, new Object[]
{"Fixo", TipoDeTP.fixo, "Dinâmico", TipoDeTP.dinamico})]
        public TipoDeTP tipoDeTP = TipoDeTP.dinamico;

        [InputParameter("multiplicador TP Dinâmico", 37, 0, 100, 3, 0.001)]
        public double mulTPdin = 0.05;
        [InputParameter("TP Dinâmico MÍN", 38, 0, 100, 3, 0.001)]
        public double TPdinMin = 2;
        [InputParameter("Saída de Segurança", 39, new Object[]
{"Preço Médio", TipoStopSeguranca.PreçoMedio, "Média Longa", TipoStopSeguranca.MédiaLonga})]
        public TipoStopSeguranca tipoStopSeguranca = TipoStopSeguranca.MédiaLonga;

        [InputParameter("Contratos Saída Segurança", 40)]
        public double contratosSaidaSeguranca = 32;

        [InputParameter("Ticks Saída Segurança", 41)]
        public double ticksSaidaSeguranca = 5;

        [InputParameter("LOGIN", 42)]
        public string login = "breno";

        [InputParameter("SENHA", 43)]
        public string senha = "leitão";

        [InputParameter("LOG BLOCK", 44)]
        public bool blockLog = true;

        [InputParameter("LER BASE WEB ?", 45)]
        public bool baseWEB = true;

        [InputParameter("ONLINE ?", 46)]
        public bool online = true;

        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            instrument = Instruments.Current;
            Print("Nome do Instrumento ativo: " + instrument.Name);
            papel = instrument.BaseName;
            Print("Nome BASE do Instrumento ativo: " + papel);
            conta = Accounts.Current;
            tickSize = instrument.TickSize;
            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote | QuoteTypes.Level2);

            Print("Papel: " + instrument.Name);
            Print("Tamanho do Point: " + Point);
            Print("Tamanho do tick: " + instrument.TickSize);

            Positions.PositionAdded += Positions_PositionAdded;
            Positions.PositionRemoved += Positions_PositionRemoved;
            Instruments.NewLevel2 += Instruments_NewLevel2;
            Orders.OrderAdded += Orders_OrderAdded;
            Orders.OrderExecuted += Orders_OrderExecuted;
            Orders.OrderRemoved += Orders_OrderRemoved;            

            Task.Run(async () =>
            {
                try
                {
                    if(online){ await funcoes.ChecaLogin(); }
                    funcoes.RefreshPositionsOrders();
                    if (positions.Length > 0)
                    {
                        timeline.CorrigeTimeline();
                    }

                    SetSyncTimer();
                    funcoes.InfoConexao(myConnection);
                    funcoes.AccountInformation(conta);
                    funcoes.SincronizaParametros();
                    funcoes.InicializaComandos();                    
                    Print("Fim da Inicialização do Robô");
                    roboTerminouInicializacao = true;
                }
                catch (Exception exc)
                {
                    Print("Erro na inicialização do Robô... " + exc.Message);
                    Print(exc.InnerException.Message);
                    FinalizaRobo();
                }
            });            

        }

        public void FinalizaRobo()
        {
            Print("Finalizando Robô");
            TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
            foreach (var robo in robos)
            {
                robo.Stop();
                Print("Nome do robô:" + robo.Name);
            }
        }

        public override void OnQuote()
        {
            //if (tipoDeExecucao == TipoDeExecucao.OnQuote)
            //{            
            if (!roboTerminouInicializacao) { Print("Aguardando o término da inicialização..."); return; }
            if (online && !parametrosSync) { Print("Parãmetros não lidos"); return; }
            lastQuote = instrument.LastQuote;
            logDin.Clear();
            watch.Restart();
            if (onQuoteWatcher) Print("ERRO OnQuote: Não FINALIZOU ANÁLISE OnQuote por alguma exceção!");
            onQuoteWatcher = true;
            ExecutaAnalise();
            onQuoteWatcher = false;
            watch.Stop();
            ms = watch.ElapsedTicks;
            if (maxMs < ms) maxMs = ms;
            if (minMs > ms) minMs = ms;
            //}
        }

        public override void NextBar()
        {
            if (estado == State.lockdown) estado = State.analise;
        }

        public async override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade | QuoteTypes.Level2);
            Positions.PositionAdded -= Positions_PositionAdded;
            Positions.PositionRemoved -= Positions_PositionRemoved;
            Instruments.NewLevel2 -= Instruments_NewLevel2;
            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;
            ATR = null; sniper = null; MM = null;
            await apiRobo.DELETE_ParametrosAtivos(nomeParam, papel);
        }

        private void Instruments_NewLevel2(Instrument inst, Level2 quote) { eventos.Instruments_NewLevel2(inst, quote); }
        private void Orders_OrderAdded(Order obj) { eventos.Orders_OrderAdded(obj); }
        private void Orders_OrderRemoved(Order obj) { eventos.Orders_OrderRemoved(obj); }
        private void Orders_OrderExecuted(Order obj) { eventos.Orders_OrderExecuted(obj); }
        private void Positions_PositionRemoved(Position obj) { eventos.Positions_PositionRemoved(obj); }
        private void Positions_PositionAdded(Position obj) { eventos.Positions_PositionAdded(obj); }
        public override void OnPaintChart(object sender, PaintChartEventArgs args) { eventos.OnPaintChart(sender, args); }

        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************         
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************  

        public double GetDiffMedias() => mediaContinua ? curta - curtaAnterior : histoResult;
        public double GetDiffMediasSuporte() => suporte - suporteAnterior;
        public double GetDiffMediasBloqueio() => mBloqueio - mBloqueioAnterior;
        public bool ExisteInfluenciaMediaSuporte() => Math.Abs(GetDiffMediasSuporte()) >= deltaSuporte;

        private void ExecutaAnalise()
        {
            if (instrument.BaseName == papel)
            {

                if (online && token == "")
                {
                    Print("USUÁRIO ou SENHA INVÁLIDO!");
                    Print(erroLogin);
                    return;
                }

                if (tipoDeTP == TipoDeTP.dinamico) { TP = Math.Floor(mulTPdin * TPatr); TPavalanche = TP; TP = (TPdinMin > TP) ? TPdinMin : TP; }

                log = "********************   NewTrade   ********************";
                Print(log);

                if (!funcoes.RefreshPositionsOrders()) return; //atualiza as ordens e posições ativas                                
                lastQuote = instrument.LastQuote;
                if (lastQuote == null) return;

                if (!funcoes.RefreshIndicators()) return;//lê os indicadores
                if (!funcoes.CheckMarketStatus()) return; //checa se bolsa está aberta e se está fechada fecha as posições                                
                if (!funcoes.CalculaCalorMaximo()) return;
                if (curta == 0 || curtaAnterior == 0 || ATRsuperior == 0 || ATRinferior == 0 || mBloqueio == 0 || mBloqueioAnterior == 0 || suporte == 0 || suporteAnterior == 0)
                { Print("Aguardando indicadores de Sinal!"); return; }

                if (comandoNet.BloquearNovasOrdens) { Print("Robô com COMANDO de Bloqueio Total!"); return; };
                if (comandoNet.PararRobo) FinalizaRobo();
                if (comandoNet.LimparOrdens) funcoes.LimparTodasAsOrdens();
                if (comandoNet.FecharPosicoes) funcoes.FechamentoTotal();

                funcoes.SaidaPorLossMaximo();

                if (estado != State.analise && estado != State.lockdown)
                {
                    funcoes.SaidaDeSeguranca();
                    funcoes.DetectaAvalanche();
                }

                try
                {
                    switch (estado)
                    {
                        case State.inicio:
                            {
                                if (horarioTerminou) { Print("Estado Início: Término das Operações do Dia."); return; }
                                estadoInicio.Run(); break;
                            }
                        case State.reentrada: { estadoReEntrada.Run(); break; }
                        case State.avalanche: { estadoAvalanche.Run(); break; }
                        case State.analise: { analise.Juiz(); break; }
                        case State.termino: { Print("Horário de Término atingido... "); break; }
                        case State.bolsaFechada: { Print(horario + " BOLSA FECHADA "); break; }
                        case State.lockdown:
                            {
                                Print("Estado Lockdown: Robô Bloqueado aguardando Eventos da corretora");
                                if (nextState != State.lockdown) { estado = nextState; funcoes.RefreshPositionsOrders(); }
                                nextState = State.lockdown;
                                break;
                            }
                    }

                }
                catch (Exception exc)
                {
                    Print("NewTrade:  Erro setor switch . Mensagem -> " + exc.Message);
                    Print("NewTrade: Erro:  InnerMessage -> " + exc.InnerException.Message);
                    return;
                }
            }
        }

        public void Print(string message)
        {
            if (!blockLog) { base.Print(message); return; }
            logDin.Add(message);
        }

        public void SetSyncTimer()
        {
            // cria um timer com 2s de intervalo
            syncTimer = new Timer(2000);
            syncTimer.Elapsed += SyncTimer_Elapsed;
            syncTimer.AutoReset = true;
            syncTimer.Enabled = true;
        }

        private void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (online)
                {
                    Task.Run(async () =>
                    {
                        apiRobo.GET_Comandos();
                        if (await apiRobo.GET_ParametrosAtivos(nomeParam, papel))
                        {
                            funcoes.Sync(parametrosAtivos);
                            funcoes.PopulaParametros();
                        }
                    });
                }
            }
            catch (Exception ext)
            {
                Alert(ext.InnerException.Message);
            }

        }


    }

}
