﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestSM
{
    public class Eventos
    {
        private ProjetoEverestSMOnQuote robo = null;
        private int i, j, k, l = 0;
        private string log;
        private Operation tipoBase = Operation.Buy;
        private Position pos = null;
        float proximaReEntrada = 0;


        public Eventos(ProjetoEverestSMOnQuote _robo)
        {
            robo = _robo; log = robo.log;
        }


        public void Orders_OrderAdded(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    if (obj.Type == OrdersType.Limit) robo.nextState = State.analise;
                    log = $"{obj.Time.ToLocalTime().TimeOfDay} EVENTO OrderAdded: ID: {obj.Id}  tipo:{obj.Side}  {obj.Comment}  Qtde:{obj.Amount}  preço:{obj.Price}  hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status:{obj.Status}  Estado:{robo.estado}";
                    robo.Print(log);
                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro OrderAdded : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Orders_OrderRemoved(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.Time.ToLocalTime().TimeOfDay} EVENTO OrderRemoved: ID: {obj.Id}  tipo:{obj.Side}  {obj.Comment}  Qtde:{obj.Amount}  preço:{obj.Price}  hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status:{obj.Status}  Estado:{robo.estado}";
                    robo.Print(log);
                    robo.nextState = State.analise;
                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro OrderRemoved : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }


        public void Orders_OrderExecuted(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.Time.ToLocalTime().TimeOfDay}  EVENTO OrderExecuted: ID: {obj.Id}  tipo: {obj.Side}  {obj.Comment}   Qtde:{obj.Amount}  preço: R${obj.Price}   hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status: {obj.Status}  Estado: {robo.estado}";
                    robo.Print(log);

                    //NOVA TIMELINE
                    if (obj.Status == OrderStatus.Filled || obj.Status == OrderStatus.PartiallyFilled)
                    {

                        robo.timeline.AddToBuffer(new PosicaoAtiva(obj.Price, obj.Amount, obj.Side, obj.Time, obj.Comment));
                    }

                }
            }
            catch (Exception exc)
            {
                robo.Print($"Erro OrderExecuted : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Positions_PositionRemoved(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.CloseTime.ToLocalTime().TimeOfDay} EVENTO PositionRemoved: ID: {obj.Id} tipo: {obj.Side}  qtde: {obj.Amount} OrderId: {obj.OpenOrderId}   Papel:{obj.Instrument.BaseName}  Estado: {robo.estado}";
                    robo.Print(log);
                    robo.modoAvalanche = false;
                    robo.timeline.ClearAll();
                    robo.nextState = State.analise;
                    robo.ultimaPosicaoAdd = null;

                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro PositionRemoved : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Positions_PositionAdded(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = "";
                    if (robo.ultimaPosicaoAdd == null)
                    {
                        log = $"{obj.OpenTime.ToLocalTime().TimeOfDay}  EVENTO PositionAdded: ID: {obj.Id}  tipo: {obj.Side}   qtde: {obj.Amount}  OrderId: {obj.OpenOrderId}   Papel: {obj.Instrument.BaseName}   Estado:  {robo.estado}";
                        if (!robo.modoAvalanche) { robo.comando.CriaParDeSaida(obj, "saída limite"); robo.estado = State.lockdown; }
                    }
                    else
                    {
                        log = $"{obj.OpenTime.ToLocalTime().TimeOfDay}  EVENTO PositionUPDATE: ID: {obj.Id}  tipo: {obj.Side}   qtde: {obj.Amount}  OrderId: {obj.OpenOrderId}   Papel: {obj.Instrument.BaseName}   Estado:  {robo.estado}";
                        if (robo.orders.Length == 0 && robo.positions.Length > 0 && !robo.modoAvalanche) { robo.comando.CriaParDeSaida(robo.positions[0], "saída limite"); robo.estado = State.lockdown; }
                    }

                    robo.ultimaPosicaoAdd = obj;
                    robo.timeline.FlushBuffer();
                    robo.nextState = State.analise;
                }
            }
            catch (Exception exc)
            {
                robo.Print($"Erro PositionAdded : {exc.Message}");
                robo.nextState = State.analise;
                robo.estado = State.analise;
            }
        }

        public void Instruments_NewLevel2(Instrument inst, Level2 quote)
        {
            if (inst.BaseName == robo.papel)
            {
                if (robo.level2.Count < robo.qtdeLevel2)
                {
                    robo.level2.Enqueue(quote);
                }
                else
                {
                    robo.level2.Dequeue();
                    robo.level2.Enqueue(quote);
                }
            }
        }

        public void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                if (args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString($"Calor máximo: {robo.calor.ToString()}  Máximo de Contratos abertos: {robo.maximoDeContratosAbertos.ToString()}  Estado atual: {robo.estado.ToString()}  contadorDeReEntradas: {robo.contadorDeReEntradas}", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 5);
                    if(robo.estado==State.reentrada) args.Graphics.DrawString($"Próx.ReEn: {robo.proximaReEntrada}", robo.font, robo.brushAlert, args.Rectangle.X + 5, args.Rectangle.Height - 110);
                    args.Graphics.DrawString($"ReEntr.Din: {robo.reEntradasDinamicas}", robo.font, robo.brushAlert, args.Rectangle.X + 5, args.Rectangle.Height - 95);
                    args.Graphics.DrawString($"TamPonto: {robo.tamanhoPonto}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 80);
                    args.Graphics.DrawString($"Cotação: {robo.cotacaoAtual}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 65);
                    args.Graphics.DrawString($"TPDin:{Math.Floor(robo.mulTPdin * robo.TPatr)}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 50);
                    args.Graphics.DrawString($"Max:{robo.maxMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 35);
                    args.Graphics.DrawString($"Min:{robo.minMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 20);
                    args.Graphics.DrawString($"Cur:{robo.ms} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 5);
                }

                if (robo.positions.Length > 0 && args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString("Posição", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Qtde", robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Médio", robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Atual", robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Tipo", robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Lucro/Prejuízo", robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 20);
                    //args.Graphics.DrawString("Comentário", robo.font, robo.brush, args.Rectangle.X + 590, args.Rectangle.Y + 20);

                    args.Graphics.DrawString("Protrader ->", robo.font, robo.brush, args.Rectangle.X + 10, args.Rectangle.Y + 40);
                    args.Graphics.DrawString("Robô ->", robo.font, robo.brush, args.Rectangle.X + 10, args.Rectangle.Y + 60);
                    args.Graphics.DrawLine(new Pen(Color.White), new PointF(args.Rectangle.X + 110, args.Rectangle.Y + 80), new PointF(args.Rectangle.X + 610, args.Rectangle.Y + 80));

                    args.Graphics.DrawString(robo.positions[0].Id, robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(robo.positions[0].Amount.ToString(), robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].OpenPrice), robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].CurrentPrice), robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(robo.positions[0].Side.ToString(), robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].GetProfitGross().ToString()), robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 40);
                    //args.Graphics.DrawString(robo.positions[0].Comment.ToString(), robo.font, robo.brush, args.Rectangle.X + 590, args.Rectangle.Y + 40);

                    if (robo.saidaDeSegurancaEmAnalise) args.Graphics.DrawString(robo.saidaDeSeguranca, robo.fontAlarm, robo.brushAlert, args.Rectangle.X + 110, args.Rectangle.Height - 57);
                    if (robo.modoAvalanche) args.Graphics.DrawString("Modo Avalanche LIGADO", robo.fontAlarm, robo.brushAlarm, args.Rectangle.X + 110, args.Rectangle.Height - 40);

                    args.Graphics.DrawString("Posicao Ref:", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 20);                    

                    if (robo.timeline.GetLastActiveItem() != null)
                    {
                        args.Graphics.DrawString($"Qtde: {robo.timeline?.GetLastActiveItem()?._amount.ToString()}", robo.font, robo.brush, args.Rectangle.X + 190, args.Rectangle.Height - 20);
                        args.Graphics.DrawString($"OpenPrice: {String.Format("{0:C}", robo.timeline?.GetLastActiveItem()?._openPrice)}", robo.font, robo.brush, args.Rectangle.X + 260, args.Rectangle.Height - 20);
                        args.Graphics.DrawString($"Tipo: {robo.timeline?.GetLastActiveItem()?._side.ToString()}", robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Height - 20);
                    }
                    else
                    {
                        args.Graphics.DrawString("NULA", robo.font, robo.brush, args.Rectangle.X + 250, args.Rectangle.Height - 20);
                    }

                    if (robo.timeline.GetAllItems()?.Count > 0 && robo.timeline?.posicaoAtivaMedia != null)
                    {
                        string precoMedioString = String.Format("{0:C}", robo.timeline?.posicaoAtivaMedia?._openPrice);
                        double pontos = (robo.timeline.posicaoAtivaMedia._side == Operation.Buy) ? Math.Round(((robo.cotacaoAtual - robo.timeline.posicaoAtivaMedia._openPrice) / robo.tamanhoPonto), 2) : -Math.Round(((robo.cotacaoAtual - robo.timeline.posicaoAtivaMedia._openPrice) / robo.tamanhoPonto), 2);

                        args.Graphics.DrawString(robo.timeline?.GetActiveItems()?.Count.ToString(), robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 60);
                        args.Graphics.DrawString(robo.timeline?.posicaoAtivaMedia?._amount.ToString(), robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 60);
                        args.Graphics.DrawString(precoMedioString, robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 60);
                        args.Graphics.DrawString(String.Format("{0:C}", pontos.ToString()), robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 60);
                        args.Graphics.DrawString(robo.timeline?.posicaoAtivaMedia?._side.ToString(), robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 60);
                        args.Graphics.DrawString(String.Format("{0:C}", robo.timeline?.posicaoAtivaMedia?._lucro.ToString()), robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 60);

                        DrawPositions(args.Graphics, args.Rectangle);
                    }
                }

                if (args.Rectangle.Height > 350 && robo.level2.Count > 0 && robo.plotarLevel2)
                {
                    args.Graphics.DrawString("Origem", robo.font, robo.brush, args.Rectangle.X + 720, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço", robo.font, robo.brush, args.Rectangle.X + 820, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Tamanho", robo.font, robo.brush, args.Rectangle.X + 920, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Lado", robo.font, robo.brush, args.Rectangle.X + 1000, args.Rectangle.Y + 20);
                    args.Graphics.DrawLine(new Pen(Color.White, 1), new PointF(args.Rectangle.X + 710, args.Rectangle.Y), new PointF(args.Rectangle.X + 710, args.Rectangle.Y + args.Rectangle.Y + args.Rectangle.Height));
                    DrawLevel2(args.Graphics, args.Rectangle);

                }
                else
                {
                    if (args.Rectangle.Height > 350 && robo.autorizaLogPrint) DrawLog(args.Graphics, args.Rectangle);
                    DrawParams(args.Graphics, args.Rectangle);
                    DrawComandos(args.Graphics, args.Rectangle);
                }



            }
            catch (Exception exc)
            {
                //robo.Print($"OnPaintChart Erro {exc.Message}");                
            }
        }


        private void DrawParams(Graphics gr, Rectangle rect)
        {
            l = 15;

            if (robo.parametros.Count > 0 && !robo.comandoNet.EsconderParametros && robo.comandoNet.EsconderComandos)
                robo.parametros.ForEach(p =>
                {
                    gr.DrawString(p, robo.font, robo.brush, rect.Width - 250, rect.Y + l);
                    l += 15;
                });
        }

        private void DrawComandos(Graphics gr, Rectangle rect)
        {
            l = 15;

            if (robo.comandosTela.Count > 0 && !robo.comandoNet.EsconderComandos && robo.comandoNet.EsconderParametros)
                robo.comandosTela.ForEach(p =>
                {
                    gr.DrawString(p, robo.font, robo.brush, rect.Width - 250, rect.Y + l);
                    l += 15;
                });
        }

        private void DrawPositions(Graphics gr, Rectangle rect)
        {
            j = 100;
            try
            {
                if (!robo.comandoNet.EsconderTimeline)
                {
                    if (robo.comandoNet.TimelineCompleta)
                    {
                        robo.timeline.GetAllItems()?.ForEach(posicao =>
                        {
                            string horaEntrada = posicao.Entrada._openTime.ToLocalTime().ToShortTimeString();
                            string qtdeEntrada = posicao.Entrada._amount.ToString();
                            string precoEntrada = String.Format("{0:C}", posicao.Entrada._openPrice);
                            string pontoEntrada = posicao.Entrada._pontos.ToString();
                            string ladoEntrada = posicao.Entrada._side.ToString();
                            string commentEntrada = posicao.Entrada._comments;
                            double lucroEntrada = posicao.Entrada._lucro;
                            string horaSaida = (posicao.Saida?._openTime.ToLocalTime().ToShortTimeString()) ?? "";
                            string qtdeSaida = posicao.Saida?._amount.ToString() ?? "0";
                            string precoSaida = String.Format("{0:C}", posicao.Saida?._openPrice) ?? "0";
                            string pontoSaida = posicao.Saida?._pontos.ToString() ?? "0";
                            string ladoSaida = posicao.Saida?._side.ToString() ?? "---";
                            double lucroSaida = posicao.Saida?._lucro ?? 0;

                            gr.DrawString(
                            $"( In: {horaEntrada} {qtdeEntrada} {ladoEntrada} R${precoEntrada} / Out: {horaSaida} {qtdeSaida} {ladoSaida} R${precoSaida} ) -> Lucro: {lucroEntrada + lucroSaida} "
                                , robo.font, robo.brush, rect.X + 110, rect.Y + j);
                            j += 15;
                        });
                    }
                    else
                    {

                        robo.timeline.GetActiveItems()?.ForEach(posicao =>
                        {
                            string horaEntrada = posicao.Entrada._openTime.ToLocalTime().ToShortTimeString();
                            string qtdeEntrada = posicao.Entrada._amount.ToString();
                            string precoEntrada = String.Format("{0:C}", posicao.Entrada._openPrice);
                            string pontoEntrada = posicao.Entrada._pontos.ToString();
                            string ladoEntrada = posicao.Entrada._side.ToString();
                            string commentEntrada = posicao.Entrada._comments;
                            double lucroEntrada = posicao.Entrada._lucro;
                            string horaSaida = (posicao.Saida?._openTime.ToLocalTime().ToShortTimeString()) ?? "";
                            string qtdeSaida = posicao.Saida?._amount.ToString() ?? "0";
                            string precoSaida = String.Format("{0:C}", posicao.Saida?._openPrice) ?? "0";
                            string pontoSaida = posicao.Saida?._pontos.ToString() ?? "0";
                            string ladoSaida = posicao.Saida?._side.ToString() ?? "---";
                            double lucroSaida = posicao.Saida?._lucro ?? 0;

                            gr.DrawString(
                            $"( In: {horaEntrada} {qtdeEntrada} {ladoEntrada} R${precoEntrada} / Out: {horaSaida} {qtdeSaida} {ladoSaida} R${precoSaida} ) -> Lucro: {lucroEntrada + lucroSaida} "
                                , robo.font, robo.brush, rect.X + 110, rect.Y + j);
                            j += 15;
                        });
                    }
                }
            }
            catch (Exception exc)
            {
                robo.Print($"DrawPositions: Erro {exc.Message}");
            }
        }

        private void DrawLog(Graphics gr, Rectangle rect)
        {
            i = 0;
            try
            {
                if (robo.logDin.Count > 0 && !robo.comandoNet.EsconderLog)
                {
                    robo.logDin.ForEach(logItem =>
                    {
                        gr.DrawString(logItem, robo.font, robo.brush, rect.X + 10, j + 50 + i);
                        i += 15;
                    });
                }
                robo.autorizaLogPrint = true;
            }
            catch (Exception exc)
            {
                robo.Print($"DrawLog: Erro {exc.Message}");
            }
        }

        
        private void DrawLevel2(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));

            k = 40;
            try
            {

                robo.level2.ToList().ForEach(_level2 =>
                {
                    gr.DrawString(_level2.Source.ToString(), robo.font, robo.brush, rect.X + 720, rect.Y + k);
                    gr.DrawString(String.Format("{0:C}", _level2.Price), robo.font, robo.brush, rect.X + 820, rect.Y + k);
                    gr.DrawString(_level2.Size.ToString(), robo.font, robo.brush, rect.X + 920, rect.Y + k);
                    gr.DrawString(_level2.Side.ToString(), robo.font, robo.brush, rect.X + 1000, rect.Y + k);

                    k += 15;
                });

            }
            catch (Exception exc)
            {
                robo.Print($"DrawLevel2: Erro {exc.Message}");

            }
        }





    }
}
