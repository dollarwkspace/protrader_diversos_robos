﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestSM
{
    public class Analise
    {
        private ProjetoEverestSMOnQuote robo = null;

        public Analise(ProjetoEverestSMOnQuote _robo)
        {
            robo = _robo;
        }

        public void Juiz()
        {
            string log = "";
            log = robo.horario + "********************   Estado Juiz ***********************";
            robo.Print(log);            
            if (robo.positions == null) robo.positions = new Position[0];
            if (robo.orders == null) robo.orders = new Order[0];

            if (robo.orders.Length > 1)
            {
                log = "Existe mais ordens do que o normal! Limpando a primeira..";
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                robo.estado = State.analise;
                return;
            }

            robo.funcoes.PrintaPosicoesOrdensAtivas();

            //Caso 1: Não existe posição e Não existe ordem
            if (robo.positions.Length == 0 && robo.orders.Length == 0)
            {
                //como chegou um caso assim para o juiz analisar? É impossível pois a migração de estados na entrada e na reentrada apenas ocorrem se a corretora aceitar as ordens
                //mesmo assim, é melhor prever isso
                log = "Estado Juiz: Não existe Posição e Não existe Ordem para ser processada. estado -> inicio";
                robo.Print(log);
                robo.estado = State.inicio;                
                return;
            }

            //Caso 2: Não existe posição, e ordem tem operação pendente
            if (robo.positions.Length == 0 && robo.orders?.ToList().FindAll(ord => ord.Status == OrderStatus.PendingNew || ord.Status == OrderStatus.PendingReplace || ord.Status == OrderStatus.PendingCancel || ord.Status == OrderStatus.PendingExecution).Count != 0)
            {
                log = "Estado Juiz: Existe Ordem com operação pendente ainda... aguarde ela definir o que vai fazer";
                robo.Print(log);
                robo.estado = State.analise;                
                return;
            }

            //Case 3: Não existe posição e Existe ordem aguardando execução
            if (robo.positions.Length == 0 && robo.orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew))).Count != 0)
            {
                log = "Estado Juiz: Não existe posição E uma ordem qualquer ainda não foi executada.";
                robo.Print(log);                
                robo.contadorOrdemTravada++;
                if (robo.contadorOrdemTravada == 100) { robo.funcoes.LimparTodasAsOrdens(); robo.contadorOrdemTravada = 0; }
                robo.estado = State.analise;
                return;
            }

            //Case 4: Existe posição , não existe ordem de saída e modo avalanche é falso
            if (robo.positions.Length != 0 && robo.orders.Length == 0 && !robo.modoAvalanche)
            {
                log = "Estado Juiz: Existe posição mas não existe ordem de saída. CriaParDeSaída()";
                robo.Print(log);
                if (robo.comando.CriaParDeSaida(null))
                {
                    log = "Estado Juiz: CriaParDeSaída Executado com sucesso. Migrando para o limbo...";
                    robo.estado = State.lockdown;
                }
                else
                {
                    log = "Estado Juiz: Erro na Criação do Par de Saída!";
                    robo.estado = State.analise;
                }
                robo.Print(log);                
                return;
            }

            //Case 4.1: Existe posição mas não existe ordem de saída e modo avalanche é true
            if (robo.positions.Length != 0 && robo.orders.Length == 0 && robo.modoAvalanche)
            {
                log = "Estado Juiz: Existe posição, não existe ordem de saída e modo avalanche é true.";
                robo.Print(log);
                robo.estado = State.avalanche;                
                return;
            }

            //Case 5: Existe Posição e Existe Ordem de Saída e modo avalanche é falso
            if (robo.positions.Length != 0 && robo.orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced || ord.Status == OrderStatus.PartiallyFilled))).Count != 0 && !robo.modoAvalanche)
            {
                log = "Estado Juiz: Existe posição e Existe ordem de saída. Equalizando e Migrando para estado ReEntrada";
                robo.Print(log);                
                robo.estado = State.reentrada;          
                return;
            }

            //Case 5.1: Existe Posição, Existe Ordem de Saída e modo avalanche está true
            if (robo.positions.Length != 0 && robo.orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced))).Count != 0 && robo.modoAvalanche)
            {
                log = "Estado Juiz: Existe posição, Existe ordem de saída e modo Avalanche está true. Migrando para estado Avalanche";
                robo.Print(log);
                robo.estado = State.avalanche;                
                return;
            }

            //padrão
            robo.estado = State.analise;            
            log = "Estado Juiz: Nenhuma opção de saída foi encontrada";
            robo.Print(log);            
        }
    }
}
