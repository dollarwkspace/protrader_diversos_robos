﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestSM
{
    public class EstadoAvalanche
    {
        private ProjetoEverestSMOnQuote robo;
        private bool liberaReEntradaAvalanche = false;
        private Operation tipoReferencia = Operation.Buy;
        private Operation estadoDoMercado = Operation.Buy;
        private string log;

        public EstadoAvalanche(ProjetoEverestSMOnQuote _robo) { robo = _robo; log = robo.log; }

        private bool CondicaoParaPermitirEqualizacao() => PreCondicoes() && robo.orders.Length != 0 && robo.timeline.GetLastActiveItem() != null;
        private bool CondicaoParaCriacaoDeSaidaAvalanche() => PreCondicoes() && robo.orders.Length == 0 && robo.timeline.GetLastActiveItem() != null;
        private bool CondicaoParaAvalancheAtingida() => PreCondicoes() && (tipoReferencia == estadoDoMercado) && liberaReEntradaAvalanche;
        private bool TendenciaMaisForteQueResiliencia() => PreCondicoes() && (Math.Abs(robo.curta - robo.curtaAnterior) * robo.multiplicador > robo.resilienciaDeSaida);


        private bool PreCondicoes()
        {
            //TESTES PARA DESVIOS DE EXECUÇÃO
            //SE NÃO TEM POSIÇÃO NÃO TEM PQ ESTAR AQUI
            robo.funcoes.RefreshPositionsOrders();
            if (robo.positions.Length == 0)
            {
                log = "Estado AVALANCHE: Não existe mais posição. Migrando para inicio";
                robo.Print(log);
                robo.estado = State.inicio;
                robo.funcoes.LimparTodasAsOrdens();
                return false;
            }
            return true;
        }

        private bool PassouTestesDeSaida()
        {
            //SAÍDA POR LUCRO ATINGIDO            
            if ((robo.timeline?.posicaoAtivaMedia?._lucro ?? 0) > robo.lucroDeSaidaAvalanche)
            {
                log = $"Estado AVALANCHE: SAÍDA POR LUCRO ATINGIDO. Lucro: {robo.timeline.posicaoAtivaMedia._lucro}";
                robo.Print(log);                
                robo.funcoes.FechamentoTotal();
                robo.estado = State.inicio;                
            }
            return true;
        }

        public void Run()
        {
            try
            {
                log = "********************   Estado AVALANCHE ***********************";
                robo.Print(log);

                if (!PreCondicoes()) { robo.Print("Estado Avalanche: Pré-Condições não satisfeitas."); return; }
                if (!PassouTestesDeSaida()) { robo.Print("Estado Avalanche: Alguma condição de saída foi atingida!"); return; }                

                double entradaAvalanche = Math.Floor(robo.contratosAvalanche * (robo.positions?[0]?.Amount??0) / 100);
                if (entradaAvalanche < robo.minimumLot) entradaAvalanche = robo.minimumLot;
                tipoReferencia = robo.positions[0]?.Side ?? Operation.Buy;
                estadoDoMercado = ((robo.curta - robo.curtaAnterior) * robo.multiplicador > 0) ? Operation.Buy : Operation.Sell;
                robo.Print($"Estado Avalanche: |media-mediaAnterior|*multiplicador  = {Math.Abs(robo.curta - robo.curtaAnterior) * robo.multiplicador} Resiliência de Entrada: {robo.resilienciaDeSaida}");

                //SE EXISTE SAÍDA, E ENTRADA AVALANCHE, EQUALIZE AS DUAS
                if (CondicaoParaPermitirEqualizacao())
                {
                    log = "Estado Avalanche: Ordem != 0 , Posição Ativa Avalanche != null, Posições > 0. Testando necessidade de Equalização.";
                    robo.Print(log);
                    if (robo.funcoes.EqualizaOrdemPosicaoAvalanche(robo.timeline.GetLastActiveItem(), robo.orders[0])) { robo.estado = State.lockdown; return; };
                }

                //SE NÃO EXISTE SAÍDA, E EXISTE ENTRADA AVALANCHE, CRIE UMA SAÍDA AVALANCHE
                if (CondicaoParaCriacaoDeSaidaAvalanche())
                {
                    log = "Estado Avalanche: Ordem == 0 , Posição Ativa Avalanche != null";
                    robo.Print(log);
                    if (robo.comando.SaidaAvalanche(robo.timeline.GetLastActiveItem())) { robo.estado = State.lockdown; }
                    return;
                }        

                //SE EXISTE ENTRADA AVALANCHE, TESTE CONDIÇÕES PARA CRIAR NOVA ENTRADA AVALANCHE
                if (robo.timeline.GetLastActiveItem() != null)
                {
                    log = "Estado Avalanche: Posição Ativa Avalanche != null. Buscando novo ponto de Entrada Avalanche.";
                    robo.Print(log);

                    double pontoCritico = Math.Abs(Math.Round(((robo.cotacaoAtual - robo.timeline.GetLastActiveItem()._openPrice) / robo.tamanhoPonto), 2));

                    log = $"Estado Avalanche: Analisando ponto crítico... Ponto crítico atual: {pontoCritico}  Limite: {robo.reEntradaAvalanche} pontos";
                    robo.Print(log);
                    if (robo.tipoDeTP == TipoDeTP.dinamico) robo.reEntradaAvalanche = robo.TP + 1;

                    if (!(pontoCritico >= Math.Abs(robo.reEntradaAvalanche))) return;
                    liberaReEntradaAvalanche = true;
                    robo.Print("Entrada Avalanche: Ponto Crítico Atingido! Verificando tendência do mercado ..");
                }

                if (!CondicaoParaAvalancheAtingida()) { robo.Print($"Estado AVALANCHE: Tendência desfavorável ! Tendência:{estadoDoMercado}"); return; }
                if (!TendenciaMaisForteQueResiliencia()) { robo.Print($"Estado AVALANCHE: Aguardando Sensibilidade da Média ficar maior que {robo.resilienciaDeSaida}"); return; }

                if (robo.comando.Send_order(OrdersType.Market, tipoReferencia, robo.cotacaoAtual, entradaAvalanche, 0, "entrada avalanche") != "-1")
                {
                    log = "Estado AVALANCHE: Entrada Avalanche enviada!";
                    robo.Print(log);
                    robo.estado = State.lockdown;
                    return;
                }

                log = "Estado AVALANCHE: Ordem de ReEntrada Avalanche negada!";
                robo.Print(log);
                robo.estado = State.avalanche;
                robo.funcoes.PrintaPosicoesOrdensAtivas();
            }
            catch (Exception exc)
            {
                string log = "Erro no estado avalanche! " + exc.Message;
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                robo.estado = State.analise;
            }
        }


    }


}
