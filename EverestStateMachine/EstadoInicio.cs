﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestSM
{
    public abstract class Estado
    {
        public abstract void Run();
    }

    public class EstadoInicio : Estado
    {
        private ProjetoEverestSMOnQuote robo;
        public EstadoInicio(ProjetoEverestSMOnQuote _robo) => robo = _robo; //CONSTRUTOR DA CLASSE

        private double GetDiffMedias() => robo.mediaContinua ? robo.curta - robo.curtaAnterior : robo.histoResult;
        private double GetDiffMediasSuporte() => robo.suporte - robo.suporteAnterior;
        private double GetDiffMediasBloqueio() => robo.mBloqueio - robo.mBloqueioAnterior;
        private Operation GetTendenciaMediaSinal() => GetDiffMedias() > 0 ? Operation.Buy : Operation.Sell;
        private Operation GetTendenciaMediaSuporte() => GetDiffMediasSuporte() > 0 ? Operation.Buy : Operation.Sell;
        private Operation GetTendenciaMediaBloqueio() => GetDiffMediasBloqueio() > 0 ? Operation.Buy : Operation.Sell;

        private bool RoboDeTendencia() => robo.tipoDeRobo == TipoDeRobo.tendencia;
        private bool RoboContraTendencia() => robo.tipoDeRobo == TipoDeRobo.contraTendencia;
        private bool RoboContraEFavorTendencia() => robo.tipoDeRobo == TipoDeRobo.tendenciaEcontra;
        private bool ExisteInfluenciaMediaSuporte() => Math.Abs(GetDiffMediasSuporte()) >= robo.deltaSuporte;
        private bool MercadoLateral() => Math.Abs(robo.suporte - robo.curta) <= robo.lateralidade;
        private bool MercadoDentroDeltaTendencia() => Math.Abs(GetDiffMedias() / robo.tickSize) <= robo.distanciaDaMedia;
        private bool MercadoForaDaFaixaATRsup() => (robo.cotacaoAtual >= robo.ATRsuperior);
        private bool MercadoForaDaFaixaATRinf() => (robo.cotacaoAtual <= robo.ATRinferior);
        private bool MercadoDentroDaFaixaATR() => (robo.cotacaoAtual <= robo.ATRsuperior && robo.cotacaoAtual >= robo.ATRinferior);

        private bool CondicoesDeTendenciaSatisfeitas() => MercadoDentroDaFaixaATR() && MercadoDentroDeltaTendencia()
            && (RoboDeTendencia() || RoboContraEFavorTendencia());

        private bool CondicoesContraTendenciaSupSatisfeitas() => MercadoForaDaFaixaATRsup()
            && (GetTendenciaMediaSinal() == Operation.Sell) && (RoboContraTendencia() || RoboContraEFavorTendencia());
        private bool CondicoesContraTendenciaInfSatisfeitas() => MercadoForaDaFaixaATRinf()
            && (GetTendenciaMediaSinal() == Operation.Buy) && (RoboContraTendencia() || RoboContraEFavorTendencia());

        private bool AutorizaEntradaTendencia()
        {
            if (ExisteInfluenciaMediaSuporte()) return CondicoesDeTendenciaSatisfeitas() && (GetTendenciaMediaSinal() == GetTendenciaMediaSuporte());
            return CondicoesDeTendenciaSatisfeitas();
        }

        private bool AutorizaEntradaContraTendênciaSup()
        {
            if (ExisteInfluenciaMediaSuporte()) return CondicoesContraTendenciaSupSatisfeitas() && (GetTendenciaMediaSinal() == GetTendenciaMediaSuporte());
            return CondicoesContraTendenciaSupSatisfeitas();
        }

        private bool AutorizaEntradaContraTendênciaInf()
        {
            if (ExisteInfluenciaMediaSuporte()) return CondicoesContraTendenciaInfSatisfeitas() && (GetTendenciaMediaSinal() == GetTendenciaMediaSuporte());
            return CondicoesContraTendenciaInfSatisfeitas();
        }


        //MÉTODOS PRIVADOS
        private bool PreCondicoesSatisfeitas()
        {            
            if (robo.orders.Length > 0)
            {
                if (robo.orders.ToList().Exists(x => x.Status == OrderStatus.PendingCancel || x.Status == OrderStatus.PendingExecution ||
                                                x.Status == OrderStatus.PendingNew || x.Status == OrderStatus.PendingReplace))
                {
                    robo.Print("Estado Início: Existe Ordem com Status de Execução Pendente.. aguardando Corretora resolver");
                    return false;
                }

                string log = "Estado Início: Tem ordem ativa ainda ! Não pode ter ordem ativa no início. Fechando...";
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                return false;
            }

            if (robo.positions.Length > 0)
            {
                string log = "Estado início: Tem posição ativa ainda. Fechando...";
                robo.Print(log);
                robo.funcoes.FechamentoTotal();
                return false;
            }

            InicializaVariaveis();
            return true;
        }

        private void InicializaVariaveis()
        {
            robo.modoAvalanche = false;
            robo.contadorDeReEntradas = 0;
            robo.timeline.ClearAll();
        }

        //MÉTODOS PÚBLICOS
        public override void Run()
        {
            try
            {
                string log = "********************   Estado Início  ********************";
                robo.Print(log);

                //TESTES DE BLOQUEIO DE EXECUÇÃO
                if (!PreCondicoesSatisfeitas()) { robo.Print("Estado Inicio: Aguardando condições de entrada"); return; } //posições e ordens devem estar zeradas
                if (MercadoLateral()) { robo.Print($"Estado Inicio: Mercado está lateral! |sinal-suporte| = {Math.Abs(robo.suporte - robo.curta)} Lateralidade:{robo.lateralidade}"); return; }

                //INICIO
                robo.Print($"hora:{robo.horario}  Diferença de médias Sinal: {GetDiffMedias()}   Diferença de médias Suporte:{GetDiffMediasSuporte()}   Diferença de médias Bloqueio: {GetDiffMediasBloqueio()} ");
                robo.Print($"hora:{robo.horario}  Tendências: Sinal, Suporte, Bloqueio: {GetTendenciaMediaSinal()} {GetTendenciaMediaSuporte()} {GetTendenciaMediaBloqueio()}");
                robo.Print($"hora:{robo.horario}  Delta Suporte: {robo.deltaSuporte}  ExisteInfluênciaDaMédiaSuporte? {ExisteInfluenciaMediaSuporte()}");
                robo.Print($"hora:{robo.horario}  MercadoDentroDoATR? {MercadoDentroDaFaixaATR()}   MercadoForaDoATRSup? {MercadoForaDaFaixaATRsup()}  MercadoForaDoATRinf? {MercadoForaDaFaixaATRinf()}  Mercado Dentro do Delta Tendência? {MercadoDentroDeltaTendencia()}");
                robo.Print($"hora:{robo.horario}  AutorizaEntradaTendência? {AutorizaEntradaTendencia()}   Aut.C.TendênciaSup? {CondicoesContraTendenciaSupSatisfeitas()} Aut.C.TendênciaInf? {CondicoesContraTendenciaInfSatisfeitas()}");

                if (AutorizaEntradaTendencia())
                {
                    if (robo.comando.Send_order(OrdersType.Market, GetTendenciaMediaSinal(), 0, robo.Contratos, 0) != "-1")
                    {
                        log = $"{robo.horario} Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo";
                        robo.estado = State.lockdown;
                    }
                    else
                    {
                        log = $"{robo.horario} Estado Inicio: Ordem {GetTendenciaMediaSinal()} recusada";
                        robo.estado = State.inicio;
                    }
                }

                if (AutorizaEntradaContraTendênciaSup())
                {

                    if (robo.comando.Send_order(OrdersType.Market, Operation.Sell, 0, robo.Contratos, 0) != "-1")
                    {
                        log = $"{robo.horario} Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo";
                        robo.estado = State.lockdown;
                    }
                    else
                    {
                        log = $"{robo.horario} Estado Inicio: Ordem SELL recusada";
                        robo.estado = State.inicio;
                    }
                }

                if (AutorizaEntradaContraTendênciaInf())
                {
                    if (robo.comando.Send_order(OrdersType.Market, Operation.Buy, 0, robo.Contratos, 0) != "-1")
                    {
                        log = $"{robo.horario}  Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo";
                        robo.estado = State.lockdown;
                    }
                    else
                    {
                        log = $"{robo.horario} Estado Inicio: Ordem BUY recusada";
                        robo.estado = State.inicio;
                    }
                }

                robo.Print(log);                
            } catch (Exception ex)
            {
                robo.Print($"Estado Inicio: Erro {ex.Message} {ex.InnerException.Message}");
            }

        }
    }
}

