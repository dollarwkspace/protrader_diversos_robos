﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace EverestSM
{
    public class EstadoReEntrada : Estado
    {
        private ProjetoEverestSMOnQuote robo;
        private string log;
        private double[] ReEntradaOriginal;

        public EstadoReEntrada(ProjetoEverestSMOnQuote _robo) { robo = _robo; log = robo.log; }

        private bool ReEntradasMaximasAtingidas(int contador) => contador > robo.ValoresDeReEntradas.Length - 1;
        //private bool CondicaoReEntradaBuyAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Buy && pos.CurrentPrice <= robo.timeline.GetLastActiveItem()._openPrice - delta;
        //private bool CondicaoReEntradaSellAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Sell && pos.CurrentPrice >= robo.timeline.GetLastActiveItem()._openPrice + delta;
        private bool CondicaoReEntradaBuyAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Buy && pos.CurrentPrice <= robo.positions[0].OpenPrice - delta;
        private bool CondicaoReEntradaSellAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Sell && pos.CurrentPrice >= robo.positions[0].OpenPrice + delta;        
        private bool ContratosAbaixoDoMaximo() => PassouTestePosicaoVazia() && robo.positions[0].Amount + robo.ValoresDeReEntradas[robo.contadorDeReEntradas] <= robo.maxPosAbertas;
        private bool CondicaoDeSaidaScalperSellAtingida(Position pos) => PassouTestePosicaoVazia() && robo.tipoDeSaida == TipoDeSaida.DifMédias && GetDiffMedias() >= robo.resilienciaDeSaida && pos.Side == Operation.Sell;
        private bool CondicaoDeSaidaScalperBuyAtingida(Position pos) => PassouTestePosicaoVazia() && robo.tipoDeSaida == TipoDeSaida.DifMédias && GetDiffMedias() <= -robo.resilienciaDeSaida && pos.Side == Operation.Buy;
        private bool RoboTipoScalper() => robo.tipoDeSaida == TipoDeSaida.DifMédias;
        private double GetDiffMedias() => robo.mediaContinua ? (robo.curta - robo.curtaAnterior) * robo.multiplicador : robo.histoResult;


        private bool PassouTestePosicaoVazia()
        {
            if (robo.positions.Length == 0)
            {
                log = "Estado ReEntrada: Não existe mais posição. Migrando para inicio";
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                robo.estado = State.inicio;
                return false;
            }
            return true;
        }

        private bool PreCondicoesSatisfeitas()
        {
            //TESTES PARA DESVIO DE EXECUÇÃO
            //SE NÃO TEM POSIÇÃO, NÃO TEM PQ ESTAR AQUI
            if (!PassouTestePosicaoVazia()) return false;

            //SE NÃO TEM ORDEM DE SAÍDA
            if (robo.orders.Length == 0)
            {
                //MAS TEM POSIÇÃO
                if (robo.positions.Length > 0)
                {
                    //TEM POSIÇÃO E NÃO TEM ORDEM DE SAÍDA: CRIA ORDEM DE SAÍDA
                    log = "Estado ReEntrada: Ordem de saída zero, e tem posição! Criando Par de Saída";
                    robo.Print(log);
                    robo.comando.CriaParDeSaida();                    
                    return false;
                }
                else
                {
                    //SE NÃO TEM POSIÇÃO E NÃO TEM ORDEM: MIGRA PRO INÍCIO
                    log = "Estado ReEntrada: Sem ordem e sem posição? Migra pro inicio...";
                    robo.Print(log);
                    robo.estado = State.inicio;
                    return false;
                }
            }

            return true;
        }

        private bool PassouTestesDeBloqueio()
        {
            //TESTES DE BLOQUEIO DE EXECUÇÃO
            if (ReEntradasMaximasAtingidas(robo.contadorDeReEntradas))
            {
                if (robo.positions.Length > 0)
                {
                    //TENTA EQUALIZAR UMA ÚLTIMA VEZ 
                    try
                    {
                        Position pos = robo.positions[0];
                        Order ord = null;
                        if (robo.orders.Length > 0) ord = robo.orders[0];
                        robo.funcoes.EqualizaOrdemPosicao(pos, ord);
                    } catch (Exception ex)
                    {
                        robo.Print("Erro na última equalização");
                    }
                }                
                robo.Print("Estado ReEntrada: Máximo número de ReEntradas foi atingido!");
                return false;
            }
            if (!ContratosAbaixoDoMaximo()) { robo.Print("Estado ReEntrada: Não pode abrir mais contratos pois irá estourar o máximo permitido!"); return false; }
            return true;
        }

        private bool PassouTestesDeSaida()
        {
            //TESTES DE SAÍDA POR DIFERENÇA DE MÉDIAS SE SCALPER                        

            if (CondicaoDeSaidaScalperSellAtingida(robo.positions[0]) || CondicaoDeSaidaScalperBuyAtingida(robo.positions[0]))
            {
                log = robo.horario + "Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                robo.Print(log);
                robo.funcoes.FechamentoTotal();                
                robo.estado = State.lockdown;                
                return false;
            }
            
            return true;
        }

        public override void Run()
        {
            try
            {
                robo.Print("********************   Estado ReEntrada   ***********************");

                if (!PreCondicoesSatisfeitas()) { robo.Print("Estado ReEntrada: Pré-Condições não satisfeitas!"); return; }
                if (!PassouTestesDeSaida()) { robo.Print("Estado ReEntrada: Uma condição de saída foi atingida!"); return; }
                if (!PassouTestesDeBloqueio()) { robo.Print("Estado ReEntrada: Existe alguma condição de bloqueio!"); return; }

                ReEntradaOriginal = robo.ticksDeReentrada.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                Position pos = robo.positions[0];
                Order ord = null;
                if (robo.orders.Length > 0) ord = robo.orders[0];

                //INÍCIO LÓGICA DE REENTRADAS
                robo.funcoes.EqualizaOrdemPosicao(pos, ord);               

                if (robo.reEntradasDinamicas)
                {                    
                    for(int i=0; i<robo.ReEntradas.Length; i++)
                    {
                        robo.ReEntradas[i] = ReEntradaOriginal[i] + Math.Round((robo.ATRsuperior - robo.ATRinferior) / robo.instrument.TickSize,0);
                    }
                    robo.ReEntradas[0] = (pos.Side == Operation.Sell) ? (robo.ATRsuperior - pos.OpenPrice) / robo.instrument.TickSize : (pos.OpenPrice - robo.ATRinferior) / robo.instrument.TickSize;
                    robo.ReEntradas[0] = Math.Round(robo.ReEntradas[0], 0);
                }
                var deltaMoeda = robo.instrument.TickSize * robo.ReEntradas[robo.contadorDeReEntradas];
                robo.proximaReEntrada = (pos.Side == Operation.Buy) ? pos.OpenPrice - deltaMoeda : pos.OpenPrice + deltaMoeda;
                var liberaReEntrada = false;
                robo.Print($"Hora: {robo.horario}");
                robo.Print($"Estado ReEntrada: |Preço Atual - Preço Abertura|/tick =  {(Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.instrument.TickSize)}");
                robo.Print($"Estado ReEntrada: Array de ReEntrada Original   -> [{String.Join(",", ReEntradaOriginal.Select(p => p.ToString()).ToArray())}]");
                robo.Print($"Estado ReEntrada: Array de ReEntradas utilizado -> [{String.Join(",", robo.ReEntradas.Select(p => p.ToString()).ToArray())}]");                
                robo.Print($"Estado ReEntrada: (AtrSuperior - AtrInferior)/tick =  {Math.Round((robo.ATRsuperior-robo.ATRinferior)/robo.instrument.TickSize, 0)}");
                robo.Print($"Estado ReEntrada: ReEntrada em {Math.Abs(robo.cotacaoAtual-robo.proximaReEntrada)} pontos");

                if (RoboTipoScalper())
                {
                    if (robo.mediaContinua) robo.Print($"Estado ReEntrada: (média - média Anterior) x multiplicador = {(robo.curta - robo.curtaAnterior) * robo.multiplicador}");
                    else robo.Print($"Estado ReEntrada: histograma = {robo.histoResult}");
                }
                robo.Print($"Estado ReEntrada: Entrada atual = {robo.contadorDeReEntradas}");

                if (CondicaoReEntradaBuyAtingida(pos, deltaMoeda) || CondicaoReEntradaSellAtingida(pos, deltaMoeda)) liberaReEntrada = true;

                if (liberaReEntrada)
                {
                    robo.Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                    if (robo.comando.ReEntrada(robo.cotacaoAtual, robo.positions[0]) == "-1") { robo.Print("Estado ReEntrada: ReEntrada Negada pela Corretora!"); return; }
                    log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo.";
                    robo.Print(log);
                    robo.contadorDeReEntradas++;
                    robo.estado = State.lockdown;
                    return;
                }

                robo.funcoes.PrintaPosicoesOrdensAtivas();
            } catch (Exception ex)
            {
                robo.Print($"Estado ReEntrada: Erro {ex.Message}");
                robo.Print($"Estado ReEntrada: Erro Interno {ex.InnerException.Message}");
            }
        }
    }
}
