﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;

namespace RoboDollar
{
    internal class Funcoes
    {
        public static void InfoConexao(Connection myConnection, NETScript robo)
        {
            //outputting of all the information about the current connection.
            robo.Print(
            "\nConnection name : \t" + myConnection.Name +
            "\nConnection address : \t" + myConnection.Address +
            "\nConnection login : \t" + myConnection.Login +
            "\nSLL on?  : \t" + myConnection.IsUseSSL +
            "\nProtocol version : \t" + myConnection.ProtocolVersion +
            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
            "\nConnection status : \t" + myConnection.Status
            );
        }

        public static void AccountInformation(Account acc, NETScript robo)
        {
            //outputting of all the 'Account' properties
            robo.Print(
                "Id : \t" + acc.Id + "\n" +
                "Name : \t" + acc.Name + "\n" +
                "User Login: \t" + acc.User.Login + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "Leverage : \t" + acc.Leverage + "\n" +
                "Currency : \t" + acc.Currency + "\n" +
                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
                "IsDemo : \t" + acc.IsDemo + "\n" +
                "IsReal : \t" + acc.IsReal + "\n" +
                "IsLocked : \t" + acc.IsLocked + "\n" +
                "IsInvestor : \t" + acc.IsInvestor + "\n" +
                "Status : \t" + acc.Status + "\n" +
                "StopReason : \t" + acc.StopReason + "\n" +
                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "BeginBalance : \t" + acc.BeginBalance + "\n" +
                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
                "Credit : \t" + acc.Credit + "\n" +
                "CashBalance : \t" + acc.CashBalance + "\n" +
                "TodayVolume : \t" + acc.TodayVolume + "\n" +
                "TodayNet : \t" + acc.TodayNet + "\n" +
                "TodayTrades : \t" + acc.TodayTrades + "\n" +
                "TodayFees : \t" + acc.TodayFees + "\n" +
                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
                "MarginTotal : \t" + acc.MarginTotal + "\n" +
                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
                "Equity : \t" + acc.Equity + "\n" +
                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
            );
        }

        public static void PosInfo(Position pos, NETScript robo)
        {
            robo.Print(
                "PosInfo()-> " +
                "  Papel: " + pos.Instrument.BaseName + "\t" +
                "  Id: " + pos.Id + "\t" +
                "  Qtde: " + pos.Amount + "\t" +
                "  Tipo: " + pos.Side.ToString() + "\t" +
                "  Preço Abertura: R$ " + pos.OpenPrice + "\t" +
                "  Preço Atual: R$ " + pos.CurrentPrice + "\t" +
                "  Número Mágico: " + pos.MagicNumber + "\t" +            
                "  Lucro ou Prejuízo: R$" + pos.GetProfitNet()
            );
        }

        public static void OrderInfo(Order ord, NETScript robo)
        {
            robo.Print(
                "OrderInfo()-> " +
                "  Papel: " + ord.Instrument.BaseName + "\t" +
                "  Id: " + ord.Id + "\t" +
                "  Qtde: " + ord.Amount + "\t" +
                "  Lado: " + ord.Side + "\t" +
                "  Tipo: " + ord.Type + "\t" +
                "  Preço: R$ " + ord.Price + "\t" +
                "  Status: " + ord.Status + "\t" +
                "  Número Mágico: " + ord.MagicNumber
            );

        }




    }
}
