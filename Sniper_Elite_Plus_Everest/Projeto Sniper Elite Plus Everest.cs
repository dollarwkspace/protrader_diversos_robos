﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;

namespace RoboDollar
{

    internal class PosicaoAtiva : NETScript
    {
        internal double _openPrice;
        internal double _amount;
        internal Operation _side;
        internal DateTime _openTime;
        internal double _lucro;
        internal double _pontos;
        internal string _comments;

        internal PosicaoAtiva(double openPrice, double amount, Operation side, DateTime openTime, string comments = "", double lucro = 0, double pontos = 0)
        {
            _openPrice = openPrice; _amount = amount; _side = side; _openTime = openTime; _lucro = lucro; _pontos = pontos; _comments = comments;

        }

    }


    public class ProjetoSniperElitePlusFinal : NETStrategy
    {
        private enum state { limbo, inicio, reentrada, saidaTendencia };
        private enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
        public enum TipoDeSaida { DifMédias, LossMaximo };
        public enum TipoDeEntrada { MA3, DifMédias };
        public enum TipoDeRobo { tendencia, contraTendencia, tendenciaEcontra }
        public enum TipoDeOrdem { limite, mercado };
        public enum Acao
        {
            eventoOrderAdded, aguardandoOrderAdded,
            eventoOrderRemoved,
            eventoOrderExecuted, aguardandoOrderExecuted,
            eventoPositionAdded, aguardandoPositionAdded,
            eventoPositionRemoved,
            passeLivre
        };

        private Instrument instrument;
        private Account conta;
        private Indicator MM;
        private Indicator sniper;
        private Connection myConnection = Connection.CurrentConnection; //create connection object.    
        private Position[] positions;
        private Position ultimaPosicaoAdd;
        private Position ultimaPosicaoRemovida;        
        private PosicaoAtiva posicaoAtivaMedia = new PosicaoAtiva(0, 0, Operation.Buy, DateTime.Now);
        private Order ultimaOrdemAdd;
        private Order ultimaOrdemExecutada;
        private Order[] orders;
        private Font font;
        private Brush brush;
        private List<PosicaoAtiva> tapeReadingPosicoes = new List<PosicaoAtiva>();
        private List<Acao> acao = new List<Acao>();
        private TimeSpan horario;

        private double calor = 0;
        private double curta = 0;
        private double curtaAnterior = 0;
        private double histoResult = 0;
        private double cotacaoAtual = 0;
        private double mmAvalanche = 0;
        private double mmAvalancheAnterior = 0;
        private int hora = 0;
        private int contadorDeReEntradas = 0;
        private ushort[] ReEntradas;
        private UInt16[] ValoresDeReEntradas;
        private double tickSize = 0;
        private double ultimaReentrada = 0;
        private string horaDoCalorMaximo = "";
        private state estado = state.inicio;
        private bool horarioTerminou = false;
        private string papel;
        private int numeroMagico = 0;
        private double maximoDeContratosAbertos = 0;
        private bool solicitouEqualizacao = false;
        private bool bloqueieNovaAdicaoNoArray = false;

        public ProjetoSniperElitePlusFinal()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto Sniper Elite Plus Final";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "08.01.2018";
            base.ExpirationDate = 0;
            base.Version = "1.0.0";
            base.Password = "";
            base.ProjectName = "Sniper Elite Plus Final";
            #endregion
        }

        [InputParameter("Média: Suporte", 0)]
        public int periodoMediaMovelSuporte = 15;
        [InputParameter("Média: Sinal", 1)]
        public int periodoMediaMovelSinal = 4;
        [InputParameter("Média contínua ou por fechamento de candle?", 2)]
        public bool mediaContinua = true;

        [InputParameter("Permitir até quantas posições abertas? ", 3)]
        public double maxPosAbertas = 40;

        [InputParameter("Contratos", 4)]
        public int Contratos = 5;

        [InputParameter("Início(hora)", 5)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

        [InputParameter("Término(hora)", 6)]
        public TimeSpan HoraDeTermino = new TimeSpan(18, 00, 00);

        [InputParameter("Array Ticks de Reentradas", 7)]
        public string ticksDeReentrada = "10,15,20";

        [InputParameter("Array Valores de Reentradas", 8)]
        public string valoresDeReentrada = "5,10,20";

        [InputParameter("Distância da Média (tend)", 9)]
        public double distanciaDaMedia = 4;

        [InputParameter("Distância da Média (contra)", 10)]
        public double distanciaDaMediaContra = 7;

        [InputParameter("Take Profit (Pontos)", 11)]
        public double TP = 1;

        [InputParameter("Multiplicador da Dif.Das Médias", 12)]
        public int multiplicador = 10;

        [InputParameter("Resiliência de Saída", 13)]
        public double resilienciaDeSaida = 2;

        [InputParameter("Loss máximo em Ticks", 14)]
        public double lossMaximo = 50;

        [InputParameter("Tipo de Saída", 15, new Object[]
     {"Dif Médias", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;

        [InputParameter("Tipo de Robô", 16, new Object[]
     {"Tendência", TipoDeRobo.tendencia, "Contra Tendência",TipoDeRobo.contraTendencia, "Tendência e Contra", TipoDeRobo.tendenciaEcontra })]
        public TipoDeRobo tipoDeRobo = TipoDeRobo.tendenciaEcontra;

        [InputParameter("Market Range", 17)]
        public int marketRange = 1;

        [InputParameter("Custo de 1 contrato por ponto", 18)]
        public double custoContratoPorPonto = 50;

        [InputParameter("Tamanho do Ponto", 19)]
        public double tamanhoPonto = 1;

        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            try
            {
                font = new Font("Tahoma", 10);
                brush = new SolidBrush(Color.White);
                instrument = Instruments.Current;
                Print("Nome do Instrumento ativo: " + instrument.Name);
                papel = instrument.BaseName;
                conta = Accounts.Current;
                tickSize = instrument.TickSize;
                Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
                Funcoes.InfoConexao(myConnection, this);
                Funcoes.AccountInformation(conta, this);

                ReEntradas = ticksDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                //ReEntradas[0] = reEntrada0; ReEntradas[1] = reEntrada1; ReEntradas[2] = reEntrada2; ReEntradas[3] = reEntrada3; ReEntradas[4] = reEntrada4; ReEntradas[5] = reEntrada5; ReEntradas[6] = reEntrada6;
                ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();

                if (ReEntradas.Length != ValoresDeReEntradas.Length)
                {
                    throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
                }

                Print("Papel: " + instrument.Name);
                Print("Tamanho do Point: " + Point);
                Print("Tamanho do tick: " + instrument.TickSize);


                //INDICADORES
                Print("Inicializando indicador Camilo 2MM");
                MM = Indicators.iCustom("camilo_2mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelSuporte);
                Print("Inicializando indicador Sniper Elite");
                sniper = Indicators.iCustom("Camilo_SniperElite", CurrentData, periodoMediaMovelSinal, resilienciaDeSaida, multiplicador);


                //EVENTOS            
                Positions.PositionAdded += Positions_PositionAdded;
                Positions.PositionRemoved += Positions_PositionRemoved;
                Instruments.NewTrade += Instruments_NewTrade;
                Orders.OrderAdded += Orders_OrderAdded;
                Orders.OrderExecuted += Orders_OrderExecuted;
                Orders.OrderRemoved += Orders_OrderRemoved;

                Print("Fim da Inicialização do Robô");

                if (myConnection.Status == ConnectionStatus.Disconnected)
                {
                    Print("RODANDO ROBÔ NO SIMULADOR. NÚMERO MÁGICO É 0");
                    numeroMagico = 0; // SE ESTIVER NO SIMULADOR O NUMERO MÁGICO É ZERO
                }
                else
                {
                    Print("RODANDO ROBÔ NA CONTA DEMO OU REAL. Número mágico deste robô é " + numeroMagico);
                }


                //VERIFICANDO ESTADO ANTERIOR                
                positions = Positions.GetPositions()?.ToList().FindAll(posit => posit.Instrument.BaseName == papel).ToArray();
                if (positions.Length > 0)
                {
                    contadorDeReEntradas = 0;
                    tapeReadingPosicoes.Add(new PosicaoAtiva(positions[0].OpenPrice, positions[0].Amount, positions[0].Side, DateTime.Now, "Inicio pré-existente"));
                    estado = state.limbo;
                    acao.Add(Acao.passeLivre);
                    return;
                }

            }
            catch (Exception exc)
            {
                Print("Erro na inicialização do Robô... " + exc.Message);
                Print("Finalizando Robô");
                TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
                foreach (var robo in robos)
                {
                    robo.Stop();
                    Print("Nome do robô:" + robo.Name);
                }
            }
        }

        private void Orders_OrderRemoved(Order obj)
        {
            if (solicitouEqualizacao) { solicitouEqualizacao = false; return; }

            Print("EVENTO: Uma ordem com id " + obj.Id + " de " + obj.Side + " " + obj.Comment + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            acao.Clear();
            acao.Add(Acao.eventoOrderRemoved);
            estado = state.limbo;
            return;
        }

        private void Orders_OrderExecuted(Order obj)
        {
            try
            {
                if (solicitouEqualizacao) { solicitouEqualizacao = false; return; }

                Print("EVENTO: Uma ordem com id " + obj.Id + " de " + obj.Side + " " + obj.Comment + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());

                if (ultimaOrdemExecutada.Id != obj.Id && obj.Instrument.BaseName == papel)
                {
                    acao.Add(Acao.eventoOrderExecuted);                    
                    double amount = obj.Amount;
                    double preco = obj.Price;

                    RefreshPositionsOrders();
                    if (positions.Length > 0)
                    {
                        Operation tipoBase = positions[0].Side;
                        if (obj.Side != tipoBase) amount = -amount;
                    }

                    if (!bloqueieNovaAdicaoNoArray) tapeReadingPosicoes.Add(new PosicaoAtiva(preco, amount, obj.Side, obj.Time, obj.Comment, 0, 0));
                }
                else
                {
                    Print("EVENTO: OrderExecuted com id " + obj.Id + " já foi chamado 1x, não precisa ser chamado de novo né Protrader!");
                }

                ultimaOrdemExecutada = obj;

            }
            catch (Exception exc)
            {
                Alert("EVENTO OrderExecuted: ERRO " + exc.Message);
                Print("EVENTO OrderExecuted: " + exc.InnerException.Message);
                estado = state.limbo;
                acao.Clear();
                acao.Add(Acao.passeLivre);
                return;
            }

        }

        private void Orders_OrderAdded(Order obj)
        {
            if (solicitouEqualizacao) { solicitouEqualizacao = false; return; }

            acao.Add(Acao.eventoOrderAdded);
            Print("EVENTO: Uma ordem com id " + obj.Id + " de " + obj.Side + " " + obj.Comment + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            ultimaOrdemAdd = obj;

        }

        private void Positions_PositionRemoved(Position obj)
        {
            try
            {
                acao.Clear();
                acao.Add(Acao.eventoPositionRemoved);
                estado = state.limbo;
                Print("EVENTO: Posição com id " + obj.Id + " " + obj.Comment + " de tipo " + obj.Side + " do Papel " + obj.Instrument.BaseName + " foi removida! MagicNumber:" + obj.MagicNumber);
                ultimaPosicaoRemovida = obj;
                return;
            }
            catch (Exception exc)
            {
                Alert("EVENTO: Erro no Position Removed: " + exc.Message);
                acao.Clear();
                estado = state.limbo;                
                acao.Add(Acao.passeLivre);
            }
        }

        private void Positions_PositionAdded(Position obj)
        {
            try
            {
                acao.Add(Acao.eventoPositionAdded);
                ultimaReentrada = obj.OpenPrice;

                if (obj.Instrument.BaseName == papel && ultimaPosicaoAdd?.Id != obj.Id)
                {
                    Print("EVENTO: Posição com id " + obj.Id + " de tipo " + obj.Side + " no Papel " + obj.Instrument.BaseName + " foi adicionada! MagicNumber:" + obj.MagicNumber);
                }
                else
                {
                    Print("EVENTO: PositionAdded com id " + obj.Id + " já foi chamado 1x, não precisa ser chamado de novo né Protrader!");
                }

                ultimaPosicaoAdd = obj;

            }
            catch (Exception ext)
            {
                Alert("EVENTO: Erro no Position Added: " + ext.Message);
                estado = state.limbo;
                LimparTodasAsOrdens();
                acao.Add(Acao.passeLivre);
            }
        }

        private void Instruments_NewTrade(Instrument inst, Trade trade)
        {
            Print("********************   NewTrade   ********************");
            if (!RefreshPositionsOrders()) return; //atualiza as ordens e posições ativas
            if (!RefreshIndicators()) return; //lê os indicadores
            if (!CheckMarketStatus(trade)) return; //checa se bolsa está aberta e se está fechada fecha as posições
            if (!CalculaCalorMaximo()) return;
            if (curta == 0 || curtaAnterior == 0) { Print("Aguardando indicadores!"); return; }

            SaidaPorLossMaximo();


            if (tapeReadingPosicoes.Count > 0)
            {
                if (tapeReadingPosicoes.First()._comments != "entrada mercado" &&
                    tapeReadingPosicoes.First()._comments != "Inicio pré-existente"
                    ) tapeReadingPosicoes.Remove(tapeReadingPosicoes.First());


                Operation tipoBase = tapeReadingPosicoes.First()._side;
                tapeReadingPosicoes.ForEach(posicao =>
                {
                    posicao._pontos = (posicao._side == Operation.Buy) ? Math.Round(((cotacaoAtual - posicao._openPrice) / tamanhoPonto), 2) : -Math.Round(((cotacaoAtual - posicao._openPrice) / tamanhoPonto), 2);
                    posicao._lucro = posicao._pontos * custoContratoPorPonto * Math.Abs(posicao._amount);
                });


                double _precoMedio = Math.Round(tapeReadingPosicoes.Sum(x => x._openPrice * x._amount) / tapeReadingPosicoes.Sum(x => x._amount), 2);
                double _lucroMedio = tapeReadingPosicoes.Sum(x => x._lucro);
                double _amountMedio = tapeReadingPosicoes.Sum(x => x._amount);

                posicaoAtivaMedia._amount = _amountMedio;
                posicaoAtivaMedia._lucro = _lucroMedio;
                posicaoAtivaMedia._openPrice = _precoMedio;
                posicaoAtivaMedia._side = tipoBase;
            }

            try
            {
                switch (estado)
                {
                    case state.limbo:
                        {
                            Print("********************   Estado Limbo *****************");
                            Print("Hora: " + horario + "  Limbo: Papel:" + instrument.BaseName + "\t Numero mágico " + numeroMagico + "\t Lucro de hj: " + conta.TodayNet + "\t Qtde de Trades: " + conta.TodayTrades + "\t hora último trade:" + horario);
                            PrintaPosicoesOrdensAtivas();
                            bool passeLivreNoLimbo = false;

                            acao.ForEach(x => Print(x));

                            if (acao.Count > 0)
                            {
                                if (acao.Exists(x => x == Acao.passeLivre))
                                {
                                    { passeLivreNoLimbo = true; Print("Estado Limbo: Saída por PasseLivre"); goto PasseLivre; }                                    
                                }
                                if (acao.Exists(x => x == Acao.eventoOrderRemoved))
                                {
                                    { passeLivreNoLimbo = true; Print("Estado Limbo: Saída por Ordem Removida"); goto PasseLivre; }
                                }
                                if (acao.Exists(x => x == Acao.eventoPositionRemoved))
                                {
                                    { passeLivreNoLimbo = true; Print("Estado Limbo: Saída por Posição Removida"); goto PasseLivre; }
                                }
                                //timeline entrada correta
                                if (
                                    acao.Exists(x => x == Acao.aguardandoPositionAdded) &&
                                    acao.Exists(x => x == Acao.eventoOrderAdded) &&
                                    acao.Exists(x => x == Acao.eventoOrderExecuted) &&
                                    acao.Exists(x => x == Acao.eventoPositionAdded) &&
                                    !acao.Exists(x => x == Acao.aguardandoOrderAdded)
                                    )
                                { passeLivreNoLimbo = true; Print("Estado Limbo: Saída por Entrada Correta"); goto PasseLivre; }

                                //timeline ordem de saída correta
                                if (
                                    !acao.Exists(x => x == Acao.aguardandoPositionAdded) &&
                                    acao.Exists(x => x == Acao.aguardandoOrderAdded) &&
                                    acao.Exists(x => x == Acao.eventoOrderAdded)
                                    )
                                { passeLivreNoLimbo = true; Print("Estado Limbo: Saída por Ordem de Saída Correta"); goto PasseLivre; }                            

                            }

                        PasseLivre:
                            if (passeLivreNoLimbo)
                            {
                                acao.Clear();
                                Juiz();
                            }
                            break;
                        }
                    case state.inicio:
                        {
                            Print("********************   Estado Início  ********************");
                            if (horarioTerminou) { Print("Horário de Término atingido, não entra mais..."); return; }

                            RefreshPositionsOrders();
                            bloqueieNovaAdicaoNoArray = false; tapeReadingPosicoes.Clear(); acao.Clear();  contadorDeReEntradas = 0;                            

                            //if (positions.Length > 0)
                            //{
                            //    estado = state.limbo; acao.Add(Acao.passeLivre);
                            //    tapeReadingPosicoes.Add(new PosicaoAtiva(positions[0].OpenPrice, positions[0].Amount, positions[0].Side, DateTime.Now, "Inicio pré-existente"));
                            //    return;
                            //}


                            Print("Hora: " + horario + "Estado Inicio: Preço atual:" + cotacaoAtual + " Média curta:" + curta + " Ticks da média:" + (Math.Abs(cotacaoAtual - curta) / tickSize));
                            Print("Estado Inicio: Modo de Entrada: Sinal Atual - Anterior = " + (curta - curtaAnterior));
                            if ((Math.Abs(cotacaoAtual - curta) / tickSize) <= distanciaDaMedia && (tipoDeRobo == TipoDeRobo.tendencia || tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                            {
                                double diff = 0;
                                if (mediaContinua) diff = curta - curtaAnterior;
                                if (!mediaContinua) diff = histoResult;

                                bloqueieNovaAdicaoNoArray = false;

                                if (EntradaDiffMedias(cotacaoAtual, diff) != "-1")
                                {
                                    Print("Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo");
                                    estado = state.limbo;
                                    acao.Add(Acao.aguardandoPositionAdded);
                                    return;
                                }
                                else Print("Estado Inicio: Ordem TENDÊNCIA recusada");

                            }
                            else if ((Math.Abs(cotacaoAtual - curta) / tickSize) >= distanciaDaMediaContra && (tipoDeRobo == TipoDeRobo.contraTendencia || tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                            {
                                bloqueieNovaAdicaoNoArray = false;
                                if (EntradaContra(cotacaoAtual) != "-1")
                                {
                                    Print("Estado Inicio: Ordem CONTRA-TENDÊNCIA foi enviada ! Aguardando sua resolução no limbo");
                                    estado = state.limbo;
                                    acao.Add(Acao.aguardandoPositionAdded);
                                    return;
                                }
                                else Print("Estado Inicio: Ordem CONTRA-TENDÊNCIA recusada");
                            }
                            else Print("Estado Inicio: Aguardando condições de entrada");
                            break;
                        }

                    case state.reentrada:
                        {
                            Print("********************   Estado ReEntrada   ***********************");

                            positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
                            orders = Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ToArray();
                            if (positions.Length == 0)
                            {
                                Print("Estado ReEntrada: Não existe mais posição. Migrando para inicio");
                                LimparTodasAsOrdens();
                                tapeReadingPosicoes.Clear();
                                estado = state.limbo;
                                acao.Clear();
                                acao.Add(Acao.passeLivre);                                
                                bloqueieNovaAdicaoNoArray = true;
                                return;
                            }
                            if (orders.Length > 1) { Print("Estado ReEntrada: Tem mais ordem do que o normal. Migrando para o limbo e chamando o Juiz!"); estado = state.limbo; acao.Add(Acao.passeLivre); return; }
                            if (orders.Length == 0) { Print("Estado ReEntrada: Não existe ordem de saída. Migrando para o limbo e chamando o Juiz!"); estado = state.limbo; acao.Add(Acao.passeLivre); return; }

                            if (positions.Length != 0)
                            {
                                Position pos = positions[0];
                                Order ord = orders[0];

                                if (pos == null || ord == null) { Print("POSIÇÃO ou ORDEM NULA NA REENTRADA? ISSO É ERRO! "); estado = state.limbo; acao.Add(Acao.passeLivre); return; }

                                EqualizaOrdemPosicao(pos, ord);
                                acao.Clear();

                                var deltaMoeda = tickSize * ReEntradas[contadorDeReEntradas];
                                var liberaReEntrada = false;
                                Print("Hora:" + horario + "Estado ReEntrada: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize));
                                if (tipoDeSaida == TipoDeSaida.DifMédias)
                                {
                                    if (mediaContinua) Print("Estado ReEntrada: (média - média Anterior) x multiplicador = " + (curta - curtaAnterior) * multiplicador);
                                    if (!mediaContinua) Print("Estado ReEntrada: histograma = " + histoResult);
                                }
                                Print("Estado ReEntrada: Entrada atual = " + contadorDeReEntradas);

                                if (pos.Side == Operation.Buy && pos.CurrentPrice <= ultimaReentrada - deltaMoeda)
                                {
                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                    liberaReEntrada = true;
                                }

                                if (pos.Side == Operation.Sell && pos.CurrentPrice >= ultimaReentrada + deltaMoeda)
                                {
                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                    liberaReEntrada = true;
                                }

                                if (liberaReEntrada)
                                {
                                    Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                                    if (positions[0].Amount <= maxPosAbertas / 2)
                                    {
                                        var result = ReEntrada(cotacaoAtual, positions[0]);
                                        if (result != "-1")
                                        {
                                            Print("Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo.");
                                            estado = state.limbo;
                                            acao.Add(Acao.aguardandoPositionAdded);
                                            if (contadorDeReEntradas < ValoresDeReEntradas.Length - 1) contadorDeReEntradas++;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        Print("Estado ReEntrada: Máximo de Contratos atingidos!");
                                    }
                                }

                                double diff = 0;
                                if (mediaContinua) diff = (curta - curtaAnterior) * multiplicador;
                                if (!mediaContinua) diff = histoResult;

                                if (tipoDeSaida == TipoDeSaida.DifMédias && diff >= resilienciaDeSaida && pos.Side == Operation.Sell)
                                {
                                    Print("Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total");
                                    FechamentoTotal();
                                    acao.Clear();
                                    estado = state.limbo;
                                    acao.Add(Acao.passeLivre);
                                }

                                if (tipoDeSaida == TipoDeSaida.DifMédias && diff <= -resilienciaDeSaida && pos.Side == Operation.Buy)
                                {
                                    Print("Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total");
                                    FechamentoTotal();
                                    acao.Clear();
                                    estado = state.limbo;
                                    acao.Add(Acao.passeLivre);
                                }

                                PrintaPosicoesOrdensAtivas();
                            }
                            break;
                        }



                    default:
                        {
                            Print("Estado Juiz: Nenhum caso foi analisado pelo Juiz. Voltando para o limbo");
                            estado = state.limbo;
                            break;
                        }
                }

            }
            catch (Exception exc)
            {
                Print("NewTrade:  Erro setor switch . Mensagem -> " + exc.Message);
                Print("NewTrade: Erro:  InnerMessage -> " + exc.InnerException.Message);
                return;
            }
        }

        public bool RefreshPositionsOrders()
        {
            try
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel)?.ToArray();
                orders = Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == papel)?.ToArray();
                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshPositionsOrders: Erro no RefreshPositionsOrders. Erro:" + exc.Message);
                return false;
            }
        }

        public bool RefreshIndicators()
        {
            try
            {
                Print("RefreshIndicators: Lendo indicadores...");
                cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
                curta = MM.GetValue(0, 0);
                curtaAnterior = MM.GetValue(0, 1);
                mmAvalanche = MM.GetValue(1, 0);
                mmAvalancheAnterior = MM.GetValue(1, 1);
                histoResult = sniper.GetValue(0, 0);
                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool CheckMarketStatus(Trade trade)
        {
            try
            {
                if (myConnection.Status == ConnectionStatus.Connected) { horario = DateTime.Now.TimeOfDay; hora = horario.Hours; } else { horario = instrument.LastQuote.Time.ToLocalTime().TimeOfDay; hora = horario.Hours; }
                Print("CheckMarketStatus: Hora Atual = " + horario.ToString());
                if (hora == 0) { return false; }
                if (hora >= 9) { horarioTerminou = false; }
                if (horario.CompareTo(HoraDeInicio) < 0) { Print("Aguardando hora de Início..."); return false; }
                if (horario.CompareTo(HoraDeTermino) >= 0) { Print("CheckMarketStatus: Hora de término atingida!"); horarioTerminou = true; }
                if (hora >= 18)
                {
                    horarioTerminou = true;
                    if (Positions.Count > 0) Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); Positions.CloseAll();
                    Print("CheckMarketStatus: Bolsa fechada !");
                    return false;
                }
                return true;
            }
            catch (Exception exc)
            {
                Print("CheckMarketStatus: Erro na coleta da Hora . Erro: " + exc.Message);
                Print("CheckMarketStatus: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public void SaidaPorLossMaximo()
        {
            try
            {
                //SAÍDA POR LOSS MÁXIMO
                if (positions.Length > 0 && tipoDeSaida == TipoDeSaida.LossMaximo)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize) + " Limite: " + lossMaximo + " ticks");

                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize >= lossMaximo)
                        {
                            Print("*****************    LOSS MÁXIMO ATINGIDO    ********************");

                            bool result = false;
                            contadorDeReEntradas = -1;
                            FechamentoTotal();
                            if (result) { Print("Fechando todas as Posições por LOSS MÁXIMO !!!"); estado = state.limbo; acao.Add(Acao.passeLivre); }
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                Print("OnQuote: Erro setor Loss Máximo . " + exc.Message);
                Print("OnQuote: InnerException " + exc.InnerException.Message);
            }
        }



        public bool CalculaCalorMaximo()
        {
            try
            {
                if (positions.Length > 0)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        if (pos.GetProfitNet() < calor) { calor = pos.GetProfitNet(); horaDoCalorMaximo = hora.ToString(); }
                        if (Math.Abs(pos.Amount) > maximoDeContratosAbertos) { maximoDeContratosAbertos = pos.Amount; }
                    }
                }
                Print("CalculaCalorMaximo: Calor Máximo até o momento: " + calor);
                return true;
            }
            catch (Exception exc)
            {
                Print("CalculaCalorMaximo: Erro setor Calor Máximo . " + exc.Message);
                Print("CalculaCalorMaximo: InnerException " + exc.InnerException.Message);
                return false;
            }
        }



        public override void OnQuote()
        {
            if (myConnection.Status == ConnectionStatus.Disconnected)
            {
                //ou seja, está no simulador onde não tem OnTrades...
                Instruments_NewTrade(instrument, instrument.GetLastTrade());
            }

        }

        public override void NextBar()
        {
            //if (estado == state.limbo) passeLivreNoLimbo = true;
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
            Positions.PositionAdded -= Positions_PositionAdded;
            Positions.PositionRemoved -= Positions_PositionRemoved;
            Instruments.NewTrade -= Instruments_NewTrade;
            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;

        }


        private string EntradaDiffMedias(double preco, double diff)
        {
            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
            return "-1";
        }

        private string EntradaContra(double preco)
        {
            if (preco > curta + distanciaDaMediaContra * tickSize) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
            if (preco < curta - distanciaDaMediaContra * tickSize) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
            return "-1";
        }

        private string ReEntrada(double preco, Position pos)
        {
            var precoMedio = pos.OpenPrice;
            var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];

            if (pos.Side == Operation.Buy)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, ValoresDeReEntradas[contadorDeReEntradas], 0, "reEntrada mercado");
            }

            if (pos.Side == Operation.Sell)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, pos.Amount, 0, "reEntrada mercado");
            }

            return "-1";
        }


        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit, string comment = "entrada mercado")
        {
            price = instrument.RoundPrice(price);

            NewOrderRequest request = new NewOrderRequest();
            request.Instrument = instrument;
            request.Account = conta;
            request.Type = type;
            request.Side = side;
            request.Amount = contratos;
            request.MarketRange = marketRange;
            request.MagicNumber = numeroMagico;
            request.Comment = comment;

            Quote quote = instrument.LastQuote;
            if (quote != null)
            {
                price = (side == Operation.Buy) ? quote.Ask : quote.Bid;

                if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? instrument.RoundPrice(price + takeProfit * instrument.TickSize) : instrument.RoundPrice(price - takeProfit * instrument.TickSize);
                if (type != OrdersType.StopLimit) { request.Price = instrument.RoundPrice(price); } else { request.StopPrice = instrument.RoundPrice(price); }

                Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + instrument.RoundPrice(price) + " Qtde:" + request.Amount + " Ponto Atual da Média:" + curta);
                var result = Orders.Send(request);
                if (result == "-1") { Print("SendOrder: Ordem não aceita pela corretora"); }
                if (result != "-1") { Print("SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!"); }
                return result;
            }

            return "-1";

        }

        private void LimparTodasAsOrdens()
        {
            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord =>
             {
                 var result = ord.Cancel();
                 if (!result)
                 {
                     Alert("LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA! Status: " + ord.Status);
                 }
                 else
                 {
                     Print("LimparTodasAsOrdens: Ordem " + ord.Id + " FOI CANCELADA! Status: " + ord.Status);
                 }
             });
        }

        private bool CriaParDeSaida(Position posicao = null, string comment = "saida")
        {
            //TAKE PROFIT LIMIT 
            Position[] positions;
            if (posicao == null)
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
                if (positions.Length > 0) posicao = positions[0];
            }

            if (posicao != null)
            {
                Print("CriaParDeSaida: Ok! Tentando setar TAKE PROFIT LIMIT");
                try
                {
                    Position pos = posicao;
                    if (pos.Side == Operation.Buy)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = marketRange;
                        request.Comment = comment;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            Print("CriaParDeSaida: Ordem Limit de saída SELL não colocada... tentando ordem STOP");
                            NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = marketRange;
                            req.Comment = comment;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1")
                            {
                                Print("CriaParDeSaida: Ordem SELL STOP não aceita!");
                                LimparTodasAsOrdens();
                                return false;
                            }
                            else { return true; }
                        }
                        else { return true; }
                    }
                    if (pos.Side == Operation.Sell)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = marketRange;
                        request.Comment = comment;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            Print("CriaParDeSaida: Ordem Limit de saída BUY não colocada... tentando ordem STOP");
                            NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = marketRange;
                            req.Comment = comment;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1") { Print("CriaParDeSaida: Ordem BUY STOP não aceita!"); LimparTodasAsOrdens(); return false; } else { return true; }
                        }
                        else { return true; }
                    }
                }
                catch (Exception ext)
                {
                    Print("CriaParDeSaida: Erro ao Setar Take Profit! " + ext.Message);
                    return false;
                }
            }
            else
            {
                Print("CriaParDeSaída: Não existe posição");
                LimparTodasAsOrdens();
            }

            return false;
        }

        private bool EqualizaOrdemPosicao(Position pos, Order ord)
        {
            double price = 0;
            price = (pos.Side == Operation.Buy) ? instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize) : instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize);
            Print("EqualizaOrdemPosição: Qtde Pos: " + pos.Amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price);

            if (pos.Amount != ord.Amount)
            {
                Print("EqualizaOrdemPosição: Ordem com diferença de quantidade em relação à posição. Equalizando...");
                solicitouEqualizacao = true;
                return ord.Modify(price, pos.Amount);
            }

            if (Math.Abs(ord.Price - price) > TP * tickSize)
            {
                Print("EqualizaOrdemPosição: Ordem com preço de saída incorreto. Equalizando...");
                solicitouEqualizacao = true;
                return ord.Modify(price);
            }
            return false;
        }


        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = Positions.GetPositions();
            Order[] ordensGlobais = Orders.GetOrders(true);
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => Funcoes.PosInfo(pos, this)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => Funcoes.OrderInfo(ord, this)); }
        }

        public void FechamentoParcial()
        {
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
        }

        public void FechamentoTotal()
        {
            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");
            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord => ord.Cancel());
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close());
        }

        private void Juiz()
        {
            Print("********************   Estado Juiz ***********************");
            orders = Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ToArray();
            positions = Positions.GetPositions()?.ToList().FindAll(posit => posit.Instrument.BaseName == papel).ToArray();
            if (positions == null) positions = new Position[0];
            if (orders == null) orders = new Order[0];

            if (orders?.Length > 1)
            {
                Alert("Existe mais ordens do que o normal!");
                LimparTodasAsOrdens();
                estado = state.limbo;
                acao.Add(Acao.passeLivre);
                return;
            }

            PrintaPosicoesOrdensAtivas();

            //Caso 1: Não existe posição e Não existe ordem
            if (positions.Length == 0 && orders.Length == 0)
            {
                //como chegou um caso assim para o juiz analisar? É impossível pois a migração de estados na entrada e na reentrada apenas ocorrem se a corretora aceitar as ordens
                //mesmo assim, é melhor prever isso
                Print("Estado Juiz: Não existe Posição e Não existe Ordem para ser processada. estado -> inicio");
                estado = state.inicio;
                acao.Clear();
                return;
            }

            //Caso 2: Não existe posição, e ordem tem operação pendente
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ord.Status == OrderStatus.PendingNew || ord.Status == OrderStatus.PendingReplace || ord.Status == OrderStatus.PendingCancel || ord.Status == OrderStatus.PendingExecution).Count != 0)
            {
                Print("Estado Juiz: Existe Ordem com operação pendente ainda... aguarde ela definir o que vai fazer");
                estado = state.limbo;
                acao.Add(Acao.passeLivre);
                return;
            }

            //Case 3: Não existe posição e Existe ordem aguardando execução
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew))).Count != 0)
            {
                Print("Estado Juiz: Não existe posição E uma ordem qualquer ainda não foi executada.");
                LimparTodasAsOrdens();
                estado = state.limbo;
                acao.Add(Acao.passeLivre);
                return;
            }

            //Case 4: Existe posição , não existe ordem de saída 
            if (positions.Length != 0 && orders.Length == 0)
            {
                Print("Estado Juiz: Existe posição mas não existe ordem de saída. CriaParDeSaída()");
                if (CriaParDeSaida(null, "ordem limite"))
                {
                    estado = state.limbo; acao.Add(Acao.aguardandoOrderAdded);
                }
                else
                {
                    Print("Estado Juiz: Erro na Criação do Par de Saída!");
                    estado = state.limbo;
                    acao.Add(Acao.passeLivre);
                }
                return;
            }


            //Case 5: Existe Posição e Existe Ordem de Saída e modo avalanche é falso
            if (positions.Length != 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced))).Count != 0)
            {
                Print("Estado Juiz: Existe posição e Existe ordem de saída. Migrando para estado ReEntrada");
                estado = state.reentrada;
                acao.Clear();
                return;
            }

            //padrão
            estado = state.limbo;
            acao.Clear();
            acao.Add(Acao.passeLivre);
        }




        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                Graphics gr = args.Graphics;
                Rectangle rect = args.Rectangle;
                RefreshPositionsOrders();

                if (positions.Length > 0 && args.Rectangle.Height > 350)
                {
                    gr.DrawString("Posição", font, brush, rect.X + 110, rect.Y + 20);
                    gr.DrawString("Qtde", font, brush, rect.X + 210, rect.Y + 20);
                    gr.DrawString("Preço Médio", font, brush, rect.X + 310, rect.Y + 20);
                    gr.DrawString("Preço Atual", font, brush, rect.X + 410, rect.Y + 20);
                    gr.DrawString("Tipo", font, brush, rect.X + 510, rect.Y + 20);
                    gr.DrawString("Lucro/Prejuízo", font, brush, rect.X + 610, rect.Y + 20);
                    gr.DrawString("Comentário", font, brush, rect.X + 710, rect.Y + 20);

                    gr.DrawString("Protrader ->", font, brush, rect.X + 10, rect.Y + 40);
                    gr.DrawString("Robô ->", font, brush, rect.X + 10, rect.Y + 60);
                    gr.DrawLine(new Pen(Color.White), new PointF(rect.X + 110, rect.Y + 80), new PointF(rect.X + 610, rect.Y + 80));

                    gr.DrawString(positions[0].Id, font, brush, rect.X + 110, rect.Y + 40);
                    gr.DrawString(positions[0].Amount.ToString(), font, brush, rect.X + 210, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].OpenPrice), font, brush, rect.X + 310, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].CurrentPrice), font, brush, rect.X + 410, rect.Y + 40);
                    gr.DrawString(positions[0].Side.ToString(), font, brush, rect.X + 510, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].GetProfitGross().ToString()), font, brush, rect.X + 610, rect.Y + 40);
                    gr.DrawString(positions[0].Comment.ToString(), font, brush, rect.X + 710, rect.Y + 40);

                    if (tapeReadingPosicoes.Count > 0 && posicaoAtivaMedia != null)
                    {
                        string precoMedioString = String.Format("{0:C}", posicaoAtivaMedia._openPrice);
                        double pontos = (posicaoAtivaMedia._side == Operation.Buy) ? Math.Round(((cotacaoAtual - posicaoAtivaMedia._openPrice) / tamanhoPonto), 2) : -Math.Round(((cotacaoAtual - posicaoAtivaMedia._openPrice) / tamanhoPonto), 2);

                        gr.DrawString(tapeReadingPosicoes.Count.ToString(), font, brush, rect.X + 110, rect.Y + 60);
                        gr.DrawString(posicaoAtivaMedia._amount.ToString(), font, brush, rect.X + 210, rect.Y + 60);
                        gr.DrawString(precoMedioString, font, brush, rect.X + 310, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", pontos.ToString()), font, brush, rect.X + 410, rect.Y + 60);
                        gr.DrawString(posicaoAtivaMedia._side.ToString(), font, brush, rect.X + 510, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", posicaoAtivaMedia._lucro.ToString()), font, brush, rect.X + 610, rect.Y + 60);
                        DrawPositions(args.Graphics, args.Rectangle);
                    }
                }

            }
            catch (Exception exc)
            {
                Print("OnPaintChart Erro " + exc.Message);
            }
        }


        private void DrawPositions(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));

            int i = 100;
            try
            {
                Operation tipoBase = tapeReadingPosicoes.First()._side;

                tapeReadingPosicoes.ForEach(posicao =>
                {
                    gr.DrawString(posicao._openTime.ToLocalTime().ToShortTimeString(), font, brush, rect.X + 110, rect.Y + i);
                    gr.DrawString(posicao._amount.ToString(), font, brush, rect.X + 210, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._openPrice), font, brush, rect.X + 310, rect.Y + i);
                    gr.DrawString(posicao._pontos.ToString(), font, brush, rect.X + 410, rect.Y + i);
                    gr.DrawString(posicao._side.ToString(), font, brush, rect.X + 510, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._lucro.ToString()), font, brush, rect.X + 610, rect.Y + i);
                    gr.DrawString(posicao._comments, font, brush, rect.X + 710, rect.Y + i);

                    i += 15;
                });

            }
            catch (Exception exc)
            {
                Print("DrawPositions: Erro " + exc.Message);
                Print("DrawPositions: Erro Intero " + exc.InnerException.Message);
            }
        }

    }

}
