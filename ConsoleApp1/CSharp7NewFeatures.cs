﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using API;

namespace ConsoleApp1
{
    class CSharp7NewFeatures
    {
        public void inlineOut()
        {
            //old way
            Console.WriteLine("What is your Age?");
            string ageText = Console.ReadLine();
            int age = 0;
            bool isValid = int.TryParse(ageText, out age);
            Console.WriteLine($"Age:{age} is {isValid}");

            //newWay
            bool isValidAge = int.TryParse(ageText, out int newAge);
            Console.WriteLine($"Age:{newAge} is {isValid}");
        }

        public void PatternMatch()
        {
            Console.WriteLine("What is your Age?");
            string ageFromConsole = Console.ReadLine();            
            object ageVal = ageFromConsole;

            if (ageVal is int age || (ageVal is string ageText && int.TryParse(ageText, out age)))
            {
                Console.WriteLine($"Your age is {age}");
            } else
            {
                Console.WriteLine("I don't know your age");
            }
        }
        
        private class Employee { public string name; public bool IsManager = false; }
        private class Customer { public string name; public decimal TotalSpent; }

        public void PowerfulSwitch()
        {
            Employee emp1 = new Employee { name = "Joe", IsManager = false};
            Employee emp2 = new Employee { name = "Darth", IsManager = true };
            Customer cust1 = new Customer { name = "Tim", TotalSpent = 100M };
            Customer cust2 = new Customer { name = "Sarah", TotalSpent = 50M };
            List<object> people = new List<object> { emp1, emp2, cust1, cust2 };
            foreach (var p in people)
            {
                switch (p)
                {
                    case Employee e when (e.IsManager == false):
                        Console.WriteLine($"{e.name} is a good employee.");
                        break;
                    case Employee e when (e.IsManager):
                        Console.WriteLine($"{e.name} runs this company.");
                        break;
                    case Customer c when (c.TotalSpent >= 100):
                        Console.WriteLine($"{c.name} is a loyal customer.");
                        break;
                    case Customer c:
                        Console.WriteLine($"{c.name} needs to spend more money.");
                        break;
                    default:
                        break;
                }
            }
        }

        public void InlineThrowExceptions()
        {
            Employee emp1 = new Employee { name = "Joe", IsManager = false };
            Employee emp2 = new Employee { name = "Darth", IsManager = true };
            List<Employee> people = new List<Employee> { emp1, emp2 };
            Employee ceo = people.Where(x => x.IsManager).FirstOrDefault() ?? throw new Exception("There is no Manager");
            Console.WriteLine($"CEO name is {ceo.name}");
        }

        public void Tuples()
        {
            //tuple is an object with multiple values
            //(string firstName, string lastName) name = SplitName("Camilo Chaves");
            var name = SplitName("Camilo Chaves");
            Console.WriteLine($"FirstName: {name.firstName}  LastName: {name.lastName}");
        }

        private (string firstName, string lastName) SplitName(string fullName)
        {
            string[] vals = fullName.Split(' ');
            return (vals[0], vals[1]);
        }

        public async Task<string> ME()
        {
            HttpClient client = new HttpClient();

            using (var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:3003/api/users/me"))
            {
                request.Headers.Add("x-auth-token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzRjYjNhOWRlYWEwMzcxMTg1MzRhZTYiLCJpYXQiOjE1NDg1MzA2MDF9.oVbR3r82Zt64h1oTggU6NYjJyakSP6sDCw4MiGkqfTk");
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = content };

                    return content;
                }
            }
        }        

    }
}
