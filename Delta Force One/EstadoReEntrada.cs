﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace Delta_Force_One
{
    public class EstadoReEntrada : Estado
    {
        private DollarEA robo;
        private string log;

        public EstadoReEntrada(DollarEA _robo) { robo = _robo; log = robo.log; }

        private bool ReEntradasMaximasAtingidas(int contador) => contador > robo.ValoresDeReEntradas.Length - 1;
        private bool CondicaoReEntradaBuyAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Buy && pos.CurrentPrice <= robo.positions[0].OpenPrice - delta;
        private bool CondicaoReEntradaSellAtingida(Position pos, double delta) => PassouTestePosicaoVazia() && pos.Side == Operation.Sell && pos.CurrentPrice >= robo.positions[0].OpenPrice + delta;
        private bool ContratosAbaixoDoMaximo() => PassouTestePosicaoVazia() && robo.positions[0].Amount + robo.ValoresDeReEntradas[robo.contadorDeReEntradas] <= robo.maxPosAbertas;
        private bool CondicaoDeSaidaScalperSellAtingida(Position pos) => PassouTestePosicaoVazia() && robo.tipoDeSaida == TipoDeSaida.DifMédias && GetDiffMedias() >= robo.resilienciaDeSaida && pos.Side == Operation.Sell;
        private bool CondicaoDeSaidaScalperBuyAtingida(Position pos) => PassouTestePosicaoVazia() && robo.tipoDeSaida == TipoDeSaida.DifMédias && GetDiffMedias() <= -robo.resilienciaDeSaida && pos.Side == Operation.Buy;
        private bool RoboTipoScalper() => robo.tipoDeSaida == TipoDeSaida.DifMédias;
        private bool MetadeDaDistEntreSuporteATRSuperiorAtingido() => (robo.cotacaoAtual >= ((robo.ATRsuperior - robo.suporte) / 2) + robo.suporte) ? true : false;
        private bool MetadeDaDistEntreSuporteATRInferiorAtingido() => (robo.cotacaoAtual <= robo.suporte - ((robo.suporte - robo.ATRinferior) / 2)) ? true : false;
        private bool MercadoForaDaFaixaATRsup() => (robo.cotacaoAtual >= robo.ATRsuperior);
        private bool MercadoForaDaFaixaATRinf() => (robo.cotacaoAtual <= robo.ATRinferior);
        private bool MercadoDentroDaFaixaATR() => (robo.cotacaoAtual <= robo.ATRsuperior && robo.cotacaoAtual >= robo.ATRinferior);
        private bool PrimeiraReEntradaDinamicaSuperior() => (robo.contadorReEntradasDinamicas == 0 && robo.positions[0].Side == Operation.Sell && MercadoDentroDaFaixaATR() && MetadeDaDistEntreSuporteATRSuperiorAtingido()) ? true : false;
        private bool PrimeiraReEntradaDinamicaInferior() => (robo.contadorReEntradasDinamicas == 0 && robo.positions[0].Side == Operation.Buy && MercadoDentroDaFaixaATR() && MetadeDaDistEntreSuporteATRInferiorAtingido()) ? true : false;
        private bool SegundaReEntradaDinamicaSuperior() => (robo.contadorReEntradasDinamicas == 1 && robo.positions[0].Side == Operation.Sell && MercadoForaDaFaixaATRsup()) ? true : false;
        private bool SegundaReEntradaDinamicaInferior() => (robo.contadorReEntradasDinamicas == 1 && robo.positions[0].Side == Operation.Buy && MercadoForaDaFaixaATRinf()) ? true : false;

        private double GetDiffMedias() => robo.mediaContinua ? (robo.curta - robo.curtaAnterior) * robo.multiplicador : robo.histoResult;



        private bool PassouTestePosicaoVazia()
        {
            if (robo.positions.Length == 0)
            {
                log = "Estado ReEntrada: Não existe mais posição. Migrando para inicio";
                robo.Print(log);
                robo.funcoes.LimparTodasAsOrdens();
                robo.estado = State.inicio;
                return false;
            }
            return true;
        }

        private bool PreCondicoesSatisfeitas()
        {
            //TESTES PARA DESVIO DE EXECUÇÃO
            //SE NÃO TEM POSIÇÃO, NÃO TEM PQ ESTAR AQUI
            if (!PassouTestePosicaoVazia()) return false;

            //SE NÃO TEM ORDEM DE SAÍDA
            if (robo.orders.Length == 0)
            {
                //MAS TEM POSIÇÃO
                if (robo.positions.Length > 0)
                {
                    //TEM POSIÇÃO E NÃO TEM ORDEM DE SAÍDA: CRIA ORDEM DE SAÍDA
                    log = "Estado ReEntrada: Ordem de saída zero, e tem posição! Criando Par de Saída";
                    robo.Print(log);
                    robo.comando.CriaParDeSaida();
                    return false;
                }
                else
                {
                    //SE NÃO TEM POSIÇÃO E NÃO TEM ORDEM: MIGRA PRO INÍCIO
                    log = "Estado ReEntrada: Sem ordem e sem posição? Migra pro inicio...";
                    robo.Print(log);
                    robo.estado = State.inicio;
                    return false;
                }
            }

            return true;
        }

        private bool PassouTestesDeBloqueio()
        {
            //TESTES DE BLOQUEIO DE EXECUÇÃO
            if (ReEntradasMaximasAtingidas(robo.contadorDeReEntradas))
            {
                if (robo.positions.Length > 0)
                {
                    //TENTA EQUALIZAR UMA ÚLTIMA VEZ 
                    try
                    {
                        Position pos = robo.positions[0];
                        Order ord = null;
                        if (robo.orders.Length > 0) ord = robo.orders[0];
                        robo.funcoes.EqualizaOrdemPosicao(pos, ord);
                    }
                    catch (Exception ex)
                    {
                        robo.Print("Erro na última equalização");
                    }
                }
                robo.Print("Estado ReEntrada: Máximo número de ReEntradas foi atingido!");
                return false;
            }
            if (!ContratosAbaixoDoMaximo()) { robo.Print("Estado ReEntrada: Não pode abrir mais contratos pois irá estourar o máximo permitido!"); return false; }
            return true;
        }

        private bool PassouTestesDeSaida()
        {
            //TESTES DE SAÍDA POR DIFERENÇA DE MÉDIAS SE SCALPER                        

            if (CondicaoDeSaidaScalperSellAtingida(robo.positions[0]) || CondicaoDeSaidaScalperBuyAtingida(robo.positions[0]))
            {
                log = robo.horario + "Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                robo.Print(log);
                robo.funcoes.FechamentoTotal();
                robo.estado = State.lockdown;
                return false;
            }

            return true;
        }

        public override void Run()
        {
            try
            {
                robo.Print("********************   Estado ReEntrada   ***********************");

                if (!PreCondicoesSatisfeitas()) { robo.Print("Estado ReEntrada: Pré-Condições não satisfeitas!"); return; }
                if (!PassouTestesDeSaida()) { robo.Print("Estado ReEntrada: Uma condição de saída foi atingida!"); return; }
                if (!PassouTestesDeBloqueio()) { robo.Print("Estado ReEntrada: Existe alguma condição de bloqueio!"); return; }

                robo.Print($"Estado ReEntrada --> Mercado Dentro ATR: {MercadoDentroDaFaixaATR()} MercadoForaATRSup: {MercadoForaDaFaixaATRsup()}  MercadoForaATRInf: {MercadoForaDaFaixaATRinf()}");
                robo.Print($"Estado ReEntrada --> Metade da Dist Superior ao ATR Atingida: {MetadeDaDistEntreSuporteATRSuperiorAtingido()} , Metade da Dist Inferior ao ATR Atingida: {MetadeDaDistEntreSuporteATRInferiorAtingido()} ");


                Position pos = robo.positions[0];
                Order ord = null;
                if (robo.orders.Length > 0) ord = robo.orders[0];

                //INÍCIO LÓGICA DE REENTRADAS
                robo.funcoes.EqualizaOrdemPosicao(pos, ord);                
                double deltaMoeda = robo.instrument.TickSize * robo.ReEntradas[robo.contadorDeReEntradas] + (robo.ATRsuperior - robo.ATRinferior) / 2;
                robo.proximaReEntrada = (pos.Side == Operation.Buy) ? pos.OpenPrice - deltaMoeda : pos.OpenPrice + deltaMoeda;                
                var liberaReEntrada = false;
                robo.Print($"Hora: {robo.horario} Estado ReEntrada: (Preço Atual - Preço Abertura)/tick =  {(Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize)}");

                if (RoboTipoScalper())
                {
                    if (robo.mediaContinua) robo.Print($"Estado ReEntrada: (média - média Anterior) x multiplicador = {(robo.curta - robo.curtaAnterior) * robo.multiplicador}");
                    else robo.Print($"Estado ReEntrada: histograma = {robo.histoResult}");
                }
                robo.Print($"Estado ReEntrada: Entrada atual = {robo.contadorDeReEntradas}");

                if (robo.DinReEntradas) //SE REENTRADAS DINÂMICAS ESTIVEREM SETADAS
                {
                    robo.Print($"Checando Segunda ReEntrada DINÂMICA Superior: {SegundaReEntradaDinamicaSuperior()}");
                    if (SegundaReEntradaDinamicaSuperior())
                    {
                        if (robo.comando.Send_order(OrdersType.Market, Operation.Sell, 0, robo.valoresDeReentrada[1], 0) != "-1")
                        {
                            log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada DINÂMICA foi emitida. Migrando para o limbo.";
                            robo.Print(log);
                            robo.estado = State.lockdown;
                            robo.contadorReEntradasDinamicas++;
                            return;
                        }

                    }
                    robo.Print($"Checando Segunda ReEntrada DINÂMICA Inferior: {SegundaReEntradaDinamicaInferior()}");
                    if (SegundaReEntradaDinamicaInferior())
                    {
                        if (robo.comando.Send_order(OrdersType.Market, Operation.Buy, 0, robo.valoresDeReentrada[1], 0) != "-1")
                        {
                            log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada DINÂMICA foi emitida. Migrando para o limbo.";
                            robo.Print(log);
                            robo.estado = State.lockdown;
                            robo.contadorReEntradasDinamicas++;
                            return;
                        }
                    }
                    robo.Print($"Checando Primeira ReEntrada DINÂMICA Superior: {PrimeiraReEntradaDinamicaSuperior()} , Região Metade do ATR Sup: {((robo.ATRsuperior - robo.suporte) / 2) + robo.suporte}");
                    if (PrimeiraReEntradaDinamicaSuperior())
                    {
                        if (robo.comando.Send_order(OrdersType.Market, Operation.Sell, 0, robo.valoresDeReentrada[0], 0) != "-1")
                        {
                            log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada DINÂMICA foi emitida. Migrando para o limbo.";
                            robo.Print(log);
                            robo.estado = State.lockdown;
                            robo.contadorReEntradasDinamicas++;
                            return;
                        }

                    }
                    robo.Print($"Checando Primeira ReEntrada DINÂMICA Inferior: {PrimeiraReEntradaDinamicaInferior()}, Região Metade do ATR inf: {robo.suporte - ((robo.suporte - robo.ATRinferior) / 2)} ");
                    if (PrimeiraReEntradaDinamicaInferior())
                    {
                        if (robo.comando.Send_order(OrdersType.Market, Operation.Buy, 0, robo.valoresDeReentrada[0], 0) != "-1")
                        {
                            log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada DINÂMICA foi emitida. Migrando para o limbo.";
                            robo.Print(log);
                            robo.estado = State.lockdown;
                            robo.contadorReEntradasDinamicas++;
                            return;
                        }
                    }
                }

                if (CondicaoReEntradaBuyAtingida(pos, deltaMoeda) || CondicaoReEntradaSellAtingida(pos, deltaMoeda)) liberaReEntrada = true;

                if (liberaReEntrada)
                {
                    robo.Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                    if (robo.comando.ReEntrada(robo.cotacaoAtual, robo.positions[0]) == "-1") { robo.Print("Estado ReEntrada: ReEntrada Negada pela Corretora!"); return; }
                    log = $"{robo.horario} Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo.";
                    robo.Print(log);
                    robo.estado = State.lockdown;
                    robo.contadorDeReEntradas++;
                    return;
                }

                robo.funcoes.PrintaPosicoesOrdensAtivas();
            }
            catch (Exception ex)
            {
                robo.Print($"Estado ReEntrada: Erro {ex.Message}");
                robo.Print($"Estado ReEntrada: Erro Interno {ex.InnerException.Message}");
            }
        }
    }
}
