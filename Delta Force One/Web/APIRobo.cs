﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delta_Force_One.Web
{
    public class APIRobo
    {
        private DollarEA robo;
        public APIRobo(DollarEA robo) => this.robo = robo;

        public void GET_Comandos()
        {
            if (robo.comandoNet != null)
            {
                if (robo.token != "")
                {
                    Task<string> TaskGetComandos = robo.api.GET("/comandos", robo.token);
                    TaskGetComandos.ContinueWith(myTask =>
                    {
                        string json = myTask.Result;
                        robo.comandoNet = JsonHelper.DeSerializar<ComandoNET>(json);
                        robo.comandosTela.Clear();
                        robo.comandosTela.Add($"EsconderComandos = {robo.comandoNet.EsconderComandos}");
                        robo.comandosTela.Add($"EsconderTimeline = {robo.comandoNet.EsconderTimeline}");
                        robo.comandosTela.Add($"EsconderLog = {robo.comandoNet.EsconderLog}");
                        robo.comandosTela.Add($"EsconderParâmetros = {robo.comandoNet.EsconderParametros}");
                        robo.comandosTela.Add($"Timeline Completa = {robo.comandoNet.TimelineCompleta}");
                        robo.comandosTela.Add($"Fechar Posições = {robo.comandoNet.FecharPosicoes}");
                        robo.comandosTela.Add($"Bloquear Compras = {robo.comandoNet.BloquearCompras}");
                        robo.comandosTela.Add($"Bloquear Vendas = {robo.comandoNet.BloquearVendas}");
                        robo.comandosTela.Add($"Bloqueio Total = {robo.comandoNet.BloquearNovasOrdens}");
                        robo.comandosTela.Add($"Finalizar Robô = {robo.comandoNet.PararRobo}");
                    });
                }
            }
        }

        public async Task<bool> GET_Parametros(string nome, string papel)
        {
            try
            {
                if (robo.token != "")
                {
                    string json = await robo.api.GET("/parametros", robo.token, nome, papel);
                    if (json != "")
                    {
                        robo.parametrosAtivos = JsonHelper.DeSerializar<Parametro>(json);
                        return true;
                    }
                    return false;
                }
                return false;
            } catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> GET_ParametrosAtivos(string nome, string papel)
        {
            try
            {
                if (robo.token != "")
                {
                    string json = await robo.api.GET("/parametrosAtivos", robo.token, nome, papel);
                    if (json != "")
                    {
                        robo.parametrosAtivos = JsonHelper.DeSerializar<Parametro>(json);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DELETE_ParametrosAtivos(string nome, string papel)
        {
            try
            {
                if (robo.token != "")
                {
                    string result = await robo.api.DELETE("/parametrosAtivos", robo.token, nome, papel);
                    robo.Print(result);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async void POST_ParametrosAtivos(Parametro parametro)
        {
            if (robo.token != "")
            {
                string json = JsonHelper.Serializar<Parametro>(parametro);
                if (json != "")
                {
                    try
                    {
                        await robo.api.POST("/parametrosAtivos", json, robo.token);
                    } catch (Exception ex)
                    {
                        robo.Alert(ex.InnerException.Message);
                    }
                }
            }
        }

        public async void PUT_ParametrosAtivos(Parametro parametro)
        {
            if (robo.token != "")
            {
                string json = JsonHelper.Serializar<Parametro>(parametro);
                if (json != "")
                {
                    try
                    {
                        await robo.api.PUT("/parametrosAtivos", json, robo.token);
                    }
                    catch (Exception ex)
                    {
                        robo.Alert(ex.InnerException.Message);
                    }
                }
            }
        }


    }
}
