﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Delta_Force_One.Web
{
    [DataContract]
    public class Usuario
    {
        //[DataMember]
        //public string _id;
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Sobrenome { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Senha { get; set; }
    }

    [DataContract]
    public class Parametro
    {
        [DataMember]
        public string nome = "";
        [DataMember]
        public string papel = "";
        [DataMember]
        public int periodoMediaMovelSinal = 2;
        [DataMember]
        public int periodoMediaMovelSuporte = 20;
        [DataMember]
        public int periodoMediaMovelBloqueio = 75;
        [DataMember]
        public bool mediaContinua = true;
        [DataMember]
        public double deltaSuporte = 20;
        [DataMember]
        public double multiplicadorATR = 1.5;
        [DataMember]
        public double maxPosAbertas = 32;
        [DataMember]
        public int Contratos = 1;
        [DataMember]
        public string ticksDeReentrada = "10,15,20,35,50";
        [DataMember]
        public string valoresDeReentrada = "1,2,4,8,16";
        [DataMember]
        public double distanciaDaMedia = 20;
        [DataMember]
        public double TP = 2;
        [DataMember]
        public double multiplicador = 200;
        [DataMember]
        public double resilienciaDeSaida = 180;
        [DataMember]
        public double lossMaximo = 100;
        [DataMember]
        public int marketRange = 10;        
        [DataMember]
        public double tamanhoPonto = 5;
        [DataMember]
        public double lateralidade = 0;
        [DataMember]                
        public int qtdeLevel2 = 30;
        [DataMember]
        public double mulTPdin = 0.05;
        [DataMember]
        public double TPdinMin = 2;
        [DataMember]
        public double contratosSaidaSeguranca = 32;
        [DataMember]
        public double ticksSaidaSeguranca = 5;        
    }


    [DataContract]
    public class Colecao
    {
        [DataMember]
        public List<Parametro> colecao = new List<Parametro>();
    }
       
    public class OrdemExecutada
    {
        public int IdNegociacao { get; set; }
        public int IdOrdem { get; set; }
        public string Data { get; set; }
        public string Ativo { get; set; }
        public string Lado { get; set; }
        public double Qtde { get; set; }
        public decimal Preco { get; set; }
        public decimal LucroBruto { get; set; }
        public decimal Taxa { get; set; }
        public decimal LucroLiquido { get; set; }
        public decimal Descontos { get; set; }
    }

    class Conexao
    {
        public string Status { get; set; }
    }

    class Conta
    {
        public decimal Equity { get; set; }
        public decimal BalancoInicial { get; set; }
        public decimal LiquidoDeHoje { get; set; }
        public bool IsDemo { get; set; }
        public bool IsReal { get; set; }
        public string Status { get; set; }
        public string StopReason { get; set; }
    }

    class PosicaoAtiva
    {
        public string Papel { get; set; }
        public string Lado { get; set; }
        public double Qtde { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public int MagicNumber { get; set; }
        public string Comment { get; set; }
    }

    class OrdemAtiva
    {
        public string Papel { get; set; }
        public string Lado { get; set; }
        public double Qtde { get; set; }
        public string Tipo { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
        public int MagicNumber { get; set; }
        public string Comment { get; set; }
    }


    [DataContract]
    public class ComandoNET
    {
        private DollarEA robo;
        [DataMember]
        public bool FecharPosicoes = false;
        [DataMember]
        public bool LimparOrdens = false;
        [DataMember]
        public bool BloquearVendas = false;
        [DataMember]
        public bool BloquearCompras = false;
        [DataMember]
        public bool BloquearNovasOrdens = false;
        [DataMember]
        public bool PararRobo = false;
        [DataMember]
        public bool TimelineCompleta = false;
        [DataMember]
        public bool EsconderParametros = true;
        [DataMember]
        public bool EsconderLog = false;
        [DataMember]
        public bool EsconderTimeline = false;
        [DataMember]
        public bool EsconderComandos = true;
    }

    class StatusRobo
    {
        public string EstadoAtual { get; set; }
        public int ContadorDeReEntradas { get; set; }
    }

}

