﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Delta_Force_One.Web
{
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }
        public string Content { get; set; }
    }

    public class JsonHelper
    {
        public static string Serializar<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        public static string Serializar<T>(T obj, ref MemoryStream ms)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            serializer.WriteObject(ms, obj);
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        public static T DeSerializar<T>(string json)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            return (T)ser.ReadObject(ms);
        }
    }


    public class APIfunc
    {
        private readonly HttpClient client = new HttpClient();
        private const string Url = "http://159.65.238.114:3000/api";

        public void InitializeClient()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> GET(string action, string token = "", string nome = "", string papel = "")
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, Url + action))
            {
                if (token != "") request.Headers.Add("x-auth-token", token);
                if (nome != "") request.Headers.Add("nome", nome);
                if (papel != "") request.Headers.Add("papel", papel);
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = content };

                    return content;
                }
            }
        }

        public async Task<string> DELETE(string action, string token = "", string nome = "", string papel = "")
        {
            using (var request = new HttpRequestMessage(HttpMethod.Delete, Url + action))
            {
                if (token != "") request.Headers.Add("x-auth-token", token);
                if (nome != "") request.Headers.Add("nome", nome);
                if (papel != "") request.Headers.Add("papel", papel);
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = content };

                    return content;
                }
            }
        }

        public async Task<string> POST(string action, string content = null, string token = "")
        {
            using (var request = new HttpRequestMessage(HttpMethod.Post, Url + action))
            using (var httpContent = new StringContent(content, Encoding.UTF8, "application/json"))
            {
                request.Content = httpContent;
                if (token != "") request.Headers.Add("x-auth-token", token);

                using (HttpResponseMessage response = await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true))
                {
                    //response.EnsureSuccessStatusCode();
                    string conteudo = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                    {
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = conteudo };
                    }

                    return conteudo;
                }
            }
        }

        public async Task<string> PUT(string action, string content = null, string token = "")
        {
            using (var request = new HttpRequestMessage(HttpMethod.Put, Url + action))
            using (var httpContent = new StringContent(content, Encoding.UTF8, "application/json"))
            {
                request.Content = httpContent;
                if (token != "") request.Headers.Add("x-auth-token", token);

                using (HttpResponseMessage response = await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true))
                {
                    //response.EnsureSuccessStatusCode();
                    string conteudo = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                    {
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = conteudo };
                    }

                    return conteudo;
                }
            }
        }

    }
}
