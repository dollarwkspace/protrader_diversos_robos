﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;

namespace Delta_Force_One
{
    public class Comando
    {
        private DollarEA robo = null;
        private string log;

        public Comando(DollarEA _robo) { robo = _robo; log = robo.log; }

        public string EntradaDiffMedias(double preco, double diff)
        {
            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada");
            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada");
            return "-1";
        }

        public string EntradaContra(double preco)
        {
            //if (preco > robo.curta + robo.distanciaDaMediaContra * robo.tickSize) return Send_order(OrdersType.Market, Operation.Sell, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada contra");
            //if (preco < robo.curta - robo.distanciaDaMediaContra * robo.tickSize) return Send_order(OrdersType.Market, Operation.Buy, robo.instrument.RoundPrice(preco), robo.Contratos, 0, "entrada contra");
            return "-1";
        }

        public string ReEntrada(double preco, Position pos)
        {            
            if (pos.Side == Operation.Buy)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0, "reEntrada mercado " + robo.contadorDeReEntradas.ToString());
            }

            if (pos.Side == Operation.Sell)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0, "reEntrada mercado " + robo.contadorDeReEntradas.ToString());
            }

            return "-1";
        }

        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit, string comment = "entrada mercado")
        {
            if (side == Operation.Buy && (robo.tipoDeBloqueio == TipoDeBloqueio.BUY || robo.comandoNet.BloquearCompras )) { robo.Print("SendOrder: ENTRADA BUY BLOQUEADA PELO USUÁRIO"); return "-1"; }
            if (side == Operation.Sell && (robo.tipoDeBloqueio == TipoDeBloqueio.SELL || robo.comandoNet.BloquearVendas )) { robo.Print("SendOrder: ENTRADA SELL BLOQUEADA PELO USUÁRIO"); return "-1"; }

            if (robo.positions.Length > 0)
            {
                if (robo.positions[0].Amount + contratos > robo.maxPosAbertas)
                {
                    log = "Send_Order: Não é possível abrir mais contratos!";
                    robo.Print(log);
                    return "-1";
                }
            }

            log = $"{robo.horario} Send_Order(): Type:{type} Side:{side} Price:{price} Contratos:{contratos} Comment:{comment}";
            robo.Print(log);
            Quote quote = robo.lastQuote;
            if (quote == null) { robo.Print("SendOrder: Cotação nula"); return "-1"; }
            price = (side == Operation.Buy) ? robo.instrument.RoundPrice(quote.Ask) : robo.instrument.RoundPrice(quote.Bid);

            NewOrderRequest request = new NewOrderRequest
            {
                Instrument = robo.instrument,
                Account = robo.conta,
                Type = type,
                Side = side,
                Price = price,
                Amount = contratos,
                MarketRange = robo.marketRange,
                MagicNumber = robo.numeroMagico,
                Comment = comment
            };

            robo.Print($"SendOrder: Emitindo uma ordem de {request.Side} no preço R${robo.instrument.RoundPrice(price)} Qtde:{request.Amount} Ponto Atual da Média:{robo.curta}");
            string result = robo.Orders.Send(request);

            if (result == "-1") { log = $"{robo.horario} SendOrder: Ordem não aceita pela corretora"; robo.Print(log); return "-1"; }

            log = $"{robo.horario} SendOrder: Ordem com ticket {result} foi enviada para a corretora!";
            robo.Print(log);
            return result;
        }

        public bool CriaParDeSaida(Position posicao = null, string comment = "saída limite")
        {
            //TAKE PROFIT LIMIT             
            Position[] positions;
            if (posicao == null)
            {
                positions = robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ToArray();
                if (positions.Length > 0) posicao = positions[0];
            }

            if (posicao == null)
            {
                robo.Print("CriaParDeSaída: Não existe posição para criar alguma saída");
                return false;
            }

            log = $"{robo.horario} CriaParDeSaida: Ok! Tentando setar TAKE PROFIT LIMIT";
            robo.Print(log);
            try
            {
                Position pos = posicao;
                if (pos.Side == Operation.Buy)
                {
                    NewOrderRequest request = NewOrderRequest.CreateSellLimit(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize), robo.conta);
                    request.MagicNumber = robo.numeroMagico;
                    request.MarketRange = robo.marketRange;
                    request.Comment = comment;
                    var id = robo.Orders.Send(request);
                    if (id != "-1") return true;
                    log = robo.horario + "CriaParDeSaida: Ordem Limit de saída SELL não colocada... tentando ordem STOP";
                    robo.Print(log);
                    NewOrderRequest req = NewOrderRequest.CreateSellStop(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize), robo.conta);
                    req.MagicNumber = robo.numeroMagico;
                    req.MarketRange = robo.marketRange;
                    req.Comment = comment;
                    var id2 = robo.Orders.Send(req);
                    if (id2 != "-1") return true;
                    log = robo.horario + "CriaParDeSaida: Ordem SELL STOP não aceita!";
                }
                if (pos.Side == Operation.Sell)
                {
                    NewOrderRequest request = NewOrderRequest.CreateBuyLimit(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize), robo.conta);
                    request.MagicNumber = robo.numeroMagico;
                    request.MarketRange = robo.marketRange;
                    request.Comment = comment;
                    var id = robo.Orders.Send(request);
                    if (id != "-1") return true;
                    log = robo.horario + "CriaParDeSaida: Ordem Limit de saída BUY não colocada... tentando ordem STOP";
                    robo.Print(log);
                    NewOrderRequest req = NewOrderRequest.CreateBuyStop(robo.instrument, pos.Amount, robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize), robo.conta);
                    req.MagicNumber = robo.numeroMagico;
                    req.MarketRange = robo.marketRange;
                    req.Comment = comment;
                    var id2 = robo.Orders.Send(req);
                    if (id2 != "-1") return true;
                    log = robo.horario + "CriaParDeSaida: Ordem BUY STOP não aceita!";
                }

                robo.Print(log);
                return false;
            }
            catch (Exception ext)
            {
                robo.Print("CriaParDeSaida: Erro ao Criar Saída! " + ext.Message);
                return false;
            }

        }

    }
}
