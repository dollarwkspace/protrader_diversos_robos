﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace Delta_Force_One
{
    public class Eventos
    {
        private DollarEA robo = null;
        private int i, j, k, l = 0;
        private string log;

        public Eventos(DollarEA _robo)
        {
            robo = _robo; log = robo.log;
        }


        public void Orders_OrderAdded(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    if (obj.Type == OrdersType.Limit) robo.nextState = State.analise;
                    log = $"{obj.Time.ToLocalTime().TimeOfDay} EVENTO OrderAdded: ID: {obj.Id}  tipo:{obj.Side}  {obj.Comment}  Qtde:{obj.Amount}  preço:{obj.Price}  hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status:{obj.Status}  Estado:{robo.estado}";
                    robo.Print(log);
                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro OrderAdded : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Orders_OrderRemoved(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.Time.ToLocalTime().TimeOfDay} EVENTO OrderRemoved: ID: {obj.Id}  tipo:{obj.Side}  {obj.Comment}  Qtde:{obj.Amount}  preço:{obj.Price}  hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status:{obj.Status}  Estado:{robo.estado}";
                    robo.Print(log);
                    robo.nextState = State.analise;
                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro OrderRemoved : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }


        public void Orders_OrderExecuted(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.Time.ToLocalTime().TimeOfDay}  EVENTO OrderExecuted: ID: {obj.Id}  tipo: {obj.Side}  {obj.Comment}   Qtde:{obj.Amount}  preço: R${obj.Price}   hora:{obj.Time.ToLocalTime().ToShortTimeString()}  Status: {obj.Status}  Estado: {robo.estado}";
                    robo.Print(log);
                }
            }
            catch (Exception exc)
            {
                robo.Print($"Erro OrderExecuted : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Positions_PositionRemoved(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = $"{obj.CloseTime.ToLocalTime().TimeOfDay} EVENTO PositionRemoved: ID: {obj.Id} tipo: {obj.Side}  qtde: {obj.Amount} OrderId: {obj.OpenOrderId}   Papel:{obj.Instrument.BaseName}  Estado: {robo.estado}";
                    robo.Print(log);
                    robo.nextState = State.analise;
                    robo.ultimaPosicaoAdd = null;
                }
            }
            catch (Exception exc)
            {
                robo.Alert($"Erro PositionRemoved : {exc.Message}");
                robo.estado = State.analise;
                robo.nextState = State.analise;
            }
        }

        public void Positions_PositionAdded(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == robo.papel)
                {
                    robo.funcoes.RefreshPositionsOrders();
                    log = "";
                    if (robo.ultimaPosicaoAdd == null)
                    {
                        log = $"{obj.OpenTime.ToLocalTime().TimeOfDay}  EVENTO PositionAdded: ID: {obj.Id}  tipo: {obj.Side}   qtde: {obj.Amount}  OrderId: {obj.OpenOrderId}   Papel: {obj.Instrument.BaseName}   Estado:  {robo.estado}";
                        robo.comando.CriaParDeSaida(obj, "saída limite");
                        robo.estado = State.lockdown; 
                    }
                    else
                    {
                        log = $"{obj.OpenTime.ToLocalTime().TimeOfDay}  EVENTO PositionUPDATE: ID: {obj.Id}  tipo: {obj.Side}   qtde: {obj.Amount}  OrderId: {obj.OpenOrderId}   Papel: {obj.Instrument.BaseName}   Estado:  {robo.estado}";
                        if (robo.orders.Length == 0 && robo.positions.Length > 0 ) { robo.comando.CriaParDeSaida(robo.positions[0], "saída limite"); robo.estado = State.lockdown; }
                    }

                    robo.ultimaPosicaoAdd = obj;
                    robo.nextState = State.analise;
                }
            }
            catch (Exception exc)
            {
                robo.Print($"Erro PositionAdded : {exc.Message}");
                robo.nextState = State.analise;
                robo.estado = State.analise;
            }
        }

        public void Instruments_NewLevel2(Instrument inst, Level2 quote)
        {
            if (inst.BaseName == robo.papel)
            {
                if (robo.level2.Count < robo.qtdeLevel2)
                {
                    robo.level2.Enqueue(quote);
                }
                else
                {
                    robo.level2.Dequeue();
                    robo.level2.Enqueue(quote);
                }
            }
        }

        public void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                if (args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString($"Calor máximo: {robo.calor.ToString()}  Máximo de Contratos abertos: {robo.maximoDeContratosAbertos.ToString()}  Estado atual: {robo.estado.ToString()}  contadorDeReEntradas: {robo.contadorDeReEntradas}   contadorDinâmico: {robo.contadorReEntradasDinamicas}  Próxima ReEntrada: {robo.proximaReEntrada}", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 5);
                    args.Graphics.DrawString($"TamPonto: {robo.tamanhoPonto}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 80);
                    args.Graphics.DrawString($"Cotação: {robo.cotacaoAtual}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 65);
                    args.Graphics.DrawString($"TPDin:{Math.Floor(robo.mulTPdin * robo.TPatr)}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 50);
                    args.Graphics.DrawString($"Max:{robo.maxMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 35);
                    args.Graphics.DrawString($"Min:{robo.minMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 20);
                    args.Graphics.DrawString($"Cur:{robo.ms} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 5);
                }

                if (robo.positions.Length > 0 && args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString("Posição", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Qtde", robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Médio", robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Atual", robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Tipo", robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Lucro/Prejuízo", robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 20);

                    args.Graphics.DrawString("Protrader ->", robo.font, robo.brush, args.Rectangle.X + 10, args.Rectangle.Y + 40);                    

                    args.Graphics.DrawString(robo.positions[0].Id, robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(robo.positions[0].Amount.ToString(), robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].OpenPrice), robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].CurrentPrice), robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(robo.positions[0].Side.ToString(), robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.positions[0].GetProfitGross().ToString()), robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 40);

                    if (robo.saidaDeSegurancaEmAnalise) args.Graphics.DrawString(robo.saidaDeSeguranca, robo.fontAlarm, robo.brushAlert, args.Rectangle.X + 110, args.Rectangle.Height - 57);
                    if(robo.contadorReEntradasDinamicas < 2) args.Graphics.DrawString($"ReEntrada Dinâmica Ativada", robo.fontAlarm, robo.brushAlert, args.Rectangle.X + 110, args.Rectangle.Height - 72);

                    if (args.Rectangle.Height > 350 && robo.level2.Count > 0 && robo.plotarLevel2)
                    {
                        args.Graphics.DrawString("Origem", robo.font, robo.brush, args.Rectangle.X + 720, args.Rectangle.Y + 20);
                        args.Graphics.DrawString("Preço", robo.font, robo.brush, args.Rectangle.X + 820, args.Rectangle.Y + 20);
                        args.Graphics.DrawString("Tamanho", robo.font, robo.brush, args.Rectangle.X + 920, args.Rectangle.Y + 20);
                        args.Graphics.DrawString("Lado", robo.font, robo.brush, args.Rectangle.X + 1000, args.Rectangle.Y + 20);
                        args.Graphics.DrawLine(new Pen(Color.White, 1), new PointF(args.Rectangle.X + 710, args.Rectangle.Y), new PointF(args.Rectangle.X + 710, args.Rectangle.Y + args.Rectangle.Y + args.Rectangle.Height));
                        DrawLevel2(args.Graphics, args.Rectangle);

                    }
                    else
                    {
                        if (args.Rectangle.Height > 350 && robo.autorizaLogPrint) DrawLog(args.Graphics, args.Rectangle);
                        DrawParams(args.Graphics, args.Rectangle);
                        DrawComandos(args.Graphics, args.Rectangle);
                    }
                }
            }
            catch (Exception exc)
            {
                //robo.Print($"OnPaintChart Erro {exc.Message}");                
            }
        }


        private void DrawParams(Graphics gr, Rectangle rect)
        {
            l = 15;

            if (robo.parametros.Count > 0 && !robo.comandoNet.EsconderParametros && robo.comandoNet.EsconderComandos)
                robo.parametros.ForEach(p =>
                {
                    gr.DrawString(p, robo.font, robo.brush, rect.Width - 250, rect.Y + l);
                    l += 15;
                });
        }

        private void DrawComandos(Graphics gr, Rectangle rect)
        {
            l = 15;

            if (robo.comandosTela.Count > 0 && !robo.comandoNet.EsconderComandos && robo.comandoNet.EsconderParametros)
                robo.comandosTela.ForEach(p =>
                {
                    gr.DrawString(p, robo.font, robo.brush, rect.Width - 250, rect.Y + l);
                    l += 15;
                });
        }


        private void DrawLog(Graphics gr, Rectangle rect)
        {
            i = 0;
            try
            {
                if (robo.logDin.Count > 0 && !robo.comandoNet.EsconderLog)
                {
                    robo.logDin.ForEach(logItem =>
                    {
                        gr.DrawString(logItem, robo.font, robo.brush, rect.X + 10, 100 + i);
                        i += 15;
                    });
                }
                robo.autorizaLogPrint = true;
            }
            catch (Exception exc)
            {
                robo.Print($"DrawLog: Erro {exc.Message}");
            }
        }





        private void DrawLevel2(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));

            k = 40;
            try
            {

                robo.level2.ToList().ForEach(_level2 =>
                {
                    gr.DrawString(_level2.Source.ToString(), robo.font, robo.brush, rect.X + 720, rect.Y + k);
                    gr.DrawString(String.Format("{0:C}", _level2.Price), robo.font, robo.brush, rect.X + 820, rect.Y + k);
                    gr.DrawString(_level2.Size.ToString(), robo.font, robo.brush, rect.X + 920, rect.Y + k);
                    gr.DrawString(_level2.Side.ToString(), robo.font, robo.brush, rect.X + 1000, rect.Y + k);

                    k += 15;
                });

            }
            catch (Exception exc)
            {
                robo.Print($"DrawLevel2: Erro {exc.Message}");

            }
        }





    }
}
