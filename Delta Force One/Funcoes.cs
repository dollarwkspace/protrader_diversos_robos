﻿using System;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using Delta_Force_One.Web;
using System.Runtime.Serialization;
using System.Timers;

namespace Delta_Force_One
{

    public enum State { inicio, reentrada, saidaTendencia, avalanche, termino, bolsaFechada, lockdown, analise };
    public enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
    public enum TipoDeSaida { DifMédias, LossMaximo };
    public enum TipoDeEntrada { MA3, DifMédias };
    public enum TipoDeRobo { tendencia, contraTendencia, tendenciaEcontra }
    public enum TipoDeOrdem { limite, mercado };
    public enum TipoDeBloqueio { BUY, SELL, NULL };
    public enum TipoDeExecucao { OnTrade, OnQuote };
    public enum TipoDeTP { fixo, dinamico };
    public enum TipoStopSeguranca { PreçoMedio, MédiaLonga }

    [DataContract]
    public class PosicaoAtiva
    {
        [DataMember]
        internal double _openPrice;
        [DataMember]
        internal double _amount;
        [DataMember]
        internal Operation _side;
        [DataMember]
        internal DateTime _openTime;
        [DataMember]
        internal double _lucro;
        [DataMember]
        internal double _pontos;
        [DataMember]
        internal string _comments;

        internal PosicaoAtiva(double openPrice, double amount, Operation side, DateTime openTime, string comments = "", double lucro = 0, double pontos = 0)
        {
            _openPrice = openPrice; _amount = amount; _side = side; _openTime = openTime; _lucro = lucro; _pontos = pontos; _comments = comments;
        }
    }

    public class LogWriter
    {
        private string m_exePath = string.Empty;

        public static void LogWrite(string logMessage, string arq = "C:\\robo\\log.txt", bool novo = true)
        {
            //m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (File.Exists(arq) && novo) File.Delete(arq);

            if (!File.Exists(arq))
            {
                //string s = ""; s = DateTime.Now.ToString("h:mm:ss tt");
                FileStream fs = File.Create(arq);
                //Byte[] info = new UTF8Encoding(true).GetBytes("File Created:" + s + "\r\n");
                //fs.Write(info, 0, info.Length);
                fs.Close();
            }

            try
            {
                using (StreamWriter w = File.AppendText(arq))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("{0}", logMessage);
                //txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }


    //public class Excellib
    //{
    //    private ProjetoEverestSMOnQuote robo;

    //    private Excel.Application xlApp = new Excel.Application();
    //    private Excel.Workbook xlWorkBook;
    //    private Excel.Worksheet xlWorkSheet;
    //    private Excel.Range range;
    //    private string log;
    //    private int rowCount;
    //    private int colCount;

    //    public Excellib(ProjetoEverestSMOnQuote _robo)
    //    {
    //        robo = _robo; log = robo.log;
    //        xlWorkBook = xlApp.Workbooks.Open(@"c:\robo\robo.xlsx");
    //        xlWorkSheet = (Excel.Worksheet)xlWorkBook.Sheets[1];
    //        rowCount = range.Rows.Count;
    //        colCount = range.Columns.Count;
    //    }

    //    public void Read()
    //    {
    //        if (xlApp == null) return;
    //        //excel is not zero based!!
    //        for (int i = 1; i <= rowCount; i++)
    //        {
    //            robo.Print($"i");
    //            //range = (xlWorkSheet.Cells[i, robo.colunaExcel] as Excel.Range);
    //            robo.parametros.Add($"{range.Value}");
    //            //do anything
    //        }
    //    }

    //    public void Release()
    //    {
    //        //cleanup
    //        xlWorkBook.Close(true, null, null);
    //        xlApp.Quit();

    //        GC.Collect();
    //        GC.WaitForPendingFinalizers();
    //        //rule of thumb for releasing com objects:
    //        //  never use two dots, all COM objects must be referenced and released individually
    //        //  ex: [somthing].[something].[something] is bad

    //        //release com objects to fully kill excel process from running in the background
    //        Marshal.ReleaseComObject(range);
    //        Marshal.ReleaseComObject(xlWorkSheet);
    //        //close and release            
    //        Marshal.ReleaseComObject(xlWorkBook);
    //        //quit and release           
    //        Marshal.ReleaseComObject(xlApp);
    //    }
    //}


    public class Funcoes
    {
        private DollarEA robo;
        private string log;

        public Funcoes(DollarEA _robo) { robo = _robo; log = robo.log; }


        public bool ExistCollection(string name, string papel)
        {
            return robo._colecao.colecao.Exists(x => x.nome == name && x.papel == papel);
        }

        public Parametro FindCollection(string name, string papel)
        {
            return robo._colecao.colecao.Find(x => x.nome == name && x.papel == papel);
        }


        public void PopulaParametros()
        {
            if (robo.parametros.Count == 0)
            {
                robo.parametros.Add($"Parâmetros = {robo.nomeParam} ");
                robo.parametros.Add($"Papel = {robo.papel}");
                robo.parametros.Add($"Período média móvel Sinal = {robo?.periodoMediaMovelSinal} p");
                robo.parametros.Add($"Período média móvel Suporte = {robo?.periodoMediaMovelSuporte} p");
                robo.parametros.Add($"Período média móvel Bloqueio = {robo?.periodoMediaMovelBloqueio} p");
                robo.parametros.Add($"Média contínua? {robo?.mediaContinua}");
                robo.parametros.Add($"Delta suporte = {robo?.deltaSuporte} ticks");
                robo.parametros.Add($"Multiplicador ATR = {robo?.multiplicadorATR}");
                robo.parametros.Add($"Máximo posições possíveis = {robo?.maxPosAbertas} un");
                robo.parametros.Add($"Contratos iniciais = {robo?.Contratos} un");
                robo.parametros.Add($"Ticks de ReEntrada = [{robo?.ticksDeReentrada}]");
                robo.parametros.Add($"Valores de ReEntrada = [{robo?.valoresDeReentrada}]");
                robo.parametros.Add($"Distância da Média Sinal = {robo?.distanciaDaMedia} ticks");
                robo.parametros.Add($"Take Profit Fixo = {robo?.TP}");
                robo.parametros.Add($"Multiplicador = {robo?.multiplicador}");
                robo.parametros.Add($"Resiliência de Saída = {robo?.resilienciaDeSaida}");
                robo.parametros.Add($"Loss Máximo = {robo?.lossMaximo} ticks");
                robo.parametros.Add($"Market Range = {robo?.marketRange}");                
                robo.parametros.Add($"Tamanho do Ponto = {robo?.tamanhoPonto} ticks");
                robo.parametros.Add($"Lateralidade = {robo?.lateralidade} ticks");                
                robo.parametros.Add($"Linhas Level2 = {robo?.qtdeLevel2}");
                robo.parametros.Add($"Multiplicador TP dinâmico = {robo?.mulTPdin}");
                robo.parametros.Add($"TP dinâmico Mínimo = {robo?.TPdinMin}");
                robo.parametros.Add($"Contratos Saída de Segurança = {robo?.contratosSaidaSeguranca} un");
                robo.parametros.Add($"Saída de Segurança em {robo?.ticksSaidaSeguranca} ticks");
            }
            else
            {
                robo.parametros[0] = ($"Parâmetros = {robo.nomeParam} ");
                robo.parametros[1] = ($"Papel = {robo.papel}");
                robo.parametros[2] = ($"Período média móvel Sinal = {robo?.periodoMediaMovelSinal} p");
                robo.parametros[3] = ($"Período média móvel Suporte = {robo?.periodoMediaMovelSuporte} p");
                robo.parametros[4] = ($"Período média móvel Bloqueio = {robo?.periodoMediaMovelBloqueio} p");
                robo.parametros[5] = ($"Média contínua? {robo?.mediaContinua}");
                robo.parametros[6] = ($"Delta suporte = {robo?.deltaSuporte} ticks");
                robo.parametros[7] = ($"Multiplicador ATR = {robo?.multiplicadorATR}");
                robo.parametros[8] = ($"Máximo posições possíveis = {robo?.maxPosAbertas} un");
                robo.parametros[9] = ($"Contratos iniciais = {robo?.Contratos} un");
                robo.parametros[10] = ($"Ticks de ReEntrada = [{robo?.ticksDeReentrada}]");
                robo.parametros[11] = ($"Valores de ReEntrada = [{robo?.valoresDeReentrada}]");
                robo.parametros[12] = ($"Distância da Média Sinal = {robo?.distanciaDaMedia} ticks");
                robo.parametros[13] = ($"Take Profit Fixo = {robo?.TP}");
                robo.parametros[14] = ($"Multiplicador = {robo?.multiplicador}");
                robo.parametros[15] = ($"Resiliência de Saída = {robo?.resilienciaDeSaida}");
                robo.parametros[16] = ($"Loss Máximo = {robo?.lossMaximo} ticks");
                robo.parametros[17] = ($"Market Range = {robo?.marketRange}");                
                robo.parametros[19] = ($"Tamanho do Ponto = {robo?.tamanhoPonto} ticks");
                robo.parametros[20] = ($"Lateralidade = {robo?.lateralidade} ticks");                
                robo.parametros[28] = ($"Linhas Level2 = {robo?.qtdeLevel2}");
                robo.parametros[29] = ($"Multiplicador TP dinâmico = {robo?.mulTPdin}");
                robo.parametros[30] = ($"TP dinâmico Mínimo = {robo?.TPdinMin}");
                robo.parametros[31] = ($"Contratos Saída de Segurança = {robo?.contratosSaidaSeguranca} un");
                robo.parametros[32] = ($"Saída de Segurança em {robo?.ticksSaidaSeguranca} ticks");
            }


        }


        public void Sync(Parametro _param)
        {
            robo.periodoMediaMovelSinal = _param.periodoMediaMovelSinal;
            robo.periodoMediaMovelSuporte = _param.periodoMediaMovelSuporte;
            robo.periodoMediaMovelBloqueio = _param.periodoMediaMovelBloqueio;
            robo.mediaContinua = _param.mediaContinua;
            robo.deltaSuporte = _param.deltaSuporte;
            robo.multiplicadorATR = _param.multiplicadorATR;
            robo.maxPosAbertas = _param.maxPosAbertas;
            robo.Contratos = _param.Contratos;
            robo.ticksDeReentrada = _param.ticksDeReentrada;
            robo.valoresDeReentrada = _param.valoresDeReentrada;
            robo.distanciaDaMedia = _param.distanciaDaMedia;
            robo.TP = _param.TP;
            robo.multiplicador = _param.multiplicador;
            robo.resilienciaDeSaida = _param.resilienciaDeSaida;
            robo.lossMaximo = _param.lossMaximo;
            robo.marketRange = _param.marketRange;            
            robo.tamanhoPonto = _param.tamanhoPonto;
            robo.lateralidade = _param.lateralidade;            
            robo.qtdeLevel2 = _param.qtdeLevel2;
            robo.mulTPdin = _param.mulTPdin;
            robo.TPdinMin = _param.TPdinMin;
            robo.contratosSaidaSeguranca = _param.contratosSaidaSeguranca;
            robo.ticksSaidaSeguranca = _param.ticksSaidaSeguranca;

            robo.ReEntradas = robo.ticksDeReentrada.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
            robo.ValoresDeReEntradas = robo.valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
            if (robo.ReEntradas.Length != robo.ValoresDeReEntradas.Length)
            {
                throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
            }

        }




        public Task<bool> ChecaLogin()
        {
            return Task.Run(() =>
            {
                try
                {
                    if (robo.token == "")
                    {
                        Usuario usuario = new Usuario
                        {
                            Nome = "",
                            Sobrenome = "",
                            Login = robo.login,
                            Senha = robo.senha
                        };
                        robo.Print($"Buscando usuário: {robo.login} -> {robo.senha}");
                        Task<string> fetchToken = robo.api.POST("/auth", JsonHelper.Serializar<Usuario>(usuario));
                        fetchToken.Wait();
                        robo.token = fetchToken.Result;
                        robo.Print($"Token: {robo.token}");
                        //robo._parametro.POST();
                    }
                }
                catch (Exception ex)
                {
                    robo.erroLogin = (ex.InnerException as ApiException).Content;
                    if (robo.erroLogin == "" || robo.erroLogin == null) robo.erroLogin = ex.InnerException.Message;
                    robo.Print(robo.erroLogin);
                    throw new ArgumentException(robo.erroLogin);
                }
                return true;
            });
        }

        public void InicializaComandos()
        {                       
            robo.comandoNet.EsconderLog = !robo.blockLog;
            if (robo.online)
            {
                robo.apiRobo.GET_Comandos();
            }
        }

        public async void SincronizaParametros()
        {
            try
            {
                robo.Print("Lendo Parâmetros");
                if (robo.baseWEB && robo.online)
                {
                    if (await robo.apiRobo.GET_Parametros(robo.nomeParam, robo.papel))
                    {
                        Sync(robo.parametrosAtivos);
                        PopulaParametros();
                        robo.parametrosSync = true;
                        robo.periodoBloqueioAntigo = robo.periodoMediaMovelBloqueio;
                        robo.periodoSinalAntigo = robo.periodoMediaMovelSinal;
                        robo.periodoSuporteAntigo = robo.periodoMediaMovelSuporte;
                        await robo.apiRobo.DELETE_ParametrosAtivos(robo.nomeParam, robo.papel);
                        robo.apiRobo.POST_ParametrosAtivos(robo.parametrosAtivos);
                        robo.Print("Inicializando indicador Camilo 3MM");
                        robo.MM = robo.Indicators.iCustom("camilo_3mm", robo.CurrentData, robo.periodoMediaMovelSinal, robo.periodoMediaMovelSuporte, robo.periodoMediaMovelBloqueio, robo.multiplicadorATR);
                        robo.Print("Inicializando indicador Sniper Elite");
                        robo.sniper = robo.Indicators.iCustom("Camilo_SniperElite", robo.CurrentData, robo.periodoMediaMovelSinal, robo.resilienciaDeSaida, robo.resilienciaDeSaida, robo.multiplicador);
                        robo.Print("Inicializando indicador ATR");
                        robo.ATR = robo.Indicators.iATR(robo.CurrentData, robo.periodoMediaMovelSinal);
                        robo.ReEntradas = robo.ticksDeReentrada.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                        robo.ValoresDeReEntradas = robo.valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                    }                    
                } else
                {                    
                    robo.parametrosAtivos.Contratos = robo.Contratos;                 
                    robo.parametrosAtivos.contratosSaidaSeguranca = robo.contratosSaidaSeguranca;                    
                    robo.parametrosAtivos.deltaSuporte = robo.deltaSuporte;
                    robo.parametrosAtivos.distanciaDaMedia = robo.distanciaDaMedia;
                    robo.parametrosAtivos.lateralidade = robo.lateralidade;
                    robo.parametrosAtivos.lossMaximo = robo.lossMaximo;                    
                    robo.parametrosAtivos.marketRange = robo.marketRange;
                    robo.parametrosAtivos.maxPosAbertas = robo.maxPosAbertas;
                    robo.parametrosAtivos.mediaContinua = robo.mediaContinua;                    
                    robo.parametrosAtivos.multiplicador = robo.multiplicador;
                    robo.parametrosAtivos.multiplicadorATR = robo.multiplicadorATR;
                    robo.parametrosAtivos.mulTPdin = robo.mulTPdin;
                    robo.parametrosAtivos.nome = robo.nomeParam;
                    robo.parametrosAtivos.papel = robo.papel;
                    robo.parametrosAtivos.periodoMediaMovelBloqueio = robo.periodoMediaMovelBloqueio;
                    robo.parametrosAtivos.periodoMediaMovelSinal = robo.periodoMediaMovelSinal;
                    robo.parametrosAtivos.periodoMediaMovelSuporte = robo.periodoMediaMovelSuporte;
                    robo.parametrosAtivos.qtdeLevel2 = robo.qtdeLevel2;                    
                    robo.parametrosAtivos.resilienciaDeSaida = robo.resilienciaDeSaida;
                    robo.parametrosAtivos.tamanhoPonto = robo.tamanhoPonto;                    
                    robo.parametrosAtivos.ticksDeReentrada = robo.ticksDeReentrada;
                    robo.parametrosAtivos.ticksSaidaSeguranca = robo.ticksSaidaSeguranca;
                    robo.parametrosAtivos.TP = robo.TP;                    
                    robo.parametrosAtivos.TPdinMin = robo.TPdinMin;
                    robo.parametrosAtivos.valoresDeReentrada = robo.valoresDeReentrada;
                    robo.periodoBloqueioAntigo = robo.periodoMediaMovelBloqueio;
                    robo.periodoSinalAntigo = robo.periodoMediaMovelSinal;
                    robo.periodoSuporteAntigo = robo.periodoMediaMovelSuporte;
                    PopulaParametros();
                    robo.parametrosSync = true;
                    if (robo.online)
                    {
                        await robo.apiRobo.DELETE_ParametrosAtivos(robo.nomeParam, robo.papel);
                        robo.apiRobo.POST_ParametrosAtivos(robo.parametrosAtivos);
                    }
                    robo.Print("Inicializando indicador Camilo 3MM");
                    robo.MM = robo.Indicators.iCustom("camilo_3mm", robo.CurrentData, robo.periodoMediaMovelSinal, robo.periodoMediaMovelSuporte, robo.periodoMediaMovelBloqueio, robo.multiplicadorATR);
                    robo.Print("Inicializando indicador Sniper Elite");
                    robo.sniper = robo.Indicators.iCustom("Camilo_SniperElite", robo.CurrentData, robo.periodoMediaMovelSinal, robo.resilienciaDeSaida, robo.resilienciaDeSaida, robo.multiplicador);
                    robo.Print("Inicializando indicador ATR");
                    robo.ATR = robo.Indicators.iATR(robo.CurrentData, robo.periodoMediaMovelSinal);
                    robo.ReEntradas = robo.ticksDeReentrada.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                    robo.ValoresDeReEntradas = robo.valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                }              


            }
            catch (Exception ex)
            {
                robo.Alert("Erro no Importa Parâmetros");
            }
        }

        public void InfoConexao(Connection myConnection)
        {
            //outputting of all the information about the current connection.
            robo.Print(
            "\nConnection name : \t" + myConnection.Name +
            "\nConnection address : \t" + myConnection.Address +
            "\nConnection login : \t" + myConnection.Login +
            "\nSLL on?  : \t" + myConnection.IsUseSSL +
            "\nProtocol version : \t" + myConnection.ProtocolVersion +
            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
            "\nConnection status : \t" + myConnection.Status
            );
        }

        public void AccountInformation(Account acc)
        {
            //outputting of all the 'Account' properties
            robo.Print(
                "Id : \t" + acc.Id + "\n" +
                "Name : \t" + acc.Name + "\n" +
                "User Login: \t" + acc.User.Login + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "Leverage : \t" + acc.Leverage + "\n" +
                "Currency : \t" + acc.Currency + "\n" +
                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
                "IsDemo : \t" + acc.IsDemo + "\n" +
                "IsReal : \t" + acc.IsReal + "\n" +
                "IsLocked : \t" + acc.IsLocked + "\n" +
                "IsInvestor : \t" + acc.IsInvestor + "\n" +
                "Status : \t" + acc.Status + "\n" +
                "StopReason : \t" + acc.StopReason + "\n" +
                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "BeginBalance : \t" + acc.BeginBalance + "\n" +
                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
                "Credit : \t" + acc.Credit + "\n" +
                "CashBalance : \t" + acc.CashBalance + "\n" +
                "TodayVolume : \t" + acc.TodayVolume + "\n" +
                "TodayNet : \t" + acc.TodayNet + "\n" +
                "TodayTrades : \t" + acc.TodayTrades + "\n" +
                "TodayFees : \t" + acc.TodayFees + "\n" +
                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
                "MarginTotal : \t" + acc.MarginTotal + "\n" +
                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
                "Equity : \t" + acc.Equity + "\n" +
                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
            );
        }

        public void PosInfo(Position pos)
        {
            log =
                "PosInfo()-> " +
                "  Papel: " + pos.Instrument.BaseName + " / " +
                //"  Id: " + pos.Id + " / " +
                "  Qtde: " + pos.Amount + " / " +
                "  Tipo: " + pos.Side.ToString() + " / " +
                "  Preço Abertura: R$ " + pos.OpenPrice + " / " +
                "  Preço Atual: R$ " + pos.CurrentPrice + " / "; //+
                                                                 //"  Número Mágico: " + pos.MagicNumber + " / " +
                                                                 //"  Lucro ou Prejuízo: R$" + pos.GetProfitNet();
            robo.Print(log);
        }

        public void OrderInfo(Order ord)
        {
            log = "OrderInfo()-> " +
                "  Papel: " + ord.Instrument.BaseName + " / " +
                //"  Id: " + ord.Id + " / " +
                "  Qtde: " + ord.Amount + " / " +
                "  Lado: " + ord.Side + " / " +
                "  Tipo: " + ord.Type + " / " +
                "  Preço: R$ " + ord.Price + " / " +
                "  Status: " + ord.Status + " / "; //+
                                                   //"  Número Mágico: " + ord.MagicNumber;
            robo.Print(log);
        }
       

        public bool EqualizaOrdemPosicao(Position pos, Order ord)
        {
            try
            {
                if (ord == null) { return false; }
                double price = 0;
                price = (pos.Side == Operation.Buy) ? robo.instrument.RoundPrice(pos.OpenPrice + robo.TP * robo.instrument.TickSize) : robo.instrument.RoundPrice(pos.OpenPrice - robo.TP * robo.instrument.TickSize);
                log = "EqualizaOrdemPosição: Qtde Pos: " + pos.Amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t TickSize: " + robo.tickSize;
                robo.Print(log);

                if (pos.Amount != ord.Amount)
                {
                    log = robo.horario + "EqualizaOrdemPosição: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.tickSize;
                    robo.Print(log);
                    return ord.Modify(price, pos.Amount);
                }

                if (Math.Abs(ord.Price - price) > robo.tickSize)
                {
                    log = robo.horario + "EqualizaOrdemPosição: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.tickSize;
                    robo.Print(log);
                    return ord.Modify(price);
                }

            }
            catch (Exception exc)
            {
                robo.Alert("Erro EqualizaOrdemPosição");
            }
            return false;
        }


        public bool CalculaCalorMaximo()
        {
            try
            {
                if (robo.positions.Length > 0)
                {
                    Position pos = robo.positions[0];
                    if (pos != null)
                    {
                        if (pos.GetProfitNet() < robo.calor) { robo.calor = pos.GetProfitNet(); robo.horaDoCalorMaximo = robo.hora.ToString(); }
                        if (Math.Abs(pos.Amount) > robo.maximoDeContratosAbertos) { robo.maximoDeContratosAbertos = pos.Amount; }
                    }
                }
                robo.Print("CalculaCalorMaximo: Calor Máximo até o momento: " + robo.calor);
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("CalculaCalorMaximo: Erro setor Calor Máximo . " + exc.Message);
                robo.Print("CalculaCalorMaximo: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool CheckMarketStatus()
        {
            try
            {
                if (robo.myConnection.Status == ConnectionStatus.Connected)
                {
                    robo.horario = DateTime.Now.TimeOfDay; robo.hora = robo.horario.Hours;
                }
                else
                {
                    robo.horario = robo.lastQuote.Time.ToLocalTime().TimeOfDay;
                    robo.hora = robo.horario.Hours;
                }

                robo.Print("CheckMarketStatus: Hora Atual = " + robo.horario.ToString());
                if (robo.hora == 0) { return false; }
                if (robo.hora >= 9) { robo.horarioTerminou = false; }
                if (robo.horario.CompareTo(robo.HoraDeInicio) < 0) { robo.Print("Aguardando hora de Início..."); return false; }
                if (robo.horario.CompareTo(robo.HoraDeTermino) >= 0) { robo.Print("CheckMarketStatus: Hora de término atingida!"); robo.horarioTerminou = true; }
                if (robo.hora >= 18)
                {
                    robo.horarioTerminou = true;
                    if (robo.Positions.Count > 0) robo.Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); FechamentoTotal();
                    robo.Print("CheckMarketStatus: Bolsa fechada !");
                    return false;
                }
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("CheckMarketStatus: Erro na coleta da Hora . Erro: " + exc.Message);
                robo.Print("CheckMarketStatus: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = robo.Positions.GetPositions();
            Order[] ordensGlobais = robo.Orders.GetOrders(true);
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => PosInfo(pos)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => OrderInfo(ord)); }
        }

        public void FechamentoParcial()
        {
            robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
        }

        public void FechamentoTotal()
        {
            log = robo.horario + " --> Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas caso existam";
            robo.Print(log);
            LimparTodasAsOrdens();
            robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel).ForEach(p =>
            {
                log = "Fechamento Total: Posição ID " + p.Id + " Qtde:" + p.Amount + " " + p.Side + " Price:" + p.OpenPrice;
                robo.Print(log);
                p.Close();
            });
        }

        public void LimparTodasAsOrdens()
        {
            log = "";
            robo.Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == robo.papel).ForEach(ord =>
            {
                ord.Cancel();
                log = "Fechamento Total: Ordem ID " + ord.Id + "  Status:" + ord.Status + " Active:" + ord.IsActive + " Qtde:" + ord.Amount + " " + ord.Side + " Price:" + ord.CurrentPrice;
                robo.Print(log);
            });
        }

        public void SaidaPorLossMaximo()
        {
            try
            {
                //SAÍDA POR LOSS MÁXIMO
                if (robo.positions.Length > 0 && robo.tipoDeSaida == TipoDeSaida.LossMaximo)
                {
                    Position pos = robo.positions[0];
                    if (pos != null)
                    {
                        robo.Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize) + " Limite: " + robo.lossMaximo + " ticks");

                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / robo.tickSize >= robo.lossMaximo)
                        {
                            string log = "*****************    LOSS MÁXIMO ATINGIDO    ********************";
                            robo.Print(log);
                            FechamentoTotal();
                            log = "Fechando todas as Posições por LOSS MÁXIMO !!!";
                            robo.Print(log);
                            robo.estado = State.lockdown;
                            return;
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                robo.Print("SaídaPorLossMáximo: Erro setor Loss Máximo . " + exc.Message);
                robo.Print("SaídaPorLossMáximo: InnerException " + exc.InnerException.Message);
            }
        }    

        public void SaidaDeSeguranca()
        {
            try
            {
                if (robo.positions.Length == 0) { robo.saidaDeSegurancaEmAnalise = false; }
                else
                {

                    Position pos = robo.positions[0];
                    if (pos == null || pos.GetProfitNet() >= 0 || pos.Amount < robo.contratosSaidaSeguranca)
                    {
                        robo.saidaDeSegurancaEmAnalise = false;
                    }
                    else
                    {

                        double saidaFinal = 0;

                        log = "*****************    SAÍDA DE SEGURANÇA SENDO ANALISADA    ********************";
                        robo.Print(log);
                        robo.saidaDeSegurancaEmAnalise = true;

                        if (robo.tipoStopSeguranca == TipoStopSeguranca.MédiaLonga)
                        {
                            saidaFinal = Math.Floor(Math.Abs((robo.mBloqueio - pos.CurrentPrice)) / robo.tickSize);
                            robo.saidaDeSeguranca = $"Saída Segurança em Observação: Saída em {robo.ticksSaidaSeguranca - saidaFinal} ticks";
                        }
                        else
                        {
                            saidaFinal = Math.Floor(Math.Abs((pos.OpenPrice - pos.CurrentPrice)) / robo.tickSize);
                            robo.saidaDeSeguranca = $"Saída Segurança em Observação: Saída em {robo.ticksSaidaSeguranca - saidaFinal} ticks";
                        }

                        if (saidaFinal <= robo.ticksSaidaSeguranca)
                        {
                            log = "*****************    SAÍDA DE SEGURANÇA    ********************";
                            robo.Print(log);
                            FechamentoTotal();
                            log = "Fechando todas as Posições por SAÍDA DE SEGURANÇA !!!";
                            robo.Print(log);
                            robo.Alert(log);
                            robo.estado = State.lockdown;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                robo.Print($"Saída De Segurança: Erro {ex.Message}");
            }
        }



        public bool RefreshIndicators()
        {
            try
            {
                robo.Print("RefreshIndicators: Lendo indicadores...");
                robo.cotacaoAtual = robo.CurrentData.GetPrice(PriceType.Close);
                robo.curta = robo.MM.GetValue(0, 0);
                robo.curtaAnterior = robo.MM.GetValue(0, 1);
                robo.suporte = robo.MM.GetValue(1, 0);
                robo.suporteAnterior = robo.MM.GetValue(1, 1);
                robo.histoResult = robo.sniper.GetValue(0, 0);
                robo.mBloqueio = robo.MM.GetValue(2, 0);
                robo.mBloqueioAnterior = robo.MM.GetValue(2, 1);
                robo.ATRsuperior = robo.MM.GetValue(3, 0);
                robo.ATRinferior = robo.MM.GetValue(4, 0);
                robo.TPatr = robo.ATR.GetValue();
                int linesCount = robo.MM.LinesCount;
                robo.Print($"RefreshIndicators: Linhas de dados disponíveis no Indicador MM: {0}" + linesCount);
                robo.Print("ATRSup:" + robo.ATRsuperior + " ATRInf:" + robo.ATRinferior + "  MSinal:" + robo.curta + "  MSuporte:" + robo.suporte + " MBloq:" + robo.mBloqueio + " Cotação:" + robo.cotacaoAtual);
                robo.Print($"Diferença de médias Sinal: {robo.GetDiffMedias()}   Diferença de médias Suporte:{robo.GetDiffMediasSuporte()}   Diferença de médias Bloqueio: {robo.GetDiffMediasBloqueio()} ");
                robo.Print($"Delta Suporte: {robo.deltaSuporte}  ExisteInfluênciaDaMédiaSuporte? {robo.ExisteInfluenciaMediaSuporte()}");
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                robo.Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool RefreshPositionsOrders()
        {
            try
            {
                robo.positions = robo.Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == robo.papel)?.ToArray();
                robo.orders = robo.Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == robo.papel)?.ToArray();                
                robo.autorizaLogPrint = true;
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("RefreshPositionsOrders: Erro no RefreshPositionsOrders. Erro:" + exc.Message);
                return false;
            }
        }


    }
}
