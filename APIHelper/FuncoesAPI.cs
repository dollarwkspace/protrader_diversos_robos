﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace API
{

    public class ApiException : Exception
    {
        public int StatusCode { get; set; }
        public string Content { get; set; }
    }

    /*public class JsonLib<T>
    {
        public static T FromJson(string strJSON)
        {
            T result = JsonConvert.DeserializeObject<T>(strJSON);
            return result;
        }
    }

    public static class Extensions
    {
        public static StringContent AsJson(this object o)
         => new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
        public static async Task<HttpResponseMessage> PostAsJsonAsync<TModel>(this HttpClient client, string requestUrl, TModel model)
            => await client.PostAsync(requestUrl, new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
       
    }*/

    public class APICALLS
    {
        private static readonly HttpClient client = new HttpClient();
        private const string Url = "http://localhost:3003/api";

        public static void InitializeClient()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static async Task<string> GET(string action, string token = "")
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, Url + action))
            {
                if (token != "") request.Headers.Add("x-auth-token", token);
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = content };

                    return content;
                }
            }
        }

        public static async Task<string> POST(string action, object content = null, string token = "")
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, Url + action))
            using (var httpContent = CreateHttpContent(content))
            {
                request.Content = (content != null) ? httpContent : null;
                if (token != "") request.Headers.Add("x-auth-token", token);

                using (var response = await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(false))
                {
                    //response.EnsureSuccessStatusCode();
                    var conteudo = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == false)
                    {
                        throw new ApiException { StatusCode = (int)response.StatusCode, Content = conteudo };
                    }

                    return conteudo;
                }
            }
        }

        public static void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, value);
                jtw.Flush();
            }
        }

        public static HttpContent CreateHttpContent(object content, string token = "")
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                var ms = new MemoryStream();
                SerializeJsonIntoStream(content, ms);
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                if (token != "") httpContent.Headers.Add("x-auth-token", token);
            }

            return httpContent;
        }        

    }
}
