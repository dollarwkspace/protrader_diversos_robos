﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;
//using PTLRuntime.NETScript.Application;

//namespace RoboDollar
//{


//    public class ProjetoSniperOne : NETStrategy
//    {
//        private enum state { limbo, inicio, reentrada, saidaTendencia };
//        private enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
//        public enum TipoDeSaida { MA3, DifMédias };
//        public enum TipoDeEntrada { MA3, DifMédias };

//        private Instrument instrument;
//        private Account conta;
//        private Indicator MM;
//        private Indicator maSignal;
//        private System.Timers.Timer syncTimer;
//        private Connection myConnection = Connection.CurrentConnection; //create connection object.    
//        private Position[] positions;
//        private Order[] orders;

//        private double calor = 0;
//        private double curta = 0;
//        private double curtaAnterior = 0;
//        private double longa = 0;
//        private double cotacaoAtual = 0;
//        private int hora = 0;
//        private int contadorDeReEntradas = -1;
//        private UInt16[] ReEntradas;
//        private UInt16[] ValoresDeReEntradas;
//        private Trend trend = Trend.Nenhuma;

//        private double tickSize = 0;
//        private double maxDePosicaoAberta = 0;
//        private double ultimaReentrada = 0;
//        private string horaDoCalorMaximo = "";
//        private state estado = state.limbo;
//        private DateTime timerSimulador;
//        private bool horarioTerminou = false;
//        private string papel;
//        private int numeroMagico = 0;
//        private TimeSpan horario;

//        public ProjetoSniperOne()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves";
//            base.Comments = "Projeto Sniper One";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "18.12.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.0";
//            base.Password = "";
//            base.ProjectName = "Sniper One";
//            #endregion
//        }

//        [InputParameter("Média: Sinal", 0)]
//        public int periodoMediaMovelSinal = 7;
//        [InputParameter("Média: Balizadora", 1)]
//        public int periodoMediaMovel = 14;
//        [InputParameter("Média: Cerca Elétrica", 2)]
//        public int periodoMediaMovelSlow = 50;

//        [InputParameter("Permitir até quantas posições abertas? ", 3)]
//        public double maxPosAbertas = 40;

//        [InputParameter("Contratos", 4)]
//        public int Contratos = 5;

//        [InputParameter("Início(hora)", 5)]
//        public TimeSpan HoraDeInicio = new TimeSpan(9, 15, 0);

//        [InputParameter("Término(hora)", 6)]
//        public TimeSpan HoraDeTermino = new TimeSpan(15, 0, 0);

//        [InputParameter("Array Ticks de Reentradas", 7)]
//        public string ticksDeReentrada = "8,8,18,32";

//        [InputParameter("Array Valores de Reentradas", 8)]
//        public string valoresDeReentrada = "5,5,5,5";

//        [InputParameter("LOSS Máximo(Ticks)", 9)]
//        public double lossMaximo = 100;

//        [InputParameter("LOSS Máximo Parcial?", 10)]
//        public bool lossMaximoParcial = true;

//        [InputParameter("Take Profit (Tick)", 11)]
//        public double TP = 1.5;

//        [InputParameter("Timer(s)", 12)]
//        public double timerParaCancelar = 10;

//        [InputParameter("Tipo de Entrada", 13, new Object[] { "3MA", TipoDeSaida.MA3, "DifMédias", TipoDeSaida.DifMédias })]
//        public TipoDeEntrada tipoDeEntrada = TipoDeEntrada.MA3;

//        [InputParameter("Usar Saída na Contra-Tendência do Papel?", 14)]
//        public bool usarSaidaNaContraTendencia = true;

//        [InputParameter("Tipo de Saída:", 15, new Object[] { "3MA", TipoDeSaida.MA3, "DifMédias", TipoDeSaida.DifMédias })]
//        public TipoDeSaida tipoDeSaida = TipoDeSaida.MA3;

//        [InputParameter("Multiplicador da Dif.Das Médias", 16)]
//        public int multiplicador = 10;

//        [InputParameter("Delta Stop Loss da Dif.Das Médias", 17)]
//        public int deltaStopLoss = 10;

//        public override void Init()
//        {
//            //******************************
//            //**** INICIALIZAÇÃO DO ROBÔ ***
//            //******************************
//            try
//            {
//                instrument = Instruments.Current;
//                Print("Nome do Instrumento ativo: " + instrument.Name);
//                papel = instrument.BaseName;
//                conta = Accounts.Current;
//                tickSize = instrument.TickSize;
//                Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
//                SetSyncTimer();
//                InfoConexao();
//                AccountInformation(conta);

//                ReEntradas = ticksDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
//                ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();

//                if (ReEntradas.Length != ValoresDeReEntradas.Length)
//                {
//                    throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
//                }

//                Print("Papel: " + instrument.Name);
//                Print("Tamanho do Point: " + Point);
//                Print("Tamanho do tick: " + instrument.TickSize);

//                //EVENTOS            
//                Positions.PositionAdded += Positions_PositionAdded;
//                Positions.PositionRemoved += Positions_PositionRemoved;

//                //INDICADORES
//                MM = Indicators.iCustom("camilo_3mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovel, periodoMediaMovelSlow);
//                maSignal = Indicators.iCustom("MAS3", CurrentData, periodoMediaMovelSinal, periodoMediaMovel, periodoMediaMovelSlow, 1);

//                Print("Fim da Inicialização do Robô");

//                if (myConnection.Status == ConnectionStatus.Disconnected)
//                {
//                    Print("RODANDO ROBÔ NO SIMULADOR. NÚMERO MÁGICO É 0");
//                    numeroMagico = 0; // SE ESTIVER NO SIMULADOR O NUMERO MÁGICO É ZERO
//                }
//                else
//                {
//                    Print("RODANDO ROBÔ NA CONTA DEMO OU REAL. Número mágico deste robô é " + numeroMagico);
//                }
//            }
//            catch (Exception exc)
//            {
//                Print("Erro na inicialização do Robô... " + exc.Message);
//                Print("Finalizando Robô");
//                TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
//                foreach (var robo in robos)
//                {
//                    robo.Stop();
//                }
//            }


//        }

//        private void Positions_PositionRemoved(Position obj)
//        {
//            try
//            {
//                Print("PositionRemoved: Posição com id " + obj.Id + " de tipo " + obj.Side + " do Papel " + obj.Instrument.BaseName + " foi removida! MagicNumber:" + obj.MagicNumber);
//                if (obj.Instrument.BaseName == papel)
//                {
//                    LimparTodasAsOrdens();
//                    estado = state.limbo;

//                    if (contadorDeReEntradas > 0) contadorDeReEntradas--;

//                }
//            }
//            catch (Exception exc)
//            {
//                Print("PositionRemoved: Erro no Position Removed: " + exc.Message);
//                LimparTodasAsOrdens();
//                estado = state.limbo;
//            }
//        }

//        private void Positions_PositionAdded(Position obj)
//        {
//            try
//            {
//                Print("PositionAdded: Posição com id " + obj.Id + " de tipo " + obj.Side + " no Papel " + obj.Instrument.BaseName + " foi adicionada! MagicNumber:" + obj.MagicNumber);

//                if (obj.Instrument.BaseName == papel)
//                {
//                    LimparTodasAsOrdens();
//                    ultimaReentrada = obj.OpenPrice;
//                    CriaParDeSaida(obj);
//                    estado = state.limbo;
//                    if (contadorDeReEntradas < ReEntradas.Length - 1) contadorDeReEntradas++;
//                }
//            }
//            catch (Exception ext)
//            {
//                Print("PositionAdded: Erro no Position Added: " + ext.Message);
//                LimparTodasAsOrdens();
//                estado = state.limbo;
//            }
//        }

//        public override void OnQuote()
//        {
//            Print("********************   OnQuote   ********************");
//            positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel)?.ToArray();
//            orders = Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel)?.ToArray();

//            if (positions == null) positions = new Position[0];
//            if (orders == null) orders = new Order[0];

//            try
//            {
//                cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
//                longa = MM.GetValue(2, 0);
//                curta = MM.GetValue(0, 0);
//                curtaAnterior = MM.GetValue(0, 1);
//                trend = (Trend)maSignal.GetValue(0);

//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
//                Print("OnQuote: InnerException " + exc.InnerException.Message);
//                return;
//            }

//            try
//            {
//                Quote quote = instrument.LastQuote;

//                if (quote != null)
//                {
//                    if (myConnection.Status == ConnectionStatus.Connected) { horario = DateTime.Now.TimeOfDay; hora = horario.Hours; } else { horario = quote.Time.ToLocalTime().TimeOfDay; hora = horario.Hours; }
//                    Print("CheckMarketStatus: Hora Atual = " + horario.ToString());
//                    if (hora == 0) { return; }
//                    if (hora >= 9) { horarioTerminou = false; }
//                    if (horario.CompareTo(HoraDeInicio) < 0) { Print("Aguardando hora de Início..."); return; }
//                    if (horario.CompareTo(HoraDeTermino) >= 0) { Print("CheckMarketStatus: Hora de término atingida!"); horarioTerminou = true; }
//                    if (hora >= 18)
//                    {
//                        horarioTerminou = true;
//                        if (Positions.Count > 0) Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); Positions.CloseAll();
//                        Print("CheckMarketStatus: Bolsa fechada !");
//                        return;
//                    }
//                }
//                else
//                {
//                    Print("OnQuote: última cotação foi nula!");
//                    return;
//                }

//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote: Erro na coleta da Hora . Erro: " + exc.Message);
//                Print("OnQuote: InnerException " + exc.InnerException.Message);
//                return;
//            }

//            try
//            {

//                if (curta == 0 || curtaAnterior == 0) { Print("Aguardando indicadores!"); return; }


//                Print("OnQuote: Computando Calor máximo...");

//                if (positions.Length > 0)
//                {
//                    Position pos = positions[0];
//                    if (pos != null)
//                    {
//                        if (pos.GetProfitNet() < calor) { calor = pos.GetProfitNet(); horaDoCalorMaximo = hora.ToString(); maxDePosicaoAberta = pos.Amount; }
//                    }
//                }

//                Print("OnQuote: Calor Máximo até o momento: " + calor);

//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote: Erro setor Calor Máximo . " + exc.Message);
//                Print("OnQuote: InnerException " + exc.InnerException.Message);
//                return;
//            }

//            try
//            {
//                //SAÍDA POR LOSS MÁXIMO
//                if (positions.Length > 0)
//                {
//                    Position pos = positions[0];
//                    if (pos != null)
//                    {
//                        Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize) + " Limite: " + lossMaximo + " ticks");

//                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize >= lossMaximo)
//                        {
//                            Print("*****************    LOSS MÁXIMO ATINGIDO    ********************");

//                            bool result = false;
//                            contadorDeReEntradas = -1;
//                            if (lossMaximoParcial)
//                            {
//                                LimparTodasAsOrdens();
//                                FechamentoParcial();
//                                if (result) { Print("Fazendo fechamento PARCIAL das Posições !!!"); estado = state.limbo; }
//                            }
//                            else
//                            {
//                                LimparTodasAsOrdens();
//                                FechamentoTotal();
//                                if (result) { Print("Fechando todas as Posições por LOSS MÁXIMO !!!"); estado = state.limbo; }
//                            }
//                        }
//                    }
//                }

//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote: Erro setor Loss Máximo . " + exc.Message);
//                Print("OnQuote: InnerException " + exc.InnerException.Message);
//            }

//            try
//            {
//                //TIMER DO SIMULADOR
//                //SE ESTIVER NO SIMULADOR, EXECUTE ABAIXO
//                if (myConnection.Status == ConnectionStatus.Disconnected)
//                {
//                    var ultimaCotacao = instrument.LastQuote;
//                    if (ultimaCotacao != null)
//                    {
//                        DateTime horaDaUltimaCotacao = ultimaCotacao.Time.ToLocalTime();
//                        var diffSegundos = Math.Abs((horaDaUltimaCotacao - timerSimulador).TotalSeconds);
//                        Print("Timer Simulador: Diferença de " + diffSegundos + " segundos");
//                        if (diffSegundos > timerParaCancelar)
//                        {
//                            Print("Timer Simulador: Estouro de TIMER SIMULADOR!");
//                            timerSimulador = horaDaUltimaCotacao;
//                            if (estado == state.limbo)
//                            {
//                                Print("TimerSimulador: Tempo de espera terminou. Mandando o caso para o Juiz!");
//                                if (estado == state.limbo) Juiz();
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote: Erro setor Timer . " + exc.Message);
//                Print("OnQuote: InnerException " + exc.InnerException.Message);
//                return;
//            }

//            try
//            {

//                switch (estado)
//                {
//                    case state.limbo:
//                        {
//                            Print("********************   Estado Limbo *****************");

//                            Print("Limbo: Tendência " + trend);
//                            Print("Limbo: Papel:" + instrument.BaseName + "\t Numero mágico " + numeroMagico + "\t Lucro de hj: " + conta.TodayNet + "\t Qtde de Trades: " + conta.TodayTrades + "\t É conta Real? " + conta.IsReal + "\t É conta Demo?" + conta.IsDemo + "\t Status:" + conta.Status);

//                            break;
//                        }
//                    case state.inicio:
//                        {
//                            Print("********************   Estado Início  ********************");
//                            if (horarioTerminou) { Print("Horário de Término atingido, não entra mais..."); return; }
//                            if (true)
//                            {
//                                contadorDeReEntradas = -1;
//                                Print("Estado Inicio: Preço atual:" + cotacaoAtual + " Média curta:" + curta + " Preço - Média:" + (cotacaoAtual - curta));
//                                if (tipoDeEntrada == TipoDeEntrada.MA3) Print("Estado Inicio: Modo de Entrada: Tendência " + trend);
//                                if (tipoDeEntrada == TipoDeEntrada.DifMédias) Print("Estado Inicio: Modo de Entrada: Sinal Atual - Anterior = " + (curta - curtaAnterior));

//                                if (tipoDeEntrada == TipoDeEntrada.MA3)
//                                {
//                                    if (Entrada(cotacaoAtual) != "-1")
//                                    {
//                                        Print("Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo");
//                                        estado = state.limbo;
//                                        return;
//                                    }
//                                    else Print("Estado Inicio: Aguardando Condições de Entrada");
//                                }

//                                if (tipoDeEntrada == TipoDeEntrada.DifMédias)
//                                {
//                                    if (EntradaDiffMedias(cotacaoAtual, curta - curtaAnterior) != "-1")
//                                    {
//                                        Print("Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo");
//                                        estado = state.limbo;
//                                        return;
//                                    }
//                                    else Print("Estado Inicio: Aguardando Condições de Entrada");
//                                }
//                            }
//                            break;
//                        }

//                    case state.reentrada:
//                        {
//                            Print("********************   Estado ReEntrada ***********************");
//                            positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
//                            if (positions.Length == 0) { Print("Estado ReEntrada: Não existe mais posição. Migrando para inicio"); LimparTodasAsOrdens(); estado = state.inicio; return; }

//                            if (positions.Length != 0)
//                            {
//                                Position pos = positions[0];
//                                var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];
//                                var liberaReEntrada = false;

//                                if (pos.Side == Operation.Buy && pos.CurrentPrice <= ultimaReentrada - deltaMoeda)
//                                {
//                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
//                                    liberaReEntrada = true;
//                                }

//                                if (pos.Side == Operation.Sell && pos.CurrentPrice >= ultimaReentrada + deltaMoeda)
//                                {
//                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
//                                    liberaReEntrada = true;
//                                }

//                                if (liberaReEntrada)
//                                {
//                                    Print("OnQuote: Ok! Tentando Reentrar...");
//                                    if (positions[0].Amount <= maxPosAbertas / 2)
//                                    {
//                                        var result = ReEntrada(cotacaoAtual, positions[0]);
//                                        if (result != "-1") { Print("Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo."); estado = state.limbo; }
//                                    }
//                                    else
//                                    {
//                                        Print("Estado ReEntrada: Máximo de Contratos atingidos!");
//                                    }
//                                }
//                            }
//                            break;
//                        }

//                    case state.saidaTendencia:
//                        {
//                            Print("********************   Estado Saída na Contra-Tendência ***********************");
//                            if (positions.Length > 0)
//                            {
//                                Position pos = positions[0];
//                                if (tipoDeSaida == TipoDeSaida.DifMédias && pos != null)
//                                {
//                                    Print("Estado Saída na Contra-Tendência: Saída por Diferença das Médias desfavorável ao papel");
//                                    Print("Estado Saída na Contra-Tendência: (curta - curtaAnterior) * multiplicador = " + ((curta - curtaAnterior) * multiplicador) + " Delta:" + deltaStopLoss + " Tipo do Papel:" + pos.Side);
//                                    if ((curta - curtaAnterior) * multiplicador >= deltaStopLoss && pos.Side == Operation.Sell)
//                                    {
//                                        Print("Estado Saída na Contra-Tendência: Condições de saída alcançadas. Fazendo saída total");
//                                        FechamentoTotal();
//                                    }

//                                    if ((curta - curtaAnterior) * multiplicador <= -deltaStopLoss && pos.Side == Operation.Buy)
//                                    {
//                                        Print("Estado Saída na Contra-Tendência: Condições de saída alcançadas. Fazendo saída total");
//                                        FechamentoTotal();
//                                    }
//                                }
//                                if (tipoDeSaida == TipoDeSaida.MA3 && pos != null)
//                                {
//                                    Print("Estado Saída na Contra-Tendência: Saída por 3MA contra Papel");
//                                    Print("Estado Saída na Contra-Tendência: Tendência Atual: " + trend + " Tipo do Papel: " + pos.Side);
//                                    if (trend == Trend.Up && pos.Side == Operation.Sell)
//                                    {
//                                        Print("Estado Saída na Contra-Tendência: Condições de saída alcançadas. Fazendo saída total");
//                                        FechamentoTotal();
//                                    }
//                                    if (trend == Trend.Down && pos.Side == Operation.Buy)
//                                    {
//                                        Print("Estado Saída na Contra-Tendência: Condições de saída alcançadas. Fazendo saída total");
//                                        FechamentoTotal();
//                                    }
//                                }
//                            }
//                            break;
//                        }

//                    default:
//                        {
//                            Print("Estado Juiz: Nenhum caso foi analisado pelo Juiz. Voltando para o limbo");
//                            estado = state.limbo;
//                            break;
//                        }
//                }

//            }
//            catch (Exception exc)
//            {
//                Print("OnQuote:  Erro setor switch . Mensagem -> " + exc.Message);
//                Print("OnQuote Erro:  InnerMessage -> " + exc.InnerException.Message);
//                return;
//            }
//        }

//        public override void NextBar()
//        {

//        }

//        public override void Complete()
//        {
//            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
//            syncTimer.Close();
//            syncTimer.Dispose();
//        }


//        private string Entrada(double preco)
//        {
//            if (trend == Trend.Down) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
//            if (trend == Trend.Up) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
//            return "-1";
//        }

//        private string EntradaDiffMedias(double preco, double diff)
//        {
//            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
//            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
//            return "-1";
//        }

//        private string ReEntrada(double preco, Position pos)
//        {
//            var precoMedio = pos.OpenPrice;
//            var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];

//            if (pos.Side == Operation.Buy)
//            {
//                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
//                return Send_order(OrdersType.Market, Operation.Buy, preco, ValoresDeReEntradas[contadorDeReEntradas], 0);
//            }

//            if (pos.Side == Operation.Sell)
//            {
//                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
//                return Send_order(OrdersType.Market, Operation.Sell, preco, pos.Amount, 0);
//            }

//            return "-1";
//        }


//        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = instrument;
//            request.Account = conta;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.MarketRange = 10;
//            request.MagicNumber = numeroMagico;

//            if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? instrument.RoundPrice(price + takeProfit * instrument.TickSize) : instrument.RoundPrice(price - takeProfit * instrument.TickSize);
//            if (type != OrdersType.StopLimit) { request.Price = instrument.RoundPrice(price); } else { request.StopPrice = instrument.RoundPrice(price); }

//            Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + instrument.RoundPrice(price) + " Qtde:" + request.Amount + " Ponto Atual da Média:" + curta);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("SendOrder: Ordem não aceita pela corretora"); }
//            if (result != "-1") Print("SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!");
//            return result;
//        }

//        private bool LiberaEntrada(double preco, double sinal, double media)
//        {
//            if (preco > media && preco < sinal) return true;
//            if (preco < media && preco > sinal) return true;

//            return false;
//        }

//        private void LimparTodasAsOrdens()
//        {
//            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord =>
//             {
//                 var result = ord.Cancel();
//                 if (!result)
//                 {
//                     Print("LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA! Status: " + ord.Status);
//                 }
//                 else
//                 {
//                     Print("LimparTodasAsOrdens: Ordem " + ord.Id + " FOI CANCELADA! Status: " + ord.Status);
//                 }
//             });
//        }

//        private bool CriaParDeSaida(Position posicao = null)
//        {
//            //TAKE PROFIT LIMIT 
//            Position[] positions;
//            if (posicao == null)
//            {
//                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
//                if (positions.Length > 0) posicao = positions[0];
//            }

//            if (posicao != null)
//            {
//                Print("OnQuote: Ok! Tentando setar TAKE PROFIT LIMIT");
//                try
//                {
//                    Position pos = posicao;
//                    if (pos.Side == Operation.Buy)
//                    {
//                        NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
//                        request.MagicNumber = numeroMagico;
//                        request.MarketRange = 1000;
//                        var id = Orders.Send(request);
//                        if (id == "-1")
//                        {
//                            Print("OnQuote: Ordem Limit de saída SELL não colocada... tentando ordem STOP");
//                            NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
//                            req.MagicNumber = numeroMagico;
//                            req.MarketRange = 1000;
//                            var id2 = Orders.Send(req);
//                            if (id2 == "-1") { Print("OnQuote: Ordem SELL STOP não aceita!"); LimparTodasAsOrdens(); return false; } else { return true; }
//                        }
//                        else
//                        {
//                            return true;
//                        }
//                    }
//                    if (pos.Side == Operation.Sell)
//                    {
//                        NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
//                        request.MagicNumber = numeroMagico;
//                        request.MarketRange = 10;
//                        var id = Orders.Send(request);
//                        if (id == "-1")
//                        {
//                            Print("OnQuote: Ordem Limit de saída BUY não colocada... tentando ordem STOP");
//                            NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
//                            req.MagicNumber = numeroMagico;
//                            req.MarketRange = 1000;
//                            var id2 = Orders.Send(req);
//                            if (id2 == "-1") { Print("OnQuote: Ordem BUY STOP não aceita!"); LimparTodasAsOrdens(); return false; } else { return true; }
//                        }
//                        else
//                        {
//                            return true;
//                        }
//                    }
//                }
//                catch (Exception ext)
//                {
//                    Print("OnQuote: Erro ao Setar Take Profit! " + ext.Message);
//                    return false;
//                }
//            }
//            else
//            {
//                Print("CriaParDeSaída: Não existe posição");
//                LimparTodasAsOrdens();
//            }

//            return false;
//        }




//        private void SetSyncTimer()
//        {
//            // cria um timer com 1s de intervalo
//            syncTimer = new System.Timers.Timer(timerParaCancelar * 1000);
//            syncTimer.Elapsed += SyncTimer_Elapsed;
//            syncTimer.AutoReset = true;
//            syncTimer.Enabled = (myConnection.Status == ConnectionStatus.Connected) ? true : false;
//        }

//        private void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//        {
//            try
//            {
//                Print("********************   Timer    *******************");
//                Print("Timer: Estado atual:" + estado);
//                Print("Timer: Estouro de Timer de " + timerParaCancelar.ToString() + " segundos. Mandando o caso para o Juiz analisar!");
//                Juiz();
//                Print("Timer: Novo estado:" + estado);
//                Print("Timer: Printando posições e ordens ativas se houver...");

//                Position[] posicoesGlobais = Positions.GetPositions();
//                Order[] ordensGlobais = Orders.GetOrders();
//                if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => PosInfo(pos)); }
//                if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => OrderInfo(ord)); }

//            }
//            catch (Exception ext)
//            {
//                Alert("Timer: Erro no SyncTimer: " + ext.Message);
//            }

//        }

//        private void AccountInformation(Account acc)
//        {
//            //outputting of all the 'Account' properties
//            Print(
//                "Id : \t" + acc.Id + "\n" +
//                "Name : \t" + acc.Name + "\n" +
//                "User Login: \t" + acc.User.Login + "\n" +
//                "Balance : \t" + acc.Balance + "\n" +
//                "Leverage : \t" + acc.Leverage + "\n" +
//                "Currency : \t" + acc.Currency + "\n" +
//                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
//                "IsDemo : \t" + acc.IsDemo + "\n" +
//                "IsReal : \t" + acc.IsReal + "\n" +
//                "IsLocked : \t" + acc.IsLocked + "\n" +
//                "IsInvestor : \t" + acc.IsInvestor + "\n" +
//                "Status : \t" + acc.Status + "\n" +
//                "StopReason : \t" + acc.StopReason + "\n" +
//                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
//                "Balance : \t" + acc.Balance + "\n" +
//                "BeginBalance : \t" + acc.BeginBalance + "\n" +
//                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
//                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
//                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
//                "Credit : \t" + acc.Credit + "\n" +
//                "CashBalance : \t" + acc.CashBalance + "\n" +
//                "TodayVolume : \t" + acc.TodayVolume + "\n" +
//                "TodayNet : \t" + acc.TodayNet + "\n" +
//                "TodayTrades : \t" + acc.TodayTrades + "\n" +
//                "TodayFees : \t" + acc.TodayFees + "\n" +
//                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
//                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
//                "MarginTotal : \t" + acc.MarginTotal + "\n" +
//                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
//                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
//                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
//                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
//                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
//                "Equity : \t" + acc.Equity + "\n" +
//                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
//                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
//                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
//                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
//                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
//            );
//        }

//        private void PosInfo(Position pos)
//        {
//            double proximaReentrada = 0;
//            if (contadorDeReEntradas > ValoresDeReEntradas.Length - 1) contadorDeReEntradas = ValoresDeReEntradas.Length - 1;
//            if (contadorDeReEntradas >= 0)
//            {
//                proximaReentrada = (pos.Side == Operation.Buy) ? ultimaReentrada - instrument.TickSize * ReEntradas[contadorDeReEntradas] : ultimaReentrada + instrument.TickSize * ReEntradas[contadorDeReEntradas];
//                Print("Entrada : " + contadorDeReEntradas);
//            }
//            Print(
//                "PosInfo()-> " +
//                "  Papel: " + pos.Instrument.BaseName + "\t" +
//                "  Id: " + pos.Id + "\t" +
//                "  Qtde: " + pos.Amount + "\t" +
//                "  Tipo: " + pos.Side.ToString() + "\t" +
//                "  Preço Abertura: R$ " + pos.OpenPrice + "\t" +
//                "  Preço Atual: R$ " + pos.CurrentPrice + "\t" +
//                "  Número Mágico: " + pos.MagicNumber + "\t" +
//                "  Última Reentrada: R$" + ultimaReentrada + "\t" +
//                "  Próxima Reentrada: R$" + proximaReentrada + "\t" +
//                "  Lucro ou Prejuízo: R$" + pos.GetProfitNet()
//            );
//        }

//        private void OrderInfo(Order ord)
//        {
//            Print(
//                "OrderInfo()-> " +
//                "  Papel: " + ord.Instrument.BaseName + "\t" +
//                "  Id: " + ord.Id + "\t" +
//                "  Qtde: " + ord.Amount + "\t" +
//                "  Tipo: " + ord.Side.ToString() + "\t" +
//                "  Preço: R$ " + ord.Price + "\t" +
//                "  Número Mágico: " + ord.MagicNumber
//            );

//        }

//        private void InfoConexao()
//        {
//            //outputting of all the information about the current connection.
//            Print(
//            "\nConnection name : \t" + myConnection.Name +
//            "\nConnection address : \t" + myConnection.Address +
//            "\nConnection login : \t" + myConnection.Login +
//            "\nSLL on?  : \t" + myConnection.IsUseSSL +
//            "\nProtocol version : \t" + myConnection.ProtocolVersion +
//            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
//            "\nConnection status : \t" + myConnection.Status
//            );
//        }

//        public void FechamentoParcial()
//        {
//            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
//        }

//        public void FechamentoTotal()
//        {
//            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");
//            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord => ord.Cancel());
//            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close());
//        }

//        private void Juiz()
//        {
//            Print("********************   Estado Juiz ***********************");
//            orders = Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ToArray();
//            positions = Positions.GetPositions()?.ToList().FindAll(posit => posit.Instrument.BaseName == papel).ToArray();
//            if (positions == null) positions = new Position[0];
//            if (orders == null) orders = new Order[0];

//            //Caso 1: Não existe posição e Não existe ordem
//            if (positions.Length == 0 && orders.Length == 0)
//            {
//                //como chegou um caso assim para o juiz analisar? É impossível pois a migração de estados na entrada e na reentrada apenas ocorrem se a corretora aceitar as ordens
//                //mesmo assim, é melhor prever isso
//                Print("Estado Juiz: Não existe Posição e Não existe Ordem para ser processada. estado -> inicio");
//                estado = state.inicio;
//                return;
//            }

//            //Caso 2: Não existe posição, e ordem tem operação pendente
//            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ord.Status == OrderStatus.PendingNew || ord.Status == OrderStatus.PendingReplace || ord.Status == OrderStatus.PendingCancel || ord.Status == OrderStatus.PendingExecution).Count != 0)
//            {
//                Print("Estado Juiz: Existe Ordem com operação pendente ainda... aguarde ela definir o que vai fazer");
//                return;
//            }

//            //Case 3: Não existe posição e Existe ordem limite aguardando execução
//            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew) && ord.Type == OrdersType.Limit)).Count != 0)
//            {
//                Print("Estado Juiz: Não existe posição e uma ordem Limite ainda não foi executada. Cancela tudo e migra estado para inicio");
//                LimparTodasAsOrdens();
//                estado = state.inicio;
//                return;
//            }

//            //Case 4: Existe posição mas não existe ordem de saída                            
//            if (positions.Length != 0 && orders.Length == 0)
//            {
//                Print("Estado Juiz: Existe posição mas não existe ordem de saída. CriaParDeSaída()");
//                if (CriaParDeSaida()) { estado = state.limbo; } else { Print("Estado Juiz: Erro na Criação do Par de Saída!"); }
//                return;
//            }

//            //Case 5: Existe Posição e Existe Ordem de Saída Limite
//            if (positions.Length != 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew) && ord.Type == OrdersType.Limit)).Count != 0)
//            {
//                //Verifique se a Ordem tem mesma quantidade que Posição
//                if (positions[0].Amount == orders[0].Amount)
//                {
//                    if (!usarSaidaNaContraTendencia)
//                    {
//                        Print("Estado Juiz: Existe posição e Existe ordem de saída. Migrando para estado ReEntrada");
//                        estado = state.reentrada;
//                    }
//                    else
//                    {
//                        Print("Estado Juiz: Existe posição e Existe ordem de saída. Migrando para Saída na Contra-Tendência do Papel");
//                        estado = state.saidaTendencia;
//                    }
//                }
//                else
//                {
//                    Print("Estado Juiz: Ordem de Saída com Qtde diferente da Posição. Modificando qtde da ordem.");
//                    if (orders[0].Modify(orders[0].Price, positions[0].Amount))
//                    {
//                        Print("Estado Juiz: Envio de solicitação de modificação de Ordem foi aceito! Migrando para o Limbo...");
//                        estado = state.limbo;
//                    }
//                }


//                return;
//            }
//        }



//    }

//}
