﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Globalization;

namespace RoboDollar
{
    enum tipoOperacao { Compra, Venda, Fechamento }

    /// <summary>
    /// RoboModelo
    /// 
    /// </summary>
    public class RoboDollar : NETStrategy
    {

        private Instrument instrument;
        private Instrument papelDestino;
        private Account CAccount;
        private Indicator indicadorHILOmin;
        private Indicator indicadorHILOmax;
        private Indicator indicadorSinal;
        private DateTime inicio;
        private DateTime termino;
        private double cotacaoAtual;
        private double HILOmin;
        private double HILOmax;
        private double Sinal;
        private int resilienciaEntradaCounter = 0;
        private int resilienciaSaidaCounter = 0;
        private DateTime tempo;
        private int hora;
        private int minutos;
        private Boolean bolsaAberta = false;

        private tipoOperacao ultimaOperacao = tipoOperacao.Fechamento;

        List<string> openedOrders = new List<string>();

        [InputParameter("Periodo Médias Móveis Max e Min", 0)]
        public int PeriodoHiLo = 12;

        [InputParameter("Periodo Sinal", 1)]
        public int PeriodoSinal = 3;

        [InputParameter("Price Type", 2, new object[] {
            "Close",PriceType.Close,
            "High",PriceType.High,
            "Low",PriceType.Low,
            "Medium",PriceType.Medium,
            "Open",PriceType.Open,
            "Typical",PriceType.Typical,
            "Weighted",PriceType.Weighted
        })]
        public PriceType priceType = PriceType.Close;

        [InputParameter("tipo de MA", 3, new object[]
       {
             "EMA - Exponencial", PTLRuntime.NETScript.Indicators.MAMode.EMA,
             "LWMA- Linear Weighted", PTLRuntime.NETScript.Indicators.MAMode.LWMA,
             "SMA - Aritmética", PTLRuntime.NETScript.Indicators.MAMode.SMA,
             "SMMA", PTLRuntime.NETScript.Indicators.MAMode.SMMA
       })]
        public PTLRuntime.NETScript.Indicators.MAMode MA_type1 = PTLRuntime.NETScript.Indicators.MAMode.EMA; // First smoothing type

        [InputParameter("Contratos", 4)]
        public int Contratos = 1;

        [InputParameter("Hora de Início", 5)]
        public string HoraDeInicio = "09:01:00";

        [InputParameter("Hora de Término", 6)]
        public string HoraDeTermino = "16:50:00";

        [InputParameter("Resiliência na Entrada", 7)]
        public int resilienciaEntrada = 3;

        [InputParameter("Resiliencia na Saída", 8)]
        public int resilienciaSaida = 3;

        public RoboDollar()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "06.09.2018";
            base.ExpirationDate = 0;
            base.Version = "0.0.1";
            base.Password = "";
            base.ProjectName = "RoboDollar";
            #endregion           

        }

        /// <summary>
        /// This function will be called after creating
        /// </summary>
        public override void Init()
        {
            instrument = Instruments.Current;
            papelDestino = instrument;
            Instruments.Subscribe(instrument, QuoteTypes.Quote);
            CAccount = Accounts.Current;

            indicadorHILOmin = Indicators.iMA(CurrentData, PeriodoHiLo, MA_type1, 0, PriceType.Low);
            indicadorHILOmax = Indicators.iMA(CurrentData, PeriodoHiLo, MA_type1, 0, PriceType.High);
            indicadorSinal = Indicators.iMA(CurrentData, PeriodoSinal, PTLRuntime.NETScript.Indicators.MAMode.SMA, 0, PriceType.Typical);

            DateTime.TryParseExact(HoraDeInicio, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out inicio);
            DateTime.TryParseExact(HoraDeTermino, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out termino);
        }

        /// <summary>
        /// Entry point. This function is called when new quote comes 
        /// </summary>
        public override void OnQuote()
        {
            tempo = instrument.LastQuote.Time.ToLocalTime();
            hora = instrument.LastQuote.Time.ToLocalTime().Hour;
            minutos = instrument.LastQuote.Time.ToLocalTime().Minute;
            cotacaoAtual = CurrentData.GetPrice(priceType);
            HILOmin = indicadorHILOmin.GetValue();
            HILOmax = indicadorHILOmax.GetValue();
            Sinal = indicadorSinal.GetValue();

            if (hora >= inicio.Hour && hora <= termino.Hour && minutos >= inicio.Minute && minutos <= termino.Minute)
            {
                bolsaAberta = true;
            }
            else
            {
                bolsaAberta = false;
            }

            if (instrument.LastQuote == null) return;
            if (HILOmin == 0 || HILOmax == 0 || cotacaoAtual == 0)
            {
                return;
            }

            //critérios de SAÍDA
            if (bolsaAberta && (ultimaOperacao == tipoOperacao.Compra || ultimaOperacao == tipoOperacao.Venda))
            {
                resilienciaSaidaCounter++;
                if (resilienciaSaidaCounter > resilienciaSaida)
                {
                    resilienciaSaidaCounter = 0;
                    if (Sinal < HILOmin && ultimaOperacao == tipoOperacao.Compra)
                    {
                        //Alert("Zerou Compra");
                        Fechar();
                        openedOrders.Clear();
                        ultimaOperacao = tipoOperacao.Fechamento;
                    }

                    if (Sinal > HILOmax && ultimaOperacao == tipoOperacao.Venda)
                    {
                        //Alert("Zerou Venda");
                        Fechar();
                        openedOrders.Clear();
                        ultimaOperacao = tipoOperacao.Fechamento;
                    }
                }

            }

            //Critérios de entrada
            if (openedOrders.Count == 0 && bolsaAberta)
            {
                resilienciaEntradaCounter++;
                if (resilienciaEntradaCounter > resilienciaEntrada)
                {
                    resilienciaEntradaCounter = 0;
                    if (Sinal > HILOmax)
                    {
                        string result = Send_order(OrdersType.Limit, Operation.Buy, instrument.LastQuote.Bid, Contratos, 150, papelDestino);
                        openedOrders.Add(result);
                        ultimaOperacao = tipoOperacao.Compra;
                    }

                    if (Sinal < HILOmin)
                    {
                        string result = Send_order(OrdersType.Limit, Operation.Sell, instrument.LastQuote.Ask, Contratos, 150, papelDestino);
                        openedOrders.Add(result);
                        ultimaOperacao = tipoOperacao.Venda;

                    }
                }
            }
        }

        public override void NextBar()
        {

        }

        private void Fechar()
        {
            if (openedOrders == null || openedOrders.Count == 0) return;

            foreach (string orderId in openedOrders)
            {
                Position pos = Positions.GetPositionById(orderId);
                if (pos != null) pos.Close();

                Order ord = Orders.GetOrderById(orderId);
                if (ord != null) ord.Cancel();
            }
        }

        /// <summary>
        /// This function will be called before removing
        /// </summary>
        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote);
            indicadorHILOmax = null;
            indicadorHILOmin = null;
            indicadorSinal = null;

            if (openedOrders == null || openedOrders.Count == 0) return;

            foreach (string orderId in openedOrders)
            {
                Position pos = Positions.GetPositionById(orderId);
                if (pos != null) pos.Close();

                Order ord = Orders.GetOrderById(orderId);
                if (ord != null) ord.Cancel();
            }
        }

        public string Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest()
            {
                Instrument = papelDestino,
                Account = Accounts.Current,
                Type = type,
                Side = side,
                Amount = contratos,
                Price = price,
                MarketRange = marketRange //market range para compra é 10 para venda é 30
                //TakeProfitOffset = Take_Profit * Point,
                //StopLossOffset = Stop_Loss * Point
            };
            return Orders.Send(request);
        }
    }
}