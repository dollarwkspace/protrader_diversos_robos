﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;

namespace Camilo_Vwap
{
    /// <summary>
    /// Camilo_Vwap
    /// 
    /// </summary>
    public class Camilo_Vwap : NETIndicator 
    {
        public Camilo_Vwap()
            : base()
        {
			#region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "29.08.2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "66b4a6416f59370e942d353f08a9ae36";
            base.ProjectName = "Camilo_Vwap";
            #endregion 
            
            base.SetIndicatorLine("Macd", Color.Green, 1, LineStyle.SimpleChart);
            base.SetIndicatorLine("Sinal", Color.White, 1, LineStyle.SimpleChart);
            base.SetIndicatorLine("Histograma", Color.Blue, 1, LineStyle.HistogrammChart);
            base.SeparateWindow = true;
        }
        
        [InputParameter("Rápido",0)]
        public int fast=0;
        [InputParameter("Lento",0)]
        public int slow=0;
        [InputParameter("Sinal",0)]
        public int signal=0;
        
        Indicator macd;
        
        /// <summary>
        /// This function will be called after creating
        /// </summary>
		public override void Init()		
		{
			macd = Indicators.iMACD(CurrentData, fast, slow, signal);
		
		}        
 
        /// <summary>
        /// Entry point. This function is called when new quote comes 
        /// </summary>
        public override void OnQuote(){
        	
			SetValue(0, macd.GetValue(0, 0));
			SetValue(1, macd.GetValue(1, 0));
			SetValue(2, macd.GetValue(0, 0)-macd.GetValue(1,0));
			
        }
        
        
        
        
        /// <summary>
        /// This function will be called before removing
        /// </summary>
		public override void Complete()
		{
			
		} 
     }
}
