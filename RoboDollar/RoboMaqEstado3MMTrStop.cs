﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollarRenko
//{
//    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }
//    public enum maquinaDeEstado { Inicio, PosicaoAberta, InicioHiLoE3MM, nula }
//    public enum algoritmo { StartSensibilidadePosAbertaRevTendencia, StartHiLo3MMPosAbertaRevTendencia }

//    interface IState
//    {
//        void Processa(tipoOperacao _operacao);
//        void Operar();
//    }

//    /// <summary>
//    /// CLASSE STATE
//    /// NÃO PODE SER INSTANCIADA POIS É ABSTRATA
//    /// </summary>

//    public abstract class State : IState
//    {
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************   Campos privados, protected, public   ********************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************

//        private Boolean _todasAsPosicoesForamCanceladas = false;
//        private static int _quantidadeDePosicoesAbertas = 0;

//        //private System.Timers.Timer syncTimer;        

//        protected ObservableCollection<tipoOperacao> operacoes = new ObservableCollection<tipoOperacao>(); //Lista das recomendações das operações - detector de tendência                     

//        protected static Boolean aguardandoUpdateDasPosicoes = false;
//        public static Boolean maquinaEstaMigrando = false;
//        public static Boolean maquinaLacrada = false;
//        public static maquinaDeEstado maquinaDeEstadoAtual = maquinaDeEstado.nula;


//        public RoboDollar context { get; set; }

//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************             actions e funcs            ********************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************



//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************             PROPRIEDADES              *********************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************


//        private Boolean todasAsOrdensForamCanceladas { get; set; }

//        private Boolean todasAsPosicoesForamCanceladas
//        {
//            get { return _todasAsPosicoesForamCanceladas; }
//            set
//            {
//                _todasAsPosicoesForamCanceladas = value;
//                if (value == true)
//                {
//                    maquinaEstaMigrando = true;
//                    context.Alert("Todas as Posições foram canceladas!");
//                    if (maquinaDeEstadoAtual != maquinaDeEstado.Inicio)
//                    {
//                        context.Alert("Migrando máquina para Início");
//                        MigrarMaqEstado(maquinaDeEstado.Inicio);
//                    }

//                }
//            }
//        }

//        protected int quantidadeDePosicoesAbertas
//        {
//            get { return _quantidadeDePosicoesAbertas; }
//            set
//            {
//                if (_quantidadeDePosicoesAbertas != value)
//                {
//                    _quantidadeDePosicoesAbertas = value;
//                    aguardandoUpdateDasPosicoes = false;
//                }
//            }
//        }

//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************             CONSTRUTOR                 ********************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************

//        public State(RoboDollar _context)
//        {
//            context = _context;
//        }


//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************             MÉTODOS                    ********************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************
//        //***********************************************************************************************

//        //private void SetSyncTimer()
//        //{
//        //    // cria um timer com 5s de intervalo
//        //    syncTimer = new System.Timers.Timer(1000);
//        //    syncTimer.Elapsed += SyncTimer_Elapsed;
//        //    syncTimer.AutoReset = true;
//        //    syncTimer.Enabled = true;
//        //}

//        //private void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//        //{
//        //    quantidadeDePosicoesAbertas = Math.Abs((int)GetQtdeContratosAtivos());
//        //}


//        public void OrderToBeProcessed(NewOrderRequest orderToBeProcessed)
//        {
//            if (aguardandoUpdateDasPosicoes) return;
//            context.Alert("Emita uma ordem de " + orderToBeProcessed.Side);
//            context.Orders.Send(orderToBeProcessed);
//        }

//        public void Orders_OrderRemoved(Order obj)
//        {
//            context.Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            maquinaLacrada = false;
//            context.Alert("Ordem REMOVIDA implica em Fechamento Total das Posições");
//            FechamentoTotal();
//        }

//        public void Orders_OrderAdded(Order obj)
//        {
//            context.Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            maquinaLacrada = true;
//        }

//        public void Orders_OrderExecuted(Order obj)
//        {
//            context.Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
//            aguardandoUpdateDasPosicoes = true; //agpra ela só será false quando novas posições forem lidas.
//            maquinaLacrada = false;
//        }


//        private void Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
//        {
//            NewOrderRequest request = new NewOrderRequest()
//            {
//                Instrument = papelDestino,
//                Account = context.Accounts.Current,
//                Type = type,
//                Side = side,
//                Amount = contratos,
//                Price = price,
//                MarketRange = marketRange,
//                TimeInForce = TimeInForce.Day
//            };

//            OrderToBeProcessed(request);
//        }

//        protected double GetQtdeContratosAtivos()
//        {
//            double qtdeAbsolutaDeContratos = 0;
//            Position[] pos = context.Positions.GetPositions();
//            if (pos == null) { return 0; }
//            List<Position> contratosAtivos = pos.ToList();
//            contratosAtivos.ForEach(el =>
//            {
//                qtdeAbsolutaDeContratos += (el.Side == Operation.Buy) ? el.Amount : (-1) * el.Amount;
//            });

//            return qtdeAbsolutaDeContratos;
//        }

//        protected void MigrarMaqEstado(maquinaDeEstado destino)
//        {
//            context.SetState(destino);
//        }

//        protected void FechamentoParcial()
//        {
//            //1.conta todas os contratos abertos
//            //2.Se a qtde de contratos aberta for 1, então é um Fechamento Total
//            //3.Se for maior que 1 em número absoluto, então fecha metade. Se for ímpar arrendonda para cima o fechamento
//            //4.Se os papéis estiverem distribuídos em várias posições, feche do mais antigo até o maior novo, até completar metade dos contratos ativos

//            double qtdeAbsolutaDeContratos = Math.Abs(GetQtdeContratosAtivos());

//            if ((int)qtdeAbsolutaDeContratos == 1) { FechamentoTotal(); return; }

//            Position[] openedPositions = context.Positions.GetPositions(); //pega todas as posições abertas
//            if (openedPositions != null)
//            {

//                int qtdeDeContratosParaFechar = (int)Math.Ceiling(Math.Abs(qtdeAbsolutaDeContratos / 2)); // qtde de contratos para fechar é metade dos contratos existentes

//                int i = 0;
//                while (i < qtdeDeContratosParaFechar)
//                {
//                    var id = openedPositions[i].Id;
//                    var amount = openedPositions[i].Amount;
//                    var closeOrderId = openedPositions[i].CloseOrderId;
//                    context.Alert(" CloseOrderId:" + closeOrderId + " Qtde:" + amount + " id:" + id);
//                    var halfAmountToClose = Math.Ceiling(Math.Abs(amount) / 2);
//                    if (amount > 1) { openedPositions[i].Close(halfAmountToClose); }
//                    if (amount == 1) { openedPositions[i].Close(); }
//                    i++;
//                }
//            }
//        }

//        protected void ExecutaOrdem(tipoOperacao op, double preco, int qtde)
//        {
//            if (preco == 0) preco = context.CurrentData.GetPrice(PriceType.Typical);

//            switch (op)
//            {
//                case tipoOperacao.Compra:
//                    {
//                        Send_order(OrdersType.Limit, Operation.Buy, preco + 2 * context.instrument.TickSize, qtde, 150, context.papelDestino);
//                        //Send_order(OrdersType.Market, Operation.Buy, preco + 2 * context.Point, qtde, 150, context.papelDestino);
//                        break;
//                    }
//                case tipoOperacao.Venda:
//                    {
//                        Send_order(OrdersType.Limit, Operation.Sell, preco - 2 * context.instrument.TickSize, qtde, 150, context.papelDestino);
//                        //Send_order(OrdersType.Limit, Operation.Sell, preco - 2 * context.Point, qtde, 150, context.papelDestino);
//                        break;
//                    }
//            }
//        }

//        public void FechamentoTotal()
//        {
//            context.Alert("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");
//            //todasAsOrdensForamCanceladas = context.Orders.CancelAllByAccount(context.CAccount); //cancela todas as ordens ainda aguardando execução
//            //todasAsPosicoesForamCanceladas = context.Positions.CloseAllByAccount(context.CAccount); //e fecha todas as posições ativas             
//            Order[] orders = context.Orders.GetOrders();
//            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

//            Position[] positions = context.Positions.GetPositions();
//            if (positions != null) positions.ToList().ForEach(pos =>
//               {
//                   pos.Close();
//               });

//        }

//        public abstract void Processa(tipoOperacao _operacao);
//        public abstract void Operar();

//    }


//    /// <summary>
//    /// MÁQUINA DE ESTADO INÍCIO
//    /// Estado inicial da máquina de estados: Não existe posição aberta
//    /// Aguarda encher buffer de tendências para processamento
//    /// Assim que abrir posição, migre a máquina de estados para PosicaoAberta
//    /// </summary>
//    public sealed class Inicio : State, IState
//    {
//        public Inicio(RoboDollar context) : base(context)
//        {
//            context.Alert("Máquina de estado Início");
//            //operacoes.CollectionChanged += Operacoes_CollectionChanged;
//            maquinaEstaMigrando = false;
//            maquinaLacrada = false;
//            maquinaDeEstadoAtual = maquinaDeEstado.Inicio;
//        }

//        private void ProcessaOperacao()
//        //private void Operacoes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            if (true)
//            //if (e.Action == NotifyCollectionChangedAction.Add)
//            {
//                context.Alert("MaqInicio: Processando tendencia " + operacoes.Last());

//                if (operacoes.Count == context.sensibilidade)
//                {
//                    if (operacoes.ToList().TrueForAll(el => el == operacoes.Last()))
//                    {
//                        context.Alert("MaqInicio: Tendencia detectada :" + operacoes.Last());
//                        context.Alert("MaqInicio: Executando ordem");
//                        ExecutaOrdem(operacoes.Last(), context.CurrentData.GetPrice(PriceType.Close), context.Contratos);
//                    }
//                    else
//                    {
//                        context.Alert("MaqInicio: Operação não realizada pois Tendências no Buffer não são iguais");
//                    }
//                    if (operacoes.Count > 0) operacoes.RemoveAt(0); //buffer circular com tamanho sempre igual à sensibilidade
//                }
//                else
//                {
//                    context.Alert("MaqInicio: Operação não realizada pois Buffer de sensibilidade é " + operacoes.Count);
//                }
//            }
//        }

//        public override void Operar()
//        {
//            quantidadeDePosicoesAbertas = Math.Abs((int)GetQtdeContratosAtivos());
//            if (quantidadeDePosicoesAbertas > 0) { maquinaEstaMigrando = true; MigrarMaqEstado(maquinaDeEstado.PosicaoAberta); }
//        }

//        public override void Processa(tipoOperacao _operacao)
//        {
//            context.Orders.CancelAll();
//            if (maquinaLacrada) { context.Alert("Maquina está aguardando processamento da última ordem pela corretora"); return; }
//            operacoes.Add(_operacao);
//            ProcessaOperacao();
//        }

//    }

//    public sealed class InicioHiLo3MM : State, IState
//    {
//        public InicioHiLo3MM(RoboDollar context) : base(context)
//        {
//            context.Alert("Máquina de estado Início Acima/Abaixo de HiLo e 3MM");
//            //operacoes.CollectionChanged += Operacoes_CollectionChanged;
//            maquinaEstaMigrando = false;
//            maquinaLacrada = false;
//            maquinaDeEstadoAtual = maquinaDeEstado.InicioHiLoE3MM;
//        }

//        public override void Operar()
//        {
//            var mm = context.MM.GetValue();
//            var hilo = context.MM_HIGH_LOW.GetValue();
//            var cotacaoAtual = context.CurrentData.GetPrice(PriceType.Close);
//            if (cotacaoAtual > hilo && cotacaoAtual > mm && !maquinaLacrada && !maquinaEstaMigrando)
//            {
//                ExecutaOrdem(tipoOperacao.Compra, cotacaoAtual, context.Contratos);
//                maquinaLacrada = true;
//            }

//            if (cotacaoAtual < hilo && cotacaoAtual < mm && !maquinaLacrada && !maquinaEstaMigrando)
//            {
//                ExecutaOrdem(tipoOperacao.Venda, cotacaoAtual, context.Contratos);
//                maquinaLacrada = true;
//            }

//            quantidadeDePosicoesAbertas = Math.Abs((int)GetQtdeContratosAtivos());
//            if (quantidadeDePosicoesAbertas > 0) { maquinaEstaMigrando = true; MigrarMaqEstado(maquinaDeEstado.PosicaoAberta); }
//        }

//        public override void Processa(tipoOperacao _operacao)
//        {

//        }

//    }


//    /// <summary>
//    /// MÁQUINA DE ESTADO POSIÇÃO ABERTA
//    /// agora já temos um papel comprado.. 
//    /// devemos detectar se: 
//    /// Ocorrer reversão de tendência do papel em 1 Renko, fazer saída parcial
//    /// Ocorrer reversão em 2 Renkos , fazer saída total
//    /// Se fizer saída total e não tiver mais papel, retorna a máquina de estados para o estado inicial
//    /// </summary>
//    public sealed class PosicaoAberta : State, IState
//    {
//        public PosicaoAberta(RoboDollar context) : base(context)
//        {
//            context.Alert("Máquina de estado Posição Aberta");
//            //operacoes.CollectionChanged += Operacoes_CollectionChanged;
//            maquinaEstaMigrando = false;
//            maquinaLacrada = false;
//            maquinaDeEstadoAtual = maquinaDeEstado.PosicaoAberta;
//        }

//        private void ProcessaOperacao()
//        //private void Operacoes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            //aqui podemos verificar se a ultima operacao adicionada é uma reversão de tendência                       
//            if (true)
//            //if (e.Action == NotifyCollectionChangedAction.Add)
//            {
//                //Será que a última operação solicitada é contrária às posições majoritárias..
//                var qtdePosicoes = GetQtdeContratosAtivos();
//                context.Alert("MaqPosAberta: (Detector de Reversão) Processando tendencia " + operacoes.Last() + "  Posições Majoritárias:" + qtdePosicoes);
//                if (qtdePosicoes > 0)
//                {
//                    //maioria dos papeis são Buy.. e se a operacao desejada for venda, pois indicou uma reversão de tendência então faz um fechamento parcial                 
//                    if (operacoes.Last() == tipoOperacao.Venda)
//                    {
//                        context.Alert("MaqPosAberta: reversão de tendência - Fechamento parcial");
//                        FechamentoParcial();
//                    }
//                    else
//                    {
//                        context.Alert("MaqPosAberta: Qtde de posições:" + qtdePosicoes + "-> tem mesma natureza da tendência.. não necessita de fechamento então!");
//                    }
//                }
//                else
//                {
//                    //maioria dos papeis são Sell.. e se a operacao desejada for Compra, pois indicou uma reversão de tendência então faz um fechamento parcial
//                    if (operacoes.Last() == tipoOperacao.Compra)
//                    {
//                        context.Alert("MaqPosAberta: reversão de tendência - Fechamento parcial");
//                        FechamentoParcial();
//                    }
//                    else
//                    {
//                        context.Alert("MaqPosAberta: Qtde de posições:" + qtdePosicoes + "-> tem mesma natureza da tendência.. não necessita de fechamento então!");
//                    }
//                }
//                operacoes.Clear();
//            }
//        }

//        public override void Operar()
//        {
//            quantidadeDePosicoesAbertas = Math.Abs((int)GetQtdeContratosAtivos());
//            if (quantidadeDePosicoesAbertas == 0)
//            {
//                maquinaEstaMigrando = true;
//                var destino = (context.algoritmo == algoritmo.StartHiLo3MMPosAbertaRevTendencia) ? maquinaDeEstado.InicioHiLoE3MM : maquinaDeEstado.Inicio;
//                MigrarMaqEstado(destino);
//            }

//        }

//        public override void Processa(tipoOperacao _operacao)
//        {
//            context.Orders.CancelAll(); //cancela todas as ordens anteriores
//            if (maquinaLacrada) { context.Alert("Maquina está aguardando processamento da última ordem pela corretora"); return; }
//            operacoes.Add(_operacao);
//            ProcessaOperacao();
//        }

//    }


//    public class RoboDollar : NETStrategy
//    {

//        public Account CAccount;
//        private DateTime inicio;
//        private DateTime termino;
//        private DateTime tempo;
//        private int hora;
//        private int minutos;
//        private Boolean bolsaAberta = false;

//        public State state;

//        public Instrument instrument;
//        public Instrument papelDestino;
        
//        public Indicator MM;
//        public Indicator MM_HIGH_LOW;
//        public Indicator MACD;

//        [InputParameter("Tipos de Máquina de Estado", 0, new Object[] 
//        { "Início Simples e Posição Aberta Simples", algoritmo.StartSensibilidadePosAbertaRevTendencia,
//          "Início 3MM Hilo e Posição Aberta Simples", algoritmo.StartHiLo3MMPosAbertaRevTendencia
//        })]
//        public algoritmo algoritmo = algoritmo.StartHiLo3MMPosAbertaRevTendencia;

//        [InputParameter("Sensibilidade n. Bars", 1)]
//        public int sensibilidade = 2;

//        [InputParameter("Período da Média Móvel", 2, new Object[] {
//            "5-10-15",1
//        })]
//        public int periodoMediasMoveis = 1;

//        [InputParameter("Período da Média HILO", 3)]
//        public int periodoHiLo = 10;

//        [InputParameter("Distancia de Disparo em Ticks da MM", 4)]
//        public int disparo = 1;

//        [InputParameter("Price Type", 5, new object[] {
//            "Close",PriceType.Close,
//            "High",PriceType.High,
//            "Low",PriceType.Low,
//            "Medium",PriceType.Medium,
//            "Open",PriceType.Open,
//            "Typical",PriceType.Typical,
//            "Weighted",PriceType.Weighted
//        })]
//        public PriceType priceType = PriceType.Medium;


//        [InputParameter("Contratos", 6)]
//        public int Contratos = 2;

//        [InputParameter("Hora de Início", 7)]
//        public string HoraDeInicio = "09:01:00";

//        [InputParameter("Hora de Término", 8)]
//        public string HoraDeTermino = "16:50:00";

//        [InputParameter("Delta do StopLoss em ticks", 9)]
//        public double delta = 2;

//        [InputParameter("Usar Trailing Stop Loss?", 10)]
//        public Boolean usarTR = false;



//        public RoboDollar()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves";
//            base.Comments = "Robo MáqEstado 3MM TrStop";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "10.09.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.4";
//            base.Password = "";
//            base.ProjectName = "RoboDollar Investimentos";
//            #endregion

//        }

//        /// <summary>
//        /// This function will be called after creating
//        /// </summary>
//        public override void Init()
//        {
//            if (algoritmo == algoritmo.StartHiLo3MMPosAbertaRevTendencia) { state = new InicioHiLo3MM(this); } //seta o estado inicial da máquina de estados 
//            else if (algoritmo == algoritmo.StartSensibilidadePosAbertaRevTendencia) { state = new Inicio(this); } //seta o estado inicial da máquina de estados

//            Orders.OrderAdded += state.Orders_OrderAdded;
//            Orders.OrderExecuted += state.Orders_OrderExecuted;
//            Orders.OrderRemoved += state.Orders_OrderRemoved;

//            MM = Indicators.iCustom("camilo_3mm", CurrentData);
//            MM_HIGH_LOW = Indicators.iCustom("camilo_hilo", CurrentData);
//            MACD = Indicators.iCustom("camilo_macd", CurrentData);


//            instrument = Instruments.Current;
//            papelDestino = instrument;
//            Instruments.Subscribe(instrument, QuoteTypes.Quote);
//            CAccount = Accounts.Current;

//            DateTime.TryParseExact(HoraDeInicio, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out inicio);
//            DateTime.TryParseExact(HoraDeTermino, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out termino);

//            Alert("Inicialização do Robô");
//            Alert("Este instrumento tem um tick de " + instrument.TickSize);
//            Alert("Descrição do instrumento " + instrument.Description);
//        }

//        /// <summary>
//        /// Entry point. This function is called when new quote comes 
//        /// </summary>
//        public override void OnQuote()
//        {
//            tempo = instrument.LastQuote.Time.ToLocalTime();
//            hora = instrument.LastQuote.Time.ToLocalTime().Hour;
//            minutos = instrument.LastQuote.Time.ToLocalTime().Minute;

//            if (hora >= inicio.Hour && hora <= termino.Hour && minutos >= inicio.Minute && minutos <= termino.Minute)
//            {
//                bolsaAberta = true;
//                if (!State.maquinaEstaMigrando)
//                {
//                    state.Operar();
//                }
//            }
//            else
//            {
//                Positions.CloseAll();
//                Orders.CancelAll();
//                bolsaAberta = false;
//            }

//        }

//        public override void NextBar()
//        {
//            if (bolsaAberta && CurrentData.HistoryCount - 1 >= 2)
//            {
//                var cotacaoRenkoAnterior = CurrentData.GetPrice(PriceType.Close, 1);
//                var cotacaoRenkoPassado = CurrentData.GetPrice(PriceType.Close, 2);
//                var result = cotacaoRenkoAnterior - cotacaoRenkoPassado;
//                var operacao = tipoOperacao.nula;
//                operacao = (result > 0) ? tipoOperacao.Compra : tipoOperacao.Venda;
//                if (result == 0) operacao = tipoOperacao.nula;

//                try
//                {
//                    Alert("Preço Anterior:" + cotacaoRenkoAnterior + "   Preço Passado:" + cotacaoRenkoPassado + "    tendencia: " + operacao);
//                    state.Processa(operacao);
//                }
//                catch (Exception e)
//                {
//                    Alert(e.Message);
//                }

//                Alert("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
//                //MessageBox.Show("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
//            }

//        }


//        /// <summary>
//        /// This function will be called before removing
//        /// </summary>
//        public override void Complete()
//        {
//            Instruments.Unsubscribe(instrument, QuoteTypes.Quote);
//            state.FechamentoTotal();
//        }

//        public void SetState(maquinaDeEstado destino)
//        {
//            Alert("MaqEstadoAtual:" + State.maquinaDeEstadoAtual + "  MaqEstadoDestino:" + destino);

//            if (destino == maquinaDeEstado.Inicio)
//            {
//                State.maquinaDeEstadoAtual = maquinaDeEstado.Inicio;
//                state = null;
//                state = new Inicio(this);
//            }
//            if (destino == maquinaDeEstado.PosicaoAberta)
//            {
//                State.maquinaDeEstadoAtual = maquinaDeEstado.PosicaoAberta;
//                state = null;
//                state = new PosicaoAberta(this);
//            }
//            if (destino == maquinaDeEstado.InicioHiLoE3MM)
//            {
//                State.maquinaDeEstadoAtual = maquinaDeEstado.InicioHiLoE3MM;
//                state = null;
//                state = new InicioHiLo3MM(this);
//            }
//        }


//    }

   
//}

