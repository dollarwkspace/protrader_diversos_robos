﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Globalization;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Threading;
using Indicadores_Camilo;

namespace RoboDollarFuncao
{
    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }

    public class RoboDollar : NETStrategy
    {
        public Account CAccount;
        private DateTime inicio;
        private DateTime termino;
        private DateTime tempo;
        private int hora;
        private int minutos;
        private Boolean bolsaAberta = false;
        private Boolean bloqueieNovasOrdens = false;

        private ObservableCollection<tipoOperacao> operacoes = new ObservableCollection<tipoOperacao>(); //Lista das recomendações das operações - detector de tendência                    

        public Instrument instrument;
        public Instrument papelDestino;

        public Indicator MM;
        public Indicator MM_HIGH_LOW;
        public Indicator MACD;
        public Indicator VWAP;

        [InputParameter("Sensibilidade n. Bars", 1)]
        public int sensibilidade = 2;

        [InputParameter("Período da Média Móvel: Sinal", 2)]
        public int periodoMediaMovelSinal = 5;
        [InputParameter("Período da Média Móvel: Fast", 3)]
        public int periodoMediaMovelFast = 10;
        [InputParameter("Período da Média Móvel: Slow", 4)]
        public int periodoMediaMovelSlow = 15;

        [InputParameter("Período MACD: Sinal", 5)]
        public int periodoMACDSinal = 5;
        [InputParameter("Período MACD: Fast", 6)]
        public int periodoMACDFast = 10;
        [InputParameter("Período MACD: Slow", 7)]
        public int periodoMACDSlow = 15;

        [InputParameter("Período da Média HILO", 8)]
        public int periodoHiLo = 10;

        [InputParameter("VWAP Price_Type", 9, new object[]
      {
             "CLOSE", PRICE_TYPE.CLOSE,
             "CLOSE_HIGH_LOW", PRICE_TYPE.CLOSE_HIGH_LOW,
             "HIGH", PRICE_TYPE.HIGH,
             "HIGH_LOW", PRICE_TYPE.HIGH_LOW,
             "LOW", PRICE_TYPE.LOW,
             "OPEN", PRICE_TYPE.OPEN,
             "OPEN_CLOSE", PRICE_TYPE.OPEN_CLOSE,
             "OPEN_CLOSE_HIGH_LOW", PRICE_TYPE.OPEN_CLOSE_HIGH_LOW
      })]
        public PRICE_TYPE Price_Type = PRICE_TYPE.CLOSE_HIGH_LOW;

        [InputParameter("VWAP Enable Daily", 10)]
        public Boolean Enable_Daily = true;

        [InputParameter("VWAP Enable Weekly", 11)]
        public Boolean Enable_Weekly = true;

        [InputParameter("VWAP Enable Monthly", 12)]
        public Boolean Enable_Monthly = true;

        [InputParameter("Contratos", 13)]
        public int Contratos = 2;

        [InputParameter("Hora de Início", 14)]
        public string HoraDeInicio = "09:01:00";

        [InputParameter("Hora de Término", 15)]
        public string HoraDeTermino = "16:50:00";

        [InputParameter("Delta em ticks da Compra/Venda", 16)]
        public double delta = 1;

        [InputParameter("Usar Trailing Stop Loss?", 17)]
        public Boolean usarTR = false;

        [InputParameter("Take Profit em Ticks", 18)]
        public double TP = 10;

        [InputParameter("Stop Loss em Ticks", 19)]
        public double SL = 6;

        [InputParameter("Tempo para Corretora processar ordem em Cotações", 20)]
        public int tempoEstouro = 200;

        [InputParameter("Ordem", 21, new Object[]
        {
            "Limit", OrdersType.Limit,
            "Market", OrdersType.Market
        })]
        public OrdersType tipoDeOrdem = OrdersType.Limit;


        public RoboDollar()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Robo MáqEstado 3MM TrStop";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "10.09.2018";
            base.ExpirationDate = 0;
            base.Version = "1.0.8";
            base.Password = "";
            base.ProjectName = "RoboDollar Investimentos";
            #endregion

        }

        /// <summary>
        /// This function will be called after creating
        /// </summary>
        public override void Init()
        {
            Orders.OrderAdded += Orders_OrderAdded;
            Orders.OrderExecuted += Orders_OrderExecuted;
            Orders.OrderRemoved += Orders_OrderRemoved;

            MM = Indicators.iCustom("camilo_3mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelFast, periodoMediaMovelSlow);
            MM_HIGH_LOW = Indicators.iCustom("camilo_hilo", CurrentData, periodoHiLo);
            MACD = Indicators.iCustom("camilo_macd", CurrentData, periodoMACDSinal, periodoMACDFast, periodoMACDSlow);

            //VWAP = Indicators.iCustom("camilo_vwap", CurrentData, Price_Type, Enable_Daily, Enable_Weekly, Enable_Monthly);


            instrument = Instruments.Current;
            papelDestino = instrument;
            Instruments.Subscribe(instrument, QuoteTypes.Quote);
            CAccount = Accounts.Current;

            DateTime.TryParseExact(HoraDeInicio, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out inicio);
            DateTime.TryParseExact(HoraDeTermino, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out termino);

            Alert("Inicialização do Robô");
        }

        /// <summary>
        /// Entry point. This function is called when new quote comes 
        /// </summary>
        public override void OnQuote()
        {
            tempo = instrument.LastQuote.Time.ToLocalTime();
            hora = instrument.LastQuote.Time.ToLocalTime().Hour;
            minutos = instrument.LastQuote.Time.ToLocalTime().Minute;

            var hiloMax = MM_HIGH_LOW.GetValue(0, 0);
            var hiloMin = MM_HIGH_LOW.GetValue(1, 0);
            var mm_sinal = MM.GetValue(0, 0);
            var mm_fast = MM.GetValue(1, 0);
            var mm_slow = MM.GetValue(2, 0);
            var cotacaoAtual = CurrentData.GetPrice(PriceType.Close);

            var qtdeContratos = GetQtdeContratosAtivos();


            if (hora >= inicio.Hour && hora <= termino.Hour && minutos >= inicio.Minute && minutos <= termino.Minute)
            {
                bolsaAberta = true;

                try
                {
                    if (qtdeContratos == 0) Entrada3mmHiLo(cotacaoAtual, hiloMax, hiloMin, mm_sinal, mm_fast, mm_slow);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message + " " + exc.InnerException.Message);
                }

            }
            else
            {
                Positions.CloseAll();
                Orders.CancelAll();
                bolsaAberta = false;
            }

        }

        public override void NextBar()
        {
            if (bolsaAberta && CurrentData.HistoryCount - 1 >= 2)
            {
                var cotacaoRenkoAnterior = CurrentData.GetPrice(PriceType.Close, 1);
                var cotacaoRenkoPassado = CurrentData.GetPrice(PriceType.Close, 2);
                var result = cotacaoRenkoAnterior - cotacaoRenkoPassado;
                var operacao = tipoOperacao.nula;

                operacao = (result > 0) ? tipoOperacao.Compra : tipoOperacao.Venda;
                if (result == 0) operacao = tipoOperacao.nula;

                try
                {
                    Alert("Preço Anterior:" + cotacaoRenkoAnterior + "   Preço Passado:" + cotacaoRenkoPassado + "    tendencia: " + operacao);
                    if (operacoes.Count > sensibilidade) operacoes.RemoveAt(0);
                    operacoes.Add(operacao);
                    ChecaSaidaReversaoTendencia();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " " + e.InnerException.Message);
                }

                Alert("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
                //MessageBox.Show("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
            }

        }


        /// <summary>
        /// This function will be called before removing
        /// </summary>
        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote);
            instrument = null;
            papelDestino = null;
            MM = null;
            MM_HIGH_LOW = null;
            MACD = null;
            VWAP = null;
        }

        public void Orders_OrderRemoved(Order obj)
        {
            Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            Alert("Ordem REMOVIDA implica em Fechamento Total das Posições");
            bloqueieNovasOrdens = false;
            FechamentoTotal();
        }

        public void Orders_OrderAdded(Order obj)
        {
            Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            bloqueieNovasOrdens = true;
        }

        public void Orders_OrderExecuted(Order obj)
        {
            Alert("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
            bloqueieNovasOrdens = false;
        }


        public void Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest();
            request.Instrument = papelDestino;
            request.Account = Accounts.Current;
            request.Type = type;
            request.Side = side;
            request.Amount = contratos;
            request.Price = price;
            request.MarketRange = marketRange;
            request.TimeInForce = TimeInForce.FOK;

            if (TP != 0)
            {
                if (side == Operation.Sell) request.TakeProfitOffset = (-1) * TP * instrument.TickSize;
                if (side == Operation.Buy) request.TakeProfitOffset = TP * instrument.TickSize;
            }
            if (SL != 0)
            {
                if (side == Operation.Sell) request.TakeProfitOffset = SL * instrument.TickSize;
                if (side == Operation.Buy) request.TakeProfitOffset = (-1) * SL * instrument.TickSize;
            }

            if (usarTR)
            {
                request.Type = OrdersType.TrailingStop;
                request.TrStopOffset = delta * instrument.TickSize;
            }

            Alert("Emita uma ordem de " + request.Side);
            Orders.Send(request);
        }

        public double GetQtdeContratosAtivos()
        {
            double qtdeAbsolutaDeContratos = 0;
            Position[] pos = Positions.GetPositions();
            if (pos == null) { return 0; }
            List<Position> contratosAtivos = pos.ToList();
            contratosAtivos.ForEach(el =>
            {
                qtdeAbsolutaDeContratos += (el.Side == Operation.Buy) ? el.Amount : (-1) * el.Amount;
            });

            return qtdeAbsolutaDeContratos;
        }

        public void FechamentoParcial()
        {
            double qtdeAbsolutaDeContratos = Math.Abs(GetQtdeContratosAtivos());

            if ((int)qtdeAbsolutaDeContratos == 1) { FechamentoTotal(); return; }

            Position[] openedPositions = Positions.GetPositions(); //pega todas as posições abertas
            if (openedPositions != null)
            {

                int qtdeDeContratosParaFechar = (int)Math.Ceiling(Math.Abs(qtdeAbsolutaDeContratos / 2)); // qtde de contratos para fechar é metade dos contratos existentes

                int i = 0;
                while (i < qtdeDeContratosParaFechar)
                {
                    var id = openedPositions[i].Id;
                    var amount = openedPositions[i].Amount;
                    var closeOrderId = openedPositions[i].CloseOrderId;
                    Alert(" CloseOrderId:" + closeOrderId + " Qtde:" + amount + " id:" + id);
                    var halfAmountToClose = Math.Ceiling(Math.Abs(amount) / 2);
                    if (amount > 1) { openedPositions[i].Close(halfAmountToClose); }
                    if (amount == 1) { openedPositions[i].Close(); }
                    i++;
                }
            }
        }

        public void FechamentoTotal()
        {
            Alert("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");

            Order[] orders = Orders.GetOrders();
            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

            Position[] positions = Positions.GetPositions();
            if (positions != null) positions.ToList().ForEach(pos =>
            {
                pos.Close();
            });
        }


        //****************************************************************************************************************
        //****************************************************************************************************************
        //****************************************************************************************************************
        //****************************************************************************************************************
        //************************************  ENTRADAS E SAÍDAS - FUNÇÕES   ********************************************
        //****************************************************************************************************************
        //****************************************************************************************************************
        //****************************************************************************************************************

        public void Entrada3mmHiLo(double preco, double hi, double lo, double sinal, double fast, double slow)
        {
            if (preco > hi && preco > sinal && preco > fast && preco > slow && !bloqueieNovasOrdens)
            {
                Send_order(tipoDeOrdem, Operation.Buy, preco + instrument.TickSize, Contratos, 150, papelDestino);
                bloqueieNovasOrdens = true;
                
            }
            if (preco < lo && preco < sinal && preco < fast && preco < slow && !bloqueieNovasOrdens)
            {
                Send_order(tipoDeOrdem, Operation.Sell, preco - instrument.TickSize, Contratos, 150, papelDestino);
                bloqueieNovasOrdens = true;
            }
        }

        public void ChecaSaidaReversaoTendencia()
        {
            var ultimaOperacao = operacoes.Last();
            if (!operacoes.ToList().TrueForAll(p=> p == ultimaOperacao))
            {
                FechamentoParcial();               
            }
        }

    }




}

