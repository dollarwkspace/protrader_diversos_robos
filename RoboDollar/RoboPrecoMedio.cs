﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }

//    public class RoboKamikaze : NETStrategy
//    {
//        public Account CAccount;

//        //private Boolean bolsaAberta = false;
//        private Boolean bloqueieNovasOrdens = false;
//        private Boolean aguardandoExecucaoDaOrdemDeReentrada = false;
//        private string ultimaOrdem = "";
//        private double qtdeDeContratos = 0;
//        private double valorDaPosicao = 0;

//        private double piorPerda = 0;
//        private double maiorQtdeDePosicoesAbertas = 0;        

//        private ObservableCollection<tipoOperacao> operacoes = new ObservableCollection<tipoOperacao>(); //Lista das recomendações das operações - detector de tendência      
//        private List<Position> openedPositions = new List<Position>();

//        public Instrument instrument;
//        public Instrument papelDestino;

//        [InputParameter("sempre, Comprar ou Vender? ", 0, new Object[]{
//            "Compra", Operation.Buy, "Vender", Operation.Sell
//        })]
//        public Operation lado = Operation.Buy;

//        [InputParameter("Permitir até quantas posições abertas? ")]
//        public double maxPosAbertas = 10;

//        [InputParameter("Contratos")]
//        public int Contratos = 1;

//        [InputParameter("Hora de Início")]
//        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

//        [InputParameter("Hora de Término")]
//        public TimeSpan HoraDeTermino = new TimeSpan(17, 55, 00);        

//        [InputParameter("Ticks de Reentrada")]
//        public int deltaDeReentrada = 10;

//        [InputParameter("Sair no lucro de")]
//        public double lucroDeSaida = 50;        

//        public RoboKamikaze()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo Preço Médio ";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "14.10.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.0";
//            base.Password = "";
//            base.ProjectName = "RoboDollar Preço Médio";
//            #endregion

//        }

//        /// <summary>
//        /// This function will be called after creating
//        /// </summary>
//        public override void Init()
//        {
//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;
//            Instruments.NewTrade += Instruments_NewTrade;

//            instrument = Instruments.Current;
//            papelDestino = instrument;

//            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);

//            CAccount = Accounts.Current;

//            Print("Início Programado:" + HoraDeInicio.Hours + "e Minutos: " + HoraDeInicio.Minutes);
//            Print("Término Programado:" + HoraDeTermino.Hours + "e Minutos: " + HoraDeTermino.Minutes);
//            Print("Tamanho do Tick: " + instrument.TickSize);
//            Print("Inicialização do Robô");
//        }

//        private void Instruments_NewTrade(Instrument instrument, Trade trade)
//        {

//        }

//        /// <summary>
//        /// Entry point. This function is called when new quote comes 
//        /// </summary>
//        public override void OnQuote()
//        {

//            base.OnQuote();

//            try
//            {
//                if (instrument.LastQuote == null) return;

//                var hora = instrument.LastQuote.Time.ToLocalTime().Hour;
//                var minutos = instrument.LastQuote.Time.ToLocalTime().Minute;
//                var segundos = instrument.LastQuote.Time.ToLocalTime().Second;

//                var currentTime = new TimeSpan(hora, minutos, segundos);

//                var totalMinStart = currentTime.Subtract(HoraDeInicio).TotalMinutes;
//                var totalMinEnd = currentTime.Subtract(HoraDeTermino).TotalMinutes;

//                if (true)
//                {
//                    var cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
                    
//                    qtdeDeContratos = GetQtdeContratosAtivos();
//                    valorDaPosicao = CalculaLucro();

//                    if (qtdeDeContratos == 0)
//                    {
//                        Entrada(cotacaoAtual);
//                    }
                    
//                    if (qtdeDeContratos != 0)
//                    {
//                        ChecaNecessidadeDeReentrada(cotacaoAtual); 
//                        SaidaNoLucro();
//                    }

//                    if (valorDaPosicao < piorPerda) piorPerda = valorDaPosicao;                    

//                }
//                //else
//                //{
//                //    FechamentoTotal();
//                //    Print("Bolsa Fechada !!!");
//                //}

//            }
//            catch (Exception ext)
//            {
//                Alert(ext.Message);
//            }

//        }

//        public override void NextBar()
//        {
//            Order[] orders = Orders.GetOrders();
//            Position[] positions = Positions.GetPositions();

//            if (orders != null)
//            {
//                orders.ToList().ForEach(ord =>
//                {
//                    if (ord.Type != OrdersType.StopLoss || ord.Type != OrdersType.TakeProfit)
//                    {
//                        ord.Cancel();
//                    }
//                    else
//                    {
//                        if (positions.First(p => p.Id == ord.LinkedTo) == null) ord.Cancel();
//                    }

//                });

//                Print("Cancelando Ordens não executadas a tempo"); bloqueieNovasOrdens = false;
//            }

//            Print("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
//            Print("Qtde de Posições Abertas:" + qtdeDeContratos + " Preço Médio das Posições Abertas " + CalculaPrecoMedio() + " Lucro: " + CalculaLucro() + " Maior Calor: " + piorPerda);            

//        }


//        /// <summary>
//        /// This function will be called before removing
//        /// </summary>
//        public override void Complete()
//        {
//            Alert("*******************      FINAL DA OPERAÇÃO DO ROBÔ      ************************* " + piorPerda);
//            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
//            instrument = null;
//            papelDestino = null;            
//            Alert("Pior perda: " + piorPerda);
//            Alert("Maior Quantidade de Posições Abertas: " + maiorQtdeDePosicoesAbertas);
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************

//        public void Orders_OrderRemoved(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//            if (aguardandoExecucaoDaOrdemDeReentrada) aguardandoExecucaoDaOrdemDeReentrada = false;
//        }

//        public void Orders_OrderAdded(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = true;
//        }

//        public void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//            if (aguardandoExecucaoDaOrdemDeReentrada) aguardandoExecucaoDaOrdemDeReentrada = false;
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************


//        public string Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = papelDestino;
//            request.Account = Accounts.Current;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.Price = price;
//            request.MarketRange = marketRange;
//            request.TimeInForce = TimeInForce.Day;

//            request.TakeProfitOffset = 5;

//            Print("Emita uma ordem de " + request.Side);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("Ordem não emitida"); bloqueieNovasOrdens = false; };
//            return result;
//        }

//        public double GetQtdeContratosAtivos()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                openedPositions = Positions.GetPositions().ToList();
//                double qtdeAbsolutaDeContratos = 0;
//                if (openedPositions.Count == 0) { return 0; }
//                openedPositions.ForEach(el =>
//                {
//                    qtdeAbsolutaDeContratos += (el.Side == Operation.Buy) ? el.Amount : (-1) * el.Amount;
//                });

//                return qtdeAbsolutaDeContratos;
//            }
//            return 0;
//        }



//        public double CalculaPrecoMedio()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                openedPositions = Positions.GetPositions().ToList();
//                if (openedPositions.Count == 0) return 0;
//                var somaQtdeXPrecoAbertura = openedPositions.Sum(p => p.Amount * p.OpenPrice);
//                var QtdeTotalDePapeisAbertos = openedPositions.Sum(p => p.Amount);
//                var precoMedio = somaQtdeXPrecoAbertura / QtdeTotalDePapeisAbertos;
//                return precoMedio;
//            }
//            return -1;
//        }

//        public void FechamentoParcial()
//        {
//            Position[] positions = Positions.GetPositions();
//            if (positions != null)
//            {
//                positions.ToList().ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
//            }
//        }

//        public void FechamentoTotal()
//        {
//            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");

//            Order[] orders = Orders.GetOrders();
//            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

//            Position[] positions = Positions.GetPositions();
//            if (positions != null) positions.ToList().ForEach(pos =>
//            {
//                pos.Close();
//            });

//            openedPositions.Clear();
//        }

//        public Boolean TendenciaSeMantem()
//        {
//            return operacoes.ToList().TrueForAll(p => p == operacoes.Last());
//        }

//        public Boolean EstamosLucrando()
//        {
//            var lucro = CalculaLucro();
//            return lucro > 0;
//        }

//        public double CalculaLucro()
//        {
//            Position[] positions = Positions.GetPositions();
//            if (positions != null)
//            {
//                var lucro = positions.ToList().Sum(p => p.GetProfitNet());
//                if (positions.Length == 0) return 0;
//                return lucro;
//            }
//            return 0;
//        }


//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //************************************  ENTRADAS E SAÍDAS - FUNÇÕES   ********************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************

//        public void Entrada(double preco)
//        {
//            OrdersType tipoDeOrdem = OrdersType.Market;

//            if (!bloqueieNovasOrdens)
//            {
//                Orders.GetOrders().ToList().ForEach(o => o.Cancel());
//                ultimaOrdem = Send_order(tipoDeOrdem, lado, preco, Contratos, 150, papelDestino);
//                if (ultimaOrdem != "-1" || ultimaOrdem != "")  bloqueieNovasOrdens = true;
//                Print("resultado da ordem: " + ultimaOrdem);
//            }            
//        }

//        public void ChecaNecessidadeDeReentrada(double cotacaoAtual)
//        {
//            if (!EstamosLucrando() && !bloqueieNovasOrdens)
//            {
//                var precoMedio = CalculaPrecoMedio();
//                OrdersType tipoDeOrdem = OrdersType.Market;

//                if (qtdeDeContratos > 0 && Math.Abs(qtdeDeContratos) <= maxPosAbertas / 2 && (precoMedio > cotacaoAtual + deltaDeReentrada * instrument.TickSize))
//                {
//                    Orders.CancelAll();
//                    ultimaOrdem = Send_order(tipoDeOrdem, Operation.Buy, cotacaoAtual, 2*qtdeDeContratos-1, 150, papelDestino);

//                    if (ultimaOrdem != "-1" || ultimaOrdem != "")
//                    {
//                        bloqueieNovasOrdens = true; Print("Ordem de Reentrada na Compra Emitida - aguardando execução ou remoção da fila");
//                    }

//                }

//                if (qtdeDeContratos < 0 && Math.Abs(qtdeDeContratos) <= maxPosAbertas / 2 && (precoMedio + deltaDeReentrada * instrument.TickSize < cotacaoAtual))
//                {                 
//                    Orders.CancelAll();
//                    ultimaOrdem = Send_order(tipoDeOrdem, Operation.Sell, cotacaoAtual, 2 * qtdeDeContratos - 1, 150, papelDestino);

//                    if (ultimaOrdem != "-1" || ultimaOrdem != "")
//                    {
//                        bloqueieNovasOrdens = true; Print("Ordem de Reentrada na Venda Emitida - aguardando execução ou remoção da fila");
//                    }
//                }
//            }
//        }


//        public void SaidaNoLucro()
//        {
//            var lucro = CalculaLucro();
//            if (lucro >= lucroDeSaida)
//            {
//                Print("Emissão de ordem por Saída no lucro: " + valorDaPosicao);
//                FechamentoParcial();
//            }
//        }
//    }

//}
