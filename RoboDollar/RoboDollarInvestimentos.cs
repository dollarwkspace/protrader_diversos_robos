﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Globalization;
using System.Windows.Forms;

namespace RoboDollar
{
    /// <summary>
    /// RoboModelo
    /// 
    /// </summary>
    public class RoboDollar : NETStrategy
    {

        private Instrument[] instruments;
        private Instrument instrument;
        private Instrument papelDestino;
        private Account CAccount;
        private Indicator indicadorMACurto;
        private Indicator indicadorMALongoMax;
        private Indicator indicadorMALongoMin;
        private Indicator MACDFast;
        private Indicator MACDSlow;
        private Indicator MACDSignal;
        private Indicator indicadorMAFechamentoEm1Periodo;
        private Indicator indicadorATR;
        private Indicator indicadorSAR;
        private Indicator indicadorDesvioPadrao;
        private Indicator indicadorMACD;
        private Indicator indicadorHILO;
        private Indicator indicadorStopATR;

        private DateTime inicio;
        private DateTime termino;
        private int resilienciaCompraCounter = 0;
        private int resilienciaVendaCounter = 0;
        private int resilienciaNebulosaCounter = 0;

        List<string> openedOrders = new List<string>();
        List<double> HiLoMax = new List<double>();
        List<double> HiLoMin = new List<double>();
        List<double> valores = new List<double>();

        private Boolean jaComprou = false;
        private Boolean jaVendeu = false;

        [InputParameter("Periodo MExp Curto", 0)]
        public int PeriodoCurto = 2;

        [InputParameter("Periodo MExp Longo", 1)]
        public int PeriodoLongo = 8;

        [InputParameter("Resiliência Compra", 2)]
        public int ResilienciaCompra = 0;

        [InputParameter("Resiliência Venda", 3)]
        public int ResilienciaVenda = 0;

        [InputParameter("Resiliência Nebulosa", 4)]
        public int ResilienciaNebulosa = 0;

        [InputParameter("Price Type", 5, new object[] {
            "Close",PriceType.Close,
            "High",PriceType.High,
            "Low",PriceType.Low,
            "Medium",PriceType.Medium,
            "Open",PriceType.Open,
            "Typical",PriceType.Typical,
            "Weighted",PriceType.Weighted
        })]
        public PriceType priceType = PriceType.Close;

        [InputParameter("Take Profit", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit = 3; //Take Profit  

        [InputParameter("Stop Loss", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss = 3; //Stop Loss

        [InputParameter("tipo de MA", 8, new object[]
       {
             "EMA - Exponencial", PTLRuntime.NETScript.Indicators.MAMode.EMA,
             "LWMA- Linear Weighted", PTLRuntime.NETScript.Indicators.MAMode.LWMA,
             "SMA - Simple Moving Average", PTLRuntime.NETScript.Indicators.MAMode.SMA,
             "SMMA - Smooth Moving Average", PTLRuntime.NETScript.Indicators.MAMode.SMMA
       })]
        public PTLRuntime.NETScript.Indicators.MAMode MA_type1 = PTLRuntime.NETScript.Indicators.MAMode.SMA; // First smoothing type

        [InputParameter("Contratos", 9)]
        public int Contratos = 1;

        [InputParameter("Tick Offset", 11)]
        public int TickOffset = 0;

        [InputParameter("Tick Mínimo", 12)]
        public double TickMinimo = 0.5;

        [InputParameter("SAR Step", 13)]
        public double stepSar = 0.02;

        [InputParameter("SAR Coef", 14)]
        public double coefSar = 0.07;

        [InputParameter("StdDev Periodo", 15)]
        public int StdDevPeriodo = 8;

        [InputParameter("StdDev Limite", 16)]
        public int StdDevLimite = 1;

        [InputParameter("Hora de Início", 17)]
        public string HoraDeInicio = "09:02:00";

        [InputParameter("Hora de Término", 18)]
        public string HoraDeTermino = "16:50:00";

        [InputParameter("Break Even", 19, 0, 9999, 1, 0.1)]
        public double Break_Even = 2; // Break Even offset when price reaches number of points 

        [InputParameter("Break Even Value", 20, 0, 9999, 1, 0.1)]
        public double Break_Even_val = 1; // Break Even (value) 

        [InputParameter("Usar BreakEven?", 21)]
        public Boolean usarBreakEven = false;

        [InputParameter("ATR: Período", 22)]
        public int ATRPeriodo = 7;

        [InputParameter("HiLo: Período", 23)]
        public int HiLoPeriodo = 7;

        //[InputParameter("Papel Destino", 13)]
        //public string PapelDestino = "WDOV18";

        public RoboDollar()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "É um leitão";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "06.09.2018";
            base.ExpirationDate = 0;
            base.Version = "0.0.1";
            base.Password = "";
            base.ProjectName = "RoboDollar";
            #endregion           

        }

        /// <summary>
        /// This function will be called after creating
        /// </summary>
        public override void Init()
        {
            instruments = Instruments.GetInstruments();
            instrument = Instruments.Current;
            papelDestino = instrument;
            //papelDestino = Instruments.GetInstrument(PapelDestino);
            Instruments.Subscribe(instrument, QuoteTypes.Quote);
            CAccount = Accounts.Current;

            indicadorMALongoMax = Indicators.iMA(CurrentData, HiLoPeriodo, MA_type1, 0, PriceType.High);
            indicadorMALongoMin = Indicators.iMA(CurrentData, HiLoPeriodo, MA_type1, 0, PriceType.Low);          
            //indicadorMAFechamentoEm1Periodo = Indicators.iMA(CurrentData, 1, MA_type1, 0, PriceType.Close);
            //indicadorATR = Indicators.iATR(CurrentData, ATRPeriodo);
            indicadorMACD = Indicators.iMACD(CurrentData, 12, 26, 9);
            MACDSlow = Indicators.iMA(CurrentData, 26, PTLRuntime.NETScript.Indicators.MAMode.EMA);
            MACDFast = Indicators.iMA(CurrentData, 12, PTLRuntime.NETScript.Indicators.MAMode.EMA);
            MACDSignal = Indicators.iMA(CurrentData, 9, PTLRuntime.NETScript.Indicators.MAMode.SMMA);

            DateTime.TryParseExact(HoraDeInicio, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out inicio);
            DateTime.TryParseExact(HoraDeTermino, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out termino);


        }

        /// <summary>
        /// Entry point. This function is called when new quote comes 
        /// </summary>
        public override void OnQuote()
        {
            base.OnQuote();

            var tempo = instrument.LastQuote.Time.ToLocalTime();
            var hora = instrument.LastQuote.Time.ToLocalTime().Hour;
            var minutos = instrument.LastQuote.Time.ToLocalTime().Minute;
            var segundos = instrument.LastQuote.Time.ToLocalTime().Second;            

            var HiLoMax = indicadorMALongoMax.GetValue();
            var HiLoMin = indicadorMALongoMin.GetValue();
            var cotacaoAtual = CurrentData.GetPrice(priceType);
            var MACD = MACDFast.GetValue() - MACDSlow.GetValue();
            var tendencia = MACD - MACDSignal.GetValue();
 
            Alert("Cotação:" + cotacaoAtual + " HiLoMax:" + HiLoMax + " HiLoMin:" + HiLoMin + " Tendencia:" + tendencia);
            

            if (instrument.LastQuote == null) return;

            //if (true)
            if (hora >= inicio.Hour && hora <= termino.Hour && minutos >= inicio.Minute && minutos <= termino.Minute)
            {
                if (hora >= termino.Hour && minutos >= termino.Minute)
                {
                    Fechar();
                    //Alert("Fechou Operações");
                    return;
                }

                Position[] posicoes = Positions.GetPositions();
                Position pos = null;

                if (posicoes.Length != 0) { pos = posicoes[posicoes.Length - 1];}

                //**********************************************
                //********* Critérios de Saída *****************
                //**********************************************
                if (pos != null)
                {
                    if (usarBreakEven)
                    {
                        if (pos.Side == Operation.Buy && Instruments.Current.LastQuote.Ask > pos.OpenPrice + Break_Even * Point)
                        {
                            pos.SetStopLoss(pos.OpenPrice + Break_Even_val * Point);
                        }

                        else if (pos.Side == Operation.Sell && Instruments.Current.LastQuote.Bid < pos.OpenPrice - Break_Even * Point)
                        {
                            pos.SetStopLoss(pos.OpenPrice - Break_Even_val * Point);
                        }
                    }

                    resilienciaNebulosaCounter++;
                    if (true)
                    {
                        if (cotacaoAtual < HiLoMin && tendencia < 0  && pos.Side == Operation.Buy)
                        {
                            //Alert("Zerou Compra Hora:" + pos.OpenTime.ToLocalTime().ToShortTimeString() + " Posição:" + pos.Side + " Preço:" + cotacaoAtual + " HiloMax:" + HiLoMax);
                            //MessageBox.Show("Sai de uma Compra");
                            resilienciaNebulosaCounter = 0;
                            resilienciaCompraCounter = 0;
                            resilienciaVendaCounter = 0;
                            openedOrders.Clear();                            
                            jaComprou = false;
                            pos.Close();
                        }

                        if (cotacaoAtual > HiLoMax && tendencia > 0  && pos.Side == Operation.Sell)
                        {
                            //Alert("Zerou Venda Hora:" + pos.OpenTime.ToLocalTime().ToShortTimeString() + " Posição:" + pos.Side + " Preço:" + cotacaoAtual + " HiloMin:" + HiLoMin);
                            //MessageBox.Show("Sai de uma Venda");
                            resilienciaNebulosaCounter = 0;
                            resilienciaCompraCounter = 0;
                            resilienciaVendaCounter = 0;
                            openedOrders.Clear();
                            jaVendeu = false;                            
                            pos.Close();
                        }
                    }
                }

                //**************************************
                //Critérios de entrada
                //*************************************
                if (HiLoMax != 0 && HiLoMin != 0 && pos==null)
                {
                    if (cotacaoAtual > HiLoMax && tendencia > 0 )
                    {
                        resilienciaCompraCounter++;
                        if (true)
                        {
                            if (!jaComprou&&!jaVendeu)
                            {
                                string result = Send_order(OrdersType.Market, Operation.Buy, instrument.LastQuote.Bid, Contratos, 150, papelDestino);
                                //Print(result);
                                //MessageBox.Show("Entrei na Compra");
                                //Alert("ENTRADA na Compra Hora:" + pos.OpenTime.ToLocalTime().ToShortTimeString() + " Posição:" + pos.Side + " Preço:" + cotacaoAtual + " HiloMin:" + HiLoMax);
                                openedOrders.Add(result);
                                jaComprou = true;
                                resilienciaCompraCounter = 0;
                            }
                        }
                    }

                    if (cotacaoAtual < HiLoMin && tendencia < 0)
                    {
                        resilienciaVendaCounter++;
                        if (true)
                        {
                            if (!jaVendeu&&!jaComprou)
                            {
                                string result = Send_order(OrdersType.Market, Operation.Sell, instrument.LastQuote.Ask, Contratos, 150, papelDestino);
                                //Print(result);
                               // Alert("ENTRADA na Venda Hora:" + pos.OpenTime.ToLocalTime().ToShortTimeString() + " Posição:" + pos.Side + " Preço:" + cotacaoAtual + " HiloMax:" + HiLoMax);
                                //MessageBox.Show("Entrei na Venda");
                                openedOrders.Add(result);
                                jaVendeu = true;
                                resilienciaVendaCounter = 0;
                            }
                        }
                    }
                }

            }

        }


        public override void NextBar()
        {

        }

        private void Fechar()
        {
            Position[] allPositions = Positions.GetPositions();
            foreach (var pos in allPositions)
            {
                pos.Close();
            }
            openedOrders.Clear();
        }

        /// <summary>
        /// This function will be called before removing
        /// </summary>
        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote);
            indicadorMALongoMax = null;
            indicadorMALongoMin = null;
            indicadorMAFechamentoEm1Periodo = null;
            indicadorATR = null;


            if (openedOrders == null) return;

            foreach (string orderId in openedOrders)
            {
                Position pos = Positions.GetPositionById(orderId);
                if (pos != null) pos.Close();

                Order ord = Orders.GetOrderById(orderId);
                if (ord != null) ord.Cancel();
            }
        }

        public string Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest()
            {
                Instrument = papelDestino,
                Account = Accounts.Current,
                Type = type,
                Side = side,
                Amount = contratos,
                Price = price,
                MarketRange = marketRange, //market range para compra é 10 para venda é 30
                TakeProfitOffset = Take_Profit * Point,
                StopLossOffset = Stop_Loss * Point
            };
            return Orders.Send(request);
        }

        //double CalculaHiLo(double valor)
        //{            
        //    valores.Add(valor);
        //    double max = valores[0];
        //    double min = valores[0];
        //    if (valores.Count >= 13)
        //    {                
        //        valores.ForEach(v =>
        //        {
        //            if (v >= max) max = v;
        //            if (v <= min) min = v;
        //        });
        //        HiLoMax.Add(max);
        //        HiLoMin.Add(min);
        //    }


        //    if(HiLoMax.)

        //    return 0;
        //}



    }
}