﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public class RoboKamikaze : NETStrategy
//    {
//        private Instrument instrument;
//        private Account conta;
//        private Indicator MM;
//        private Indicator MACD;
//        private Indicator STOC;

//        private bool liberaEntrada = false;
//        private double calor = 0;
//        private double verde = 0;
//        private double azul = 0;
//        private double tickSize = 0;
//        private string horaDoCalorMaximo = "";
//        private double maxDePosicaoAberta = 0;
//        private double ultimaReentrada = 0;

//        private bool bloqueieNovasOrdens = false;
//        private System.Timers.Timer syncTimer;

//        public RoboKamikaze()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo Kamikaze";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "26.10.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.0";
//            base.Password = "";
//            base.ProjectName = "RoboDollar Investimentos";
//            #endregion
//        }

//        [InputParameter("MACD/STOC para entrada", 0, new Object[]
//        {
//            "MACD","MACD", "STOC", "STOC"
//        })]
//        public string indiceDeEntrada = "STOC";

//        [InputParameter("STOC MAXIMO")]
//        public int stocMaximo = 80;
//        [InputParameter("STOC MINIMO")]
//        public int stocMinimo = 20;

//        [InputParameter("Diff verde/azul: STOC")]
//        public double deltaStoc = 0.5;
      

//        [InputParameter("Período MACD: Sinal")]
//        public int periodoMACDSinal = 9;
//        [InputParameter("Período MACD: Fast")]
//        public int periodoMACDFast = 12;
//        [InputParameter("Período MACD: Slow")]
//        public int periodoMACDSlow = 26;
//        [InputParameter("Média: Sinal")]
//        public int periodoMediaMovelSinal = 4;
//        [InputParameter("Média: Fast")]
//        public int periodoMediaMovelFast = 12;
//        [InputParameter("Média: Slow")]
//        public int periodoMediaMovelSlow = 55;        

//        [InputParameter("Permitir até quantas posições abertas? ")]
//        public double maxPosAbertas = 50;

//        [InputParameter("Contratos")]
//        public int Contratos = 2;

//        [InputParameter("Hora de Término")]
//        public int HoraDeTermino = 15;

//        [InputParameter("Ticks de Reentrada")]
//        public int tickDeReentrada = 10;

//        [InputParameter("LOSS Máximo Tolerado")]
//        public double lossMaximo = -10000;

//        [InputParameter("Take Profit de Entrada")]
//        public double TP = 3;

//        [InputParameter("Take Profit de ReEntrada")]
//        public double TPR = 1;

//        [InputParameter("Timer")]
//        public double timerParaCancelar = 250;



//        public override void Init()
//        {
//            instrument = Instruments.Current;
//            conta = Accounts.Current;
//            tickSize = instrument.TickSize;

//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;            
//            Positions.PositionAdded += Positions_PositionAdded;
//            Positions.PositionRemoved += Positions_PositionRemoved;

//            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);

//            MM = Indicators.iCustom("camilo_3mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelFast, periodoMediaMovelSlow);
//            MACD = Indicators.iCustom("camilo_macd", CurrentData, periodoMACDSinal, periodoMACDFast, periodoMACDSlow);
//            STOC = Indicators.iCustom("STOCHASTIC", CurrentData);

//            SetSyncTimer();
//        }

//        private void Positions_PositionRemoved(Position obj)
//        {
//            bloqueieNovasOrdens = false;            
//        }

//        private void Positions_PositionAdded(Position obj)
//        {
//            try
//            {                
//                if (obj.Side == Operation.Buy && obj.TakeProfitOrder==null) obj.SetTakeProfit(obj.OpenPrice + tickSize * TPR);
//                if (obj.Side == Operation.Sell && obj.TakeProfitOrder==null) obj.SetTakeProfit(obj.OpenPrice - tickSize * TPR);

//                bloqueieNovasOrdens = false;                

//            } catch (Exception ext)
//            {
//                Alert("Erro no Position Added: " + ext.Message);
//            }
//        }        

//        public override void OnQuote()
//        {
//            try
//            {
//                var cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
//                var mac = MACD.GetValue(2, 0);
//                var media = MM.GetValue(2, 0);
//                var sinal = MM.GetValue(0, 0);
//                verde = STOC.GetValue(0, 0);
//                azul = STOC.GetValue(1, 0);

//                Position[] positions = Positions.GetPositions();

//                var hora = 0;

//                if(instrument.LastQuote != null)
//                {
//                    hora = instrument.LastQuote.Time.ToLocalTime().Hour;
//                }


//                if (positions.Length != 0)
//                {
//                    double temp = 0;
//                    temp = positions[0].GetProfitNet();

//                    if (temp < calor) { calor = temp; horaDoCalorMaximo = instrument.LastQuote.Time.ToLocalTime().ToShortTimeString(); maxDePosicaoAberta = positions[0].Amount; }
//                    }

//                if (mac != 0 && media != 0)
//                {
//                    if (positions.Length == 0 && !bloqueieNovasOrdens)
//                    {
//                        if (LiberaEntrada(cotacaoAtual, sinal, media)) liberaEntrada = true;

//                        if (liberaEntrada && hora < HoraDeTermino)
//                        {
//                            Entrada(cotacaoAtual, mac, media);
//                        }
//                    }
//                    if (positions.Length != 0 && !bloqueieNovasOrdens && positions[0].Amount <= maxPosAbertas/2)
//                    {
//                        var reentrou = ReEntrada(cotacaoAtual, positions[0], verde, azul);
//                    }
//                }

//            }
//            catch (Exception exc)
//            {
//                Alert("Erro no OnQuote: " + exc.Message);
//            }
//        }

//        public override void NextBar()
//        {

//        }

//        public override void Complete()
//        {
//            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
//            syncTimer.Close();
//            syncTimer.Dispose();

//            Alert("Maior calor: " + calor + " hora:"+horaDoCalorMaximo +" contratos abertos no calor:"+maxDePosicaoAberta);
//        }


//        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = instrument;
//            request.Account = Accounts.Current;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.Price = price;
//            request.MarketRange = 10;
//            request.TimeInForce = TimeInForce.Day;

//            if (takeProfit != 0) request.TakeProfitOffset = takeProfit;

//            Print("Emita uma ordem de " + request.Side);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("Ordem não aceita pela corretora"); };
//            if (result != "-1") { bloqueieNovasOrdens = true; };
//            return result;
//        }

//        private void Entrada(double preco, double macd, double amarelo)
//        {
//            if (preco > amarelo && !bloqueieNovasOrdens)
//            {
//                if (indiceDeEntrada == "STOC" && verde + deltaStoc < azul) return;
//                if (indiceDeEntrada == "MACD" && macd < 0) return;
//                if (verde > stocMaximo || azul > stocMaximo) return;
//                var result = Send_order(OrdersType.Limit, Operation.Buy, preco+instrument.TickSize, Contratos, TP);
//                if (result != "-1") { bloqueieNovasOrdens = true; liberaEntrada = false; ultimaReentrada = preco; }
//            }

//            if (preco < amarelo && !bloqueieNovasOrdens)
//            {
//                if (indiceDeEntrada == "STOC" && verde > azul + deltaStoc) return;
//                if (indiceDeEntrada == "MACD" && macd > 0) return;
//                if (verde < stocMinimo || azul < stocMinimo) return;
//                var result = Send_order(OrdersType.Limit, Operation.Sell, preco-instrument.TickSize, Contratos, TP);
//                if (result != "-1") { bloqueieNovasOrdens = true; liberaEntrada = false; ultimaReentrada = preco; }
//            }
//        }

//        private bool ReEntrada(double preco, Position pos, double verde, double azul)
//        {
//            var precoMedio = pos.OpenPrice;
//            var resultadoFinanceiro = pos.GetProfitNet();
//            var reentrou = false;
//            if (resultadoFinanceiro < 0)
//            {
//                var deltaMoeda = instrument.TickSize * tickDeReentrada;

//                if (pos.Side == Operation.Buy && preco <= ultimaReentrada - deltaMoeda && !bloqueieNovasOrdens && verde > azul + deltaStoc && verde < stocMinimo && azul < stocMinimo)
//                {
//                    var result = Send_order(OrdersType.Market, Operation.Buy, preco, pos.Amount, TPR);
//                    if (result != "-1") { bloqueieNovasOrdens = true; reentrou = true; ultimaReentrada = preco; }
//                }

//                if (pos.Side == Operation.Sell && preco >= ultimaReentrada + deltaMoeda && !bloqueieNovasOrdens && verde + deltaStoc < azul && verde > stocMaximo && azul > stocMaximo)
//                {
//                    var result = Send_order(OrdersType.Market, Operation.Sell, preco, pos.Amount, TPR);
//                    if (result != "-1") { bloqueieNovasOrdens = true; reentrou = true; ultimaReentrada = preco; }
//                }
//            }
//            return reentrou;
//        }

//        private bool LiberaEntrada(double preco, double sinal, double amarela)
//        {
//            if (preco > amarela && preco < sinal) return true;
//            if (preco < amarela && preco > sinal) return true;

//            return false;
//        }

//        private bool ExisteTakeProfitVinculado(string idDaPosicao)
//        {
//            try
//            {
//                Order[] orders = Orders.GetOrders(true);
//                var existe = false;
//                if (orders.Length != 0)
//                {
//                    orders.ToList().ForEach(ord =>
//                    {
//                        if (ord.IsTakeProfitOrder && ord.LinkedTo == idDaPosicao)
//                        {
//                            existe = true;
//                        }
//                    });
//                }
//                return existe;
//            } catch (Exception ext)
//            {
//                Alert("Erro no ExisteTakeProfitVinculado: " + ext.Message);
//                return false;
//            }
//        }

//        private Position BuscaPosicao()
//        {
//            try
//            {
//                Position[] positions = Positions.GetPositions();
//                if (positions.Length != 0)
//                {
//                    return positions[0];
//                }
//                return null;
//            } catch (Exception ext)
//            {
//                Alert("Erro no BuscaPosicao: " + ext.Message);
//                return null;
//            }
//        }

//        private Order BuscaOrdem(string idDaPosicao)
//        {
//            try
//            {
//                Order[] ordens = Orders.GetOrders(true);
//                Order ordem = null;
//                if (ordens.Length != 0)
//                {
//                    ordens.ToList().ForEach(ord =>
//                    {
//                        if (ord.IsTakeProfitOrder && ord.LinkedTo == idDaPosicao)
//                        {
//                            ordem = ord;
//                        }
//                    });
//                }
//                return ordem;
//            } catch (Exception ext)
//            {
//                Alert("Erro no BuscaOrdem: " + ext.Message);
//                return null;
//            }
//        }


//        private void Orders_OrderRemoved(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//        }

//        private void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());            
//        }

//        private void Orders_OrderAdded(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = true;
//        }

//        private void SetSyncTimer()
//        {
//            // cria um timer com 1s de intervalo
//            syncTimer = new System.Timers.Timer(timerParaCancelar);
//            syncTimer.Elapsed += SyncTimer_Elapsed;
//            syncTimer.AutoReset = true;
//            syncTimer.Enabled = true;
//        }

//        private void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//        {
//            try
//            {
//                if (bloqueieNovasOrdens)
//                {
//                    Order[] orders = Orders.GetOrders(false);
//                    if (orders.Length != 0)
//                    {
//                        orders.ToList().ForEach(ord =>
//                        {
//                            ord.Cancel();
//                        });
//                    }                    
//                }
//            } catch (Exception ext)
//            {
//                Alert("Erro no SyncTimer: " + ext.Message);
//            }

//        }
//    }
//}
