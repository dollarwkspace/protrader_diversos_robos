﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }

//    public class Piranhas : NETStrategy
//    {
//        public Account CAccount;

//        private Boolean bloqueieNovasOrdens = false;
//        public Instrument instrument;
//        //public Indicator volume;
//        public Indicator media;
//        public double lucroTemp = 0;
//        public DateTime timeAnterior = DateTime.Now;
//        public double variacaoPositivaAnterior = 0;
//        public double variacaoNegativaAnterior = 0;
//        public bool entradaNaCompraAutorizada = false;
//        public bool entradaNaVendaAutorizada = false;
//        public double derivadaMax = 0;
//        public double derivadaMin = 0;
//        public double dpDerivadaMax = 0;
//        public double dpDerivadaMin = 0;
//        public double derMax = 0;
//        public double derMin = 0;
//        public double dpVal = 0;

//        public List<double> tradeVolume = new List<double>();
//        public List<double> volumeDer = new List<double>();
//        public List<double> dpDerMax = new List<double>();
//        public List<double> dpDerMin = new List<double>();


//        [InputParameter("Contratos")]
//        public int Contratos = 2;

//        [InputParameter("Lucro de Saída")]
//        public double lucroSaida = 10;

//        [InputParameter("Fechar posições no Tick se lucro negativo?")]
//        public bool closeAll = true;

//        [InputParameter("Fechar posições no BAR se lucro negativo?")]
//        public bool closeAllBar = false;

//        [InputParameter("Buffer do Desvio Padrão")]
//        public int StdDevPeriod = 8;

//        [InputParameter("Desvio Padrão para Entrada (volume ou derivada)")]
//        public int StdDevIn = 4;

//        [InputParameter("Usar Derivada com StdDev?")]
//        public bool derivada = true;

//        [InputParameter("Período da Derivada em ms")]
//        public int milisec = 100;

//        [InputParameter("Período da Média de Preços")]
//        public int PricePeriod = 26;

//        public Piranhas()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo XXX-Money-Maker Piranhas";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "08.10.2018";
//            base.ExpirationDate = 0;
//            base.Version = "0.0.4";
//            base.Password = "";
//            base.ProjectName = "Piranhas";
//            #endregion

//        }

//        /// <summary>
//        /// This function will be called after creating
//        /// </summary>
//        public override void Init()
//        {
//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;

//            Instruments.NewTrade += OnTrade;

//            //volume = Indicators.iCustom("camilo_Volume", CurrentData, false, false, true, milisec);
//            media = Indicators.iMA(CurrentData, PricePeriod, PTLRuntime.NETScript.Indicators.MAMode.EMA, 0, PriceType.Medium);

//            instrument = Instruments.Current;

//            Instruments.Subscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
//            CAccount = Accounts.Current;
//        }

//        private void OnTrade(Instrument inst, Trade trade)
//        {
//            if (trade.Aggressor == AggressorFlag.Ask || trade.Aggressor == AggressorFlag.Bid)
//            {
//                double qtdeContratos = GetQtdeContratosAtivos();
//                double preco = media.GetValue(0);

//                var volumeSize = trade.Size;
//                if (trade.Aggressor == AggressorFlag.Bid) volumeSize = -volumeSize;

//                double deltaTime = trade.Time.Subtract(timeAnterior).TotalMilliseconds;
//                volumeDer.Add(volumeSize);

//                if (volumeDer.Count > StdDevPeriod)
//                {
//                    volumeDer.RemoveAt(0);
//                    dpVal = CalculaDesvioPadrao(volumeDer);
//                    if(!derivada) Print("Desvio Padrão de Volume:    " + dpVal);
//                }

//                if (deltaTime >= milisec)
//                {
//                    var deltaVolume = volumeDer.Sum();
//                    var derivada = deltaVolume / (deltaTime / 1000);
//                    timeAnterior = trade.Time;
//                    volumeDer.Clear();

//                    if (derivada > derivadaMax) derivadaMax = derivada;
//                    if (derivada < derivadaMin) derivadaMin = derivada;

//                    dpDerMax.Add(derivadaMax);
//                    dpDerMin.Add(derivadaMin);
//                }

//                if (derivada)
//                {
//                    //Print("Derivada Máxima: " + derivadaMax + " ***************************** Derivada Min:    " + derivadaMin);                 

//                    if(dpDerMax.Count>StdDevPeriod)
//                    {
//                        dpDerivadaMax = CalculaDesvioPadrao(dpDerMax);
//                        dpDerivadaMin = CalculaDesvioPadrao(dpDerMin);

//                        Print("Desvio Padrão Derivada Max:   " + Math.Round(dpDerivadaMax,2) + " *** Desvio Padrão Derivada Min:   "+Math.Round(dpDerivadaMin,2)+ " *** Desvio Padrão de Volume:    "+Math.Round(dpVal,2));

//                        dpDerMax.RemoveAt(0);
//                        dpDerMin.RemoveAt(0);

//                        if (Math.Abs(dpDerivadaMax) > Math.Abs(dpDerivadaMin)) dpVal = dpDerivadaMax;
//                        if (Math.Abs(dpDerivadaMax) < Math.Abs(dpDerivadaMin)) dpVal = dpDerivadaMin;
//                    }

//                }                                         


//                string result = "";


//                /********** CRITÉRIO DE ENTRADA ************************/
//                if (!bloqueieNovasOrdens && Math.Abs(qtdeContratos) == 0 && (trade.Aggressor == AggressorFlag.Ask || trade.Aggressor == AggressorFlag.Bid) && dpVal >= StdDevIn)
//                {
//                    if (trade.Aggressor == AggressorFlag.Ask) { result = Orders.Send(NewOrderRequest.CreateBuyMarket(instrument, Contratos, CAccount, TimeInForce.Day)); Print("Uma ordem foi solicitada! ID da ordem:" + result); }
//                    if (trade.Aggressor == AggressorFlag.Bid) { result = Orders.Send(NewOrderRequest.CreateSellMarket(instrument, Contratos, CAccount, TimeInForce.Day)); Print("Uma ordem foi solicitada! ID da ordem:" + result); }

//                    if (result != "-1") bloqueieNovasOrdens = true;
//                }

//                /********** CRITÉRIO DE SAÍDA ************************/
//                var lucroAtual = CalculaLucro();
//                if (Math.Abs(qtdeContratos) > 0 && lucroAtual > lucroSaida)
//                {
//                    //monitorando lucro
//                    if (lucroAtual > lucroTemp)
//                    {
//                        lucroTemp = lucroAtual;
//                    }
//                    else
//                    {
//                        FechamentoTotal();
//                        lucroTemp = 0;
//                    }
//                }
//            }
//        }

//        /// <summary>
//        /// Entry point. This function is called when new quote comes 
//        /// </summary>
//        public override void OnQuote()
//        {
//            if (closeAll && !EstamosLucrando() && Math.Abs(GetQtdeContratosAtivos()) > 0) { FechamentoTotal(); bloqueieNovasOrdens = false; }
//        }

//        public override void NextBar()
//        {
//            Orders.CancelAll();
//            bloqueieNovasOrdens = false;
//            derivadaMax = 0;
//            derivadaMin = 0;

//            if (closeAllBar && !EstamosLucrando() && Math.Abs(GetQtdeContratosAtivos()) > 0) { FechamentoTotal(); bloqueieNovasOrdens = false; }


//        }


//        /// <summary>
//        /// This function will be called before removing
//        /// </summary>
//        public override void Complete()
//        {
//            Print("*******************      FINAL DA OPERAÇÃO DO ROBÔ      ************************* ");
//            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
//            instrument = null;
//            //volume = null;
//            media = null;
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************

//        public void Orders_OrderRemoved(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//        }

//        public void Orders_OrderAdded(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = true;
//        }

//        public void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************

//        public double CalculaDesvioPadrao(List<double> lista)
//        {

//            var N = lista.Count;

//            double media = lista.Sum() / N;

//            double variancia = lista.Sum(val => Math.Pow((val - media), 2));

//            return Math.Sqrt(variancia / N);

//        }


//        public string Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = papelDestino;
//            request.Account = Accounts.Current;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.Price = price;
//            request.MarketRange = marketRange;
//            request.TimeInForce = TimeInForce.Day;

//            Print("Emita uma ordem de " + request.Side);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("Ordem não emitida"); bloqueieNovasOrdens = false; };
//            return result;
//        }

//        public double GetQtdeContratosAtivos()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                var openedPositions = Positions.GetPositions().ToList();
//                double qtdeAbsolutaDeContratos = 0;
//                if (openedPositions.Count == 0) { return 0; }
//                openedPositions.ForEach(el =>
//                {
//                    qtdeAbsolutaDeContratos += (el.Side == Operation.Buy) ? el.Amount : (-1) * el.Amount;
//                });

//                return qtdeAbsolutaDeContratos;
//            }
//            return 0;
//        }



//        public double CalculaPrecoMedio()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                var openedPositions = Positions.GetPositions().ToList();
//                if (openedPositions.Count == 0) return 0;
//                var somaQtdeXPrecoAbertura = openedPositions.Sum(p => p.Amount * p.OpenPrice);
//                var QtdeTotalDePapeisAbertos = openedPositions.Sum(p => p.Amount);
//                var precoMedio = somaQtdeXPrecoAbertura / QtdeTotalDePapeisAbertos;
//                return precoMedio;
//            }
//            return -1;
//        }

//        public void FechamentoParcial()
//        {
//            Position[] positions = Positions.GetPositions();
//            if (positions != null)
//            {
//                positions.ToList().ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
//            }
//        }

//        public void FechamentoTotal()
//        {
//            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");

//            Order[] orders = Orders.GetOrders();
//            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

//            Position[] positions = Positions.GetPositions();
//            if (positions != null) positions.ToList().ForEach(pos =>
//            {
//                pos.Close();
//            });

//        }

//        public Boolean EstamosLucrando()
//        {
//            var lucro = CalculaLucro();
//            return lucro > 0;
//        }

//        public double CalculaLucro()
//        {
//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                var openedPositions = Positions.GetPositions().ToList();
//                var lucro = openedPositions.Sum(p => p.GetProfitNet());
//                if (openedPositions.Count == 0) return 0;
//                return lucro;
//            }
//            return 0;
//        }


//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //************************************  ENTRADAS E SAÍDAS - FUNÇÕES   ********************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************


//    }

//}
