﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public class RoboDicionario : NETStrategy
//    {
//        private Instrument instrument;
//        private Account conta;
//        private Indicator MM;
//        private Indicator MACD;
//        private Indicator STOC;

//        private bool liberaEntrada = false;
//        private bool liberaReEntrada = false;
//        private double calor = 0;
//        private double verde = 0;
//        private double azul = 0;
//        private double tickSize = 0;
//        private string horaDoCalorMaximo = "";
//        private double maxDePosicaoAberta = 0;
//        private double ultimaReentrada = 0;

//        private bool bloqueieNovasOrdens = false;
//        private System.Timers.Timer syncTimer;

//        public RoboDicionario()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo Dicionario";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "12.12.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.1";
//            base.Password = "";
//            base.ProjectName = "Robô Dollar";
//            #endregion
//        }

//        [InputParameter("MACD/STOC para entrada", 0, new Object[]
//        {
//            "MACD","MACD", "STOC", "STOC", "NENHUM", "NENHUM"
//        })]
//        public string indiceDeEntrada = "STOC";

//        [InputParameter("STOC MAXIMO")]
//        public int stocMaximo = 80;
//        [InputParameter("STOC MINIMO")]
//        public int stocMinimo = 20;

//        [InputParameter("Diff verde/azul: STOC")]
//        public double deltaStoc = 0;

//        [InputParameter("Período MACD: Sinal")]
//        public int periodoMACDSinal = 9;
//        [InputParameter("Período MACD: Fast")]
//        public int periodoMACDFast = 12;
//        [InputParameter("Período MACD: Slow")]
//        public int periodoMACDSlow = 26;
//        [InputParameter("Média: Sinal")]
//        public int periodoMediaMovelSinal = 4;
//        [InputParameter("Média: Fast")]
//        public int periodoMediaMovelFast = 12;
//        [InputParameter("Média: Slow")]
//        public int periodoMediaMovelSlow = 55;

//        [InputParameter("Permitir até quantas posições abertas? ")]
//        public double maxPosAbertas = 50;

//        [InputParameter("Contratos")]
//        public int Contratos = 5;

//        [InputParameter("Hora de Término")]
//        public int HoraDeTermino = 15;

//        [InputParameter("Ticks de Reentrada")]
//        public int tickDeReentrada = 10;

//        [InputParameter("LOSS Máximo Tolerado")]
//        public double lossMaximo = 10000;

//        [InputParameter("Take Profit de Entrada")]
//        public double TP = 3;

//        [InputParameter("Timer")]
//        public double timerParaCancelar = 15000;

//        [InputParameter("Número Mágico")]
//        public int numeroMagico = 123;

//        public override void Init()
//        {
//            //******************************
//            //**** INICIALIZAÇÃO DO ROBÔ ***
//            //******************************

//            instrument = Instruments.Current;
//            conta = Accounts.Current;
//            tickSize = instrument.TickSize;
//            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
//            SetSyncTimer();
//            //AccountInformation(conta);

//            Print("Papel: " + instrument.Name);
//            Print("Tamanho do Point: " + Point);
//            Print("Tamanho do tick: " + instrument.TickSize);

//            //EVENTOS
//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;
//            Positions.PositionAdded += Positions_PositionAdded;
//            Positions.PositionRemoved += Positions_PositionRemoved;

//            //INDICADORES
//            MM = Indicators.iCustom("camilo_3mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelFast, periodoMediaMovelSlow);
//            MACD = Indicators.iCustom("camilo_macd", CurrentData, periodoMACDSinal, periodoMACDFast, periodoMACDSlow);
//            STOC = Indicators.iCustom("STOCHASTIC", CurrentData);

//            Print("Fim da Inicialização do Robô");

//        }

//        private void Positions_PositionRemoved(Position obj)
//        {
//            Print("PositionRemoved: Posição com id " + obj.Id + " de tipo " + obj.Side + " foi removida! bloqueieNovasOrdens:" + bloqueieNovasOrdens);
//            LimparTodasAsOrdens();
//        }

//        private void Positions_PositionAdded(Position obj)
//        {
//            try
//            {
//                LimparTodasAsOrdens();
//                ultimaReentrada = obj.OpenPrice;

//                if (obj.Side == Operation.Buy)
//                {
//                    NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, obj.Amount, instrument.RoundPrice(obj.OpenPrice + TP * instrument.TickSize), conta);
//                    request.MagicNumber = numeroMagico;
//                    request.MarketRange = 1000;
//                    Print("PositionAdded: Tentando setar takeprofit LIMIT");
//                    var result = Orders.Send(request);
//                    if (result == "-1") { Print("PositionAdded: Ordem Limit Sell de Saída recusada!"); } else { liberaReEntrada = false; }

//                }
//                if (obj.Side == Operation.Sell)
//                {
//                    NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, obj.Amount, instrument.RoundPrice(obj.OpenPrice - TP * instrument.TickSize), conta);
//                    request.MagicNumber = numeroMagico;
//                    request.MarketRange = 1000;
//                    Print("PositionAdded: Tentando setar takeprofit LIMIT");
//                    var result = Orders.Send(request);
//                    if (result == "-1") { Print("PositionAdded: Ordem Limit Buy de Saída recusada!"); } else { liberaReEntrada = false; }
//                }


//            }
//            catch (Exception ext)
//            {
//                Alert("PositionAdded: Erro no Position Added: " + ext.Message);
//                LimparTodasAsOrdens();
//            }
//        }

//        public override void OnQuote()
//        {
//            try
//            {
//                var cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
//                var mac = MACD.GetValue(2, 0);
//                var media = MM.GetValue(2, 0);
//                var sinal = MM.GetValue(0, 0);
//                verde = STOC.GetValue(0, 0);
//                azul = STOC.GetValue(1, 0);

//                var hora = 0;

//                if (instrument.LastQuote != null)
//                {
//                    hora = DateTime.Now.Hour;
//                    if (hora == 0) { Print("Hora zerada.. saindo do OnQuote()"); return; }
//                    if (hora > HoraDeTermino) { Print("OnQuote: Hora de término atingida!"); return; }
//                }
//                else
//                {
//                    Alert("OnQuote: última cotação foi nula!");
//                    return;
//                }

//                Position[] positions = Positions.GetPositions();
//                Order[] orders = Orders.GetOrders();

//                if (positions.Length != 0) positions.ToList().ForEach(pos => PosInfo(pos));
//                if (orders.Length != 0) orders.ToList().ForEach(ord => OrderInfo(ord));

//                if (positions.Length != 0)
//                {
//                    double temp = 0;
//                    temp = positions[0].GetProfitNet();

//                    if (temp < calor) { calor = temp; horaDoCalorMaximo = instrument.LastQuote.Time.ToLocalTime().ToShortTimeString(); maxDePosicaoAberta = positions[0].Amount; }
//                }

//                //SAÍDA POR LOSS MÁXIMO
//                if (Positions.Count != 0)
//                {
//                    Position pos = positions[0];
//                    if (Math.Abs(pos.GetProfitNet()) > lossMaximo)
//                    {
//                        var result = pos.Close();
//                        if (result)
//                        {
//                            Print("Posições Fechadas por LOSS MÁXIMO !!!");
//                            LimparTodasAsOrdens();
//                        }
//                    }
//                }

//                if (mac != 0 && media != 0)
//                {
//                    //CHECANDO SE EXISTE MAIS DE 1 ORDEM DE SAÍDA... SE EXISTIR É ERRO
//                    if (Orders.Count != 0)
//                    {
//                        //bom.. tem ordem na fila.. SE tiver apenas 1, então ela tem q ser exatamente oposta à posição , se não for limpatudo!
//                        if (positions.Length != 0 && Orders.Count == 1)
//                        {
//                            Print("OnQuote Verificação de Erro: Verificando se Ordem aberta é oposta à Posição");
//                            if (!ExisteTakeProfitVinculado(positions[0])) { Print("OnQuote ERRO: Não pode ter ordem de mesmo tipo que a posição! Limpando tudo"); LimparTodasAsOrdens(); return; }
//                        }
//                        if (positions.Length != 0 && Orders.Count > 1)
//                        {
//                            Position pos = positions[0];
//                            if (!ExisteTakeProfitVinculado(pos)) { Print("OnQuote Erro: Existe mais de 2 ordens e nenhuma delas é de saída!"); LimparTodasAsOrdens(); return; }
//                        }

//                        if (positions.Length == 0 && Orders.Count > 1)
//                        {
//                            Print("OnQuote Erro: Existe mais de 2 ordens pendentes não existe posição aberta ainda"); LimparTodasAsOrdens(); return; 
//                        }
//                    }

//                    Print("OnQuote: Checando se NÃO tem posição E se tem Ordem . " + "Posições: " + positions.Length + " Ordens:" + Orders.Count);
//                    if (positions.Length == 0 && Orders.Count != 0)
//                    {
//                        //não tem posição mas tem Ordem. Precisamos aguardar ela ser executada                        
//                        Print("OnQuote: OK ! Não tem posição mas tem Ordem E se ela não for executada até o timer estourar ela será limpada...");
//                        return;
//                    }

//                    //TENTANDO ENTRAR
//                    Print("OnQuote: Checando se NÃO tem posição E NÃO existe bloqueio de novas ordens (código tentando entrar...); bloqueieNovasOrdens:" + bloqueieNovasOrdens + " Posições:" + positions.Length);
//                    if (positions.Length == 0 && !bloqueieNovasOrdens)
//                    {
//                        liberaReEntrada = false;
//                        Print("OnQuote: OK! Verificando Liberação de Entrada...");
//                        if (LiberaEntrada(cotacaoAtual, sinal, media)) { liberaEntrada = true; Print("OnQuote: Entrada LIBERADA!"); } else { Print("OnQuote: Entrada não Liberada"); }
//                        if (liberaEntrada)
//                        {
//                            Print("OnQuote: Tentando Entrar...");
//                            Entrada(cotacaoAtual, mac, media);
//                        }
//                    }

//                    //TENTANDO REENTRAR
//                    Print("OnQuote: Checando se tem posição E não existe bloqueio de novas ordens (código tentando REENTRAR...); bloqueieNovasOrdens:" + bloqueieNovasOrdens + " Posições:" + positions.Length);
//                    if (positions.Length != 0)
//                    {
//                        Position pos = positions[0];
//                        var deltaMoeda = instrument.TickSize * tickDeReentrada;
//                        liberaReEntrada = false;
//                        Print("Análise de Reentrada: Preço atual está no ponto de reentrada? ");
//                        PosInfo(pos);
//                        if (pos.Side == Operation.Buy && pos.CurrentPrice <= ultimaReentrada - deltaMoeda)
//                        {
//                            Print("OnQuote: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
//                            liberaReEntrada = true;
//                            bloqueieNovasOrdens = false;
//                        }

//                        if (pos.Side == Operation.Sell && pos.CurrentPrice >= ultimaReentrada + deltaMoeda)
//                        {
//                            Print("OnQuote: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
//                            liberaReEntrada = true;
//                            bloqueieNovasOrdens = false;
//                        }
//                    }
//                    if (positions.Length != 0 && liberaReEntrada)
//                    {
//                        Print("OnQuote: Ok! Tentando Reentrar...");
//                        if (positions[0].Amount <= maxPosAbertas / 2) { ReEntrada(cotacaoAtual, positions[0], verde, azul); } else { Print("OnQuote: Máximo de Contratos atingidos!"); }
//                    }

//                    //TAKE PROFIT LIMIT
//                    Print("OnQuote: Checando se TEM posição, NÃO existe ordens E NÃO existe bloqueio (código de criação do Take Profit...); bloqueieNovasOrdens:" + bloqueieNovasOrdens + " Posições: " + positions.Length + " Ordens:" + Orders.Count);
//                    if (positions.Length != 0 && Orders.Count == 0 && !bloqueieNovasOrdens)
//                    {
//                        Print("OnQuote: Ok! Tentando setar TAKE PROFIT LIMIT");
//                        Position pos = positions[0];
//                        try
//                        {
//                            if (pos.Side == Operation.Buy)
//                            {
//                                NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
//                                request.MagicNumber = numeroMagico;
//                                request.MarketRange = 1000;
//                                var id = Orders.Send(request);
//                                if (id == "-1")
//                                {
//                                    Print("OnQuote: Ordem Limit de saída SELL não colocada... tentando ordem STOP");
//                                    NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
//                                    req.MagicNumber = numeroMagico;
//                                    req.MarketRange = 1000;
//                                    var id2 = Orders.Send(req);
//                                    if (id2 == "-1") { Print("OnQuote: Ordem SELL STOP não aceita!"); LimparTodasAsOrdens(); }
//                                }
//                            }
//                            if (pos.Side == Operation.Sell)
//                            {
//                                NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
//                                request.MagicNumber = numeroMagico;
//                                request.MarketRange = 10;
//                                var id = Orders.Send(request);
//                                if (id == "-1")
//                                {
//                                    Print("OnQuote: Ordem Limit de saída BUY não colocada... tentando ordem STOP");
//                                    NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
//                                    req.MagicNumber = numeroMagico;
//                                    req.MarketRange = 1000;
//                                    var id2 = Orders.Send(req);
//                                    if (id2 == "-1") { Print("OnQuote: Ordem BUY STOP não aceita!"); LimparTodasAsOrdens(); }
//                                }
//                            }
//                        }
//                        catch (Exception ext)
//                        {
//                            Alert("OnQuote: Erro ao Setar Take Profit! " + ext.Message);
//                        }
//                    }

//                    //AJUSTANDO TAKE PROFIT
//                    Print("OnQuote: Checando se tem posição e se tem Ordens (código de modificação de Take Profit...) . Positions:" + positions.Length + " Orders:" + Orders.Count);
//                    if (positions.Length != 0 && Orders.Count != 0)
//                    {
//                        Position pos = positions[0];
//                        Print("OnQuote: Ok! Verificando ativamente se Ordem de Saída está com mesma qtde de contratos e distância correta");
//                        if (ExisteTakeProfitVinculado(pos))
//                        {
//                            Order ord = BuscaOrdemTakeProfit(pos);
//                            if (ord != null)
//                            {
//                                if (ord.Amount != pos.Amount)
//                                {
//                                    Print("OnQuote: Existem diferenças de qtde entre Posição e Ordem de Saída.. ajustando parâmetros");
//                                    double preco = (ord.Side == Operation.Buy) ? instrument.RoundPrice(pos.OpenPrice - TP * tickSize) : instrument.RoundPrice(pos.OpenPrice + TP * tickSize);
//                                    ord.ModifyAsync(preco, pos.Amount, (result) => { if (!result) Print("OnQuote: Modificação de TAKE PROFIT de REENTRADA Falhou"); });
//                                }
//                            }
//                        }
//                    }
//                }




//            }
//            catch (Exception exc)
//            {
//                Alert("OnQuote Erro: " + exc.Message);
//            }
//        }

//        public override void NextBar()
//        {

//        }

//        public override void Complete()
//        {
//            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
//            syncTimer.Close();
//            syncTimer.Dispose();

//            Alert("Maior calor: " + calor + " hora:" + horaDoCalorMaximo + " contratos abertos no calor:" + maxDePosicaoAberta);
//        }


//        private void Entrada(double preco, double macd, double media)
//        {
//            if (preco >= media && !bloqueieNovasOrdens)
//            {
//                if (indiceDeEntrada != "NENHUM")
//                {
//                    if (indiceDeEntrada == "STOC" && verde + deltaStoc < azul) { Print("Entrada: Condições STOC não atingidas"); liberaEntrada = false; return; }
//                    if (indiceDeEntrada == "MACD" && macd < 0) { Print("Entrada: Condições MACD não atingidas"); liberaEntrada = false; return; }
//                    if (verde > stocMaximo || azul > stocMaximo) { Print("Entrada: STOC acima de máximo ou mínimo"); liberaEntrada = false; return; }
//                }
//                if (Orders.Count == 0) Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
//            }

//            if (preco < media && !bloqueieNovasOrdens)
//            {
//                if (indiceDeEntrada != "NENHUM")
//                {
//                    if (indiceDeEntrada == "STOC" && verde > azul + deltaStoc) { Print("Entrada: Condições STOC não atingidas"); liberaEntrada = false; return; }
//                    if (indiceDeEntrada == "MACD" && macd > 0) { Print("Entrada: Condições MACD não atingidas"); liberaEntrada = false; return; }
//                    if (verde < stocMinimo || azul < stocMinimo) { Print("Entrada: STOC acima de máximo ou mínimo"); liberaEntrada = false; return; }
//                }
//                if (Orders.Count == 0) Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
//            }
//        }

//        private void ReEntrada(double preco, Position pos, double verde, double azul)
//        {
//            var precoMedio = pos.OpenPrice;
//            var deltaMoeda = instrument.TickSize * tickDeReentrada;
            

//            if (pos.Side == Operation.Buy)
//            {                
//                if(verde < azul + deltaStoc) { Print("ReEntrada: Condições STOC não atingidas"); return; }
//                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
//                Send_order(OrdersType.Market, Operation.Buy, preco, pos.Amount, 0);
//            }            

//            if (pos.Side == Operation.Sell)
//            {                
//                if(verde + deltaStoc > azul) { Print("ReEntrada: Condições STOC não atingidas"); return; }
//                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
//                Send_order(OrdersType.Market, Operation.Sell, preco, pos.Amount, 0);
//            }
//        }


//        public void Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = instrument;
//            request.Account = conta;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.MarketRange = 10;
//            request.MagicNumber = numeroMagico;

//            if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? instrument.RoundPrice(price + takeProfit * instrument.TickSize) : instrument.RoundPrice(price - takeProfit * instrument.TickSize);
//            if (type != OrdersType.StopLimit) { request.Price = instrument.RoundPrice(price); } else { request.StopPrice = instrument.RoundPrice(price); }

//            Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + instrument.RoundPrice(price) + " Qtde:" + request.Amount);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("SendOrder: Ordem não aceita pela corretora"); bloqueieNovasOrdens = false; liberaEntrada = false; }
//            if (result != "-1") Print("SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!");

//        }

//        private bool LiberaEntrada(double preco, double sinal, double media)
//        {
//            if (preco > media && preco < sinal) return true;
//            if (preco < media && preco > sinal) return true;

//            return false;
//        }

//        private bool ExisteTakeProfitVinculado(Position pos)
//        {
//            Operation side = (pos.Side == Operation.Buy) ? Operation.Sell : Operation.Buy;
//            return (Orders.GetOrders(true)?.ToList().Find((ord) => ord.Side == side) != null) ? true : false;
//        }

//        private Order BuscaOrdemTakeProfit(Position pos)
//        {
//            Operation side = (pos.Side == Operation.Buy) ? Operation.Sell : Operation.Buy;
//            return Orders.GetOrders(true)?.ToList().Find(ord => ord.Side == side);
//        }

//        private void LimparTodasAsOrdens()
//        {
//            Orders.GetOrders()?.ToList().ForEach(ord => ord.CancelAsync((result) =>
//            {
//                if (!result)
//                {
//                    Alert("LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA!");
//                }
//                else
//                {
//                    Print("LimparTodasAsOrdens: Ordem " + ord.Id + " FOI CANCELADA!");
//                    bloqueieNovasOrdens = false;
//                    liberaReEntrada = false;
//                }
//            }));
//        }


//        private void Orders_OrderRemoved(Order obj)
//        {
//            bloqueieNovasOrdens = false;
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString() + " bloqueieNovasOrdens:" + bloqueieNovasOrdens);
//        }

//        private void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString() + " bloqueieNovasOrdens:" + bloqueieNovasOrdens);
//        }

//        private void Orders_OrderAdded(Order obj)
//        {
//            bloqueieNovasOrdens = true;
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString() + " bloqueieNovasOrdens:" + bloqueieNovasOrdens);
//        }

//        private void SetSyncTimer()
//        {
//            // cria um timer com 1s de intervalo
//            syncTimer = new System.Timers.Timer(timerParaCancelar);
//            syncTimer.Elapsed += SyncTimer_Elapsed;
//            syncTimer.AutoReset = true;
//            syncTimer.Enabled = true;
//        }

//        private void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//        {
//            try
//            {
//                Print("Timer: Timer de " + (timerParaCancelar / 1000).ToString() + " segundos");
//                Position[] positions = Positions.GetPositions();
//                if (positions.Length != 0)
//                {
//                    if (ExisteTakeProfitVinculado(positions[0])) { Print("Timer: Existe Posição E uma ordem de TP aberta...  aguardando sua execução. Timer faz nada!"); return; }
//                    bloqueieNovasOrdens = false;
//                }
//                else
//                {
//                    if (Orders.Count != 0)
//                    {
//                        Print("Timer: Limpando todas as ordens não executadas...");
//                        LimparTodasAsOrdens();
//                    }
//                }

//                if (positions.Length == 0 && Orders.Count == 0 && bloqueieNovasOrdens)
//                {
//                    Print("Timer: SEM posição E SEM Ordem e bloqueio de ordens está ativado? Então limpa... ");
//                    LimparTodasAsOrdens();
//                    bloqueieNovasOrdens = false;
//                    liberaReEntrada = false;
//                }

//            }
//            catch (Exception ext)
//            {
//                Alert("Timer: Erro no SyncTimer: " + ext.Message);
//            }

//        }

//        private void AccountInformation(Account acc)
//        {
//            //outputting of all the 'Account' properties
//            Print(
//                "Id : \t" + acc.Id + "\n" +
//                "Name : \t" + acc.Name + "\n" +
//                "User Login: \t" + acc.User.Login + "\n" +
//                "Balance : \t" + acc.Balance + "\n" +
//                "Leverage : \t" + acc.Leverage + "\n" +
//                "Currency : \t" + acc.Currency + "\n" +
//                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
//                "IsDemo : \t" + acc.IsDemo + "\n" +
//                "IsReal : \t" + acc.IsReal + "\n" +
//                "IsLocked : \t" + acc.IsLocked + "\n" +
//                "IsInvestor : \t" + acc.IsInvestor + "\n" +
//                "Status : \t" + acc.Status + "\n" +
//                "StopReason : \t" + acc.StopReason + "\n" +
//                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
//                "Balance : \t" + acc.Balance + "\n" +
//                "BeginBalance : \t" + acc.BeginBalance + "\n" +
//                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
//                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
//                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
//                "Credit : \t" + acc.Credit + "\n" +
//                "CashBalance : \t" + acc.CashBalance + "\n" +
//                "TodayVolume : \t" + acc.TodayVolume + "\n" +
//                "TodayNet : \t" + acc.TodayNet + "\n" +
//                "TodayTrades : \t" + acc.TodayTrades + "\n" +
//                "TodayFees : \t" + acc.TodayFees + "\n" +
//                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
//                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
//                "MarginTotal : \t" + acc.MarginTotal + "\n" +
//                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
//                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
//                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
//                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
//                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
//                "Equity : \t" + acc.Equity + "\n" +
//                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
//                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
//                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
//                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
//                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
//            );
//        }

//        private void PosInfo(Position pos)
//        {
//            double proximaReentrada = (pos.Side == Operation.Buy) ? ultimaReentrada - instrument.TickSize * tickDeReentrada : ultimaReentrada + instrument.TickSize * tickDeReentrada;
//            Print(
//                "PosInfo()-> " +
//                "Id: " + pos.Id + "\t" +
//                "  Qtde: " + pos.Amount + "\t" +
//                "  Tipo: " + pos.Side.ToString() + "\t" +
//                "  Preço Abertura: R$ " + pos.OpenPrice + "\t" +
//                "  Preço Atual: R$ " + pos.CurrentPrice + "\t" +
//                "  Número Mágico: " + pos.MagicNumber + "\t" +
//                "  Última Reentrada: R$" + ultimaReentrada + "\t" +
//                "  Próxima Reentrada: R$" + proximaReentrada + "\t" +
//                "  Lucro ou Prejuízo: R$" + pos.GetProfitNet()
//            );
//        }

//        private void OrderInfo(Order ord)
//        {
//            Print(
//                "OrderInfo()-> " +
//                "Id: " + ord.Id + "\t" +
//                "  Qtde: " + ord.Amount + "\t" +
//                "  Tipo: " + ord.Side.ToString() + "\t" +
//                "  Preço: R$ " + ord.Price + "\t" +
//                "  Número Mágico: " + ord.MagicNumber
//            );

//        }
//    }
//}
