﻿                  using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Windows.Forms;
using System.Globalization;

namespace RoboDollar
{
    /// <summary>
    /// RoboModelo
    /// 
    /// </summary>
    public class RoboDollar_Glaciano : NETStrategy
    {

        //private RoboDesign roboDesign = new RoboDesign();
        private Instrument[] instruments;
        private Instrument instrument;
        private Instrument papelDestino;
        private Account CAccount;
        private Indicator indicadorMASinal;
		private Indicator indicadorMACurto;
        private Indicator indicadorMALongo;
        private Indicator indicadorSAR;
        private Indicator indicadorHILO;
        private Indicator indicadorStopATR;
        private Indicator indicadorDesvioPadrao;
		private Indicator indicadorhilomax;
		private Indicator indicadorhilomin;


        private DateTime inicio;
        private DateTime termino;
        private int resilienciaCompraCounter = 0;
        private int resilienciaVendaCounter = 0;
        private int resilienciaNebulosaCounter = 0;
        List<string> openedOrders = new List<string>();
        private Boolean jaComprou = false;
        private Boolean jaVendeu = false;
		private Boolean Virado = true;
        
        
        [InputParameter("Periodo MExp Sinal", 1)]
        public int PeriodoSinal = 10;

        [InputParameter("Periodo MExp Curto", 0)]
        public int PeriodoCurto = 5;

        [InputParameter("Periodo MExp Longo", 1)]
        public int PeriodoLongo = 15;

        [InputParameter("Resiliência Compra", 2)]
        public int ResilienciaCompra = 0;

        [InputParameter("Resiliência Venda", 3)]
        public int ResilienciaVenda = 0;

        [InputParameter("Resiliência Nebulosa", 4)]
        public int ResilienciaNebulosa = 15;

        [InputParameter("Price Type", 5, new object[] {
            "Close",PriceType.Close,
            "High",PriceType.High,
            "Low",PriceType.Low,
            "Medium",PriceType.Medium,
            "Open",PriceType.Open,
            "Typical",PriceType.Typical,
            "Weighted",PriceType.Weighted
        })]
        public PriceType priceType = PriceType.Close;

        [InputParameter("Take Profit", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit = 1; //Take Profit  
        
         [InputParameter("Take Profit II", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit_2 = 4; //Take Profit  
        
        [InputParameter("Take Profit III", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit_3 = 2; //Take Profit  
        
        [InputParameter("Take Profit IV", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit_4 = 45; //Take Profit  
        
        [InputParameter("Take Profit V", 6, 0, 9999, 1, 0.1)]
        public double Take_Profit_5 = 55; //Take Profit  

        [InputParameter("Stop Loss", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss = 3; //Stop Loss
        
        [InputParameter("Stop Loss II", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss_2 = 4; //Stop Loss
        
        [InputParameter("Stop Loss III", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss_3 = 3; //Stop Loss
        
        [InputParameter("Stop Loss IV", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss_4 = 20; //Stop Loss
        
        [InputParameter("Stop Loss V", 7, 0, 9999, 1, 0.1)]
        public double Stop_Loss_5 = 20; //Stop Loss

        [InputParameter("tipo de MA", 8, new object[]
       {
             "EMA - Exponencial", PTLRuntime.NETScript.Indicators.MAMode.EMA,
             "LWMA- Linear Weighted", PTLRuntime.NETScript.Indicators.MAMode.LWMA,
             "SMA - Smoothed", PTLRuntime.NETScript.Indicators.MAMode.SMA,
             "SMMA", PTLRuntime.NETScript.Indicators.MAMode.SMMA
       })]
        public PTLRuntime.NETScript.Indicators.MAMode MA_type1 = PTLRuntime.NETScript.Indicators.MAMode.SMA; // First smoothing type

        [InputParameter("Contratos", 9)]
        public int Contratos = 1;

        [InputParameter("Tick Offset", 11)]
        public int TickOffset = 0;

        [InputParameter("Tick Mínimo", 12)]
        public double TickMinimo = 0.5;

        [InputParameter("SAR Step", 13)]
        public double stepSar = 0.02;

        [InputParameter("SAR Coef", 14)]
        public double coefSar = 0.07;

        [InputParameter("StdDev Periodo", 15)]
        public int StdDevPeriodo = 8;

        [InputParameter("StdDev Limite", 16)]
        public int StdDevLimite = 1;

        [InputParameter("Hora de Início", 17)]
        public string HoraDeInicio = "09:00:00";

        [InputParameter("Hora de Término", 18)]
        public string HoraDeTermino = "17:58:00";

        [InputParameter("Break Even", 19, 0, 9999, 1, 0.1)]
        public double Break_Even = 4; // Break Even offset when price reaches number of points 

        [InputParameter("Break Even Value", 20, 0, 9999, 1, 0.1)]
        public double Break_Even_val = 1; // Break Even (value) 

        [InputParameter("Usar BreakEven?", 21)]
        public Boolean usarBreakEven = true;
        
        [InputParameter("Delta", 22)]
        public int Delta = 3;
        
        [InputParameter("valor hilo", 24)]
        public int valorhilo = 8;
        
        
               

        //[InputParameter("Papel Destino", 13)]
        //public string PapelDestino = "WDOV18";

        public RoboDollar_Glaciano()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "É um leitão";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "06.09.2018";
            base.ExpirationDate = 0;
            base.Version = "0.0.1";
            base.Password = "";
            base.ProjectName = "RoboDollar";
            #endregion           

        }

        /// <summary>
        /// This function will be called after creating
        /// </summary>
        public override void Init()
        {
            instruments = Instruments.GetInstruments();

            //roboDesign.PopularAtivos(instruments);            


            instrument = Instruments.Current;
            papelDestino = instrument;
            //papelDestino = Instruments.GetInstrument(PapelDestino);
            Instruments.Subscribe(instrument, QuoteTypes.Quote);
            CAccount = Accounts.Current;

			indicadorMASinal = Indicators.iMA(CurrentData, PeriodoSinal, MA_type1, 0, priceType);            
            indicadorMACurto = Indicators.iMA(CurrentData, PeriodoCurto, MA_type1, 0, priceType);
            indicadorMALongo = Indicators.iMA(CurrentData, PeriodoLongo, MA_type1, 0, priceType);
            indicadorhilomax = Indicators.iMA(CurrentData, valorhilo, MA_type1, 0, PriceType.High);
            indicadorhilomin = Indicators.iMA(CurrentData, valorhilo, MA_type1, 0, PriceType.Low);
            indicadorSAR = Indicators.iSAR(CurrentData, stepSar, coefSar);
            indicadorHILO = Indicators.iCustom("HiLoEscadinha", CurrentData);
            //indicadorStopATR = Indicators.iCustom("StopATRv2", CurrentData);
            indicadorDesvioPadrao = Indicators.iStdDev(CurrentData, StdDevPeriodo, MA_type1, priceType);

            DateTime.TryParseExact(HoraDeInicio, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out inicio);
            DateTime.TryParseExact(HoraDeTermino, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out termino);

            //roboDesign.Show();
        }

        /// <summary>
        /// Entry point. This function is called when new quote comes 
        /// </summary>
        public override void OnQuote()
        {
            base.OnQuote();

            var hora = instrument.LastQuote.Time.ToLocalTime().Hour;
            var minutos = instrument.LastQuote.Time.ToLocalTime().Minute;

            if (instrument.LastQuote == null) return;

            if (hora >= inicio.Hour && hora <= termino.Hour && minutos >= inicio.Minute && minutos <= termino.Minute)
            {
                Alert("Inicio:" + inicio.ToShortTimeString() + " --> Desvio Padrão:" + indicadorDesvioPadrao.GetValue().ToString() + " Hilo:" + indicadorHILO.GetValue().ToString());

                if (hora == termino.Hour && minutos == termino.Minute)
                {
                    Fechar();
                    return;
                }

                var cotacaoAtual = CurrentData.GetPrice(priceType);
				var mediaMovelSinal = indicadorMASinal.GetValue();
                var mediaMovelCurta = indicadorMACurto.GetValue();
                var mediaMovelLonga = indicadorMALongo.GetValue();
                var stdDevCurrent = indicadorDesvioPadrao.GetValue();
				var varhilomax = indicadorhilomax.GetValue();
				var varhilomin = indicadorhilomin.GetValue();
				var varSAR = indicadorSAR.GetValue();

                //var SAR = indicadorSAR.GetValue();

                Position[] posicoes = Positions.GetPositions();
                Position pos = null;

                if (posicoes.Length != 0)
                {
                    pos = posicoes[posicoes.Length - 1];
                }
                else
                {
                    jaVendeu = false;
                    jaComprou = false;
                    resilienciaNebulosaCounter = 0;
                    resilienciaCompraCounter = 0;
                    resilienciaVendaCounter = 0;
                    openedOrders.Clear();
                }

                //critérios de SAÍDA
                if (pos != null)
                {
                    if (usarBreakEven)
                    {
                        if (pos.Side == Operation.Buy && Instruments.Current.LastQuote.Ask > pos.OpenPrice + Break_Even * Point)                        {
                            pos.SetStopLoss(pos.OpenPrice + Break_Even_val * Point);
                        }

                        else if (pos.Side == Operation.Sell && Instruments.Current.LastQuote.Bid < pos.OpenPrice - Break_Even * Point)
                        {
                            pos.SetStopLoss(pos.OpenPrice - Break_Even_val * Point);
                        }
                    }

                    resilienciaNebulosaCounter++;
                    if (resilienciaNebulosaCounter > ResilienciaNebulosa)
                    {
                    	if (cotacaoAtual < ( varhilomin + Delta)&& pos.Side == Operation.Buy)
                        {

                            resilienciaNebulosaCounter = 0;
                            resilienciaCompraCounter = 0;
                            resilienciaVendaCounter = 0;
                            openedOrders.Clear();
                            jaVendeu = false;
                            jaComprou = false;
                            pos.Close();



                        }

                    	if (cotacaoAtual > ( varhilomax + Delta ) && pos.Side == Operation.Sell)
                        {
                            resilienciaNebulosaCounter = 0;
                            resilienciaCompraCounter = 0;
                            resilienciaVendaCounter = 0;
                            openedOrders.Clear();
                            jaVendeu = false;
                            jaComprou = false;
                            pos.Close();
                        }
                    }
                }

                //Critérios de entrada
                if (
                    (cotacaoAtual + TickOffset * TickMinimo) > mediaMovelCurta &&
                    (cotacaoAtual + TickOffset * TickMinimo) > mediaMovelLonga &&
                    (cotacaoAtual + TickOffset * TickMinimo) > mediaMovelSinal &&
                    (cotacaoAtual + TickOffset * TickMinimo) > varhilomax 
                    )
                              {
                    resilienciaCompraCounter++;
                    if (resilienciaCompraCounter > ResilienciaCompra)
                    {
                        // tipo de ordem, operação, preço de compra, contratos, market range
                        if (!jaComprou) 
                        {
							
								string result = Send_order(OrdersType.Market, Operation.Buy, cotacaoAtual + Point, Contratos, 10, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								string result_2 = Send_order_2(OrdersType.Market, Operation.Buy, cotacaoAtual + Point, Contratos, 10, papelDestino);
								Print(result);
								openedOrders.Add(result);
								
                                string result_3 = Send_order_3(OrdersType.Market, Operation.Buy, cotacaoAtual + Point, Contratos, 10, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								//string result_4 = Send_order_3(OrdersType.Market, Operation.Buy, cotacaoAtual + Point, Contratos, 10, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								//string result_5 = Send_order_3(OrdersType.Market, Operation.Buy, cotacaoAtual + Point, Contratos, 10, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								jaComprou = true;
								resilienciaCompraCounter = 0;
								
							
                        }

                    }
                }

                if (
                    (cotacaoAtual + TickOffset * TickMinimo) < mediaMovelCurta &&
                    (cotacaoAtual + TickOffset * TickMinimo) < mediaMovelLonga &&
                    (cotacaoAtual + TickOffset * TickMinimo) < mediaMovelSinal &&
                    (cotacaoAtual + TickOffset * TickMinimo) < varhilomin 
                    )
                {
                    resilienciaVendaCounter++;
                    if (resilienciaVendaCounter > ResilienciaVenda)
                    {
                        // tipo de ordem, operação, preço de compra, contratos, market range, papelDestino
                        if (!jaVendeu)
                        {
							
								string result = Send_order(OrdersType.Market, Operation.Sell, cotacaoAtual - Point, Contratos, 30, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								string result_2 = Send_order_2(OrdersType.Market, Operation.Sell, cotacaoAtual - Point, Contratos, 30, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								string result_3 = Send_order_3(OrdersType.Market, Operation.Sell, cotacaoAtual - Point, Contratos, 30, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								//string result_4 = Send_order_3(OrdersType.Market, Operation.Sell, cotacaoAtual - Point, Contratos, 30, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								//string result_5 = Send_order_3(OrdersType.Market, Operation.Sell, cotacaoAtual - Point, Contratos, 30, papelDestino);
								Print(result);
								openedOrders.Add(result);
                            
								jaVendeu = true;
								resilienciaVendaCounter = 0;
							
                        }
                    }
                }

            }

        }


        public override void NextBar()
        {

        }

        private void Fechar()
        {

            Position[] allPositions = Positions.GetPositions();
            CloseAllPositions(allPositions, Operation.Buy);
            CloseAllPositions(allPositions, Operation.Sell);
            resilienciaNebulosaCounter = 0;
            resilienciaCompraCounter = 0;
            resilienciaVendaCounter = 0;
            openedOrders.Clear();
            jaVendeu = false;
            jaComprou = false;

        }

        /// <summary>
        /// This function will be called before removing
        /// </summary>
        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote);
            indicadorMACurto = null;
            indicadorMALongo = null;

            if (openedOrders == null) return;

            foreach (string orderId in openedOrders)
            {
                Position pos = Positions.GetPositionById(orderId);
                if (pos != null) pos.Close();

                Order ord = Orders.GetOrderById(orderId);
                if (ord != null) ord.Cancel();
            }
        }


        //public void BreakEven(Position[] All_pos)
        //{
        //    foreach (var pos in All_pos)
        //    {
        //        if (pos.Side == Operation.Buy && Instruments.Current.LastQuote.Ask > pos.OpenPrice + Break_Even * Point)
        //        {
        //            pos.SetStopLoss(pos.OpenPrice + Break_Even_val * Point);

        //        }

        //        else if (pos.Side == Operation.Sell && Instruments.Current.LastQuote.Bid < pos.OpenPrice - Break_Even * Point)
        //        {
        //            pos.SetStopLoss(pos.OpenPrice - Break_Even_val * Point);

        //        }
        //    }
        //}

        public void CloseAllPositions(Position[] Allpos, Operation side)
        {
            if (Allpos != null)
                foreach (var pos in Allpos)
                {
                    if (pos.Side == side)
                    {
                        try
                        {
                            pos.Close();
                        }
                        catch (System.ArgumentNullException e)
                        {
                            Print("Nevermind");
                        }
                    }
                }
        }
        
      

        public string Send_order(OrdersType type, Operation side, double price, double lots, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest()
            {
                Instrument = papelDestino,
                Account = Accounts.Current,
                Type = type,
                Side = side,
                Amount = lots,
                Price = price,
                MarketRange = marketRange, //market range para compra é 10 para venda é 30
                TakeProfitOffset = Take_Profit * Point,
                StopLossOffset = Stop_Loss * Point
            };
            return Orders.Send(request);
        }
        
        public string Send_order_2(OrdersType type, Operation side, double price, double lots, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest()
            {
                Instrument = papelDestino,
                Account = Accounts.Current,
                Type = type,
                Side = side,
                Amount = lots,
                Price = price,
                MarketRange = marketRange, //market range para compra é 10 para venda é 30
                TakeProfitOffset = Take_Profit_2 * Point,
                StopLossOffset = Stop_Loss_2 * Point
            };
            return Orders.Send(request);
        }
        
		public string Send_order_3(OrdersType type, Operation side, double price, double lots, int marketRange, Instrument papelDestino)
		{
			NewOrderRequest request = new NewOrderRequest()
			{
				Instrument = papelDestino,
				Account = Accounts.Current,
				Type = type,
				Side = side,
				Amount = lots,
				Price = price,
				MarketRange = marketRange, //market range para compra é 10 para venda é 30
				TakeProfitOffset = Take_Profit_3 * Point,
				StopLossOffset = Stop_Loss_3 * Point
			};
			return Orders.Send(request);
		}
            
		public string Send_order_4(OrdersType type, Operation side, double price, double lots, int marketRange, Instrument papelDestino)
		{
			NewOrderRequest request = new NewOrderRequest()
			{
				Instrument = papelDestino,
				Account = Accounts.Current,
				Type = type,
				Side = side,
				Amount = lots,
				Price = price,
				MarketRange = marketRange, //market range para compra é 10 para venda é 30
				TakeProfitOffset = Take_Profit_4 * Point,
				StopLossOffset = Stop_Loss_4 * Point
			};
			return Orders.Send(request);
		}
            
            public string Send_order_5(OrdersType type, Operation side, double price, double lots, int marketRange, Instrument papelDestino)
        {
            NewOrderRequest request = new NewOrderRequest()
            {
                Instrument = papelDestino,
                Account = Accounts.Current,
                Type = type,
                Side = side,
                Amount = lots,
                Price = price,
                MarketRange = marketRange, //market range para compra é 10 para venda é 30
                TakeProfitOffset = Take_Profit_5 * Point,
                StopLossOffset = Stop_Loss_5 * Point
            };
            return Orders.Send(request);
        }
        
    }
}