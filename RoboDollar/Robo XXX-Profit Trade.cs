﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }

//    public class RoboXXXProfitTrade : NETStrategy
//    {
//        public Account CAccount;

//        //private Boolean bolsaAberta = false;
//        private Boolean bloqueieNovasOrdens = false;
//        private Boolean aguardandoExecucaoDaOrdemDeReentrada = false;
//        private string ultimaOrdem = "";
//        private double qtdeDeContratos = 0;
//        private double valorDaPosicao = 0;

//        private double piorPerda = 0;
//        private double maiorQtdeDePosicoesAbertas = 0;
//        private double MACDHistograma;

//        private ObservableCollection<tipoOperacao> operacoes = new ObservableCollection<tipoOperacao>(); //Lista das recomendações das operações - detector de tendência      
//        private List<Position> openedPositions = new List<Position>();

//        public Instrument instrument;
//        public Instrument papelDestino;

//        public Indicator MM;
//        public Indicator MM_HIGH_LOW;
//        public Indicator MACD;
//        public Indicator VWAP;
//        public Indicator Stochastic;

//        [InputParameter("Usar MACD na Entrada?")]
//        public Boolean usarMacd = false;

//        [InputParameter("Período da Média Móvel: Sinal")]
//        public int periodoMediaMovelSinal = 9;
//        [InputParameter("Período da Média Móvel: Fast")]
//        public int periodoMediaMovelFast = 12;
//        [InputParameter("Período da Média Móvel: Slow")]
//        public int periodoMediaMovelSlow = 55;

//        [InputParameter("Período MACD: Sinal")]
//        public int periodoMACDSinal = 9;
//        [InputParameter("Período MACD: Fast")]
//        public int periodoMACDFast = 12;
//        [InputParameter("Período MACD: Slow")]
//        public int periodoMACDSlow = 26;

//        [InputParameter("Período da Média HILO")]
//        public int periodoHiLo = 5;

//        [InputParameter("MACD para reentrada")]
//        public double MACD_Reentrada = 2;

//        [InputParameter("Permitir até quantas posições abertas? ")]
//        public double maxPosAbertas = 10;

//        [InputParameter("Contratos")]
//        public int Contratos = 1;

//        [InputParameter("Hora de Início")]
//        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

//        [InputParameter("Hora de Término")]
//        public TimeSpan HoraDeTermino = new TimeSpan(17, 55, 00);

//        [InputParameter("Delta em ticks da Compra/Venda")]
//        public double delta = 1;

//        [InputParameter("Ticks de Reentrada")]
//        public int deltaDeReentrada = 10;

//        [InputParameter("LOSS Máximo Tolerado")]
//        public double lossMaximo = -200;                

//        [InputParameter("Take Profit")]
//        public double TP = 10;

//        [InputParameter("Take Profit de Saída")]
//        public double TPS = 2;

//        [InputParameter("TR Stop Loss")]
//        public double SL = 0;

//        public RoboXXXProfitTrade()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo XXX-Money-Maker ";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "14.10.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.1";
//            base.Password = "";
//            base.ProjectName = "RoboDollar Investimentos";
//            #endregion

//        }

//        /// <summary>
//        /// This function will be called after creating
//        /// </summary>
//        public override void Init()
//        {
//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;
//            Instruments.NewTrade += Instruments_NewTrade;

//            MM = Indicators.iCustom("camilo_3mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelFast, periodoMediaMovelSlow);
//            MM_HIGH_LOW = Indicators.iCustom("camilo_hilo", CurrentData, periodoHiLo);
//            MACD = Indicators.iCustom("camilo_macd", CurrentData, periodoMACDSinal, periodoMACDFast, periodoMACDSlow);

//            instrument = Instruments.Current;
//            papelDestino = instrument;

//            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);

//            CAccount = Accounts.Current;

//            Print("Início Programado:" + HoraDeInicio.Hours + "e Minutos: " + HoraDeInicio.Minutes);
//            Print("Término Programado:" + HoraDeTermino.Hours + "e Minutos: " + HoraDeTermino.Minutes);
//            Print("Tamanho do Tick: " + instrument.TickSize);
//            Print("Inicialização do Robô");
//        }

//        private void Instruments_NewTrade(Instrument instrument, Trade trade)
//        {

//        }

//        /// <summary>
//        /// Entry point. This function is called when new quote comes 
//        /// </summary>
//        public override void OnQuote()
//        {

//            base.OnQuote();

//            try
//            {
//                if (instrument.LastQuote == null) return;

//                var hora = instrument.LastQuote.Time.ToLocalTime().Hour;
//                var minutos = instrument.LastQuote.Time.ToLocalTime().Minute;
//                var segundos = instrument.LastQuote.Time.ToLocalTime().Second;

//                var currentTime = new TimeSpan(hora, minutos, segundos);

//                var totalMinStart = currentTime.Subtract(HoraDeInicio).TotalMinutes;
//                var totalMinEnd = currentTime.Subtract(HoraDeTermino).TotalMinutes;

//                if (true)
//                {
//                    var cotacaoAtual = CurrentData.GetPrice(PriceType.Close);

//                    var hiloMax = MM_HIGH_LOW.GetValue(0, 0);
//                    var hiloMin = MM_HIGH_LOW.GetValue(1, 0);
//                    var mm_sinal = MM.GetValue(0, 0);
//                    var mm_fast = MM.GetValue(1, 0);
//                    var mm_slow = MM.GetValue(2, 0);

//                    MACDHistograma = MACD.GetValue(2, 0);
//                    qtdeDeContratos = GetQtdeContratosAtivos();                    

//                    if (qtdeDeContratos == 0)
//                    {
//                        Entrada3mmHiLo(cotacaoAtual, hiloMax, hiloMin, mm_sinal, mm_fast, mm_slow, MACDHistograma);
//                    }


//                    if (Math.Abs(qtdeDeContratos) != Contratos && qtdeDeContratos != 0)
//                    {
//                        SaidaNoPrecoMedioZerado();
//                    }

//                    if (qtdeDeContratos != 0)
//                    {
//                        ChecaNecessidadeDeReentrada(cotacaoAtual, MACDHistograma, hiloMax, hiloMin);

//                        if (SL == 0) SaidaNoLossMaximoTolerado();

//                        SaidaNoLucro();
//                    }

//                    if (valorDaPosicao < piorPerda) piorPerda = valorDaPosicao;

//                    /* VERIFICANDO SE EXISTE ALGUMA POSIÇÃO SEM TP E SL, E CORRIGINDO O TRAILING STOP MANUALMENTE */
//                    Position[] positions = Positions.GetPositions();
//                    if (positions != null && !bloqueieNovasOrdens)
//                    {
//                        positions.ToList().ForEach(pos =>
//                        {
//                            if (pos != null)
//                            {
//                                if (pos.StopLossOrder == null && SL != 0)
//                                {
//                                    if (pos.Side == Operation.Buy) pos.SetStopLoss(pos.OpenPrice - SL * instrument.TickSize);
//                                    if (pos.Side == Operation.Sell) pos.SetStopLoss(pos.OpenPrice + SL * instrument.TickSize);
//                                }

//                                if (pos.TakeProfitOrder == null && TP != 0)
//                                {
//                                    if (pos.Side == Operation.Buy) pos.SetTakeProfit(pos.OpenPrice + TP * instrument.TickSize);
//                                    if (pos.Side == Operation.Sell) pos.SetTakeProfit(pos.OpenPrice - TP * instrument.TickSize);
//                                }

//                                //TRAILING STOP LOSS MANUAL
//                                if (pos.StopLossOrder != null && SL != 0)
//                                {
//                                    Order[] orders = Orders.GetOrders();
//                                    orders.ToList().ForEach(ord =>
//                                    {
//                                        if (ord.Type == OrdersType.StopLoss && ord.LinkedTo == pos.Id)
//                                        {
//                                            if (pos.Side == Operation.Buy)
//                                            {
//                                                if (ord.StopLimitPrice <= pos.CurrentPrice - SL * instrument.TickSize) ord.SetStopLoss(pos.CurrentPrice - SL * instrument.TickSize);
//                                            }
//                                            if (pos.Side == Operation.Sell)
//                                            {
//                                                if (ord.StopLimitPrice >= pos.CurrentPrice + SL * instrument.TickSize) ord.SetStopLoss(pos.OpenPrice + SL * instrument.TickSize);
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });

//                    }

//                }
//                //else
//                //{
//                //    FechamentoTotal();
//                //    Print("Bolsa Fechada !!!");
//                //}

//            }
//            catch (Exception ext)
//            {
//                Alert(ext.Message);
//            }

//        }

//        public override void NextBar()
//        {
//            Order[] orders = Orders.GetOrders();
//            Position[] positions = Positions.GetPositions();

//            if (orders != null)
//            {
//                orders.ToList().ForEach(ord =>
//                {
//                    if (ord.Type != OrdersType.StopLoss || ord.Type != OrdersType.TakeProfit)
//                    {
//                        ord.Cancel();
//                    }
//                    else
//                    {
//                        if (positions.First(p => p.Id == ord.LinkedTo) == null) ord.Cancel();
//                    }

//                });

//                Print("Cancelando Ordens não executadas a tempo"); bloqueieNovasOrdens = false;
//            }

//            Print("***************   Terminei BAR " + (CurrentData.HistoryCount - 1) + "   ***************");
//            Print("Qtde de Posições Abertas:" + qtdeDeContratos + " Preço Médio das Posições Abertas " + CalculaPrecoMedio() + " Lucro: " + CalculaLucro() + " Maior Calor: " + piorPerda);
//            Print("MACD Histograma: " + MACDHistograma);

//        }


//        /// <summary>
//        /// This function will be called before removing
//        /// </summary>
//        public override void Complete()
//        {
//            Alert("*******************      FINAL DA OPERAÇÃO DO ROBÔ      ************************* " + piorPerda);
//            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
//            instrument = null;
//            papelDestino = null;
//            MM = null;
//            MM_HIGH_LOW = null;
//            MACD = null;
//            VWAP = null;
//            Alert("Pior perda: " + piorPerda);
//            Alert("Maior Quantidade de Posições Abertas: " + maiorQtdeDePosicoesAbertas);
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************

//        public void Orders_OrderRemoved(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//            if (aguardandoExecucaoDaOrdemDeReentrada) aguardandoExecucaoDaOrdemDeReentrada = false;
//        }

//        public void Orders_OrderAdded(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = true;
//        }

//        public void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//            if (aguardandoExecucaoDaOrdemDeReentrada) aguardandoExecucaoDaOrdemDeReentrada = false;
//        }

//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************
//        //***************************************************************************************************************


//        public string Send_order(OrdersType type, Operation side, double price, double contratos, int marketRange, Instrument papelDestino)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = papelDestino;
//            request.Account = Accounts.Current;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = contratos;
//            request.Price = price;
//            request.MarketRange = marketRange;
//            request.TimeInForce = TimeInForce.Day;

//            if (TP != 0) request.TakeProfitOffset = TP;

//            Print("Emita uma ordem de " + request.Side);
//            var result = Orders.Send(request);
//            if (result == "-1") { Print("Ordem não emitida"); bloqueieNovasOrdens = false; };
//            return result;
//        }

//        public double GetQtdeContratosAtivos()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                openedPositions = Positions.GetPositions().ToList();
//                double qtdeAbsolutaDeContratos = 0;
//                if (openedPositions.Count == 0) { return 0; }
//                openedPositions.ForEach(el =>
//                {
//                    qtdeAbsolutaDeContratos += (el.Side == Operation.Buy) ? el.Amount : (-1) * el.Amount;
//                });

//                return qtdeAbsolutaDeContratos;
//            }
//            return 0;
//        }



//        public double CalculaPrecoMedio()
//        {

//            Position[] pos = Positions.GetPositions();
//            if (pos != null)
//            {
//                openedPositions = Positions.GetPositions().ToList();
//                if (openedPositions.Count == 0) return 0;
//                var somaQtdeXPrecoAbertura = openedPositions.Sum(p => p.Amount * p.OpenPrice);
//                var QtdeTotalDePapeisAbertos = openedPositions.Sum(p => p.Amount);
//                var precoMedio = somaQtdeXPrecoAbertura / QtdeTotalDePapeisAbertos;
//                return precoMedio;
//            }
//            return -1;
//        }

//        public void FechamentoParcial()
//        {
//            Position[] positions = Positions.GetPositions();
//            if (positions != null)
//            {
//                positions.ToList().ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
//            }
//        }

//        public void FechamentoTotal()
//        {
//            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");

//            Order[] orders = Orders.GetOrders();
//            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

//            Position[] positions = Positions.GetPositions();
//            if (positions != null) positions.ToList().ForEach(pos =>
//            {
//                pos.Close();
//            });

//            openedPositions.Clear();
//        }

//        public Boolean TendenciaSeMantem()
//        {
//            return operacoes.ToList().TrueForAll(p => p == operacoes.Last());
//        }

//        public Boolean EstamosLucrando()
//        {
//            var lucro = CalculaLucro();
//            return lucro > 0;
//        }

//        public double CalculaLucro()
//        {
//            Position[] positions = Positions.GetPositions();
//            if (positions != null)
//            {
//                var lucro = positions.ToList().Sum(p => p.GetProfitNet());
//                if (positions.Length == 0) return 0;
//                return lucro;
//            }
//            return 0;
//        }


//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //************************************  ENTRADAS E SAÍDAS - FUNÇÕES   ********************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************
//        //****************************************************************************************************************

//        public void Entrada3mmHiLo(double preco, double hi, double lo, double sinal, double fast, double slow, double MACD_Histo)
//        {
//            OrdersType tipoDeOrdem = OrdersType.Market;

//            if (preco >= hi && preco >= sinal && preco >= fast && preco >= slow && !bloqueieNovasOrdens)
//            {
//                if (usarMacd && MACD_Histo < 0) return;

//                Orders.GetOrders().ToList().ForEach(o => o.Cancel());
//                ultimaOrdem = Send_order(tipoDeOrdem, Operation.Buy, preco - delta * instrument.TickSize, Contratos, 150, papelDestino);
//                if (ultimaOrdem != "-1" || ultimaOrdem != "")
//                {
//                    bloqueieNovasOrdens = true;
//                }

//                Print("resultado da ordem: " + ultimaOrdem);
//            }
//            if (preco <= lo && preco <= sinal && preco <= fast && preco <= slow && !bloqueieNovasOrdens)
//            {
//                if (usarMacd && MACD_Histo > 0) return;

//                Orders.CancelAll();
//                ultimaOrdem = Send_order(tipoDeOrdem, Operation.Sell, preco + delta * instrument.TickSize, Contratos, 150, papelDestino);
//                if (ultimaOrdem != "-1" || ultimaOrdem != "") bloqueieNovasOrdens = true;
//                Print("resultado da ordem: " + ultimaOrdem);
//            }
//        }

//        public void ChecaNecessidadeDeReentrada(double cotacaoAtual, double MACD_Histo, double HiLoMax, double HiLoMin)
//        {
//            if (!EstamosLucrando() && !bloqueieNovasOrdens)
//            {
//                var precoMedio = CalculaPrecoMedio();
//                OrdersType tipoDeOrdem = OrdersType.Market;

//                if (qtdeDeContratos > 0 && Math.Abs(qtdeDeContratos) <= maxPosAbertas / 2 && (precoMedio > cotacaoAtual + deltaDeReentrada * instrument.TickSize))
//                {
//                    if (MACD_Histo < 0) return;

//                    //busque ID atual
//                    Position[] positions = Positions.GetPositions();

//                    Orders.CancelAll();
//                    if (MACD_Histo >= MACD_Reentrada) ultimaOrdem = Send_order(tipoDeOrdem, Operation.Buy, cotacaoAtual - delta * instrument.TickSize, positions[0].Amount, 150, papelDestino);

//                    if (ultimaOrdem != "-1" || ultimaOrdem != "")
//                    {
//                        bloqueieNovasOrdens = true; Print("Ordem de Reentrada na Compra Emitida - aguardando execução ou remoção da fila");
//                    }

//                }

//                if (qtdeDeContratos < 0 && Math.Abs(qtdeDeContratos) <= maxPosAbertas / 2 && (precoMedio + deltaDeReentrada * instrument.TickSize < cotacaoAtual))
//                {
//                    if (MACD_Histo > 0) return;

//                    //busque ID atual
//                    Position[] positions = Positions.GetPositions();

//                    Orders.CancelAll();
//                    if (MACD_Histo <= MACD_Reentrada) ultimaOrdem = Send_order(tipoDeOrdem, Operation.Sell, cotacaoAtual + delta * instrument.TickSize, positions[0].Amount, 150, papelDestino);

//                    if (ultimaOrdem != "-1" || ultimaOrdem != "")
//                    {
//                        bloqueieNovasOrdens = true; Print("Ordem de Reentrada na Venda Emitida - aguardando execução ou remoção da fila");
//                    }
//                }
//            }
//        }


//        public void SaidaNoLossMaximoTolerado()
//        {
//            var lucro = CalculaLucro();
//            if (lucro <= lossMaximo)
//            {
//                Print("Saída no LOSS MÁXIMO Tolerado");
//                FechamentoTotal();
//            }
//        }

//        public void SaidaNoPrecoMedioZerado()
//        {
//            double cotacaoAtual = CurrentData.GetPrice(PriceType.Close);

//            Position[] positions = Positions.GetPositions();
//            if (positions.Length != 0)
//            {
//                Position pos = positions[0];

//                if (pos.Side == Operation.Buy)
//                {
//                    if (cotacaoAtual > pos.OpenPrice + instrument.TickSize * TPS) FechamentoTotal();
//                }

//                if (pos.Side == Operation.Sell)
//                {
//                    if (cotacaoAtual < pos.OpenPrice - instrument.TickSize * TPS) FechamentoTotal();
//                }
//            }
//        }

//        public void SaidaNoLucro()
//        {
//            double cotacaoAtual = CurrentData.GetPrice(PriceType.Close);

//            Position[] positions = Positions.GetPositions();
//            if (positions.Length != 0)
//            {
//                Position pos = positions[0]; 

//                if(pos.Side == Operation.Buy)
//                {
//                    if (cotacaoAtual > pos.OpenPrice + instrument.TickSize * TP) FechamentoTotal();
//                }

//                if (pos.Side == Operation.Sell)
//                {
//                    if (cotacaoAtual < pos.OpenPrice - instrument.TickSize * TP) FechamentoTotal();
//                }
//            }
//        }
//    }

//}
