﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using PTLRuntime.NETScript;
//using System.Globalization;
//using System.Windows.Forms;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Collections.Specialized;
//using System.Threading.Tasks;
//using System.Threading;

//namespace RoboDollar
//{
//    public enum tipoOperacao { Compra, Venda, FechamentoTotal, FechamentoParcial, nula }

//    public class RoboKamikaze : NETStrategy
//    {
//        public Account CAccount;

//        private Boolean bloqueieNovasOrdens = false;

//        private List<string> openedOrders = new List<string>();

//        public Instrument instrument;

//        [InputParameter("Contratos", 1)]
//        public int Contratos = 1;

//        [InputParameter("Hora de Início", 2)]
//        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

//        [InputParameter("Hora de Término", 3)]
//        public TimeSpan HoraDeTermino = new TimeSpan(17, 55, 00);

//        [InputParameter("Take Profit", 4)]
//        public int TP = 5;

//        [InputParameter("sempre, Comprar ou Vender ?", 5, new Object[] {
//          "Comprar", Operation.Buy, "Vender", Operation.Sell })]
//        public Operation lado = Operation.Buy;

//        public RoboKamikaze()
//            : base()
//        {
//            #region Initialization
//            base.Author = "Camilo Chaves e Glaciano Nogueira";
//            base.Comments = "Robo Kamikaze ";
//            base.Company = "Dollar Investimentos";
//            base.Copyrights = "Dollar Investimentos";
//            base.DateOfCreation = "16.10.2018";
//            base.ExpirationDate = 0;
//            base.Version = "1.0.0";
//            base.Password = "";
//            base.ProjectName = "RoboDollar Investimentos";
//            #endregion

//        }

//        /// <summary>
//        /// This function will be called after creating
//        /// </summary>
//        public override void Init()
//        {
//            Orders.OrderAdded += Orders_OrderAdded;
//            Orders.OrderExecuted += Orders_OrderExecuted;
//            Orders.OrderRemoved += Orders_OrderRemoved;

//            instrument = Instruments.Current;

//            Instruments.Subscribe(instrument, QuoteTypes.Quote);

//            CAccount = Accounts.Current;

//        }


//        /// <summary>
//        /// Entry point. This function is called when new quote comes 
//        /// </summary>
//        public override void OnQuote()
//        {

//        }

//        public override void NextBar()
//        {
//            bloqueieNovasOrdens = false;

//            try
//            {
//                double cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
//                string result = "";

//                if (!bloqueieNovasOrdens)
//                {
//                    //verifica se existe alguma ordem com preço acima
//                    Order[] orders = Orders.GetOrders();
//                    //conta quantas ordens STOP
//                    int count = orders.Count(ord => ord.Type == OrdersType.Limit);
//                    Print("Quantas ordens Limit que tem na fila? " + count);

//                    if (count == 0)
//                    {
//                        var result1 = "";
//                        var result2 = "";
//                        Print("Não existe nenhuma ordem limit na fila... criando uma então");

//                        if (lado == Operation.Buy) { result1 = Send_order(OrdersType.Limit, Operation.Buy, cotacaoAtual + instrument.TickSize * TP); result2 = Send_order(OrdersType.Limit, Operation.Sell, cotacaoAtual + 2 * instrument.TickSize * TP); }
//                        if (lado == Operation.Sell) { result1 = Send_order(OrdersType.Limit, Operation.Sell, cotacaoAtual - instrument.TickSize * TP); result2 = Send_order(OrdersType.Limit, Operation.Sell, cotacaoAtual - 2 * instrument.TickSize * TP); }
//                        if (result1 != "-1" && result1 != "") bloqueieNovasOrdens = true;
//                    }
//                    else
//                    {
//                        //qual a ordem com preço mais perto da cotação atual?
//                        if (lado == Operation.Buy)
//                        {
//                            Print("Já existe ordem Limit na fila... então verifica se tem que colocar mais caso a cotação atual esteja a certa distância da ordem com preço mais próximo");
//                            List<Order> orderList = orders.Where(ord1 => ord1.Type == OrdersType.Limit).ToList();
//                            Order order = orderList.First(ord2 => ord2.Price == orderList.Min(ord3 => ord3.Price));

//                            Print("Preço da ordem mais inferior: " + order.Price);
//                            Print("cotacaoAtual + 2 * instrument.TickSize * TP < order.Price " + (cotacaoAtual + 2 * instrument.TickSize * TP < order.Price));

//                            if (cotacaoAtual + 2 * instrument.TickSize * TP < order.Price)
//                            {
//                                Print("Opa.. a cotação atual já está a uma certa distância, cabe mais ordem aí...");
//                                result = Send_order(OrdersType.Stop, Operation.Buy, cotacaoAtual + instrument.TickSize * TP);
//                                result = Send_order(OrdersType.Stop, Operation.Sell, cotacaoAtual + 2 * instrument.TickSize * TP);
//                                if (result != "-1" && result != "") bloqueieNovasOrdens = true;
//                            }
//                        }
//                        if (lado == Operation.Sell)
//                        {
//                            Print("Já existe ordem Limit na fila... então verifica se tem que colocar mais caso a cotação atual esteja a certa distância da ordem com preço mais próximo");
//                            List<Order> orderList = orders.Where(ord1 => ord1.Type == OrdersType.Limit).ToList();
//                            Order order = orderList.First(ord2 => ord2.Price == orderList.Max(ord3 => ord3.Price));

//                            Print("Preço da ordem mais superior: " + order.Price);
//                            Print("cotacaoAtual - 2 * instrument.TickSize * TP > order.Price " + (cotacaoAtual - 2 * instrument.TickSize * TP > order.Price));

//                            if (cotacaoAtual - 2 * instrument.TickSize * TP > order.Price)
//                            {
//                                Print("Opa.. a cotação atual já está a uma certa distância, cabe mais ordem aí...");
//                                result = Send_order(OrdersType.Stop, Operation.Sell, cotacaoAtual - instrument.TickSize * TP);
//                                result = Send_order(OrdersType.Stop, Operation.Buy, cotacaoAtual - 2 * instrument.TickSize * TP);
//                                if (result != "-1" && result != "") bloqueieNovasOrdens = true;
//                            }
//                        }
//                    }

//                }


//            }
//            catch (Exception ext)
//            {
//                Alert(ext.Message);
//            }
//        }


//        public override void Complete()
//        {
//            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
//            instrument = null;
//        }

//        public void Orders_OrderRemoved(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//        }

//        public void Orders_OrderAdded(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = true;
//        }

//        public void Orders_OrderExecuted(Order obj)
//        {
//            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
//            bloqueieNovasOrdens = false;
//        }


//        public string Send_order(OrdersType type, Operation side, double price)
//        {
//            NewOrderRequest request = new NewOrderRequest();
//            request.Instrument = instrument;
//            request.Account = CAccount;
//            request.Type = type;
//            request.Side = side;
//            request.Amount = Contratos;
//            request.Price = price;
//            request.TimeInForce = TimeInForce.Day;
//            //request.TakeProfitOffset = TP;
//            request.MarketRange = 150;

//            Print("Emita uma ordem de " + request.Side);
//            var result = Orders.Send(request);
//            if (result == "-1" || result == "") { Print("Ordem não emitida"); bloqueieNovasOrdens = false; };
//            return result;
//        }

//        public void FechamentoTotal()
//        {
//            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");

//            Order[] orders = Orders.GetOrders();
//            if (orders != null) orders.ToList().ForEach(ord => ord.Cancel());

//            Position[] positions = Positions.GetPositions();
//            if (positions != null) positions.ToList().ForEach(pos =>
//            {
//                pos.Close();
//            });

//        }




//    }

//}
