﻿using System;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Reflection;

namespace Everest
{

    internal class PosicaoAtiva
    {
        internal double _openPrice;
        internal double _amount;
        internal Operation _side;
        internal DateTime _openTime;
        internal double _lucro;
        internal double _pontos;
        internal string _comments;

        internal PosicaoAtiva(double openPrice, double amount, Operation side, DateTime openTime, string comments = "", double lucro = 0, double pontos = 0)
        {
            _openPrice = openPrice; _amount = amount; _side = side; _openTime = openTime; _lucro = lucro; _pontos = pontos; _comments = comments;
        }

    }

    public class LogWriter
    {
        private static string m_exePath = string.Empty;                

        public static void LogWrite(string logMessage, string arq = "log.txt")
        {
            //m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (!File.Exists(arq))
            {
                string s = ""; s = DateTime.Now.ToString("h:mm:ss tt");
                FileStream fs = File.Create(arq);
                Byte[] info = new UTF8Encoding(true).GetBytes("File Created:" + s + "\r\n");
                fs.Write(info, 0, info.Length);
                fs.Close();
            }

            try
            {
                using (StreamWriter w = File.AppendText(arq))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("{0};", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }


    public class ProjetoEverest : NETStrategy
    {
        private enum State { limbo, inicio, reentrada, saidaTendencia, avalanche };
        private enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
        public enum TipoDeSaida { DifMédias, LossMaximo };
        public enum TipoDeEntrada { MA3, DifMédias };
        public enum TipoDeRobo { tendencia, contraTendencia, tendenciaEcontra }
        public enum TipoDeOrdem { limite, mercado };
        public enum Acao
        {
            eventoOrderAdded, aguardandoOrderAdded,
            eventoOrderRemoved,
            eventoOrderExecuted, aguardandoOrderExecuted,
            eventoPositionAdded, aguardandoPositionAdded,
            eventoPositionRemoved,
            eventoPositionUpdate, aguardandoPositionUpdate,
            eventoFillAdded,
            passeLivre,
            solicitouReEntrada, reEntradaConcluida,
            solicitouEntrada, entradaConcluida,
            solicitouEntradaAvalanche, entradaAvalancheConcluida,
            solicitouSaidaLimite, saidaLimiteConcluida,

        };

        private Instrument instrument;
        private Account conta;
        private Indicator MM;
        private Indicator sniper;
        private Connection myConnection = Connection.CurrentConnection; //create connection object.    
        private Position[] positions;
        private Position ultimaPosicaoAdd = null;
        private PosicaoAtiva posicaoAtivaAvalanche;
        private PosicaoAtiva ultimaPosicaoAtivaAvalanche;
        private PosicaoAtiva posicaoAtivaMedia = new PosicaoAtiva(0, 0, Operation.Buy, DateTime.Now);
        private Order[] orders;
        private Font font;
        private Brush brush;
        private List<PosicaoAtiva> tapeReadingPosicoes = new List<PosicaoAtiva>();
        private List<PosicaoAtiva> bufferDeOrdens = new List<PosicaoAtiva>();
        private List<Acao> acao = new List<Acao>();
        private Queue<Level2> level2 = new Queue<Level2>();
        private LogWriter logWriter = new LogWriter();

        private TimeSpan horario;

        private double calor = 0;
        private double curta = 0;
        private double curtaAnterior = 0;
        private double histoResult = 0;
        private double cotacaoAtual = 0;
        private double suporte = 0;
        private double suporteAnterior = 0;
        private int hora = 0;
        private int contadorDeReEntradas = 0;
        private ushort[] ReEntradas;
        private ushort[] ValoresDeReEntradas;
        private double tickSize = 0;
        private double ultimaReentrada = 0;
        private string horaDoCalorMaximo = "";
        private State estado = State.limbo;
        private bool horarioTerminou = false;
        private bool modoAvalanche = false;
        private string papel;
        private int numeroMagico = 0;
        private double maximoDeContratosAbertos = 0;
        private string ultimoComentario = "";
        private bool liberaReEntradaAvalanche = false;
        private bool bloqueieNovaEntradaAvalanche = false;
        private bool bloqueioGeral = false;
        private bool bloqueioGeralPorOrdemRemovida = false;
        private int contadorOrdemTravada = 0;
        private bool ordemDeSaida = false;

        public ProjetoEverest()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto Everest One";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "16.01.2019";
            base.ExpirationDate = 0;
            base.Version = "1.0.5";
            base.Password = "";
            base.ProjectName = "Everest One";
            #endregion
        }

        [InputParameter("Média: Sinal", 0)]
        public int periodoMediaMovelSinal = 4;
        [InputParameter("Média: Suporte", 1)]
        public int periodoMediaMovelSuporte = 15;
        [InputParameter("Média contínua ou por fechamento de candle?", 2)]
        public bool mediaContinua = true;

        [InputParameter("Permitir até quantas posições abertas? ", 3)]
        public double maxPosAbertas = 40;

        [InputParameter("Contratos", 4)]
        public int Contratos = 5;

        [InputParameter("Início(hora)", 5)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);

        [InputParameter("Término(hora)", 6)]
        public TimeSpan HoraDeTermino = new TimeSpan(17, 50, 00);

        [InputParameter("Array Ticks de Reentradas", 7)]
        public string ticksDeReentrada = "10,10";

        [InputParameter("Array Valores de Reentradas", 8)]
        public string valoresDeReentrada = "5,5";

        [InputParameter("Distância da Média (tend)", 9)]
        public double distanciaDaMedia = 4;

        [InputParameter("Distância da Média (contra)", 10)]
        public double distanciaDaMediaContra = 7;

        [InputParameter("Take Profit (Pontos)", 11)]
        public double TP = 1;

        [InputParameter("Multiplicador da Dif.Das Médias", 12, 0, 100, 3, 0.001)]
        public double multiplicador = 10;

        [InputParameter("Resiliência de Saída", 13, 0, 100, 3, 0.001)]
        public double resilienciaDeSaida = 0;

        [InputParameter("Loss máximo em Ticks", 14)]
        public double lossMaximo = 50;

        [InputParameter("Tipo de Saída", 15, new Object[]
     {"Dif Médias", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;

        [InputParameter("Tipo de Robô", 16, new Object[]
     {"Tendência", TipoDeRobo.tendencia, "Contra Tendência",TipoDeRobo.contraTendencia, "Tendência e Contra", TipoDeRobo.tendenciaEcontra })]
        public TipoDeRobo tipoDeRobo = TipoDeRobo.tendenciaEcontra;

        [InputParameter("Market Range", 17)]
        public int marketRange = 1;

        [InputParameter("Custo de 1 contrato por ponto", 18)]
        public double custoContratoPorPonto = 50;

        [InputParameter("Tamanho do Ponto", 19)]
        public double tamanhoPonto = 1;

        [InputParameter("Lateralidade abaixo de: (suporte-sinal) ", 20)]
        public double lateralidade = 0.2;

        [InputParameter("Plotar Level2? ", 21)]
        public bool plotarLevel2 = true;

        [InputParameter("Qtde Level2 na tela  ", 22)]
        public int qtdeLevel2 = 30;

        [InputParameter("log:", 23)]
        public string _log = "log.txt";

        [InputParameter("log tela:", 24)]
        public string _logPosicoes = "logPosicoes.txt";

        [InputParameter("TP Avalanche", 25)]
        public double TPavalanche = 2;

        [InputParameter("Avalanche após N contratos", 26)]
        public double avalanche = 5;

        [InputParameter("ReEntrada % Avalanche", 27)]
        public double contratosAvalanche = 10;

        [InputParameter("Lote Mínimo Avalanche", 28)]
        public double minimumLot = 5;

        [InputParameter("ReEntrada Avalanche em Ticks", 29)]
        public double reEntradaAvalanche = 4;

        [InputParameter("Lucro de saída avalanche R$", 30)]
        public double lucroDeSaidaAvalanche = 250;

        [InputParameter("Pontos para Inicio Avalanche", 31)]
        public double ticksAvalanche = 5;


        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            try
            {
                font = new Font("Tahoma", 10);
                brush = new SolidBrush(Color.White);
                instrument = Instruments.Current;
                Print("Nome do Instrumento ativo: " + instrument.Name);
                papel = instrument.BaseName;
                conta = Accounts.Current;
                tickSize = instrument.TickSize;
                Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote | QuoteTypes.Level2);
                Funcoes.InfoConexao(myConnection, this);
                Funcoes.AccountInformation(conta, this);

                ReEntradas = ticksDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                //ReEntradas[0] = reEntrada0; ReEntradas[1] = reEntrada1; ReEntradas[2] = reEntrada2; ReEntradas[3] = reEntrada3; ReEntradas[4] = reEntrada4; ReEntradas[5] = reEntrada5; ReEntradas[6] = reEntrada6;
                ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();

                if (ReEntradas.Length != ValoresDeReEntradas.Length)
                {
                    throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
                }

                Print("Papel: " + instrument.Name);
                Print("Tamanho do Point: " + Point);
                Print("Tamanho do tick: " + instrument.TickSize);


                //INDICADORES
                Print("Inicializando indicador Camilo 2MM");
                MM = Indicators.iCustom("camilo_2mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelSuporte);
                Print("Inicializando indicador Sniper Elite");
                sniper = Indicators.iCustom("Camilo_SniperElite", CurrentData, periodoMediaMovelSinal, resilienciaDeSaida, multiplicador);


                //EVENTOS            
                Positions.PositionAdded += Positions_PositionAdded;
                Positions.PositionRemoved += Positions_PositionRemoved;
                Instruments.NewTrade += Instruments_NewTrade;
                Instruments.NewLevel2 += Instruments_NewLevel2;
                Orders.OrderAdded += Orders_OrderAdded;
                Orders.OrderExecuted += Orders_OrderExecuted;
                Orders.OrderRemoved += Orders_OrderRemoved;


                Print("Fim da Inicialização do Robô");

                if (myConnection.Status == ConnectionStatus.Disconnected)
                {
                    Print("RODANDO ROBÔ NO SIMULADOR. NÚMERO MÁGICO É 0");
                    numeroMagico = 0; // SE ESTIVER NO SIMULADOR O NUMERO MÁGICO É ZERO
                }
                else
                {
                    Print("RODANDO ROBÔ NA CONTA DEMO OU REAL. Número mágico deste robô é " + numeroMagico);
                }


                //VERIFICANDO ESTADO ANTERIOR                  
                LogWriter.LogWrite("Inicio ");
                tapeReadingPosicoes.Clear();

                RefreshIndicators();

                if (estado == State.limbo) acao.Add(Acao.passeLivre);                

            }
            catch (Exception exc)
            {
                Print("Erro na inicialização do Robô... " + exc.Message);
                Print("Finalizando Robô");
                TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
                foreach (var robo in robos)
                {
                    //robo.Stop();
                    Print("Nome do robô:" + robo.Name);
                }
            }
        }

        public override void OnQuote()
        {
            RefreshPositionsOrders();

            if (myConnection.Status == ConnectionStatus.Disconnected)
            {
                //ou seja, está no simulador onde não tem OnTrades...                
                Instruments_NewTrade(instrument, instrument.GetLastTrade());
            }

        }

        public override void NextBar()
        {
            if (estado == State.limbo) acao.Add(Acao.passeLivre);
            if (bloqueioGeralPorOrdemRemovida) { bloqueioGeral = false; bloqueioGeralPorOrdemRemovida = false; } 
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade | QuoteTypes.Level2);
            Positions.PositionAdded -= Positions_PositionAdded;
            Positions.PositionRemoved -= Positions_PositionRemoved;
            Instruments.NewTrade -= Instruments_NewTrade;
            Instruments.NewLevel2 -= Instruments_NewLevel2;
            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;
        }

        private void Instruments_NewLevel2(Instrument inst, Level2 quote)
        {
            if (inst.BaseName == papel)
            {
                if (level2.Count < qtdeLevel2)
                {
                    level2.Enqueue(quote);
                }
                else
                {
                    level2.Dequeue();
                    level2.Enqueue(quote);
                }
            }
        }

        private string Log(string log)
        {
            return " Papel:" + papel + " Mercado R$" + cotacaoAtual + " --> " + log;
        }


        private void LogDoTotal(TimeSpan tempo)
        {
            RefreshPositionsOrders();

            double qtdeTotal = 0;
            double qtdeMedia = 0;
            double precoMedioProtrader = 0;
            double precoMedioTapeRead = 0;
            double precoAtual = 0;
            double lucroProtrader = 0;
            double lucroTapeRead = 0;
            string tipo = "VAZIO";
            string tipoTapeRead = "VAZIO";


            if (positions.Length > 0)
            {
                qtdeTotal = positions[0].Amount;
                tipo = (positions[0].Side == Operation.Buy) ? "BUY" : "SELL";
                precoMedioProtrader = positions[0].OpenPrice;
                precoAtual = positions[0].CurrentPrice;
                lucroProtrader = positions[0].GetProfitGross();
                lucroTapeRead = posicaoAtivaMedia._lucro;
            }

            if (tapeReadingPosicoes.Count > 0)
            {
                tipoTapeRead = (tapeReadingPosicoes.First()._side == Operation.Buy) ? "BUY" : "SELL";
                qtdeMedia = posicaoAtivaMedia._amount;
                precoMedioTapeRead = posicaoAtivaMedia._openPrice;

            }

            string log = tempo + " TOTALIZANDO : TapeRead=> Qtty:" + qtdeMedia + " Pmédio:" + precoMedioTapeRead + " _side:" + tipoTapeRead + " lucro:" + lucroTapeRead + "\n  Protrader-> qtde:" + qtdeTotal + " OpenPrice:" + precoMedioProtrader + " Current:" + precoAtual + " Tipo:" + tipo + " lucro:" + lucroProtrader + " \n Qtde Ordens ativas:" + orders.Length;
            LogWriter.LogWrite(Log(log + " Estado:" + estado.ToString() + "  ReEntrada:" + contadorDeReEntradas), _log);            

        }

        private void Orders_OrderAdded(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == papel)
                {
                    bloqueioGeral = false;
                    RefreshPositionsOrders();
                    acao.Add(Acao.eventoOrderAdded);
                    string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderAdded: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + estado;
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                }
            }
            catch (Exception exc)
            {
                Alert("Erro OrderAdded : " + exc.Message);
                bloqueioGeral = false;
            }
        }

        private void Orders_OrderRemoved(Order obj)
        {
            if (obj.Instrument.BaseName == papel)
            {
                bloqueioGeral = true;
                bloqueioGeralPorOrdemRemovida = true;
                RefreshPositionsOrders();
                acao.Add(Acao.eventoOrderRemoved);
                string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderRemoved: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + estado;
                Print(log); LogWriter.LogWrite(Log(log), _log);
                posicaoAtivaAvalanche = null;
                ultimaPosicaoAtivaAvalanche = null;

                estado = State.limbo;
            }
        }

        private void Orders_OrderExecuted(Order obj)
        {
            try
            {
                if (obj.Instrument.BaseName == papel)
                {
                    bloqueioGeral = true;
                    RefreshPositionsOrders();
                    acao.Add(Acao.eventoOrderExecuted);
                    string log = obj.Time.ToLocalTime().TimeOfDay + " EVENTO OrderExecuted: ID:" + obj.Id + " tipo:" + obj.Side + " " + obj.Comment + " Qtde:" + obj.Amount + " preço:" + obj.Price + " hora:" + obj.Time.ToLocalTime().ToShortTimeString() + " Status:" + obj.Status + " Estado: " + estado;
                    Print(log); LogWriter.LogWrite(Log(log), _log);

                    //NOVA TIMELINE
                    if (obj.Status == OrderStatus.Filled || obj.Status == OrderStatus.PartiallyFilled)
                    {
                        double amount = obj.Amount;
                        double preco = obj.Price;

                        bool inversaoDetectada = false;
                        if (positions.Length > 0)
                        {
                            Operation tipoBase = positions[0].Side;
                            if (obj.Side != tipoBase) { amount = -amount; inversaoDetectada = true; }
                        }

                        RefreshPositionsOrders();

                        if (inversaoDetectada)
                        {
                            if (positions.Length > 0)
                            {
                                if (modoAvalanche) { ultimoComentario = "saida avalanche"; } else { ultimoComentario = "saida limite"; }
                                bufferDeOrdens.Add(new PosicaoAtiva(preco, amount, obj.Side, obj.Time, ultimoComentario));
                                ultimaPosicaoAtivaAvalanche = new PosicaoAtiva(preco, amount, obj.Side, obj.Time, ultimoComentario);
                                posicaoAtivaAvalanche = null;
                                bloqueieNovaEntradaAvalanche = false;
                            }
                        }
                        else
                        {
                            ultimoComentario = obj.Comment;
                            if (modoAvalanche) ultimoComentario = "entrada avalanche";
                            posicaoAtivaAvalanche = new PosicaoAtiva(preco, amount, obj.Side, DateTime.Now, ultimoComentario);
                            bufferDeOrdens.Add(new PosicaoAtiva(preco, amount, obj.Side, obj.Time, ultimoComentario, 0, 0));
                            PosicaoAtiva pos = bufferDeOrdens.Last();
                            bloqueieNovaEntradaAvalanche = false;
                            ultimaPosicaoAtivaAvalanche = posicaoAtivaAvalanche;
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                Alert("Erro OrderExecuted : " + exc.Message);
                bloqueioGeral = false;
            }
        }

        private void Positions_PositionRemoved(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == papel)
                {
                    bloqueioGeral = false;
                    RefreshPositionsOrders();
                    acao.Add(Acao.eventoPositionRemoved);
                    string log = obj.CloseTime.ToLocalTime().TimeOfDay + " EVENTO PositionRemoved: ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + estado;
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                    ultimaPosicaoAdd = null;
                    LogDoTotal(obj.CloseTime.ToLocalTime().TimeOfDay);
                    modoAvalanche = false;
                    posicaoAtivaAvalanche = null;
                    bufferDeOrdens.Clear();
                    acao.Clear();
                    estado = State.limbo;
                    acao.Add(Acao.eventoPositionRemoved);
                    bloqueieNovaEntradaAvalanche = false;
                    return;
                }
            }
            catch (Exception exc)
            {
                Alert("Erro PositionRemoved : " + exc.Message);
                bloqueioGeral = false;
            }
        }

        private void Positions_PositionAdded(Position obj)
        {
            try
            {
                if (obj.Instrument.BaseName == papel)
                {
                    bloqueioGeral = false;
                    string log = "";
                    RefreshPositionsOrders();
                    if (ultimaPosicaoAdd == null)
                    {
                        acao.Add(Acao.eventoPositionAdded);
                        log = obj.OpenTime.ToLocalTime().TimeOfDay + " EVENTO PositionAdded: ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + estado;
                        LogWriter.LogWrite(Log(log), _log);
                        tapeReadingPosicoes.Clear();
                        Position pos = positions[0];
                        tapeReadingPosicoes.Add(new PosicaoAtiva(obj.OpenPrice, obj.Amount, obj.Side, obj.OpenTime, "entrada mercado"));
                        if (!modoAvalanche) { CriaParDeSaida(obj); acao.Add(Acao.aguardandoOrderAdded); }
                    }
                    else
                    {
                        acao.Add(Acao.eventoPositionUpdate);
                        log = obj.OpenTime.ToLocalTime().TimeOfDay + " EVENTO PositionAdded: (POSITION UPDATE) ID:" + obj.Id + " tipo:" + obj.Side + " qtde:" + obj.Amount + " OrderId:" + obj.OpenOrderId + " Papel:" + obj.Instrument.BaseName + " Estado: " + estado;
                        LogWriter.LogWrite(Log(log), _log);

                        bufferDeOrdens.ForEach(buffer => tapeReadingPosicoes.Add(buffer));

                        if (modoAvalanche) { acao.Add(Acao.entradaAvalancheConcluida); }

                        if (orders.Length == 0 && positions.Length > 0 && !modoAvalanche) { CriaParDeSaida(positions[0]); acao.Add(Acao.aguardandoOrderAdded); estado = State.limbo; }

                    }


                    ultimaPosicaoAdd = obj;
                    LogDoTotal(obj.OpenTime.ToLocalTime().TimeOfDay);
                    ultimaReentrada = obj.OpenPrice;
                    bufferDeOrdens.Clear();
                    bloqueieNovaEntradaAvalanche = false;
                    return;
                }
            }
            catch (Exception exc)
            {
                Alert("Erro PositionAdded : " + exc.Message);
                bloqueioGeral = false;
            }
        }

        private void Instruments_NewTrade(Instrument inst, Trade trade)
        {
            if (inst.BaseName == papel)
            {
                string log = "********************   NewTrade   ********************";
                Print(log);
                if (!RefreshPositionsOrders()) return; //atualiza as ordens e posições ativas
                if (!RefreshIndicators()) return; //lê os indicadores
                if (!CheckMarketStatus(trade)) return; //checa se bolsa está aberta e se está fechada fecha as posições
                if (!CalculaCalorMaximo()) return;
                if (curta == 0 || curtaAnterior == 0) { Print("Aguardando indicadores!"); return; }

                SaidaPorLossMaximo();

                if (bloqueioGeralPorOrdemRemovida) { Print("ROBÔ EM LOCKDOWN TEMPORÁRIO POR ORDEM REMOVIDA - Saída no próximo candle"); return; }

                if (bloqueioGeral)
                {
                    Print("Robô está em LOCKDOWN!");
                    return;
                }

                DetectaAvalanche();

                try
                {
                    switch (estado)
                    {
                        case State.limbo:
                            {
                                log = horario + "  ********************   Estado Limbo *****************";
                                RefreshPositionsOrders();
                                Print(log);
                                LogWriter.LogWrite(Log(log), _log);
                                Print("Hora: " + horario + "  Limbo: Papel:" + instrument.BaseName + "\t Numero mágico " + numeroMagico + "\t Lucro de hj: " + conta.TodayNet + "\t Qtde de Trades: " + conta.TodayTrades + "\t hora último trade:" + horario);
                                PrintaPosicoesOrdensAtivas();
                                bool passeLivreNoLimbo = false;

                                acao.ForEach(x => { Print(x); LogWriter.LogWrite(Log(x.ToString()), _log); });

                                if (acao.Count > 0)
                                {
                                    if (acao.Exists(x => x == Acao.passeLivre) ||
                                        acao.Exists(x => x == Acao.eventoOrderRemoved) ||
                                        acao.Exists(x => x == Acao.eventoPositionRemoved))
                                    {
                                        passeLivreNoLimbo = true;
                                        log = "Estado Limbo: Saída por PasseLivre, Ordem Removida ou Posição Removida";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        goto PasseLivre;
                                    }

                                    if ((acao.Exists(x => x == Acao.aguardandoPositionAdded) && acao.Exists(x => x == Acao.eventoPositionAdded)) ||
                                        (acao.Exists(x => x == Acao.aguardandoOrderAdded) && acao.Exists(x => x == Acao.eventoOrderAdded))
                                        )
                                    {
                                        passeLivreNoLimbo = true;
                                        log = "Estado Limbo: Saída por Posição Adicionada ou Ordem Adicionada";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        goto PasseLivre;
                                    }

                                    if ((acao.Exists(x => x == Acao.aguardandoPositionUpdate) && acao.Exists(x => x == Acao.eventoPositionUpdate)) ||
                                      (acao.Exists(x => x == Acao.solicitouEntradaAvalanche) && acao.Exists(x => x == Acao.entradaAvalancheConcluida))
                                      )
                                    {
                                        passeLivreNoLimbo = true;
                                        log = "Estado Limbo: Saída por Posição Atualizada ou entrada Avalanche concluída ";
                                        bloqueieNovaEntradaAvalanche = false;
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        goto PasseLivre;
                                    }


                                }

                            PasseLivre:
                                if (passeLivreNoLimbo)
                                {
                                    acao.Clear();
                                    Juiz();
                                }
                                break;
                            }
                        case State.inicio:
                            {
                                log = "********************   Estado Início  ********************";
                                Print(log); LogWriter.LogWrite(Log(log), _log);

                                if (horarioTerminou) { Print("Horário de Término atingido, não entra mais..."); return; }

                                ultimoComentario = "";
                                posicaoAtivaAvalanche = null;
                                modoAvalanche = false;
                                tapeReadingPosicoes.Clear();
                                contadorDeReEntradas = 0;

                            LoopInicio:
                                RefreshPositionsOrders();
                                if (orders.Length > 0)
                                {
                                    log = "Estado Início: Tem ordem ativa ainda ... verificando o status dela .. " + orders[0].Status;
                                    Print(log); LogWriter.LogWrite(Log(log), _log);

                                    if (orders[0].Status == OrderStatus.PendingCancel || orders[0].Status == OrderStatus.PendingExecution)
                                    {
                                        Print("Devemos aguardar a corretora definir o que fazer com a ordem.");
                                        goto LoopInicio;
                                    }
                                }

                                if (positions.Length > 0)
                                {
                                    log = "Estado início: Tem posição ativa ainda. Fecha tudo e continua...";
                                    Print(log); LogWriter.LogWrite(Log(log), _log);
                                    FechamentoTotal();
                                }

                                acao.Clear();

                                if (Math.Abs(suporte - curta) >= lateralidade)
                                {
                                    Print("Hora: " + horario + "Estado Inicio: Preço atual:" + cotacaoAtual + " Média curta:" + curta + " Ticks da média:" + (Math.Abs(cotacaoAtual - curta) / tickSize));
                                    Print("Estado Inicio: Modo de Entrada: Sinal Atual - Anterior = " + (curta - curtaAnterior));
                                    Print("Estado Inicio: Indicador de Lateralidade |sinal-suporte| = " + Math.Abs(suporte - curta) + " Lateralidade:" + lateralidade);
                                    if ((Math.Abs(cotacaoAtual - curta) / tickSize) <= distanciaDaMedia && (tipoDeRobo == TipoDeRobo.tendencia || tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                                    {
                                        double diff = 0;
                                        if (mediaContinua) diff = curta - curtaAnterior;
                                        if (!mediaContinua) diff = histoResult;

                                        if (EntradaDiffMedias(cotacaoAtual, diff) != "-1")
                                        {
                                            log = horario + " Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo";
                                            Print(log); LogWriter.LogWrite(Log(log), _log);
                                            acao.Add(Acao.aguardandoPositionAdded);
                                            acao.Add(Acao.solicitouEntrada);
                                            estado = State.limbo;
                                            return;
                                        }
                                        else
                                        {
                                            log = horario + " Estado Inicio: Ordem TENDÊNCIA recusada"; Print(log); LogWriter.LogWrite(Log(log), _log);
                                            estado = State.inicio;
                                            return;
                                        }

                                    }
                                    else if ((Math.Abs(cotacaoAtual - curta) / tickSize) >= distanciaDaMediaContra && (tipoDeRobo == TipoDeRobo.contraTendencia || tipoDeRobo == TipoDeRobo.tendenciaEcontra))
                                    {
                                        if (EntradaContra(cotacaoAtual) != "-1")
                                        {
                                            log = horario + " Estado Inicio: Ordem CONTRA-TENDÊNCIA foi enviada ! Aguardando sua resolução no limbo";
                                            Print(log); LogWriter.LogWrite(Log(log), _log);
                                            acao.Add(Acao.aguardandoPositionAdded);
                                            acao.Add(Acao.solicitouEntrada);
                                            estado = State.limbo;
                                            return;
                                        }
                                        else
                                        {
                                            log = horario + "Estado Inicio: Ordem CONTRA-TENDÊNCIA recusada"; Print(log); LogWriter.LogWrite(Log(log), _log);
                                            estado = State.inicio;
                                            return;
                                        }
                                    }
                                    else Print("Estado Inicio: Aguardando condições de entrada");
                                }
                                else Print("Estado Inicio: Mercado está lateral! |sinal-suporte| = " + Math.Abs(suporte - curta) + " Lateralidade:" + lateralidade);

                                break;
                            }

                        case State.reentrada:
                            {
                                Print("********************   Estado ReEntrada   ***********************");
                                RefreshPositionsOrders();

                                if (positions.Length == 0)
                                {
                                    log = "Estado ReEntrada: Não existe mais posição. Migrando para inicio";
                                    Print(log); LogWriter.LogWrite(Log(log), _log);
                                    LimparTodasAsOrdens();
                                    estado = State.inicio;
                                    acao.Clear();
                                    modoAvalanche = false;
                                    posicaoAtivaAvalanche = null;
                                    return;
                                }

                                if (orders.Length == 0)
                                {
                                    if (positions.Length > 0)
                                    {
                                        log = "Estado ReEntrada: Ordem de saída zero, e tem posição! Criando Par de Saída";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        CriaParDeSaida();
                                        acao.Clear();
                                        estado = State.limbo;
                                        acao.Add(Acao.aguardandoOrderAdded);
                                        return;
                                    }
                                    else
                                    {
                                        log = "Estado ReEntrada: Sem ordem e sem posição? Migra pro inicio...";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        estado = State.inicio;
                                        acao.Clear();
                                        return;
                                    }
                                }

                                if (positions.Length > 0) SaidaPorLossMaximo();

                                if (contadorDeReEntradas > ValoresDeReEntradas.Length - 1)
                                {
                                    Print("Estado ReEntrada: Máximo número de ReEntradas atingido!");
                                    return;
                                }

                                RefreshPositionsOrders();
                                if (positions.Length != 0)
                                {
                                    Position pos = positions[0];

                                    Order ord = null;

                                    if (orders.Length > 0) ord = orders[0];

                                    if (ord != null) EqualizaOrdemPosicao(pos, ord);
                                    acao.Clear();

                                    var deltaMoeda = tickSize * ReEntradas[contadorDeReEntradas];
                                    var liberaReEntrada = false;
                                    Print("Hora:" + horario + "Estado ReEntrada: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize));

                                    if (tipoDeSaida == TipoDeSaida.DifMédias)
                                    {
                                        if (mediaContinua) Print("Estado ReEntrada: (média - média Anterior) x multiplicador = " + (curta - curtaAnterior) * multiplicador);
                                        if (!mediaContinua) Print("Estado ReEntrada: histograma = " + histoResult);
                                    }
                                    Print("Estado ReEntrada: Entrada atual = " + contadorDeReEntradas);

                                    if (pos.Side == Operation.Buy && pos.CurrentPrice <= ultimaReentrada - deltaMoeda)
                                    {
                                        Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                        liberaReEntrada = true;
                                    }

                                    if (pos.Side == Operation.Sell && pos.CurrentPrice >= ultimaReentrada + deltaMoeda)
                                    {
                                        Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                        liberaReEntrada = true;
                                    }

                                    if (liberaReEntrada)
                                    {
                                        Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                                        if (positions[0].Amount <= maxPosAbertas / 2)
                                        {
                                            var result = ReEntrada(cotacaoAtual, positions[0]);
                                            if (result != "-1")
                                            {
                                                log = horario + " Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo.";
                                                Print(log); LogWriter.LogWrite(Log(log), _log);
                                                acao.Add(Acao.aguardandoPositionAdded);
                                                acao.Add(Acao.solicitouReEntrada);
                                                contadorDeReEntradas++;
                                                estado = State.limbo;
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            Print("Estado ReEntrada: Máximo de Contratos atingidos!");
                                        }
                                    }

                                    double diff = 0;
                                    if (mediaContinua) diff = (curta - curtaAnterior) * multiplicador;
                                    if (!mediaContinua) diff = histoResult;

                                    if (tipoDeSaida == TipoDeSaida.DifMédias && diff >= resilienciaDeSaida && pos.Side == Operation.Sell)
                                    {
                                        log = horario + "Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        FechamentoTotal();
                                        acao.Clear();
                                        estado = State.limbo;
                                        acao.Add(Acao.passeLivre);
                                        return;
                                    }

                                    if (tipoDeSaida == TipoDeSaida.DifMédias && diff <= -resilienciaDeSaida && pos.Side == Operation.Buy)
                                    {
                                        log = horario + " Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        FechamentoTotal();
                                        acao.Clear();
                                        estado = State.limbo;
                                        acao.Add(Acao.passeLivre);
                                        return;
                                    }

                                    PrintaPosicoesOrdensAtivas();
                                }
                                break;
                            }

                        case State.avalanche:
                            {
                                try
                                {

                                    log = "********************   Estado AVALANCHE ***********************";
                                    Print(log); LogWriter.LogWrite(Log(log), _log);
                                    liberaReEntradaAvalanche = false;

                                    if (ultimaPosicaoAtivaAvalanche == null)
                                    {
                                        tapeReadingPosicoes.Clear();
                                    }

                                    RefreshPositionsOrders();

                                    if (positions.Length == 0)
                                    {
                                        log = "Estado AVALANCHE: Não existe mais posição. Migrando para inicio";
                                        Print(log); LogWriter.LogWrite(Log(log), _log);
                                        estado = State.inicio;
                                        LimparTodasAsOrdens();
                                        modoAvalanche = false;
                                        return;
                                    }

                                    if (positions.Length > 0)
                                    {
                                        Print("Estado AVALANCHE: Lucro Atual: " + posicaoAtivaMedia._lucro);
                                        if (posicaoAtivaMedia._lucro >= lucroDeSaidaAvalanche)
                                        {
                                            log = "Estado AVALANCHE: SAÍDA POR LUCRO ATINGIDO. Lucro: " + posicaoAtivaMedia._lucro;
                                            Print(log); LogWriter.LogWrite(Log(log), _log);

                                            FechamentoTotal();
                                            bufferDeOrdens.Clear();
                                            modoAvalanche = false;
                                            estado = State.inicio;
                                            acao.Clear();
                                            return;
                                        }

                                        double entradaAvalanche = Math.Floor(contratosAvalanche * positions[0].Amount / 100);
                                        if (entradaAvalanche < minimumLot) entradaAvalanche = minimumLot;

                                        Operation tipoReferencia = positions[0].Side;
                                        Operation estadoDoMercado = ((curta - curtaAnterior) * multiplicador > 0) ? Operation.Buy : Operation.Sell;

                                        Print("Estado Avalanche: |media-mediaAnterior|*multiplicador  = " + Math.Abs(curta - curtaAnterior) * multiplicador + " Resiliência de Entrada:" + resilienciaDeSaida + " Preço Última Posição:" + ultimaPosicaoAtivaAvalanche._openPrice + " Tipo:" + ultimaPosicaoAtivaAvalanche._side);

                                        if (orders.Length != 0 && posicaoAtivaAvalanche != null && positions.Length > 0)
                                        {
                                            log = "Estado Avalanche: Ordem != 0 , Posição Ativa Avalanche != null, Posições > 0";
                                            Print(log);
                                            EqualizaOrdemPosicaoAvalance(posicaoAtivaAvalanche, orders[0]);
                                            acao.Clear();
                                        }

                                        if (posicaoAtivaAvalanche == null && ultimaPosicaoAtivaAvalanche != null)
                                        {
                                            log = "Estado Avalanche: Ordem == 0 , Posição Ativa Avalanche == null";
                                            Print(log);
                                            double pontoCritico = Math.Abs(Math.Round(((cotacaoAtual - ultimaPosicaoAtivaAvalanche._openPrice) / tamanhoPonto), 2));
                                            log = "Estado Avalanche: Posição Ativa é NULA. Aguardando Ponto Crítico. PontoCrítico: " + pontoCritico + " Limite:" + reEntradaAvalanche;
                                            Print(log); LogWriter.LogWrite(Log(log), _log);

                                            if (pontoCritico >= Math.Abs(reEntradaAvalanche))
                                            {
                                                liberaReEntradaAvalanche = true;
                                                goto LoopReEntradaAvalanche;
                                            }
                                            else
                                            {
                                                return;
                                            }
                                        }

                                        if (orders.Length == 0 && posicaoAtivaAvalanche != null)
                                        {
                                            log = "Estado Avalanche: Ordem == 0 , Posição Ativa Avalanche != null";
                                            Print(log);
                                            SaidaAvalanche(posicaoAtivaAvalanche);
                                            acao.Clear();
                                            estado = State.limbo;
                                            acao.Add(Acao.aguardandoOrderAdded);
                                            return;
                                        }

                                        if (posicaoAtivaAvalanche != null)
                                        {
                                            log = "Estado Avalanche: Posição Ativa Avalanche != null";
                                            Print(log);

                                            if (orders.Length > 0)
                                                if (EqualizaOrdemPosicaoAvalance(posicaoAtivaAvalanche, orders[0]))
                                                    Print("Estado Avalanche: Equalização BEM SUCEDIDA!");

                                            double pontoCritico = Math.Abs(Math.Round(((cotacaoAtual - posicaoAtivaAvalanche._openPrice) / tamanhoPonto), 2));

                                            log = "Estado Avalanche: Analisando ponto crítico... Ponto crítico atual: " + pontoCritico + "   Limite: " + reEntradaAvalanche + " pontos";
                                            Print(log);

                                            if (pontoCritico >= Math.Abs(reEntradaAvalanche))
                                            {
                                                liberaReEntradaAvalanche = true;
                                                Print("Entrada Avalanche: Ponto Crítico Atingido! Verificando tendência do mercado ..");
                                            }
                                        }

                                        if (orders.Length > 1 && posicaoAtivaAvalanche != null)
                                        {
                                            Print("Estado Avalanche: Existe mais do que 1 ordem. Limpando uma delas");
                                            LimparTodasAsOrdens();
                                            acao.Clear();
                                            acao.Add(Acao.passeLivre);
                                            estado = State.limbo;
                                            return;
                                        }

                                    LoopReEntradaAvalanche:

                                        if (tipoReferencia == estadoDoMercado && liberaReEntradaAvalanche)
                                        {
                                            if (bloqueieNovaEntradaAvalanche)
                                            {
                                                Print("Estado Avalanche: Aguardando desbloqueio avalanche..."); return;
                                            }
                                            if ((Math.Abs(curta - curtaAnterior) * multiplicador > resilienciaDeSaida))
                                            {
                                                acao.Clear();
                                                if (Send_order(OrdersType.Market, tipoReferencia, cotacaoAtual, entradaAvalanche, 0, "entrada avalanche") != "-1")
                                                {
                                                    log = "Estado AVALANCHE: Entrada Avalanche enviada!";
                                                    Print(log); LogWriter.LogWrite(Log(log), _log);
                                                    bloqueieNovaEntradaAvalanche = true;
                                                    estado = State.limbo;
                                                    acao.Add(Acao.aguardandoPositionUpdate);
                                                    acao.Add(Acao.solicitouEntradaAvalanche);
                                                    return;
                                                }
                                                else
                                                {
                                                    log = "Estado AVALANCHE: Ordem de ReEntrada Avalanche negada!";
                                                    Print(log); LogWriter.LogWrite(Log(log), _log);
                                                    estado = State.avalanche;
                                                    acao.Clear();
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                Print("Estado AVALANCHE: Aguardando Sensibilidade da Média ficar maior que " + resilienciaDeSaida);
                                            }
                                        }
                                        else
                                        {
                                            Print("Estado AVALANCHE: Tendência desfavorável ! bloqueioAvalanche: " + bloqueieNovaEntradaAvalanche + "   Tendência:" + estadoDoMercado);
                                        }
                                    }

                                    PrintaPosicoesOrdensAtivas();
                                }
                                catch (Exception exc)
                                {
                                    log = "Erro no estado avalanche! " + exc.Message;
                                    LogWriter.LogWrite(Log(log), _log);
                                    Alert(log);
                                    acao.Clear();
                                    estado = State.limbo;
                                    LimparTodasAsOrdens();
                                    acao.Add(Acao.passeLivre);
                                    modoAvalanche = false;
                                }

                                break;
                            }

                        default:
                            {
                                Print("Estado Juiz: Nenhum caso foi analisado pelo Juiz. Voltando para o limbo");
                                estado = State.limbo;
                                break;
                            }
                    }

                }
                catch (Exception exc)
                {
                    Print("NewTrade:  Erro setor switch . Mensagem -> " + exc.Message);
                    Print("NewTrade: Erro:  InnerMessage -> " + exc.InnerException.Message);
                    return;
                }
            }
        }

        public bool RefreshPositionsOrders()
        {
            try
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel)?.ToArray();
                orders = Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == papel)?.ToArray();

                if (tapeReadingPosicoes.Count > 0)
                {
                    double lucro = tapeReadingPosicoes.Sum(x => x._lucro);

                    Operation tipoBase = tapeReadingPosicoes.First()._side;

                    tapeReadingPosicoes.ForEach(posicao =>
                    {
                        posicao._pontos = (posicao._side == Operation.Buy) ? Math.Round(((cotacaoAtual - posicao._openPrice) / tamanhoPonto), 2) : -Math.Round(((cotacaoAtual - posicao._openPrice) / tamanhoPonto), 2);
                        posicao._lucro = posicao._pontos * custoContratoPorPonto * Math.Abs(posicao._amount);
                    });


                    double _precoMedio = Math.Round(tapeReadingPosicoes.Sum(x => x._openPrice * x._amount) / tapeReadingPosicoes.Sum(x => x._amount), 2);
                    double _lucroMedio = tapeReadingPosicoes.Sum(x => x._lucro);
                    double _amountMedio = tapeReadingPosicoes.Sum(x => x._amount);

                    posicaoAtivaMedia._amount = _amountMedio;
                    posicaoAtivaMedia._lucro = _lucroMedio;
                    posicaoAtivaMedia._openPrice = _precoMedio;
                    posicaoAtivaMedia._side = tipoBase;

                }
                else
                {
                    posicaoAtivaMedia._amount = 0;
                    posicaoAtivaMedia._lucro = 0;
                    posicaoAtivaMedia._openPrice = 0;
                    posicaoAtivaMedia._comments = "";

                }

                if (positions.Length > 0 && tapeReadingPosicoes.Count == 0 && posicaoAtivaAvalanche == null && ultimaPosicaoAtivaAvalanche==null)
                {                 
                    LogWriter.LogWrite("Existe posição pré-existente." + "\t Total Posição: " + positions[0]?.Amount + "\t Total Ordens: " + orders.Length);
                    tapeReadingPosicoes.Add(new PosicaoAtiva(positions[0].OpenPrice, positions[0].Amount, positions[0].Side, DateTime.Now, "Inicio Timeline"));
                    ultimaPosicaoAtivaAvalanche = new PosicaoAtiva(positions[0].OpenPrice, positions[0].Amount, positions[0].Side, DateTime.Now, "Inicio Timeline");
                    ultimaReentrada = positions[0].OpenPrice;                    
                    LogWriter.LogWrite("Posição ativa inicial setada", _log);                    
                }

                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshPositionsOrders: Erro no RefreshPositionsOrders. Erro:" + exc.Message);
                return false;
            }
        }

        public bool RefreshIndicators()
        {
            try
            {
                Print("RefreshIndicators: Lendo indicadores...");
                cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
                curta = MM.GetValue(0, 0);
                curtaAnterior = MM.GetValue(0, 1);
                suporte = MM.GetValue(1, 0);
                suporteAnterior = MM.GetValue(1, 1);
                histoResult = sniper.GetValue(0, 0);
                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool CheckMarketStatus(Trade trade)
        {
            try
            {
                if (myConnection.Status == ConnectionStatus.Connected)
                {
                    horario = DateTime.Now.TimeOfDay; hora = horario.Hours;
                }
                else
                {
                    horario = instrument.LastQuote.Time.ToLocalTime().TimeOfDay;
                    hora = horario.Hours;
                }

                Print("CheckMarketStatus: Hora Atual = " + horario.ToString());
                if (hora == 0) { return false; }
                if (hora >= 9) { horarioTerminou = false; }
                if (horario.CompareTo(HoraDeInicio) < 0) { Print("Aguardando hora de Início..."); return false; }
                if (horario.CompareTo(HoraDeTermino) >= 0) { Print("CheckMarketStatus: Hora de término atingida!"); horarioTerminou = true; }
                if (hora >= 18)
                {
                    horarioTerminou = true;
                    if (Positions.Count > 0) Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); FechamentoTotal();
                    Print("CheckMarketStatus: Bolsa fechada !");
                    return false;
                }
                return true;
            }
            catch (Exception exc)
            {
                Print("CheckMarketStatus: Erro na coleta da Hora . Erro: " + exc.Message);
                Print("CheckMarketStatus: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public void SaidaPorLossMaximo()
        {
            try
            {
                //SAÍDA POR LOSS MÁXIMO
                if (positions.Length > 0 && tipoDeSaida == TipoDeSaida.LossMaximo)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize) + " Limite: " + lossMaximo + " ticks");

                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize >= lossMaximo)
                        {
                            string log = "*****************    LOSS MÁXIMO ATINGIDO    ********************";
                            Print(log); LogWriter.LogWrite(Log(log), _log);
                            contadorDeReEntradas = 0;
                            FechamentoTotal();
                            log = "Fechando todas as Posições por LOSS MÁXIMO !!!"; Print(log); LogWriter.LogWrite(Log(log), _log);
                            estado = State.limbo; acao.Clear(); acao.Add(Acao.passeLivre); return;
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                Print("SaídaPorLossMáximo: Erro setor Loss Máximo . " + exc.Message);
                Print("SaídaPorLossMáximo: InnerException " + exc.InnerException.Message);
            }
        }

        public void DetectaAvalanche()
        {
            try
            {
                RefreshPositionsOrders();
                if (positions.Length > 0)
                {
                    if (!modoAvalanche)
                    {
                        Position pos = positions[0];
                        if (pos != null)
                        {
                            if (pos.Amount >= avalanche)
                            {
                                if (Math.Abs(pos.OpenPrice - pos.CurrentPrice) / tickSize > ticksAvalanche)
                                {
                                    Print("*****************    MODO AVALANCHE ACIONADO   ********************");

                                    estado = State.avalanche;
                                    modoAvalanche = true;
                                    ultimaPosicaoAtivaAvalanche = posicaoAtivaAvalanche;
                                    posicaoAtivaAvalanche = null;
                                    LimparTodasAsOrdens();

                                }
                                else
                                {
                                    Print("Modo Avalanche após " + ticksAvalanche + " pontos. Posição atual: " + Math.Abs(pos.OpenPrice - pos.CurrentPrice) / tickSize);
                                }
                            }
                            else
                            {
                                Print("Detecta Avalanche: Avalanche será acionada após " + avalanche + " contratos");
                            }
                        }
                    }
                }
                else
                {
                    modoAvalanche = false;
                    //estado = State.inicio;
                    //if (orders.Length > 0) LimparTodasAsOrdens();

                }

            }
            catch (Exception exc)
            {
                Print("Detecta Avalanche: Erro setor Detector de Avalanche . " + exc.Message);
                Print("Detecta Avalanche: InnerException " + exc.InnerException.Message);
            }
        }

        public bool CalculaCalorMaximo()
        {
            try
            {
                if (positions.Length > 0)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        if (pos.GetProfitNet() < calor) { calor = pos.GetProfitNet(); horaDoCalorMaximo = hora.ToString(); }
                        if (Math.Abs(pos.Amount) > maximoDeContratosAbertos) { maximoDeContratosAbertos = pos.Amount; }
                    }
                }
                Print("CalculaCalorMaximo: Calor Máximo até o momento: " + calor);
                return true;
            }
            catch (Exception exc)
            {
                Print("CalculaCalorMaximo: Erro setor Calor Máximo . " + exc.Message);
                Print("CalculaCalorMaximo: InnerException " + exc.InnerException.Message);
                return false;
            }
        }


        private string EntradaDiffMedias(double preco, double diff)
        {
            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0, "entrada");
            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
            return "-1";
        }

        private string EntradaContra(double preco)
        {
            if (preco > curta + distanciaDaMediaContra * tickSize) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0, "entrada contra");
            if (preco < curta - distanciaDaMediaContra * tickSize) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
            return "-1";
        }

        private string ReEntrada(double preco, Position pos)
        {
            var precoMedio = pos.OpenPrice;
            var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];

            if (pos.Side == Operation.Buy)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, ValoresDeReEntradas[contadorDeReEntradas], 0, "reEntrada mercado " + contadorDeReEntradas.ToString());
            }

            if (pos.Side == Operation.Sell)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, ValoresDeReEntradas[contadorDeReEntradas], 0, "reEntrada mercado " + contadorDeReEntradas.ToString());
            }

            return "-1";
        }


        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit, string comment = "entrada")
        {
            acao.Clear();
            ultimoComentario = comment;
            price = instrument.RoundPrice(price);

            string log = horario + " Send_Order(): Type:" + type + " Side:" + side + " Price:" + price + " Contratos:" + contratos + " Comment:" + comment;
            Print(log); LogWriter.LogWrite(Log(log), _log);

            NewOrderRequest request = new NewOrderRequest
            {
                Instrument = instrument,
                Account = conta,
                Type = type,
                Side = side,
                Amount = contratos,
                MarketRange = marketRange,
                MagicNumber = numeroMagico,
                Comment = comment
            };

            Quote quote = instrument.LastQuote;
            if (quote != null)
            {
                price = (side == Operation.Buy) ? quote.Ask : quote.Bid;

                if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? instrument.RoundPrice(price + takeProfit * instrument.TickSize) : instrument.RoundPrice(price - takeProfit * instrument.TickSize);
                if (type != OrdersType.StopLimit) { request.Price = instrument.RoundPrice(price); } else { request.StopPrice = instrument.RoundPrice(price); }

                Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + instrument.RoundPrice(price) + " Qtde:" + request.Amount + " Ponto Atual da Média:" + curta);
                var result = Orders.Send(request);
                if (result == "-1") { log = horario + " SendOrder: Ordem não aceita pela corretora"; Print(log); LogWriter.LogWrite(Log(log), _log); ultimoComentario = ""; }
                if (result != "-1") { if (acao.Count == 0) bloqueioGeral = true; log = horario + " SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!"; Print(log); LogWriter.LogWrite(Log(log), _log); }
                return result;
            }

            return "-1";

        }


        private void LimparTodasAsOrdens()
        {
            LogWriter.LogWrite(Log("LimparTodasAsOrdens()"), _log);
            string log = "";

        LoopLimpar:

            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel
            && ord.Status != OrderStatus.PendingCancel
            && ord.Status != OrderStatus.PendingExecution
            && ord.Status != OrderStatus.PendingNew
            && ord.Status != OrderStatus.PendingReplace
            ).ForEach(ord =>
             {
                 var result = ord.Cancel();
                 if (!result)
                 {
                     log = "LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA! Status: " + ord.Status;
                     Print(log);
                 }
                 else
                 {
                     log = "LimparTodasAsOrdens: Ordem " + ord.Id + " " + ord.Side + " FOI CANCELADA! Status: " + ord.Status;
                     Print(log);
                 }
             });

            RefreshPositionsOrders();
            if (orders.Length > 0)
            {
                log = "LimparTodasAsOrdens: Existem ordens que não foram limpadas ainda";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                goto LoopLimpar;
            }



        }

        private bool CriaParDeSaida(Position posicao = null, string comment = "saida limite")
        {
            //TAKE PROFIT LIMIT 
            acao.Clear();
            LogWriter.LogWrite(Log(horario + " CriaParDeSaida(); Comment=" + comment), _log);
            Position[] positions;
            if (posicao == null)
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
                if (positions.Length > 0) posicao = positions[0];
            }

            if (posicao != null)
            {
                string log = horario + " CriaParDeSaida: Ok! Tentando setar TAKE PROFIT LIMIT";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                try
                {
                    Position pos = posicao;
                    if (pos.Side == Operation.Buy)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = marketRange;
                        request.Comment = comment;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            log = horario + "CriaParDeSaida: Ordem Limit de saída SELL não colocada... tentando ordem STOP";
                            Print(log); LogWriter.LogWrite(Log(log), _log);
                            NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = marketRange;
                            ultimoComentario = comment;
                            req.Comment = comment;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1") { log = horario + "CriaParDeSaida: Ordem SELL STOP não aceita!"; Print(log); LogWriter.LogWrite(Log(log), _log); return false; } else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                        }
                        else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                    }
                    if (pos.Side == Operation.Sell)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = marketRange;
                        ultimoComentario = comment;
                        request.Comment = comment;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            log = horario + "CriaParDeSaida: Ordem Limit de saída BUY não colocada... tentando ordem STOP";
                            Print(log); LogWriter.LogWrite(Log(log), _log);
                            NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = marketRange;
                            req.Comment = comment;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1") { log = horario + "CriaParDeSaida: Ordem BUY STOP não aceita!"; Print(log); LogWriter.LogWrite(Log(log), _log); return false; } else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                        }
                        else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                    }
                }
                catch (Exception ext)
                {
                    Print("CriaParDeSaida: Erro ao Setar Take Profit! " + ext.Message);
                    return false;
                }
            }
            else
            {
                Print("CriaParDeSaída: Não existe posição");
                LimparTodasAsOrdens();
            }

            return false;
        }

        private bool SaidaAvalanche(PosicaoAtiva posicao, string comment = "saida avalanche")
        {

            acao.Clear();
            string log = "SaidaAvalanche: Ok! Tentando setar TAKE PROFIT LIMIT";
            Print(log); LogWriter.LogWrite(Log(log), _log);
            try
            {
                PosicaoAtiva pos = posicao;
                ultimoComentario = comment;
                if (pos._side == Operation.Buy)
                {
                    NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos._amount, instrument.RoundPrice(pos._openPrice + TPavalanche * tickSize), conta);
                    request.MagicNumber = numeroMagico;
                    request.MarketRange = marketRange;
                    request.Comment = comment;
                    var id = Orders.Send(request);
                    if (id == "-1")
                    {
                        log = "SaidaAvalanche: Ordem Limit de saída SELL não colocada... tentando ordem STOP";
                        Print(log); LogWriter.LogWrite(Log(log), _log);
                        NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos._amount, instrument.RoundPrice(pos._openPrice + TPavalanche * tickSize), conta);
                        req.MagicNumber = numeroMagico;
                        req.MarketRange = marketRange;
                        req.Comment = comment;
                        var id2 = Orders.Send(req);
                        if (id2 == "-1")
                        {
                            log = "SaidaAvalanche: Ordem SELL STOP não aceita!";
                            Print(log); LogWriter.LogWrite(Log(log), _log);
                            return false;
                        }
                        else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                    }
                    else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                }
                if (pos._side == Operation.Sell)
                {
                    NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos._amount, instrument.RoundPrice(pos._openPrice - TPavalanche * instrument.TickSize), conta);
                    request.MagicNumber = numeroMagico;
                    request.MarketRange = marketRange;
                    request.Comment = comment;
                    var id = Orders.Send(request);
                    if (id == "-1")
                    {
                        log = "SaidaAvalanche: Ordem Limit de saída BUY não colocada... tentando ordem STOP";
                        Print(log); LogWriter.LogWrite(Log(log), _log);
                        NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos._amount, instrument.RoundPrice(pos._openPrice - TPavalanche * instrument.TickSize), conta);
                        req.MagicNumber = numeroMagico;
                        req.MarketRange = marketRange;
                        req.Comment = comment;
                        var id2 = Orders.Send(req);
                        if (id2 == "-1") { log = "SaidaAvalanche: Ordem BUY STOP não aceita!"; Print(log); LogWriter.LogWrite(Log(log), _log); return false; } else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                    }
                    else { ordemDeSaida = true; if (acao.Count == 0) bloqueioGeral = true; return true; }
                }
            }
            catch (Exception ext)
            {
                Print("SaidaAvalanche: Erro ao Setar Take Profit! " + ext.Message);
                return false;
            }

            return false;
        }

        private bool EqualizaOrdemPosicao(Position pos, Order ord)
        {
            double price = 0;
            price = (pos.Side == Operation.Buy) ? instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize) : instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize);
            string log = "EqualizaOrdemPosição: Qtde Pos: " + pos.Amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + TP * tickSize;
            Print(log);

            if (pos.Amount != ord.Amount)
            {
                log = horario + "EqualizaOrdemPosição: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + TP * tickSize;
                Print(log); LogWriter.LogWrite(Log(log), _log);
                return ord.Modify(price, pos.Amount);
            }

            if (Math.Abs(ord.Price - price) > TP * tickSize)
            {
                log = horario + "EqualizaOrdemPosição: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + TP * tickSize;
                Print(log); LogWriter.LogWrite(Log(log), _log);
                return ord.Modify(price);
            }
            return false;
        }

        private bool EqualizaOrdemPosicaoAvalance(PosicaoAtiva pos, Order ord)
        {
            try
            {
                double price = 0;
                price = (pos._side == Operation.Buy) ? instrument.RoundPrice(pos._openPrice + TPavalanche * tickSize) : instrument.RoundPrice(pos._openPrice - TPavalanche * tickSize);
                string log = "EqualizaOrdemPosiçãoAvalanche: Qtde Pos: " + pos._amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + TPavalanche * tickSize;
                Print(log);
                if (pos._amount != ord.Amount)
                {
                    log = horario + "EqualizaOrdemPosiçãoAvalanche: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + TPavalanche * tickSize;
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                    return ord.Modify(price, pos._amount);
                }

                if (Math.Abs(ord.Price - price) > TPavalanche * tickSize)
                {
                    log = horario + "EqualizaOrdemPosiçãoAvalanche: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t TpAvalanche x TickSize: " + TPavalanche * tickSize;
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                    return ord.Modify(price);
                }
            }
            catch (Exception exc)
            {
                Alert("Erro equalizaOrdemAvalanche");
            }
            return false;
        }


        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = Positions.GetPositions();
            Order[] ordensGlobais = Orders.GetOrders(true);
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => Funcoes.PosInfo(pos, this)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => Funcoes.OrderInfo(ord, this)); }
        }

        public void FechamentoParcial()
        {
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
        }

        public void FechamentoTotal()
        {
            string log = horario + " --> Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas caso existam";
            Print(log); LogWriter.LogWrite(Log(log), _log);

        LoopFechamento:
            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord =>
            {
                log = "Fechamento Total: Ordem ID " + ord.Id + "  Status:" + ord.Status + " Active:" + ord.IsActive + " Qtde:" + ord.Amount + " " + ord.Side + " Price:" + ord.CurrentPrice;
                Print(log); LogWriter.LogWrite(Log(log), _log);
                ord.Cancel();
            });
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p =>
            {
                log = "Fechamento Total: Posição ID " + p.Id + " Qtde:" + p.Amount + " " + p.Side + " Price:" + p.OpenPrice;
                Print(log); LogWriter.LogWrite(Log(log), _log);
                p.Close();
            });

            RefreshPositionsOrders();
            if (positions.Length > 0 || orders.Length > 0)
            {
                log = "ERRO FechamentoTotal: Aguardando fechamento de Ordens e Posições";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                goto LoopFechamento;
            }


        }

        private void Juiz()
        {
            string log = "";
            log = horario + "********************   Estado Juiz ***********************";
            Print(log); LogWriter.LogWrite(Log(log), _log);
            RefreshPositionsOrders();
            if (positions == null) positions = new Position[0];
            if (orders == null) orders = new Order[0];

            acao.Clear();

            if (orders.Length > 1)
            {
                log = "Existe mais ordens do que o normal! Limpando a primeira..";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                LimparTodasAsOrdens();
                estado = State.limbo;
                acao.Add(Acao.passeLivre);
                return;
            }

            PrintaPosicoesOrdensAtivas();

            //Caso 1: Não existe posição e Não existe ordem
            if (positions.Length == 0 && orders.Length == 0)
            {
                //como chegou um caso assim para o juiz analisar? É impossível pois a migração de estados na entrada e na reentrada apenas ocorrem se a corretora aceitar as ordens
                //mesmo assim, é melhor prever isso
                log = "Estado Juiz: Não existe Posição e Não existe Ordem para ser processada. estado -> inicio";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                estado = State.inicio;
                acao.Clear();
                return;
            }

            //Caso 2: Não existe posição, e ordem tem operação pendente
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ord.Status == OrderStatus.PendingNew || ord.Status == OrderStatus.PendingReplace || ord.Status == OrderStatus.PendingCancel || ord.Status == OrderStatus.PendingExecution).Count != 0)
            {
                log = "Estado Juiz: Existe Ordem com operação pendente ainda... aguarde ela definir o que vai fazer";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                estado = State.limbo;
                acao.Add(Acao.passeLivre);
                return;
            }

            //Case 3: Não existe posição e Existe ordem aguardando execução
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew))).Count != 0)
            {
                log = "Estado Juiz: Não existe posição E uma ordem qualquer ainda não foi executada.";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                estado = State.limbo;
                acao.Add(Acao.passeLivre);
                contadorOrdemTravada++;
                if (contadorOrdemTravada == 100) { LimparTodasAsOrdens(); contadorOrdemTravada = 0; }
                return;
            }

            //Case 4: Existe posição , não existe ordem de saída e modo avalanche é falso
            if (positions.Length != 0 && orders.Length == 0 && !modoAvalanche)
            {
                log = "Estado Juiz: Existe posição mas não existe ordem de saída. CriaParDeSaída()";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                if (CriaParDeSaida(null))
                {
                    log = "Estado Juiz: CriaParDeSaída Executado com sucesso. Migrando para o limbo...";
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                    estado = State.limbo; acao.Add(Acao.aguardandoOrderAdded);
                    acao.Add(Acao.solicitouSaidaLimite);
                }
                else
                {
                    log = "Estado Juiz: Erro na Criação do Par de Saída!";
                    Print(log); LogWriter.LogWrite(Log(log), _log);
                    estado = State.limbo;
                    acao.Add(Acao.passeLivre);
                }
                return;
            }

            //Case 4.1: Existe posição mas não existe ordem de saída e modo avalanche é true
            if (positions.Length != 0 && orders.Length == 0 && modoAvalanche)
            {
                log = "Estado Juiz: Existe posição, não existe ordem de saída e modo avalanche é true.";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                estado = State.avalanche;
                acao.Clear();
                return;
            }

            //Case 5: Existe Posição e Existe Ordem de Saída e modo avalanche é falso
            if (positions.Length != 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced || ord.Status == OrderStatus.PartiallyFilled))).Count != 0 && !modoAvalanche)
            {
                log = "Estado Juiz: Existe posição e Existe ordem de saída. Equalizando e Migrando para estado ReEntrada";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                //EqualizaOrdemPosicao(positions[0], orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced || ord.Status == OrderStatus.PartiallyFilled)))[0]);
                estado = State.reentrada;
                acao.Clear();
                return;
            }

            //Case 5.1: Existe Posição, Existe Ordem de Saída e modo avalanche está true
            if (positions.Length != 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew || ord.Status == OrderStatus.Replaced))).Count != 0 && modoAvalanche)
            {
                log = "Estado Juiz: Existe posição, Existe ordem de saída e modo Avalanche está true. Migrando para estado Avalanche";
                Print(log); LogWriter.LogWrite(Log(log), _log);
                estado = State.avalanche;
                acao.Clear();
                return;
            }

            //padrão
            estado = State.limbo;
            acao.Clear();
            log = "Estado Juiz: Nenhuma opção de saída foi encontrada. Migrando para limbo";
            Print(log); LogWriter.LogWrite(Log(log), _log);
            acao.Add(Acao.passeLivre);
        }




        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                Graphics gr = args.Graphics;
                Rectangle rect = args.Rectangle;
                RefreshPositionsOrders();

                if (args.Rectangle.Height > 350)
                {
                    if (!plotarLevel2 && acao.Count > 0 && Instruments.Current.BaseName == papel)
                    {
                        int i = 0;
                        acao.ForEach(x =>
                        {
                            gr.DrawString(x.ToString(), font, brush, rect.Width - 200, rect.Height - 5 - i);
                            i += 15;
                        });
                    }


                    gr.DrawString(("Calor máximo:" + calor.ToString() + " Máximo de Contratos abertos: " + maximoDeContratosAbertos.ToString() + " Estado atual:" + estado.ToString()) + " contadorDeReEntradas:" + contadorDeReEntradas, font, brush, rect.X + 110, rect.Height - 5);

                    if (level2.Count > 0 && plotarLevel2)
                    {
                        gr.DrawString("Origem", font, brush, rect.X + 720, rect.Y + 20);
                        gr.DrawString("Preço", font, brush, rect.X + 820, rect.Y + 20);
                        gr.DrawString("Tamanho", font, brush, rect.X + 920, rect.Y + 20);
                        gr.DrawString("Lado", font, brush, rect.X + 1000, rect.Y + 20);
                        gr.DrawLine(new Pen(Color.White, 1), new PointF(rect.X + 710, rect.Y), new PointF(rect.X + 710, rect.Y + rect.Y + rect.Height));
                        DrawLevel2(gr, rect);

                    }
                }

                if (positions.Length > 0 && args.Rectangle.Height > 350)
                {
                    gr.DrawString("Posição", font, brush, rect.X + 110, rect.Y + 20);
                    gr.DrawString("Qtde", font, brush, rect.X + 180, rect.Y + 20);
                    gr.DrawString("Preço Médio", font, brush, rect.X + 220, rect.Y + 20);
                    gr.DrawString("Preço Atual", font, brush, rect.X + 320, rect.Y + 20);
                    gr.DrawString("Tipo", font, brush, rect.X + 420, rect.Y + 20);
                    gr.DrawString("Lucro/Prejuízo", font, brush, rect.X + 480, rect.Y + 20);
                    gr.DrawString("Comentário", font, brush, rect.X + 590, rect.Y + 20);

                    gr.DrawString("Protrader ->", font, brush, rect.X + 10, rect.Y + 40);
                    gr.DrawString("Robô ->", font, brush, rect.X + 10, rect.Y + 60);
                    gr.DrawLine(new Pen(Color.White), new PointF(rect.X + 110, rect.Y + 80), new PointF(rect.X + 610, rect.Y + 80));

                    gr.DrawString(positions[0].Id, font, brush, rect.X + 110, rect.Y + 40);
                    gr.DrawString(positions[0].Amount.ToString(), font, brush, rect.X + 180, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].OpenPrice), font, brush, rect.X + 220, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].CurrentPrice), font, brush, rect.X + 320, rect.Y + 40);
                    gr.DrawString(positions[0].Side.ToString(), font, brush, rect.X + 420, rect.Y + 40);
                    gr.DrawString(String.Format("{0:C}", positions[0].GetProfitGross().ToString()), font, brush, rect.X + 480, rect.Y + 40);
                    gr.DrawString(positions[0].Comment.ToString(), font, brush, rect.X + 590, rect.Y + 40);


                    if (modoAvalanche) gr.DrawString("Modo Avalanche LIGADO", font, brush, rect.X + 110, rect.Height - 40);

                    gr.DrawString("Posicao Ref:", font, brush, rect.X + 110, rect.Height - 20);
                    if (posicaoAtivaAvalanche != null)
                    {
                        gr.DrawString(("Qtde:" + posicaoAtivaAvalanche._amount.ToString()), font, brush, rect.X + 190, rect.Height - 20);
                        gr.DrawString(("OpenPrice:" + String.Format("{0:C}", posicaoAtivaAvalanche._openPrice)), font, brush, rect.X + 260, rect.Height - 20);
                        gr.DrawString(("Tipo:" + posicaoAtivaAvalanche._side.ToString()), font, brush, rect.X + 420, rect.Height - 20);
                    }
                    else
                    {
                        gr.DrawString("NULA", font, brush, rect.X + 250, rect.Height - 20);
                    }



                    if (tapeReadingPosicoes.Count > 0 && posicaoAtivaMedia != null)
                    {
                        string precoMedioString = String.Format("{0:C}", posicaoAtivaMedia._openPrice);
                        double pontos = (posicaoAtivaMedia._side == Operation.Buy) ? Math.Round(((cotacaoAtual - posicaoAtivaMedia._openPrice) / tamanhoPonto), 2) : -Math.Round(((cotacaoAtual - posicaoAtivaMedia._openPrice) / tamanhoPonto), 2);

                        gr.DrawString(tapeReadingPosicoes.Count.ToString(), font, brush, rect.X + 110, rect.Y + 60);
                        gr.DrawString(posicaoAtivaMedia._amount.ToString(), font, brush, rect.X + 180, rect.Y + 60);
                        gr.DrawString(precoMedioString, font, brush, rect.X + 220, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", pontos.ToString()), font, brush, rect.X + 320, rect.Y + 60);
                        gr.DrawString(posicaoAtivaMedia._side.ToString(), font, brush, rect.X + 420, rect.Y + 60);
                        gr.DrawString(String.Format("{0:C}", posicaoAtivaMedia._lucro.ToString()), font, brush, rect.X + 480, rect.Y + 60);
                        DrawPositions(args.Graphics, args.Rectangle);
                    }
                }

            }
            catch (Exception exc)
            {
                Print("OnPaintChart Erro " + exc.Message);
            }
        }


        private void DrawPositions(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));           

            int i = 100;
            try
            {
                Operation tipoBase = tapeReadingPosicoes.First()._side;

                tapeReadingPosicoes.ForEach(posicao =>
                {
                    gr.DrawString(posicao._openTime.ToLocalTime().ToShortTimeString(), font, brush, rect.X + 110, rect.Y + i);
                    gr.DrawString(posicao._amount.ToString(), font, brush, rect.X + 180, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._openPrice), font, brush, rect.X + 220, rect.Y + i);
                    gr.DrawString(posicao._pontos.ToString(), font, brush, rect.X + 320, rect.Y + i);
                    gr.DrawString(posicao._side.ToString(), font, brush, rect.X + 420, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", posicao._lucro.ToString()), font, brush, rect.X + 480, rect.Y + i);
                    gr.DrawString(posicao._comments, font, brush, rect.X + 590, rect.Y + i);

                    i += 15;
                });

            }
            catch (Exception exc)
            {
                Print("DrawPositions: Erro " + exc.Message);
                Print("DrawPositions: Erro Intero " + exc.InnerException.Message);
            }
        }

        private void DrawLevel2(Graphics gr, Rectangle rect)
        {

            //string text_trade_time = String.Format("{0:HH:mm:ss}", cotacaoAtual.Time);
            //string text_trade_price = String.Format("{0}", instrument.FormatPrice(cotacaoAtual.Last));


            //gr.DrawString("Origem", font, brush, rect.X + 720, rect.Y + 20);
            //gr.DrawString("Preço", font, brush, rect.X + 820, rect.Y + 20);            
            //gr.DrawString("Tamanho", font, brush, rect.X + 920, rect.Y + 20);
            //gr.DrawString("Lado", font, brush, rect.X + 1000, rect.Y + 20);

            int i = 40;
            try
            {

                level2.ToList().ForEach(_level2 =>
                {
                    gr.DrawString(_level2.Source.ToString(), font, brush, rect.X + 720, rect.Y + i);
                    gr.DrawString(String.Format("{0:C}", _level2.Price), font, brush, rect.X + 820, rect.Y + i);
                    gr.DrawString(_level2.Size.ToString(), font, brush, rect.X + 920, rect.Y + i);
                    gr.DrawString(_level2.Side.ToString(), font, brush, rect.X + 1000, rect.Y + i);

                    i += 15;
                });

            }
            catch (Exception exc)
            {
                Print("DrawLevel2: Erro " + exc.Message);
                Print("DrawLevel2: Erro Interno " + exc.InnerException.Message);
            }
        }

    }

}
