﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using System.Globalization;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Threading;
using PTLRuntime.NETScript.Application;

namespace RoboDollar
{


    public class ProjetoSniperElite : NETStrategy
    {
        private enum state { limbo, inicio, reentrada, saidaTendencia };
        private enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
        public enum TipoDeSaida { DifMédias, LossMaximo };
        public enum TipoDeEntrada { MA3, DifMédias };
        public enum TipoDeOrdem { limite, mercado };

        private Instrument instrument;
        private Account conta;
        private Indicator MM;
        private Indicator sniper;
        private Connection myConnection = Connection.CurrentConnection; //create connection object.    
        private Position[] positions;
        private Order[] orders;

        private double calor = 0;
        private double curta = 0;
        private double curtaAnterior = 0;
        private double histoResult = 0;
        private double cotacaoAtual = 0;
        private int hora = 0;
        private int contadorDeReEntradas = -1;
        //private int[] ReEntradas = new int[7];
        private ushort[] ReEntradas;
        private UInt16[] ValoresDeReEntradas;
        private double tickSize = 0;
        private double maxDePosicaoAberta = 0;
        private double ultimaReentrada = 0;
        private string horaDoCalorMaximo = "";
        private state estado = state.limbo;
        private bool horarioTerminou = false;
        private string papel;
        private int numeroMagico = 0;
        private bool passeLivreNoLimbo = true;
        private TimeSpan horario;

        public ProjetoSniperElite()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto Sniper Elite";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "28.12.2018";
            base.ExpirationDate = 0;
            base.Version = "3.0.0";
            base.Password = "";
            base.ProjectName = "Sniper Elite";
            #endregion
        }

        [InputParameter("Média: Sinal", 0)]
        public int periodoMediaMovelSinal = 7;
        [InputParameter("Média: Cerca Elétrica", 1)]
        public int periodoMediaMovelSlow = 50;
        [InputParameter("Média contínua ou por fechamento de candle?", 2)]
        public bool mediaContinua = false;

        [InputParameter("Permitir até quantas posições abertas? ", 3)]
        public double maxPosAbertas = 1000;

        [InputParameter("Contratos", 4)]
        public int Contratos = 5;

        [InputParameter("Início(hora)", 5)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 15, 0);

        [InputParameter("Término(hora)", 6)]
        public TimeSpan HoraDeTermino = new TimeSpan(18, 00, 00);

        [InputParameter("Array Ticks de Reentradas", 7)]
        public string ticksDeReentrada = "15,24,31,40,50,60,70";

        [InputParameter("Array Valores de Reentradas", 8)]
        public string valoresDeReentrada = "5,10,20,40,80,160,320";

        [InputParameter("Distância da Média (ticks) para entrar", 9)]
        public double distanciaDaMedia = 2;

        [InputParameter("Take Profit (Pontos)", 10)]
        public double TP = 1;

        [InputParameter("Multiplicador da Dif.Das Médias", 11)]
        public int multiplicador = 10;

        [InputParameter("Tipo de Saída", 12, new Object[]
        {"Dif Médias", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;

        [InputParameter("Resiliência de Saída", 13)]
        public int resilienciaDeSaida = 2;

        [InputParameter("Loss máximo em Ticks", 14)]
        public double lossMaximo = 20;

        [InputParameter("Tipo de Ordem de Saída", 15, new Object[]
        {"Limite", TipoDeOrdem.limite, "Mercado",TipoDeOrdem.mercado })]
        public TipoDeOrdem tipoDeOrdem = TipoDeOrdem.limite;

        //[InputParameter("ReEntrada 0", 15)]
        //public int reEntrada0 = 2;
        //[InputParameter("ReEntrada 1",16)]
        //public int reEntrada1 = 20;
        //[InputParameter("ReEntrada 2",17)]
        //public int reEntrada2 = 0;
        //[InputParameter("ReEntrada 3",18)]
        //public int reEntrada3 = 32;
        //[InputParameter("ReEntrada 4",19)]
        //public int reEntrada4 = 49;
        //[InputParameter("ReEntrada 5",20)]
        //public int reEntrada5 = 57;
        //[InputParameter("ReEntrada 6",21)]
        //public int reEntrada6 = 62;


        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            try
            {
                instrument = Instruments.Current;
                Print("Nome do Instrumento ativo: " + instrument.Name);
                papel = instrument.BaseName;
                conta = Accounts.Current;
                tickSize = instrument.TickSize;
                Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
                InfoConexao();
                AccountInformation(conta);

                ReEntradas = ticksDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();
                //ReEntradas[0] = reEntrada0; ReEntradas[1] = reEntrada1; ReEntradas[2] = reEntrada2; ReEntradas[3] = reEntrada3; ReEntradas[4] = reEntrada4; ReEntradas[5] = reEntrada5; ReEntradas[6] = reEntrada6; 
                ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToUInt16(n)).ToArray();

                if (ReEntradas.Length != ValoresDeReEntradas.Length)
                {
                    throw new ArgumentException("Array Valores de ReEntradas e Ticks de ReEntradas tem que ter o mesmo número de elementos");
                }

                Print("Papel: " + instrument.Name);
                Print("Tamanho do Point: " + Point);
                Print("Tamanho do tick: " + instrument.TickSize);


                //INDICADORES
                Print("Inicializando indicador Camilo 2MM");
                MM = Indicators.iCustom("camilo_2mm", CurrentData, periodoMediaMovelSinal, periodoMediaMovelSlow);
                Print("Inicializando indicador Sniper Elite");
                sniper = Indicators.iCustom("Camilo_SniperElite", CurrentData, periodoMediaMovelSinal, resilienciaDeSaida, multiplicador);


                //EVENTOS            
                Positions.PositionAdded += Positions_PositionAdded;
                Positions.PositionRemoved += Positions_PositionRemoved;
                Instruments.NewTrade += Instruments_NewTrade;
                Orders.OrderAdded += Orders_OrderAdded;
                Orders.OrderExecuted += Orders_OrderExecuted;
                Orders.OrderRemoved += Orders_OrderRemoved;

                Print("Fim da Inicialização do Robô");

                if (myConnection.Status == ConnectionStatus.Disconnected)
                {
                    Print("RODANDO ROBÔ NO SIMULADOR. NÚMERO MÁGICO É 0");
                    numeroMagico = 0; // SE ESTIVER NO SIMULADOR O NUMERO MÁGICO É ZERO
                }
                else
                {
                    Print("RODANDO ROBÔ NA CONTA DEMO OU REAL. Número mágico deste robô é " + numeroMagico);
                }
            }
            catch (Exception exc)
            {
                Print("Erro na inicialização do Robô... " + exc.Message);
                Print("Finalizando Robô");
                TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
                foreach (var robo in robos)
                {
                    robo.Stop();
                    Print("Nome do robô:" + robo.Name);
                }
            }
        }

        private void Orders_OrderRemoved(Order obj)
        {
            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi REMOVIDA da FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            passeLivreNoLimbo = true;
        }

        private void Orders_OrderExecuted(Order obj)
        {
            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi EXECUTADA no preço " + obj.Price + " às " + obj.Time.ToLocalTime().ToShortTimeString());
        }

        private void Orders_OrderAdded(Order obj)
        {
            Print("Uma ordem com id " + obj.Id + " de " + obj.Side + " foi ADICIONADA na FILA para execução às " + obj.Time.ToLocalTime().ToShortTimeString());
            passeLivreNoLimbo = false;
            if (obj.Type == OrdersType.Limit) passeLivreNoLimbo = true;
        }

        private void Positions_PositionRemoved(Position obj)
        {
            passeLivreNoLimbo = true;
            try
            {
                Print("PositionRemoved: Posição com id " + obj.Id + " de tipo " + obj.Side + " do Papel " + obj.Instrument.BaseName + " foi removida! MagicNumber:" + obj.MagicNumber);
                if (obj.Instrument.BaseName == papel)
                {
                    LimparTodasAsOrdens();
                    estado = state.limbo;
                    if (contadorDeReEntradas > 0) contadorDeReEntradas--;
                    passeLivreNoLimbo = true;
                }
            }
            catch (Exception exc)
            {
                Print("PositionRemoved: Erro no Position Removed: " + exc.Message);
                LimparTodasAsOrdens();
                estado = state.limbo;
                passeLivreNoLimbo = true;
            }
        }

        private void Positions_PositionAdded(Position obj)
        {
            try
            {
                Print("PositionAdded: Posição com id " + obj.Id + " de tipo " + obj.Side + " no Papel " + obj.Instrument.BaseName + " foi adicionada! MagicNumber:" + obj.MagicNumber);

                if (obj.Instrument.BaseName == papel)
                {                    
                    ultimaReentrada = obj.OpenPrice;
                    estado = state.limbo;
                    if (contadorDeReEntradas < ReEntradas.Length - 1) contadorDeReEntradas++;
                    ModificaOuCriaParDeSaida(obj);
                    passeLivreNoLimbo = true;
                }
            }
            catch (Exception ext)
            {
                Print("PositionAdded: Erro no Position Added: " + ext.Message);
                LimparTodasAsOrdens();
                estado = state.limbo;
                passeLivreNoLimbo = true;
            }
        }

        private void Instruments_NewTrade(Instrument inst, Trade trade)
        {
            Print("********************   NewTrade   ********************");
            if (!RefreshPositionsOrders()) return; //atualiza as ordens e posições ativas
            if (!RefreshIndicators()) return; //lê os indicadores
            if (!CheckMarketStatus(trade)) return; //checa se bolsa está aberta e se está fechada fecha as posições
            if (!CalculaCalorMaximo()) return;
            if (curta == 0 || curtaAnterior == 0) { Print("Aguardando indicadores!"); return; }

            SaidaPorLossMaximo();

            try
            {
                switch (estado)
                {
                    case state.limbo:
                        {
                            Print("********************   Estado Limbo *****************");
                            Print("Hora: " + horario + "  Limbo: Papel:" + instrument.BaseName + "\t Numero mágico " + numeroMagico + "\t Lucro de hj: " + conta.TodayNet + "\t Qtde de Trades: " + conta.TodayTrades + "\t hora último trade:" + horario);
                            PrintaPosicoesOrdensAtivas();
                            if (passeLivreNoLimbo)
                            {
                                passeLivreNoLimbo = false;
                                Juiz();
                            }
                            break;
                        }
                    case state.inicio:
                        {
                            Print("********************   Estado Início  ********************");
                            if (horarioTerminou) { Print("Horário de Término atingido, não entra mais..."); return; }
                            if (true)
                            {
                                contadorDeReEntradas = -1;
                                Print("Hora: " + horario + "Estado Inicio: Preço atual:" + cotacaoAtual + " Média curta:" + curta + " Ticks da média:" + (Math.Abs(cotacaoAtual - curta) / tickSize));
                                Print("Estado Inicio: Modo de Entrada: Sinal Atual - Anterior = " + (curta - curtaAnterior));
                                if ((Math.Abs(cotacaoAtual - curta) / tickSize) <= distanciaDaMedia)
                                {
                                    double diff = 0;
                                    if (mediaContinua) diff = curta - curtaAnterior;
                                    if (!mediaContinua) diff = histoResult;

                                    if (EntradaDiffMedias(cotacaoAtual, diff) != "-1")
                                    {
                                        Print("Estado Inicio: Ordem foi enviada ! Aguardando sua resolução no limbo");
                                        estado = state.limbo;
                                        return;
                                    }
                                    else Print("Estado Inicio: Aguardando Condições de Entrada");
                                }
                                else Print("Estado Inicio: Preço muito elevado. Aguardando aproximar da média para entrar...");
                            }
                            break;
                        }

                    case state.reentrada:
                        {
                            Print("********************   Estado ReEntrada ***********************");
                            positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
                            if (positions.Length == 0) { Print("Estado ReEntrada: Não existe mais posição. Migrando para inicio"); LimparTodasAsOrdens(); estado = state.inicio; return; }

                            if (positions.Length != 0)
                            {
                                Position pos = positions[0];
                                var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];
                                var liberaReEntrada = false;
                                Print("Hora:" + horario + "Estado ReEntrada: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize));
                                if (tipoDeSaida == TipoDeSaida.DifMédias)
                                {
                                    if (mediaContinua) Print("Estado ReEntrada: (média - média Anterior) x multiplicador = " + (curta - curtaAnterior) * multiplicador);
                                    if (!mediaContinua) Print("Estado ReEntrada: histograma = " + histoResult);
                                }
                                Print("Estado ReEntrada: Entrada atual = " + contadorDeReEntradas);

                                if (pos.Side == Operation.Buy && pos.CurrentPrice <= ultimaReentrada - deltaMoeda)
                                {
                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                    liberaReEntrada = true;
                                }

                                if (pos.Side == Operation.Sell && pos.CurrentPrice >= ultimaReentrada + deltaMoeda)
                                {
                                    Print("Estado ReEntrada: OK! Preço atingiu o ponto de reentrada ... permitindo entrada de novas ordens");
                                    liberaReEntrada = true;
                                }

                                if (liberaReEntrada)
                                {
                                    Print("Estado ReEntrada: Ok! Tentando Reentrar...");
                                    if (positions[0].Amount <= maxPosAbertas / 2)
                                    {
                                        var result = ReEntrada(cotacaoAtual, positions[0]);
                                        if (result != "-1") { Print("Estado ReEntrada: Uma ordem de reentrada foi emitida. Migrando para o limbo."); estado = state.limbo; }
                                    }
                                    else
                                    {
                                        Print("Estado ReEntrada: Máximo de Contratos atingidos!");
                                    }
                                }

                                double diff = 0;
                                if (mediaContinua) diff = (curta - curtaAnterior) * multiplicador;
                                if (!mediaContinua) diff = histoResult;

                                if (tipoDeSaida == TipoDeSaida.DifMédias && diff >= resilienciaDeSaida && pos.Side == Operation.Sell)
                                {
                                    Print("Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total");
                                    FechamentoTotal();
                                }

                                if (tipoDeSaida == TipoDeSaida.DifMédias && diff <= -resilienciaDeSaida && pos.Side == Operation.Buy)
                                {
                                    Print("Estado ReEntrada: Condições de saída alcançadas. Fazendo saída total");
                                    FechamentoTotal();
                                }

                                PrintaPosicoesOrdensAtivas();
                            }
                            break;
                        }

                    default:
                        {
                            Print("Estado Juiz: Nenhum caso foi analisado pelo Juiz. Voltando para o limbo");
                            estado = state.limbo;
                            break;
                        }
                }

            }
            catch (Exception exc)
            {
                Print("NewTrade:  Erro setor switch . Mensagem -> " + exc.Message);
                Print("NewTrade: Erro:  InnerMessage -> " + exc.InnerException.Message);
                return;
            }
        }

        public bool RefreshPositionsOrders()
        {
            try
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel)?.ToArray();
                orders = Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel)?.ToArray();
                if (positions == null) positions = new Position[0];
                if (orders == null) orders = new Order[0];
                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshPositionsOrders: Erro no RefreshPositionsOrders. Erro:" + exc.Message);
                return false;
            }
        }

        public bool RefreshIndicators()
        {
            try
            {
                Print("RefreshIndicators: Lendo indicadores...");
                cotacaoAtual = CurrentData.GetPrice(PriceType.Close);
                curta = MM.GetValue(0, 0);
                curtaAnterior = MM.GetValue(0, 1);
                histoResult = sniper.GetValue(0, 0);
                return true;
            }
            catch (Exception exc)
            {
                Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public bool CheckMarketStatus(Trade trade)
        {
            try
            {
                if (myConnection.Status == ConnectionStatus.Connected) { horario = DateTime.Now.TimeOfDay; hora = horario.Hours; } else { horario = instrument.LastQuote.Time.ToLocalTime().TimeOfDay; hora = horario.Hours; }
                Print("CheckMarketStatus: Hora Atual = " + horario.ToString());
                if (hora == 0) { return false; }
                if (hora >= 9) { horarioTerminou = false; }
                if (horario.CompareTo(HoraDeInicio) < 0) { Print("Aguardando hora de Início..."); return false; }
                if (horario.CompareTo(HoraDeTermino) >= 0) { Print("CheckMarketStatus: Hora de término atingida!"); horarioTerminou = true; }
                if (hora >= 18)
                {
                    horarioTerminou = true;
                    if (Positions.Count > 0) Print("CheckMarketStatus: Fechando todas as POSIÇÕES pois BOLSA VAI FECHAR!"); Positions.CloseAll();
                    Print("CheckMarketStatus: Bolsa fechada !");
                    return false;
                }
                return true;
            }
            catch (Exception exc)
            {
                Print("CheckMarketStatus: Erro na coleta da Hora . Erro: " + exc.Message);
                Print("CheckMarketStatus: InnerException " + exc.InnerException.Message);
                return false;
            }
        }

        public void SaidaPorLossMaximo()
        {
            try
            {
                //SAÍDA POR LOSS MÁXIMO
                if (positions.Length > 0 && tipoDeSaida == TipoDeSaida.LossMaximo)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        Print("Loss Máximo: (Preço Atual - Preço Abertura)/tick = " + (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize) + " Limite: " + lossMaximo + " ticks");

                        if (Math.Abs((pos.CurrentPrice - pos.OpenPrice)) / tickSize >= lossMaximo)
                        {
                            Print("*****************    LOSS MÁXIMO ATINGIDO    ********************");

                            bool result = false;
                            contadorDeReEntradas = -1;
                            FechamentoTotal();
                            if (result) { Print("Fechando todas as Posições por LOSS MÁXIMO !!!"); estado = state.limbo; }
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                Print("OnQuote: Erro setor Loss Máximo . " + exc.Message);
                Print("OnQuote: InnerException " + exc.InnerException.Message);
            }
        }

        public bool CalculaCalorMaximo()
        {
            try
            {
                if (positions.Length > 0)
                {
                    Position pos = positions[0];
                    if (pos != null)
                    {
                        if (pos.GetProfitNet() < calor) { calor = pos.GetProfitNet(); horaDoCalorMaximo = hora.ToString(); maxDePosicaoAberta = pos.Amount; }
                    }
                }
                Print("CalculaCalorMaximo: Calor Máximo até o momento: " + calor);
                return true;
            }
            catch (Exception exc)
            {
                Print("CalculaCalorMaximo: Erro setor Calor Máximo . " + exc.Message);
                Print("CalculaCalorMaximo: InnerException " + exc.InnerException.Message);
                return false;
            }
        }



        public override void OnQuote()
        {
            if (myConnection.Status == ConnectionStatus.Disconnected)
            {
                //ou seja, está no simulador onde não tem OnTrades...
                Instruments_NewTrade(instrument, instrument.GetLastTrade());
            }

        }

        public override void NextBar()
        {
            passeLivreNoLimbo = true;
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Quote | QuoteTypes.Trade);
            Positions.PositionAdded -= Positions_PositionAdded;
            Positions.PositionRemoved -= Positions_PositionRemoved;
            Instruments.NewTrade -= Instruments_NewTrade;
            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;

        }


        private string EntradaDiffMedias(double preco, double diff)
        {
            if (diff < 0) return Send_order(OrdersType.Market, Operation.Sell, instrument.RoundPrice(preco), Contratos, 0);
            if (diff > 0) return Send_order(OrdersType.Market, Operation.Buy, instrument.RoundPrice(preco), Contratos, 0);
            return "-1";
        }

        private string ReEntrada(double preco, Position pos)
        {
            var precoMedio = pos.OpenPrice;
            var deltaMoeda = instrument.TickSize * ReEntradas[contadorDeReEntradas];

            if (pos.Side == Operation.Buy)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, ValoresDeReEntradas[contadorDeReEntradas], 0);
            }

            if (pos.Side == Operation.Sell)
            {
                Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, pos.Amount, 0);
            }

            return "-1";
        }


        public string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit)
        {
            NewOrderRequest request = new NewOrderRequest();
            request.Instrument = instrument;
            request.Account = conta;
            request.Type = type;
            request.Side = side;
            request.Amount = contratos;
            request.MarketRange = 10;
            request.MagicNumber = numeroMagico;

            if (takeProfit != 0) request.TakeProfitOffset = (side == Operation.Buy) ? instrument.RoundPrice(price + takeProfit * instrument.TickSize) : instrument.RoundPrice(price - takeProfit * instrument.TickSize);
            if (type != OrdersType.StopLimit) { request.Price = instrument.RoundPrice(price); } else { request.StopPrice = instrument.RoundPrice(price); }

            Print("SendOrder: Emitindo uma ordem de " + request.Side + " no preço R$" + instrument.RoundPrice(price) + " Qtde:" + request.Amount + " Ponto Atual da Média:" + curta);
            var result = Orders.Send(request);
            if (result == "-1") { Print("SendOrder: Ordem não aceita pela corretora"); }
            if (result != "-1") Print("SendOrder: Ordem com ticket " + result + " foi enviada para a corretora!");
            return result;
        }

        private bool LiberaEntrada(double preco, double sinal, double media)
        {
            if (preco > media && preco < sinal) return true;
            if (preco < media && preco > sinal) return true;

            return false;
        }

        private void LimparTodasAsOrdens()
        {
            Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord =>
             {
                 var result = ord.Cancel();
                 if (!result)
                 {
                     Print("LimparTodasAsOrdens: Ordem " + ord.Id + " NÃO FOI CANCELADA! Status: " + ord.Status);
                 }
                 else
                 {
                     Print("LimparTodasAsOrdens: Ordem " + ord.Id + " FOI CANCELADA! Status: " + ord.Status);
                 }
             });
        }

        private bool CriaParDeSaida(Position posicao = null)
        {
            //TAKE PROFIT LIMIT 
            Position[] positions;
            if (posicao == null)
            {
                positions = Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ToArray();
                if (positions.Length > 0) posicao = positions[0];
            }

            if (posicao != null)
            {
                Print("CriaParDeSaida: Ok! Tentando setar TAKE PROFIT LIMIT");
                try
                {
                    Position pos = posicao;
                    if (pos.Side == Operation.Buy)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateSellLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = 1000;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            Print("CriaParDeSaida: Ordem Limit de saída SELL não colocada... tentando ordem STOP");
                            NewOrderRequest req = NewOrderRequest.CreateSellStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = 1000;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1")
                            {
                                Print("CriaParDeSaida: Ordem SELL STOP não aceita!");
                                LimparTodasAsOrdens();
                                return false;
                            }
                            else { return true; }
                        }
                        else { return true; }
                    }
                    if (pos.Side == Operation.Sell)
                    {
                        NewOrderRequest request = NewOrderRequest.CreateBuyLimit(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                        request.MagicNumber = numeroMagico;
                        request.MarketRange = 10;
                        var id = Orders.Send(request);
                        if (id == "-1")
                        {
                            Print("CriaParDeSaida: Ordem Limit de saída BUY não colocada... tentando ordem STOP");
                            NewOrderRequest req = NewOrderRequest.CreateBuyStop(instrument, pos.Amount, instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize), conta);
                            req.MagicNumber = numeroMagico;
                            req.MarketRange = 1000;
                            var id2 = Orders.Send(req);
                            if (id2 == "-1") { Print("CriaParDeSaida: Ordem BUY STOP não aceita!"); LimparTodasAsOrdens(); return false; } else { return true; }
                        }
                        else { return true; }
                    }
                }
                catch (Exception ext)
                {
                    Print("CriaParDeSaida: Erro ao Setar Take Profit! " + ext.Message);
                    return false;
                }
            }
            else
            {
                Print("CriaParDeSaída: Não existe posição");
                LimparTodasAsOrdens();
            }

            return false;
        }

        private bool ModificaOuCriaParDeSaida(Position pos)
        {
            Order[] orders = Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ToArray();
            if (orders.Length == 1)
            {
                Order ord = orders[0];
                double price = 0;
                price = (ord.Side == Operation.Buy)? instrument.RoundPrice(pos.OpenPrice + TP * instrument.TickSize) : instrument.RoundPrice(pos.OpenPrice - TP * instrument.TickSize);
                return ord.Modify(price, pos.Amount);                
            }
            else
            {
                if (orders.Length > 1) { Alert("ModificaOuCriaParDeSaída: ERRO!! Ordem não modificada pois existe mais de 1 ordem de saída");  return false; }
                return CriaParDeSaida(pos);                
            }            
        }


        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = Positions.GetPositions();
            Order[] ordensGlobais = Orders.GetOrders();
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => PosInfo(pos)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => OrderInfo(ord)); }
        }


        private void AccountInformation(Account acc)
        {
            //outputting of all the 'Account' properties
            Print(
                "Id : \t" + acc.Id + "\n" +
                "Name : \t" + acc.Name + "\n" +
                "User Login: \t" + acc.User.Login + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "Leverage : \t" + acc.Leverage + "\n" +
                "Currency : \t" + acc.Currency + "\n" +
                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
                "IsDemo : \t" + acc.IsDemo + "\n" +
                "IsReal : \t" + acc.IsReal + "\n" +
                "IsLocked : \t" + acc.IsLocked + "\n" +
                "IsInvestor : \t" + acc.IsInvestor + "\n" +
                "Status : \t" + acc.Status + "\n" +
                "StopReason : \t" + acc.StopReason + "\n" +
                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "BeginBalance : \t" + acc.BeginBalance + "\n" +
                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
                "Credit : \t" + acc.Credit + "\n" +
                "CashBalance : \t" + acc.CashBalance + "\n" +
                "TodayVolume : \t" + acc.TodayVolume + "\n" +
                "TodayNet : \t" + acc.TodayNet + "\n" +
                "TodayTrades : \t" + acc.TodayTrades + "\n" +
                "TodayFees : \t" + acc.TodayFees + "\n" +
                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
                "MarginTotal : \t" + acc.MarginTotal + "\n" +
                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
                "Equity : \t" + acc.Equity + "\n" +
                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
            );
        }

        private void PosInfo(Position pos)
        {
            double proximaReentrada = 0;
            if (contadorDeReEntradas > ValoresDeReEntradas.Length - 1) contadorDeReEntradas = ValoresDeReEntradas.Length - 1;
            if (contadorDeReEntradas >= 0)
            {
                proximaReentrada = (pos.Side == Operation.Buy) ? ultimaReentrada - instrument.TickSize * ReEntradas[contadorDeReEntradas] : ultimaReentrada + instrument.TickSize * ReEntradas[contadorDeReEntradas];
            }
            Print(
                "PosInfo()-> " +
                "  Papel: " + pos.Instrument.BaseName + "\t" +
                "  Id: " + pos.Id + "\t" +
                "  Qtde: " + pos.Amount + "\t" +
                "  Tipo: " + pos.Side.ToString() + "\t" +
                "  Preço Abertura: R$ " + pos.OpenPrice + "\t" +
                "  Preço Atual: R$ " + pos.CurrentPrice + "\t" +
                "  Número Mágico: " + pos.MagicNumber + "\t" +
                "  Última Reentrada: R$" + ultimaReentrada + "\t" +
                "  Próxima Reentrada: R$" + proximaReentrada + "\t" +
                "  Lucro ou Prejuízo: R$" + pos.GetProfitNet()
            );
        }

        private void OrderInfo(Order ord)
        {
            Print(
                "OrderInfo()-> " +
                "  Papel: " + ord.Instrument.BaseName + "\t" +
                "  Id: " + ord.Id + "\t" +
                "  Qtde: " + ord.Amount + "\t" +
                "  Tipo: " + ord.Side.ToString() + "\t" +
                "  Preço: R$ " + ord.Price + "\t" +
                "  Número Mágico: " + ord.MagicNumber
            );

        }

        private void InfoConexao()
        {
            //outputting of all the information about the current connection.
            Print(
            "\nConnection name : \t" + myConnection.Name +
            "\nConnection address : \t" + myConnection.Address +
            "\nConnection login : \t" + myConnection.Login +
            "\nSLL on?  : \t" + myConnection.IsUseSSL +
            "\nProtocol version : \t" + myConnection.ProtocolVersion +
            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
            "\nConnection status : \t" + myConnection.Status
            );
        }

        public void FechamentoParcial()
        {
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close(Math.Ceiling(p.Amount / 2)));
            estado = state.limbo;
            passeLivreNoLimbo = true;
        }

        public void FechamentoTotal()
        {
            Print("Fechamento Total: Cancelando todas as Ordens pendentes e Posições abertas");
            Positions.GetPositions()?.ToList().FindAll(pos => pos.Instrument.BaseName == papel).ForEach(p => p.Close());
            Orders.GetOrders(true)?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ForEach(ord => ord.Cancel());
            estado = state.limbo;
            passeLivreNoLimbo = true;
        }

        private void Juiz()
        {
            Print("********************   Estado Juiz ***********************");
            orders = Orders.GetOrders()?.ToList().FindAll(ord => ord.Instrument.BaseName == papel).ToArray();
            positions = Positions.GetPositions()?.ToList().FindAll(posit => posit.Instrument.BaseName == papel).ToArray();
            if (positions == null) positions = new Position[0];
            if (orders == null) orders = new Order[0];

            //Caso 1: Não existe posição e Não existe ordem
            if (positions.Length == 0 && orders.Length == 0)
            {
                //como chegou um caso assim para o juiz analisar? É impossível pois a migração de estados na entrada e na reentrada apenas ocorrem se a corretora aceitar as ordens
                //mesmo assim, é melhor prever isso
                Print("Estado Juiz: Não existe Posição e Não existe Ordem para ser processada. estado -> inicio");
                estado = state.inicio;
                return;
            }

            //Caso 2: Não existe posição, e ordem tem operação pendente
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ord.Status == OrderStatus.PendingNew || ord.Status == OrderStatus.PendingReplace || ord.Status == OrderStatus.PendingCancel || ord.Status == OrderStatus.PendingExecution).Count != 0)
            {
                Print("Estado Juiz: Existe Ordem com operação pendente ainda... aguarde ela definir o que vai fazer");
                return;
            }

            //Case 3: Não existe posição e Existe ordem limite aguardando execução
            if (positions.Length == 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew) && ord.Type == OrdersType.Limit)).Count != 0)
            {
                Print("Estado Juiz: Não existe posição e uma ordem Limite ainda não foi executada. Cancela tudo e migra estado para inicio");
                LimparTodasAsOrdens();
                estado = state.inicio;
                return;
            }

            //Case 4: Existe posição mas não existe ordem de saída                            
            if (positions.Length != 0 && orders.Length == 0)
            {
                Print("Estado Juiz: Existe posição mas não existe ordem de saída. CriaParDeSaída()");
                if (CriaParDeSaida()) { estado = state.limbo; } else { Print("Estado Juiz: Erro na Criação do Par de Saída!"); Juiz(); }
                return;
            }

            //Case 5: Existe Posição e Existe Ordem de Saída Limite
            if (positions.Length != 0 && orders?.ToList().FindAll(ord => ((ord.Status == OrderStatus.New || ord.Status == OrderStatus.ConfirmedNew) && ord.Type == OrdersType.Limit)).Count != 0)
            {
                //Verifique se a Ordem tem mesma quantidade que Posição
                if (positions[0].Amount == orders[0].Amount)
                {

                    Print("Estado Juiz: Existe posição e Existe ordem de saída. Migrando para estado ReEntrada");
                    estado = state.reentrada;
                }
                else
                {
                    Print("Estado Juiz: Ordem de Saída com Qtde diferente da Posição. Modificando qtde da ordem.");
                    if (orders[0].Modify(orders[0].Price, positions[0].Amount))
                    {
                        Print("Estado Juiz: Envio de solicitação de modificação de Ordem foi aceito! Migrando para o Limbo...");
                        estado = state.limbo;
                    }
                }


                return;
            }
        }



    }

}
